import os, sys, subprocess
import xml.etree.ElementTree as ET
from datetime import datetime, timedelta

def main():
	# set default start date
	start_dt=datetime.now()-timedelta(weeks=4)

	# set default end date
	end_dt=datetime.now()
	
	# configuration is required
	config=sys.argv[1]
	
	# start and end dates are optional
	if len(sys.argv) > 2:
		start_dt=datetime.strptime(sys.argv[2],'%Y-%m-%d')
	if len(sys.argv) > 3:
		end_dt=datetime.strptime(sys.argv[3],'%Y-%m-%d')
	
	# parse the config file
	tree = ET.parse(config)
	
	# start an instance of the validator
	validator = DataOnboardingAuditor(start_dt, end_dt)
		
	# loop through all sources and run through the validation steps	
	for source_params in tree.findall('source'):
		validator.run(source_params)
	
	validator.uploadDetails()
		
		
class DataOnboardingAuditor():
	def __init__(self, start_dt, end_dt):
		self.start_dt = start_dt
		self.end_dt = end_dt
		# initialize variables that can be used across sources
		# they store data retrieved by hdfs queries that might be needed across sources		
		self.hdfs_files_list = {}  # by location
		self.ingestion_table_count = {}  # by table/date
		self.xref_missing_reason = {}  # by date
		self.start_time = datetime.now()
		self.results = ""
	
	def initializeSource(self, params):
		
		self.ingestion_table = params.find('ingestion_table').text.lower()	
		self.primary_trigger_location = params.find('ingestion_trigger_location').text		
		self.primary_landing_location = params.find('ingestion_landing_location').text
				
		# final table is optional
		self.final_table = None
		if params.find('final_table') != None:
			self.final_table = params.find('final_table').text.lower()		
				
				
		# database/schema containing the ingestion tables
		self.ingestion_database="default"
		if params.find('schema') != None:   #deprecated
			self.ingestion_database = params.find('schema').text.lower()
		if params.find('ingestion_database') != None:
			self.ingestion_database = params.find('ingestion_database').text.lower()
			
		# database/schema containing the audit tables
		self.audit_database="default"
		if params.find('audit_database') != None:
			self.audit_database = params.find('audit_database').text.lower()
					
		# database/schema containing the audit tables
		self.final_database="default"
		if params.find('final_database') != None:
			self.final_database = params.find('final_database').text.lower()		
				
		# xref is optional
		self.xref_table = None
		self.xref_trigger_location = None
		self.xref_landing_location = None		
		if params.find('xref_table') != None:
			# if xref table is specified, all parameters must be provided
			self.xref_table = params.find('xref_table').text.lower()
			self.xref_trigger_location = params.find('xref_trigger_location').text
			self.xref_landing_location = params.find('xref_landing_location').text
		
		# other optional parameters
		self.check_pel_count = False
		if params.find('check_pel_count') != None and params.find('check_pel_count').text.lower() == 'true':
			self.check_pel_count = True
			
		self.minimum_pel_match_pct = None	
		if params.find('minimum_pel_match_pct') != None:
			self.minimum_pel_match_pct = float(params.find('minimum_pel_match_pct').text)
			
		self.partition_conversion_hql = None
		# legacy field, remove at some point
		if params.find('event_dt_conversion_syntax') != None:
			self.partition_conversion_hql = params.find('event_dt_conversion_syntax').text
		if params.find('partition_conversion_hql') != None:
			self.partition_conversion_hql = params.find('partition_conversion_hql').text
			
		self.expect_daily_updates = False
		if params.find('expect_daily_updates') != None and params.find('expect_daily_updates').text.lower() == 'true':
			self.expect_daily_updates = True
		
		self.maximum_ingestion_diff_count = 0
		if params.find('maximum_ingestion_diff_count') != None:
			self.maximum_ingestion_diff_count = int(params.find('maximum_ingestion_diff_count').text)
			
		self.add_jar_from_local = None
		if params.find('add_jar_path_for_ingestion_table') != None:  #deprecated
			self.add_jar_path_for_ingestion_table = params.find('add_jar_path_for_ingestion_table').text
		if params.find('add_jar_from_local') != None:
			self.add_jar_from_local = params.find('add_jar_from_local').text
			
		self.add_jar_from_hdfs = None
		if params.find('add_jar_from_hdfs') != None:
			self.add_jar_from_hdfs = params.find('add_jar_from_hdfs').text			
		
		self.final_table_partition_field_name="event_dt"
		if params.find('final_table_partition_field_name') != None:
			self.final_table_partition_field_name = params.find('final_table_partition_field_name').text
		
		self.ingestion_dt_partition_exists=True
		if params.find('ingestion_dt_partition_exists') != None and params.find('ingestion_dt_partition_exists').text.lower() == 'false':
			self.ingestion_dt_partition_exists=False
		
		# initialize	
		self.ingestion_table_audit_counts = {}
		self.xref_table_audit_counts = {}
		self.final_table_audit_counts = {}
		self.pel_audit_counts = {}
		self.last_success_dt=""
		self.gaps_found=False
		self.total_ingestion_count = 0
		self.total_final_table_count = 0
		self.total_processed = 0		
		self.audit_info={}
		self.recent_file_delta = timedelta(hours=12)
		self.results_table_hdfs_location="/user/delivery/tables/" + self.audit_database + "/delv_onboarding_audit/"
		
		
		# log paramaters
		print "+++++++++ OPTIONS ++++++++++"
		print "+ final_table=" + str(self.final_table)
		print "+ ingestion_database=" + self.ingestion_database
		print "+ final_database=" + self.final_database
		print "+ audit_database=" + self.audit_database
		print "+ ingestion_table=" + self.ingestion_table
		print "+ primary_trigger_location=" + self.primary_trigger_location
		print "+ primary_landing_location=" + self.primary_landing_location
		print "+ xref_table=" + str(self.xref_table)
		print "+ xref_trigger_location=" + str(self.xref_trigger_location)
		print "+ xref_landing_location=" + str(self.xref_landing_location)
		print "+ check_pel_count=" + str(self.check_pel_count)
		print "+ minimum_pel_match_pct=" + str(self.minimum_pel_match_pct)
		print "+ final_table_partition=" + str(self.final_table_partition_field_name)
		print "+ partition_conversion_syntax=" + str(self.partition_conversion_hql)
		print "+ expect_daily_updates=" + str(self.expect_daily_updates)
		print "+ maximum_ingestion_diff_count=" + str(self.maximum_ingestion_diff_count)
		print "+ add_jar_from_local_for_ingestion=" + str(self.add_jar_from_local)
		print "+ add_jar_from_hdfs_for_ingestion=" + str(self.add_jar_from_hdfs)
		print "+ results_table_location=" + self.results_table_hdfs_location
		print "+ ingestion_dt_partition_exists=" + str(self.ingestion_dt_partition_exists)
		print "++++++++++++++++++++++++++++"
			
	# runs audit routine on a new source
	def run(self, params):
		
		# initialize new source
		self.initializeSource(params)
		self.prepare()
		
		# get audit data
		self.getIngestionAuditData()
		self.getFinalTableAuditData()
		
		# take care of date gaps
		self.setupAuditInfoBuckets()
		
		issues_found = 0
		
		# start auditing
		for dt in sorted(self.audit_info):		
			print "<++++++++++>"
			self.trackingLog("PROCESSING NEW DATE", dt)
						
			passed_audit=False		
			
			if self.findPrimaryTableIssues(dt):
				print "Issue found in Ingestion"
				issues_found += 1
			elif self.findXrefTableIssues(dt):
				print "Issue found in Xref"
				issues_found += 1
			elif self.findFinalTableIssues(dt):
				print "Issue found in Final Table"
				issues_found += 1
			elif self.findOtherIssues(dt):
				print "Other issues found"
				issues_found += 1
			elif self.findWarningTriggers(dt):
				print "Warning triggers found"
				issues_found += 1
			else:
				print "No issues found!"
				self.audit_info[dt].statusUpdate("PASSES AUDIT", "No Issues Found")
				#self.addInsertCompleteFlag(dt)
				#self.addPartitionFlag(dt)
				self.last_success_dt=dt
			
				if issues_found > 0 and not self.gaps_found:
					self.gaps_found = True
					
			self.total_ingestion_count += self.audit_info[dt].ingestion_audit_count
			self.total_final_table_count += self.audit_info[dt].final_table_audit_count
			self.total_processed += 1
			
		# report findings
		self.reportFindings()
	
	def prepare(self):
		if self.add_jar_from_hdfs != None:
			os.system("hadoop fs -get " + self.add_jar_from_hdfs)
			self.add_jar_from_local = self.add_jar_from_hdfs.split('/')[-1]
	
	# adds a _FINAL_INSERT_UPDATED flag to the data location
	def addInsertCompleteFlag(self, ingestion_dt):
		flag_path = self.primary_landing_location + "/" + ingestion_dt.replace("-","/") + "/_FINAL_TABLE_UPDATED"
		print "ADDING FINAL_TABLE_UPDATE flag,path=" + flag_path
		os.system("hadoop fs -touchz " + flag_path)	
		
	# adds a _PARTITION_ADDED flag to the data location
	def addPartitionFlag(self, ingestion_dt):
		flag_path = self.primary_landing_location + "/" + ingestion_dt.replace("-","/") + "/_PARTITION_ADDED"
		print "ADDING PARTITION_ADDED flag,path=" + flag_path
		os.system("hadoop fs -touchz " + flag_path)	
	
	
	def reportFindings(self):
		# create a file and push it to the cluster in a partitioned folder structure
		
		for dt in sorted(self.audit_info):
			print str(self.audit_info[dt])
			self.results += self.audit_info[dt].tableFormat(self.start_time) +  "\n"
				
		summary = "SUMMARY,final_table=" + str(self.final_table) + ",total_days_processed=" + str(self.total_processed) 
		summary +=",ingestion_count=" + str(self.total_ingestion_count) + ",final_table_count=" + str(self.total_final_table_count) 
		summary	+=",last_success_dt=" + str(self.last_success_dt) + ",gaps_found=" + str(self.gaps_found)
		print summary
		
	
	def uploadDetails(self):
		file_name = "details_" + self.start_time.strftime("%Y%m%d%H%M%S") + ".dat" 
		f=open(file_name,'w')
		f.write(self.results)
		f.close()
		
		print "POSTING RESULTS FILE,location=" + self.results_table_hdfs_location + "/" + file_name
		os.system('hadoop fs -put ' + file_name + ' ' + self.results_table_hdfs_location + '/' + file_name) 
		
		
	def setupAuditInfoBuckets(self):
		dts = self.start_dt
		while dts <= self.end_dt:
			dt = dts.strftime('%Y-%m-%d')
			
			if self.expect_daily_updates or dt in self.ingestion_table_audit_counts:
				self.audit_info[dt] = AuditInfo(self.ingestion_table, self.final_table, dt)
				
				if dt in self.ingestion_table_audit_counts:
					self.audit_info[dt].ingestion_audit_count = self.ingestion_table_audit_counts[dt]
					
				if self.xref_table != None and dt in self.xref_table_audit_counts:
					self.audit_info[dt].xref_audit_count = self.xref_table_audit_counts[dt]
					
				if dt in self.final_table_audit_counts:
					self.audit_info[dt].final_table_audit_count = self.final_table_audit_counts[dt]

				if dt in self.pel_audit_counts:
					self.audit_info[dt].pel_audit_count = self.pel_audit_counts[dt]
			
			dts += timedelta(days=1)
	
	
	# checks for ingestion table issues
	def findPrimaryTableIssues(self, dt):
		step = "PrimaryTable"
		self.trackingLog("VALIDATING PRIMARY INGESTION TABLE", dt)
		if self.audit_info[dt].ingestion_audit_count == 0:
			print "INGESTION AUDIT COUNT IS ZERO,table=" + self.ingestion_table + ",dt=" + dt
			if self.findIngestionTableIssues(step, self.ingestion_table, dt, self.primary_landing_location + dt.replace("-","/"), self.primary_trigger_location):
				return True
		self.trackingLog("PRIMARY INGESTION AUDIT OK", dt)
		return False
		
					
	# checks for xref table issues
	def findXrefTableIssues(self, dt):
		if self.xref_table == None:
			# if no xref file is provided, there are no xref file issues
			return False
						
		dt_ymd=dt.replace("-","")
		step = "XrefTable"
		
		self.trackingLog("VALIDATING XREF INGESTION TABLE", dt)
		if self.audit_info[dt].xref_audit_count == 0:		
			self.trackingLog("XREF TABLE AUDIT COUNT IS ZERO", dt)
			if self.findIngestionTableIssues(step, self.xref_table, dt, self.xref_landing_location + dt.replace("-","/"), self.xref_trigger_location):
				self.trackingLog("XREF NOT READY", dt)
				if self.audit_info[dt].final_table_audit_count > 0 and self.audit_info[dt].pel_audit_count == 0:
					# final table insert occurred with no xref file causing no pel matches
					self.trackingLog("FINAL TABLE PEL COUNT EXISTS WITH NO XREF DATA - VERIFYING FINAL TABLE COUNT", dt)
					table_count = int(self.getTableCountForIngestionDate(self.final_table, dt, True, self.ingestion_table))
					if table_count == self.audit_info[dt].final_table_audit_count:
						# final table count verified
						self.trackingLog("FINAL TABLE COUNT VERIFIED - UPDATE LIKELY RAN WITHOUT XREF DATA", dt)
						self.audit_info[dt].addAction("INTERVENTION_REQUIRED", "Final Table Update ran without Xref Data", "Clean up Final Table and Reset Trigger")
						return True
					elif table_count == 0:
						#final table count is zero but audit shows a count; need to manual reset audit count
						self.trackingLog("FINAL TABLE COUNT IS ZERO BUT AUDIT COUNT EXISTS - RESETTING COUNT IN AUDIT TABLE", dt)
						self.resetFinalAuditCount(dt)
						self.audit_info[dt].addAction("FINAL AUDIT UPDATE", "Final Table Count is Zero but Audit Shows Counts", "Reset Final Audit Counts for Date")
						return True
					else:
						# final table count is different than audit
						self.trackingLog("FINAL TABLE COUNT DIFFERENT FROM AUDIT - RERUN FINAL TABLE COUNT", dt)
						self.audit_info[dt].addAction("AUTOMATION TRIGGER ADDED", "Final Table Count Exists but Audit is Zero", "RERUN_FINAL_AUDIT trigger created")
						self.touchHdfsFile(self.primary_trigger_location, "RERUN_FINAL_AUDIT_" + dt_ymd)
						return True	
				return True	
		
		self.trackingLog("XREF INGESTION AUDIT OK", dt)
		return False
		
		
	# identify issues with ingestion table
	def findIngestionTableIssues(self, step, table, dt, landing_location, trigger_location):	
		dt_ymd=dt.replace("-","")
		log_append = ",step=" + step + ",table=" + table + ",dt=" + dt
		# there should be no ingestion count in audit table when it gets here
		self.trackingLog("LOOKING FOR 'FILES_ADDED' TRIGGER", dt)
		if self.fileExistsInHdfs(trigger_location, "FILES_ADDED_" + dt_ymd):	
			# FILE_ADDED trigger exists
			self.trackingLog("FILES_ADDED TRIGGER FOUND - CHECKING RECENCY OF FILE", dt)
			if self.hdfsFileIsRecent(trigger_location, "FILES_ADDED_" + dt_ymd):
				self.trackingLog("RECENT 'FILES_ADDED' TRIGGER FOUND", dt)
				# trigger file is recent, just waiting to be picked up by partition manager
				self.audit_info[dt].statusUpdate("PENDING", step + ":Waiting on Partition Manager to Start")
				return True
			else:		
				self.trackingLog("OLD 'FILES_ADDED' TRIGGER FOUND - PARTITION MANAGER MAY BE HAVING PROBLEMS", dt)
				# trigger file is old, user needs to check that partition manager is running with no errors
				self.audit_info[dt].addAction("INTERVENTION REQUIRED", step + ":Waiting on Partition Manager for Longer than the Allowable Time", "Check Partition Manager Logs for Error")
				return True
		else:					
			# no ingestion count in audit table, no FILES_ADDED trigger
			self.trackingLog("NO 'FILES_ADDED' TRIGGER - RUNNING TABLE COUNT", dt)
			table_count = self.getTableCountForIngestionDate(table, dt)
			self.trackingLog("TABLE COUNT OBTAINED,count=" + str(table_count), dt)
			if table_count > 0:					
				# counts exist in the ingestion table, but not in the audit table	
				self.trackingLog("TABLE HAS ROWS FOR DATE", dt)
				self.audit_info[dt].addAction("AUTOMATION TRIGGER ADDED", step + ":Partition Exists on Table without Audit Count", "'RERUN_INGESTION_AUDIT' Trigger Created")
				self.touchHdfsFile(trigger_location, "RERUN_INGESTION_AUDIT_" + dt_ymd)
				if not self.hdfsFileIsRecent(landing_location, "_PARTITION_ADDED"):
					# partition added flag is missing, but partition exists, need to add partition flag		
					self.trackingLog("'_PARTITION_ADDED' FLAG MISSING IN LANDING LOCATION - ADDING", dt)
					self.touchHdfsFile(landing_location, "_PARTITION_ADDED")
					self.audit_info[dt].addAction("AUTOMATION TRIGGER ADDED", step + ":Partition Flag in Landing Area is Missing", "'PARTITION_ADDED' Trigger Created in Landing Area")	
				return True
			elif table_count < 0:
				self.trackingLog("TABLE COUNT FOR DATE RETURNED AN ERROR - COULD BE BAD DATA", dt)
				# ingestion query for partition return an error
				# TODO: if getfiles source, just remove partition added flag and add the GETFILES trigger to re-pull
				self.audit_info[dt].addAction("INTERVENTION REQUIRED", step + ":Table Count Query for Date Returned an Error", "Re-pull Source from External Site")
				return True
			else:
				# there are no counts in the ingestion table
				self.trackingLog("NO ROWS IN TABLE - CHECKING FOR NON-EMPTY FILES", dt)
				if self.nonEmptyFilesExistInHdfs(landing_location):
					# non-empty files exist in the landing area		
					self.trackingLog("FILES EXIST IN LANDING AREA - CHECKING RECENCY", dt)
					if self.nonEmptyFilesInHdfsRecent(landing_location):
						# files have been recently added, probably still loading
						self.trackingLog("FILES IN LANDING AREA ARE RECENT", dt)
						self.audit_info[dt].statusUpdate("IN PROGRESS", step + ":Files Recently Added to Landing Area")
						return True
					else:
						# files are not recent, something is not right
						self.trackingLog("FILES IN LANDING AREA HAVE BEEN THERE AWHILE", dt)
						self.audit_info[dt].addAction("INTERVENTION REQUIRED", step + ":Files Exist in Landing Area with No 'FILES_ADDED' Trigger", "Check Ingestion Process Logs for Error")
						return True
				else:
					# no files exist in landing area
					self.trackingLog("NO FILES IN LANDING AREA", dt)
					if self.expect_daily_updates:
						self.audit_info[dt].statusUpdate("IN PROGRESS", step + ":No files in Landing Location")
					else:
						self.audit_info[dt].statusUpdate("PASSES AUDIT", step + ":No files in Landing Location")
					return True
		
		return False
	
	# checks for final table issues	
	def findFinalTableIssues(self, dt):
		if self.final_table == None:
			return False
			
		dt_ymd=dt.replace("-","")
		step = "final"
		self.trackingLog("RUNNING FINAL INSERT AUDITS", dt)
		# we can only get this far if ingestion and xref tables pass with no issues
		if self.audit_info[dt].final_table_audit_count == 0:
			# audit counts are missing for final table
			self.trackingLog("NO AUDIT COUNTS FOR FINAL TABLE FOUND - CHECKING FOR PARTITION ADDED TRIGGER", dt)
			if self.fileExistsInHdfs(self.primary_trigger_location, "PARTITION_ADDED_" + dt_ymd):
				# partition added trigger for ingestion table and xref tables exists
				self.trackingLog("PARTITION ADDED TRIGGER EXISTS - CHECKING FOR RECENCY", dt)
				if self.xref_table != None and self.hdfsFileIsRecent(self.xref_landing_location, "_PARTITION_ADDED" + dt_ymd):
					# xref table has recently added files
					self.trackingLog("XREF PARTITION RECENTLY ADDED", dt)
					self.audit_info[dt].statusUpdate("IN PROGRESS", "Waiting for Final Table Manager to Process Xref Partition")
					return True
				elif self.hdfsFileIsRecent(self.primary_trigger_location, "PARTITION_ADDED_" + dt_ymd):
					# trigger file is recent, just waiting to be picked up by partition manager
					self.trackingLog("PRIMARY PARTITION RECENTLY ADDED", dt)
					self.audit_info[dt].statusUpdate("IN PROGRESS", "Waiting for Final Table Manager to Process Primary Partition")
					return True
				else:
					# trigger file(s) exist but are not recent
					self.trackingLog("PARTITIONS HAVE BEEN OUT THERE AWHILE", dt)
					self.audit_info[dt].addAction("INTERVENTION REQUIRED", "Waiting on Final Table Manager Longer than Allowable Time", "Check Status of Final Table Manager")
					return True
			else:
				# partition added trigger does not exist for ingestion table
				self.trackingLog("NO 'PARTITION_ADDED' TRIGGER EXISTS - RUNNING TABLE COUNT", dt)
				table_count = self.getTableCountForIngestionDate(self.final_table, dt, True, self.ingestion_table)
				self.trackingLog("TABLE COUNT OBTAINED,count=" + str(table_count), dt)
				if table_count > 0:
					# found a count on the final table
					self.trackingLog("TABLE HAS ROWS - PLACING 'RERUN_FINAL_AUDIT' TRIGGER FILE TO FIX AUDIT COUNT", dt)
					self.audit_info[dt].addAction("AUTOMATION TRIGGER ADDED", "Final Table has Rows but Audit Count is Missing", "'RERUN_FINAL_AUDIT' trigger created")
					self.touchHdfsFile(self.primary_trigger_location, "RERUN_FINAL_AUDIT_" + dt_ymd)
					return True
				else:
					# no count in final table, no partition added trigger, but we have an ingestion partition
					self.trackingLog("NO ROWS IN TABLE - CHECKING FOR FAILED ATTEMPTS", dt)
					fail_file = self.getFailFileInHdfs(self.primary_trigger_location, dt)
					if fail_file:
						# failed attempt found in trigger location
						self.trackingLog("FAILED ATTEMPT FOUND", dt)
						self.audit_info[dt].addAction("INTERVENTION REQUIRED", "Previous Final Insert Attempt Failed", "Resolve Issue and Restart")
						return True
					else:
						self.trackingLog("NO FAILED ATTEMPTS - CHECKING TO SEE IF IT IS RUNNING", dt)
						running_file = self.getRunningFileInHdfs(self.primary_trigger_location, dt)
						if running_file:
							# a running trigger file exists
							self.trackingLog("RUNNING FILE EXISTS - CHECK RECENCY", dt)
							if self.hdfsFileIsRecent(self.primary_trigger_location, running_file):
								# running trigger is recent, assume it's still running
								self.trackingLog("RUNNING FILE IS RECENT - PROCESS IS STILL RUNNING", dt)
								self.audit_info[dt].setStatus("IN PROGRESS", "Final Table Manager Running")
								return True
							else:
								# running file is old
								self.trackingLog("RUNNING FILE OLD - CLEANING IT UP", dt)								
								self.audit_info[dt].addAction("RUNNING TRIGGER REMOVED", "Old RUNNING Trigger Exists", "Removing Running Trigger File")
								self.removeHdfsFile(self.primary_trigger_location, running_file)
								return True					
						else:
							# everything is ready, but there is not final table count in the audit table
							if self.fileExistsInHdfs(self.primary_landing_location + "/" + dt.replace("-","/"), "_FINAL_TABLE_UPDATED"):
								# final table appears to have been updated, verify by running a count on the final table
								self.trackingLog("FINAL_TABLE_UPDATED flag found - CHECKING TO SEE IF ROWS EXIST IN FINAL TABLE", dt)
								table_count = int(self.getTableCountForIngestionDate(self.final_table, dt, True, self.ingestion_table))
								if table_count != 0:
									# rows have been inserted into the final table for this ingestion_dt; need to rerun the audit
									self.trackingLog("ROWS EXIST IN FINAL TABLE - ADDING 'RERUN_FINAL_AUDIT' TRIGGER", dt)
									self.audit_info[dt].addAction("AUTOMATION TRIGGER ADDED", "Final Table Audit Counts Missing, but Rows Found In Final Table for Ingestion Date", "RERUN_FINAL_AUDIT Trigger Created")
									self.touchHdfsFile(self.primary_trigger_location, "RERUN_FINAL_AUDIT_" + dt_ymd)
								else:
									# FINAL_TABLE_UPDATED flag exists, but there are no row for ingestion_dt in the final table
									if self.audit_info[dt].ingestion_audit_count <= 1:
										# ingestion table has only 1 row (probably a header row that doesn't get pulled in to the final table
										self.trackingLog("INGESTION TABLE ONLY CONTAINS A SINGLE ROW - ASSUMING IT'S A HEADER ROW THAT DOESN'T GET MOVED INTO THE FINAL TABLE", dt)
										return False
									else:
										# final table needs to be update; remove FINAL_TABLE_ADDED flag and reset triggers
										# TODO: not comfortable automating this part, yet.  Not sure how to accommodate replace files since they won't have final table counts from previous days
										# self.removeHdfsFile(self.primary_landing_location + "/" + ingestion_dt.replace("-","/") + "/_FINAL_TABLE_UPDATED")
										# self.audit_info[dt].addAction("COMPLETE FLAG REMOVED", "FINAL_TABLE_UPDATE flag exists but no rows added to final table", "_FINAL_TABLE_UPDATED flag removed")
										# self.audit_info[dt].addAction("AUTOMATION TRIGGER ADDED", "Final Table is Ready but 'PARTITION_ADDED' Trigger is Missing", "PARTITION_ADDED Trigger Created")
										# self.touchHdfsFile(self.primary_trigger_location, "PARTITION_ADDED_" + dt_ymd)
										self.audit_info[dt].addAction("INTERVENTION REQUIRED", "FINAL_TABLE_UPDATE flag exists but no rows added to final table", "Remove _FINAL_TABLE_UPDATED flag and add PARTITION_ADDED_ trigger")
							else:
								# final table has not been updated
								self.trackingLog("NO RUNNING FILE - ADDING 'PARTITION_ADDED' TRIGGER", dt)
								self.audit_info[dt].addAction("AUTOMATION TRIGGER ADDED", "Final Table is Ready but 'PARTITION_ADDED' Trigger is Missing", "PARTITION_ADDED Trigger Created")
								self.touchHdfsFile(self.primary_trigger_location, "PARTITION_ADDED_" + dt_ymd)
								return True	
		
		self.trackingLog("FINAL INSERT AUDITS OK", dt)
		return False
	
	# check for other issues
	def findOtherIssues(self, dt):	
		if self.final_table == None:
			return False	
			
		dt_ymd=dt.replace("-","")
		step = "other"
		self.trackingLog("RUNNING MISC. AUDITS", dt)
		# we can only get here if everything looks good with all tables involved
		if self.maximum_ingestion_diff_count >= 0:
			self.trackingLog("CHECKING DIFFERENCE BETWEEN INGESTION AND FINAL TABLE COUNTS", dt)
			if abs(self.audit_info[dt].final_table_audit_count - self.audit_info[dt].ingestion_audit_count) > self.maximum_ingestion_diff_count:
				# final table count off by more than allowable threshold
				self.trackingLog("TABLE COUNTS ARE DIFFERENT - VERIFYING FINAL TABLE COUNT,ingestion=" + str(self.audit_info[dt].ingestion_audit_count) + ",final=" + str(self.audit_info[dt].final_table_audit_count), dt)
				table_count = int(self.getTableCountForIngestionDate(self.final_table, dt, True, self.ingestion_table))
				self.trackingLog("FINAL TABLE COUNT OBTAINED,final_table_count=" + str(table_count), dt)
				if table_count == self.audit_info[dt].final_table_audit_count:
					# final table count verified
					self.trackingLog("FINAL TABLE COUNT VERIFIED - THERE IS A COUNTS ISSUE", dt)
					self.audit_info[dt].addAction("INTERVENTION REQUIRED", "Final Table is Off by More than Specified Limits", "Check Table for Issues")
					return True
				else:
					self.trackingLog("FINAL TABLE COUNTS DON'T MATCH AUDIT", dt)
					if table_count == 0:
						self.trackingLog("FINAL TABLE COUNT IS ZERO - RESETTING COUNT IN AUDIT TABLE", dt)
						self.resetFinalAuditCount(dt)
						self.audit_info[dt].addAction("FINAL AUDIT UPDATE", "Final Table Count is Zero but Audit Shows Counts", "Reset Final Audit Counts for Date")
					else:
						self.trackingLog("REQUESTING RERUN OF FINAL AUDIT COUNT", dt)
						self.audit_info[dt].addAction("AUTOMATION TRIGGER ADDED", "Final Table Count does not Match Final Audit Count", "RERUN_FINAL_AUDIT Trigger Created")
						self.touchHdfsFile(self.primary_trigger_location, "RERUN_FINAL_AUDIT_" + dt_ymd)
						return True
		
		# TODO: create a max_pct_increase_in_final_table that will check to make sure more we didn't add alot more entries than expected to the final table
		#       this will be beneficial for accounts that have more than 1 pel per cookie id (or consolidated xref files)
				
	
		# pel count validation
		if self.check_pel_count:
			if self.audit_info[dt].final_table_audit_count > 0:  # this should already be true by getting here, but just in case
				# final table counts exist
				if self.audit_info[dt].pel_audit_count == 0:
					self.trackingLog("PEL COUNT IS ZERO", dt)
					# pel count is zero, something went wrong with the final table build (or xref file is bad)
					self.audit_info[dt].addAction("INTERVENTION REQUIRED", "No Pel Matches Found on Final Table", "Investigate Xref for Issues")
					return True
				elif self.minimum_pel_match_pct != None and float(self.audit_info[dt].pel_audit_count) / self.audit_info[dt].final_table_audit_count < self.minimum_pel_match_pct:
					# pel match rate is lower than threshold
					self.trackingLog("PEL MATCH COUNT BELOW ALLOWABLE THRESHOLD,pel_count=" + str(self.audit_info[dt].pel_audit_count) + ",final_table_count=" + str(self.audit_info[dt].final_table_audit_count), dt)
					self.audit_info[dt].addAction("INTERVENTION REQUIRED", "Pel Match Rate Below Specified Threshold", "Investigate Final Table Insert and Xref Data")
					return True	
		
		self.trackingLog("MISC. AUDITS OK", dt)
		return False
	
	def findWarningTriggers(self, dt):
		warnings_found = False
		self.trackingLog("LOOKING FOR WARNING TRIGGERS", dt)
		triggers = self.getWarningTriggers(self.primary_trigger_location, dt)
		if triggers == None or len(triggers) == 0:
			return False
		
		for trigger in triggers:
			self.trackingLog("WARNING TRIGGER FOUND,trigger=" + trigger, dt)
			self.audit_info[dt].addAction("INTERVENTION_REQUIRED", "Warning trigger found for source (" + trigger + ")", "Investigate/cleanup trigger file")
			warnings_found = True
			
		return warnings_found
		
	# retrieves the audit counts from the ingestion audit table	
	def getIngestionAuditData(self):	
		# build hive query language
		hql  = "select "
		hql += "  lower(table_name) as table_name, "
		hql += "  insert_dts, "
		hql += "  ingestion_dt, "
		hql += "  row_count "
		hql += "from "
		hql += "  " + self.audit_database + ".delv_ingestion_table_audit "
		hql += "where "
		hql += "  ingestion_dt between '" + self.start_dt.strftime('%Y-%m-%d') + "' and '" + self.end_dt.strftime('%Y-%m-%d') + "' "
		hql += "and "
		if self.xref_table != None:
			hql += "  lower(table_name) in ('" + self.xref_table.lower() + "','" + self.ingestion_table.lower() + "') "
		else:
			hql += "  lower(table_name) = '" + self.ingestion_table.lower() + "' "
		hql += "order by "
		hql += "  ingestion_dt, insert_dts; "
		
		# run query
		results = self.getHiveQueryResults(hql)
		
		# check for error in query
		if results['rc'] != 0:
			print "ERROR,ingestion table audit query failed,rc=" + str(results['rc'])
			return
			
		# pull out counts needed for each date
		for row in results['records']:
			table, insert_dts, ingestion_dt, row_count = row
			if self.xref_table != None and table == self.xref_table:
				self.xref_table_audit_counts[ingestion_dt] = int(row_count)
			elif table == self.ingestion_table:
				self.ingestion_table_audit_counts[ingestion_dt] = int(row_count)
				
	# retrieves the audit counts from the final audit table			
	def getFinalTableAuditData(self):
		# build hive query language
		hql  = "select "
		hql += "  lower(table_name) as table_name, "
		hql += "  lower(ingestion_table) as ingestion_table, "
		hql += "  insert_dts, "
		hql += "  ingestion_dt, "
		hql += "  row_count, "
		hql += "  str_to_map(other_counts,'&','=')['pel_count'] as pel_count "
		hql += "from "
		hql += "  " + self.audit_database + ".delv_final_table_audit "
		hql += "where "
		hql += "  ingestion_dt between '" + self.start_dt.strftime('%Y-%m-%d') + "' and '" + self.end_dt.strftime('%Y-%m-%d') + "' "
		hql += "and "
		hql += "  lower(ingestion_table) = '" + self.ingestion_table.lower() + "' "
		hql += "order by "
		hql += "  ingestion_dt, insert_dts; "
		
		# run query
		results = self.getHiveQueryResults(hql)
		
		if results['rc'] != 0:
			print "ERROR,final table audit query failed,rc=" + str(results['rc'])
			return
			
		# pull out counts needed for each date
		for row in results['records']:
			final_table, ingestion_table, insert_dts, ingestion_dt, row_count, pel_count = row
			if ingestion_table == self.ingestion_table and final_table == self.final_table:
				self.final_table_audit_counts[ingestion_dt] = int(row_count)
				if pel_count.upper() == 'NULL':
					pel_count = 0
				self.pel_audit_counts[ingestion_dt] = int(pel_count)
	
	
	# runs hive query specified and returns rows (as array of arrays) as well as status
	def getHiveQueryResults(self, hql):
		return_data = {}
		return_data['rc'] = ""
		return_data['records'] = []
		
		#run the query
		print "RUNNING HIVE QUERY,hql=" + hql
		process = subprocess.Popen(["hive", "-e", '"' + hql + '"'], stdout=subprocess.PIPE)
		data = process.communicate()[0]
		return_data['rc'] = process.returncode
		
		# return if the query ended with an error
		if process.returncode != 0:
			return return_data
		
		# retrieve the data
		if data:
			for row in data.split('\n'):
				if row.strip():
					return_data['records'].append(row.split('\t'))
			
		return return_data

	# check to see if files exist in external location
	def filesExistOnExternalLocation(dt):
		# TODO: implement
		return True
	
	# checks hdfs location for non-empty files
	def nonEmptyFilesExistInHdfs(self, location):
		list = self.getHdfsFilesList(location)
		for file in list:
			if list[file]['size'] > 0:
				return True
		return False

	# checks recency of non-empty hdfs files
	def nonEmptyFilesInHdfsRecent(self, location):
		list = self.getHdfsFilesList(location)
		for file in list:
			if list[file]['date'] + self.recent_file_delta > self.start_time:
				return True
		return False
			
	# checks hdfs location for existing file
	def fileExistsInHdfs(self, location, file):
		return file in self.getHdfsFilesList(location)
			
	# check if hdfs file was recently created
	def hdfsFileIsRecent(self, location, file):
		list = self.getHdfsFilesList(location)
		if file in list:
			if list[file]['date'] + self.recent_file_delta > self.start_time:
				return True
		return False
			
	# checks for a failure file in Hdfs trigger location
	def getFailFileInHdfs(self, trigger_location, dt):
		for file in self.getHdfsFilesList(trigger_location + dt.replace("-","/")):
			if dt.replace("-","") in file:
				if 'fail' in file.lower():
					return file
		return None		
		
	def getWarningTriggers(self, trigger_location, dt):
		files = []
		for file in self.getHdfsFilesList(trigger_location):
			if dt.replace("-","") in file:
				if 'getfiles_' in file.lower():
					continue
				#if 'files_added_' in file.lower():
				#	continue
				if 'partition_added_' in file.lower():
					continue
				if 'rerun_' in file.lower():
					continue
					
				files.append(file)
				
		return files		
			
	# checks for a failure file in Hdfs trigger location
	def getRunningFileInHdfs(self, trigger_location, dt):
		for file in self.getHdfsFilesList(trigger_location + dt.replace("-","/")):
			if dt.replace("-","") in file:
				if 'running' in file.lower():
					return file
		return None
	
	# gets and saves a listing of all files and sizes in an hdfs location		
	def getHdfsFilesList(self, location):
		# return the location it's already known
		if location in self.hdfs_files_list:
			return self.hdfs_files_list[location]
			
		# initialize new location
		self.hdfs_files_list[location] = {}
		
		# get the list of files from hdfs
		process = subprocess.Popen(["hadoop","fs","-ls", location], stdout=subprocess.PIPE)
		data = process.communicate()[0]
		
		# stop here if there was an error
		if process.returncode != 0:
			print "HDFS LISTING ERROR,location=" + location + ",rc=" + str(process.returncode)
			return self.hdfs_files_list[location]
		
		# pull out required information
		for line in data.split('\n'):
			fields = line.split()
			if len(fields) < 7:
				continue
			# save information
			file_name = fields[-1].split('/')[-1]
			file_size = int(fields[4])
			file_date = datetime.strptime(fields[5] + " " + fields[6], "%Y-%m-%d %H:%M")
			
			self.hdfs_files_list[location][file_name] = {}
			self.hdfs_files_list[location][file_name]['size'] = file_size
			self.hdfs_files_list[location][file_name]['date'] = file_date
			
		return self.hdfs_files_list[location] 
		
	def touchHdfsFile(self, location, file):
		#print "would have touched file " + location + "/" + file
		#return True
		os.system("hadoop fs -touchz " + location + "/" + file)
		return self.fileExistsInHdfs(location, file)
		
	def removeHdfsFile(self, location, file):
		#print "would have touched file " + location + "/" + file
		#return True
		os.system("hadoop fs -rm " + location + "/" + file)
		return self.fileExistsInHdfs(location, file)
	
	# gets and saves row count for an ingestion table by date	
	def getTableCountForIngestionDate(self, table, dt, use_partition=None, ingestion_table=None):
		# return count if known
		if table in self.ingestion_table_count:
			if dt in self.ingestion_table_count[table]:
				return self.ingestion_table_count[table][dt]
		else:
			# initialize new table
			self.ingestion_table_count[table] = {}
						
		database = self.final_database
		
		if "ingestion_" in table:
			database = self.ingestion_database

			# initialize date for the table
		self.ingestion_table_count[table][dt] = 0
		
		# build hive query
		hql = ""
		
		if self.add_jar_from_local != None:
		  hql += "add jar " + self.add_jar_from_local + "; "
		
		if self.ingestion_dt_partition_exists == True:		
			hql += "select ingestion_dt,count(*) from " + database + "." + table + " where ingestion_dt = '" + dt + "'" 
			if use_partition and self.partition_conversion_hql != None:
				# get the event_dt list if requested (only if conversion syntax is provided)
				partition_value_list = self.getSQLPartitionValueList(ingestion_table, dt)
				# return an error if the event_dt query erros
				if not partition_value_list:
					return -1
				# add event_dt filter
				hql += " and " + self.final_table_partition_field_name + " in (" + partition_value_list + ") "
			hql += " group by ingestion_dt;"
		else:
			hql += "select '" + dt + "' as ingestion_dt, count(*) from " + database + "." + table
		
		# run the query
		results = self.getHiveQueryResults(hql)
		
		# check for error
		if results['rc'] != 0:
			print "INGESTION TABLE QUERY ERROR,table=" + database + "." + table + ",dt=" + dt + ",hql=" + hql
			self.ingestion_table_count[table][dt] = -1
			return -1
				
		# find record with the current date and return the count
		for record in results['records']:
			ingestion_dt, count = record
			if ingestion_dt == dt:
				self.ingestion_table_count[table][dt] = count
				return count
		
		return 0
			
	# returns an event date list that can be used to optimize a final table query		
	def getSQLPartitionValueList(self, table, ingestion_dt):
		
		# build the hive query
		hql = ""
		
		if self.add_jar_from_local != None:
		  hql += "add jar " + self.add_jar_from_local + "; "
		
		hql += "select distinct " + self.partition_conversion_hql + " from " + self.ingestion_database + "." + table + " where ingestion_dt = '" + ingestion_dt + "';"
		
		# run the query
		results = self.getHiveQueryResults(hql)
		
		# check for error
		if results['rc'] != 0:
			print "ERROR,reason=failed to run event date query,table=" + table + ",dt=" + ingestion_dt + ",hql=" + hql
			return
				
		# build the sql list	
		sql_list = ""
		for record in results['records']:
			event_dt = record[0]
			if record[0].lower() != "null":
				sql_list += "'" + record[0] + "',"
		
		# return all but the last comma
		return sql_list[0:-1]

	def resetFinalAuditCount(self, dt):
		# need a way to reset the the final table count when it should be zero.  Current process won't insert
		# a row when this happens, so we need to force it here.
		hql  = "insert into table " + self.audit_database + ".delv_final_table_audit "
		hql += "select "
		hql += "  FROM_UNIXTIME(UNIX_TIMESTAMP()) as insert_dts, "
		hql += "  'fromOnboardAuditor' as insert_oozie_wfid, "
		hql += "  '" + self.final_table + "' as table_name, "
		hql += "  '" + self.ingestion_table + "', "
		hql += "  '" + dt + "', "
		hql += "  '0', "
		hql += "  concat('pel_count=','0') as other_counts "
		hql += "from "
		hql += "  " + self.audit_database + ".delv_final_table_audit "
		hql += "limit 1;"
		
		print "RUNNING HIVE QUERY,hql=" + hql
		
		os.system('hive -e "' + hql)
		
	def trackingLog(self, entry, dt):
		print "<+> " + str(datetime.now()) + "," + entry + ",table=" + self.ingestion_table + ",date=" + dt
		
class AuditInfo():
	def __init__(self, table, final, dt):
		self.ingestion_table = table
		self.final_table = final
		self.ingestion_dt = dt
		self.ingestion_audit_count = 0
		self.final_table_audit_count = 0
		self.xref_audit_count = 0
		self.pel_audit_count = 0
		
		self.messages = []
		self.actions = []
		self.status = ""
		
	def addAction(self, status, message, action):
		self.status = status
		self.messages.append(message)
		self.actions.append(action)
		
	def statusUpdate(self, status, message):
		self.status = status
		self.messages.append(message)
		
	def tableFormat(self, dts):
		#layout: table
		row = dts.strftime("%Y-%m-%d %H:%M:%S") + "\t"
		row += str(self.ingestion_table) + "\t"		
		row += str(self.final_table) + "\t"
		row += self.ingestion_dt + "\t"
		row += self.status + "\t"
		row += ";".join(self.messages) + "\t"
		row += ";".join(self.actions) + "\t"
		row += str(self.ingestion_audit_count) + "\t"		
		row += str(self.xref_audit_count) + "\t"
		row += str(self.final_table_audit_count) + "\t"
		row += "pel=" + str(self.pel_audit_count)
		return row
			
	def __str__(self):
		logline = "table=" + str(self.ingestion_table) + ",dt=" + self.ingestion_dt + ",status=" + self.status
		logline += ",messages=" + str(self.messages)			
		logline += ",actions=" + str(self.actions)
		logline += ",ingestion_audit_count=" + str(self.ingestion_audit_count)	
		logline += ",xref_audit_count=" + str(self.xref_audit_count)
		logline += ",final_table_audit_count=" + str(self.final_table_audit_count)
		logline += ",pel_audit_count=" + str(self.pel_audit_count)
		return logline

	
if __name__ == "__main__":
   main()
