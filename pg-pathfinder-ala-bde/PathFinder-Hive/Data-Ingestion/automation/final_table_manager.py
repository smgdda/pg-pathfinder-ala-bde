import sys, os, subprocess
import xml.etree.ElementTree as ET
from datetime import datetime, timedelta

def main():
	config=sys.argv[1]
	wfid=sys.argv[2]
	runid=sys.argv[3]
	
	tree = ET.parse(config)
	
	processlog_location="/user/delivery/tables/default/delv_process_log"
	if tree.find('processlog_location') != None:
		processlog_location=tree.find('processlog_location').text
	
	for source_params in tree.findall('source'):
		controller = FinalTableController(wfid, runid, source_params)
		controller.processlog_location=processlog_location
		controller.run()
		
		
class FinalTableController:
	
	def __init__(self, wfid, runid, params):
		self.wfid = wfid
		self.runid = runid
		self.final_table = params.find('final_table').text.lower()
		self.insert_script_path = params.find('insert_script_path').text
		self.audit_script_path = params.find('audit_script_path').text
		self.trigger_location = params.find('ingestion_trigger_location').text
		self.landing_location = params.find('ingestion_landing_location').text
		self.ingestion_table = params.find('ingestion_table').text
				
		self.logfile_name = None
		self.logfile_handle = None
		
		self.start_time = datetime.now()
		
		# database/schema containing the ingestion tables
		self.ingestion_database="default"
		if params.find('schema') != None:   #deprecated
			self.ingestion_database = params.find('schema').text.lower()
		if params.find('ingestion_database') != None:
			self.ingestion_database = params.find('ingestion_database').text.lower()
			
		# database/schema containing the audit tables
		self.audit_database="default"
		if params.find('audit_database') != None:
			self.audit_database = params.find('audit_database').text.lower()
					
		# database/schema containing the audit tables
		self.final_database="default"
		if params.find('final_database') != None:
			self.final_database = params.find('final_database').text.lower()
		
		# location of xref files
		self.xref_root_location = None
		if params.find('xref_landing_location') != None:
			self.xref_root_location = params.find('xref_landing_location').text
		
		self.partition_conversion_syntax = None
		if params.find('partition_conversion_syntax') != None:
			self.partition_conversion_syntax = params.find('partition_conversion_syntax').text
		# legacy
		if params.find('event_dt_conversion_syntax') != None:
			self.partition_conversion_syntax = params.find('event_dt_conversion_syntax').text
		
		self.allow_multiple_insert_attempts=False
		if params.find('allow_multiple_insert_attempts') != None:
			if params.find('allow_multiple_insert_attempts').text.lower() == "true":
				self.allow_multiple_insert_attempts = True
						
		self.add_jar_from_hdfs = None
		if params.find('add_jar_from_hdfs') != None:
			self.add_jar_from_hdfs = params.find('add_jar_from_hdfs').text		
			
		self.ignore_duplicate_triggers = False
		if params.find('ignore_duplicate_triggers') != None and params.find('ignore_duplicate_triggers').text.lower() in ['true','t','y']:
			self.ignore_duplicate_triggers = True

		self.processlog_location = "/user/delivery/tables/default/delv_process_log/"
					
		self.hive_command = "hive "
		self.beeline_user = None
		if params.find('beeline_user') != None:
			self.beeline_user = params.find('beeline_user').text
			self.hive_command = "beeline -u jdbc:hive2// --outputformat=tsv -n " + self.beeline_user + " "
			#beeline -u jdbc:hive2://localhost:10000/default -n delv_chase
			#beeline -u jdbc:hive2://localhost:10000/default;auth=NoSasl???
		
		self.current_trigger = ""		
		self.lock_path = "/user/delivery/locks/"
		self.lock_filename = self.final_table.upper() + "-" + self.wfid + "." + self.runid	
		
		self.insert_script = self.insert_script_path.split('/')[-1]
		self.audit_script = self.audit_script_path.split('/')[-1]
		
		# log paramaters
		print ("+++++++++ OPTIONS ++++++++++")
		print ("+ wfid=" + self.wfid)
		print ("+ runid=" + self.runid)
		print ("+ final_table=" + self.final_table)
		print ("+ ingestion_table=" + self.ingestion_table)
		print ("+ ingestion_database=" + self.ingestion_database)
		print ("+ final_database=" + self.final_database)
		print ("+ audit_database=" + self.audit_database)
		print ("+ trigger_location=" + self.trigger_location)
		print ("+ landing_location=" + self.landing_location)
		print ("+ insert_script_path=" + self.insert_script_path)
		print ("+ insert_script_name=" + self.insert_script)
		print ("+ audit_script_path=" + self.audit_script_path)
		print ("+ audit_script_name=" + self.audit_script)
		print ("+ xref_landing_location=" + str(self.xref_root_location))
		print ("+ partition_conversion_syntax=" + str(self.partition_conversion_syntax))
		print ("+ allow_multiple_insert_attempts=" + str(self.allow_multiple_insert_attempts))
		print ("+ add_jar_from_hdfs=" + str(self.add_jar_from_hdfs))
		print ("+ ignore_duplicate_triggers=" + str(self.ignore_duplicate_triggers))
		print ("+ process_log_location=" + self.processlog_location)
		print ("++++++++++++++++++++++++++++")
			
		
	# Main loop	
	def run(self):
	
		self.openLogFile()
		
		# check that all required files are available
		if not self.requiredFilesExist():
			print ("FAILED,reason=necessary files are missing. Listing:")
			os.system("ls -lrt")
			return -1	
			
		self.get_jar_from_hdfs()
		
		# if lock exists, fail otherwise create lock file
		if not self.lockTable():
			print ("FAILED,reason=table locked")
			return -1
		
		insert_steps=0
		
		# process ready files (process latest first for historical loads)
		try:
			for ingestion_dt in sorted(self.findReadyDates("PARTITION_ADDED"), reverse=True):
				print ("PROCESSING,ingestion_dt=" + ingestion_dt)
				self.current_trigger="PARTITION_ADDED"
				
				if insert_steps > 0 and self.ignore_duplicate_triggers == True:
					"Skipping " + ingestion_dt + "...ignore_duplicate_triggers flag is set"
					self.removeTrigger(ingestion_dt)
					self.addInsertCompleteFlag(ingestion_dt)
					continue
				
				# check that we haven't already updated the final table with this date
				if self.checkForInsertCompleteFlag(ingestion_dt):
					continue
				
				# attempt to run the insert step
				if self.runInsertStep(ingestion_dt) != 0:
					insert_steps += 1
					continue
				
				self.addInsertCompleteFlag(ingestion_dt)
					
				# attempt to run the audit step
				if self.runAuditStep(ingestion_dt) != 0:
					continue
					
				# cleanup and note successful insert
				self.removeTrigger(ingestion_dt)				
		finally:	
			# Unlock on any type of failure
			self.unlockTable()
		
		# process any final audit rerun requests
		for ingestion_dt in self.findReadyDates("RERUN_FINAL_AUDIT"):
			self.current_trigger="RERUN_FINAL_AUDIT"
			if self.runAuditStep(ingestion_dt) == 0:
				self.removeTrigger(ingestion_dt)
						
		self.uploadLogFile()
			
	def get_jar_from_hdfs(self):
		if self.add_jar_from_hdfs != None:
			print ("DOWNLOADING JAR FILE,file=" + self.add_jar_from_hdfs)
			os.system("hadoop fs -get " + self.add_jar_from_hdfs)
			self.add_jar_from_local = self.add_jar_from_hdfs.split('/')[-1]
			
	# check that all required files are available before processing
	def requiredFilesExist(self):
		self.pullScriptsFromHDFS()
			
		if not os.path.isfile(self.insert_script):
			print ("ERROR,reason=insert script not found,script=" + self.insert_script)
			return False
		
		if not os.path.isfile(self.audit_script):
			print ("ERROR,reason=audit script not found,script=" + self.audit_script)
			
			return False
			
		return True
	
	def pullScriptsFromHDFS(self):
		if self.insert_script_path.startswith('/'):
			if os.path.exists(self.insert_script):
				os.remove(self.insert_script)
			os.system("hadoop fs -get " + self.insert_script_path + " .")
			
		if self.audit_script_path.startswith('/'):
			if os.path.exists(self.audit_script):
				os.remove(self.audit_script)
			os.system("hadoop fs -get " + self.audit_script_path + " .")
			
		return
	
	
	# creates lock file, fails if table is already locked
	def lockTable(self):
		lines = subprocess.Popen(["hadoop","fs","-ls", self.lock_path], stdout=subprocess.PIPE).communicate()[0]
		for line in lines.split('\n'):
			filename = line.split('/')[-1]
			if filename.lower().startswith(self.final_table.lower()):
				print ("TABLE LOCKED,table=" + self.final_table + ",location=" + self.lock_path + ",name=" + filename)
				return False
				
		os.system("hadoop fs -touchz " + self.lock_path + self.lock_filename)
		print ("LOCKING TABLE,table=" + self.final_table + ",location=" + self.lock_path + ",filename=" + self.lock_filename)
		return True

		
	# remove lock file	
	def unlockTable(self):
		os.system("hadoop fs -rm " + self.lock_path + self.lock_filename)
		return

		
	# get a list of dates ready to be processed	
	def findReadyDates(self, trigger_type):
		readyList = []
		triggers = subprocess.Popen(["hadoop","fs","-ls", self.trigger_location], stdout=subprocess.PIPE).communicate()[0]
		
		for trigger in triggers.split('\n'):
			filename = trigger.split('/')[-1]
			if trigger_type in filename:
				ingestion_dt_ymd = filename.strip().replace(trigger_type + "_", "")
				ingestion_dt = ingestion_dt_ymd[0:4] + "-" + ingestion_dt_ymd[4:6] + "-" + ingestion_dt_ymd[6:8]
				if self.xref_root_location != None:
					if self.checkForXrefPartition(ingestion_dt):
						readyList.append(ingestion_dt)
				else:
					readyList.append(ingestion_dt)
		
		return readyList

	
	# check that the xref partition for the date has been added before running insert
	def checkForXrefPartition(self, ingestion_dt):
		xref_landing_location = self.xref_root_location + "/" + ingestion_dt.replace('-','/') + "/"
		lines = subprocess.Popen(["hadoop","fs","-ls",xref_landing_location], stdout=subprocess.PIPE).communicate()[0]
		
		for line in lines.split('\n'):
			file = line.split('/')[-1]
			if file == "_PARTITION_ADDED":
				print ("Xref partition found for " + ingestion_dt)
				return True
		
		print ("No xref partition found for " + ingestion_dt)
		return False
		

	# update trigger file
	def updateTrigger(self, new, ingestion_dt):
		old_trigger = self.trigger_location + "/" + self.current_trigger + "_" + ingestion_dt.replace("-","")
		new_trigger = self.trigger_location + "/" + new + "_" + ingestion_dt.replace("-","")		
		self.current_trigger = new
		
		print ("UPDATING TRIGGER,old=" + old_trigger + ",new=" + new_trigger)
		os.system("hadoop fs -rm " + old_trigger)
		os.system("hadoop fs -touchz " + new_trigger)
				

	# removes trigger file
	def removeTrigger(self, ingestion_dt):
		old_trigger = self.trigger_location + "/" + self.current_trigger + "_" + ingestion_dt.replace("-","")
		os.system("hadoop fs -rm " + old_trigger)

		
	# adds a _FINAL_INSERT_COMPLETE flag to the data location
	def addInsertCompleteFlag(self, ingestion_dt):
		flag_path = self.landing_location + "/" + ingestion_dt.replace("-","/") + "/_FINAL_TABLE_UPDATED"
		os.system("hadoop fs -touchz " + flag_path)
		
		
	# searches for existing FINAL_TABLE_UPDATED flag	
	def checkForInsertCompleteFlag(self, ingestion_dt):
		if self.allow_multiple_insert_attempts == True:
			print ("Multiple inserts allowed, not checking for complete flag")
			return False
			
		flag_path = self.landing_location + "/" + ingestion_dt.replace("-","/") + "/" #ingestion_dt[0:4] + "/" + ingestion_dt[4:6] + "/" + ingestion_dt[6:8] + "/"
		lines = subprocess.Popen(["hadoop","fs","-ls", flag_path], stdout=subprocess.PIPE).communicate()[0]
		
		for line in lines.split('\n'):
			file = line.split('/')[-1]
			if file == "_FINAL_TABLE_UPDATED":
				print ("FINAL TABLE UPDATE flag found for " + ingestion_dt)
				self.updateTrigger("PARTITION_MODIFIED", ingestion_dt)
				return True
		
		print ("No FINAL TABLE UPDATE flag found for " + ingestion_dt)
		return False
	
	
	# run insert hql script	
	def runInsertStep(self, ingestion_dt):
		print ("INSERTING ROWS,script=" + self.insert_script + ",ingestion_dt=" + ingestion_dt + ",time=" + str(datetime.now()))
		self.updateTrigger("RUNNING_INSERT", ingestion_dt)
		insert_hivevar  = " -hivevar FINAL_TABLE=" + self.final_table
		insert_hivevar += " -hivevar INGESTION_DATABASE=" + self.ingestion_database
		insert_hivevar += " -hivevar FINAL_DATABASE=" + self.final_database
		insert_hivevar += " -hivevar INGESTION_DT=" + ingestion_dt
		insert_hivevar += " -hivevar WFID=" + self.wfid
		insert_hivevar += " -hivevar RUNID=" + self.runid
		insert_hivevar += " -hivevar INGESTION_TABLE=" + self.ingestion_table.lower()
		
		# Run insert script
		print ("Command: hive -f " + self.insert_script + " " + insert_hivevar + ",time=" + str(datetime.now()))
		insert_rc = os.system('hive -f ' + self.insert_script + ' ' + insert_hivevar)
		
		# On failure, update trigger and log
		if insert_rc != 0:
			print ("Final Table Insert Failed!")
			self.insertProcessLog("ERROR", ingestion_dt, ",STATUS=FINAL_INSERT_FAILED,RETURN_CODE=" + str(insert_rc))
			self.updateTrigger("INSERT_FAILED",ingestion_dt)
			return insert_rc
			
		return 0
		

	# insert counts into audit table
	def runAuditStep(self, ingestion_dt):
		
		if self.audit_script == None:
			return
			
		audit_hivevar  = " -hivevar INGESTION_DT=" + ingestion_dt
		audit_hivevar += " -hivevar WFID=" + self.wfid
		audit_hivevar += " -hivevar RUNID=" + self.runid
		audit_hivevar += " -hivevar FINAL_TABLE=" + self.final_table
		audit_hivevar += " -hivevar INGESTION_DATABASE=" + self.ingestion_database
		audit_hivevar += " -hivevar FINAL_DATABASE=" + self.final_database
		audit_hivevar += " -hivevar AUDIT_DATABASE=" + self.audit_database
		
		if self.ingestion_table:
			audit_hivevar += " -hivevar INGESTION_TABLE=" + self.ingestion_table
			
		if self.partition_conversion_syntax != None:
			partition_value_list = self.getIngestionPartitionList(self.ingestion_table, ingestion_dt)
			if partition_value_list == None:
				print ("ERROR,reason=no rows found on ingestion for partition specified,time=" + str(datetime.now()))
				self.insertProcessLog("ERROR", ingestion_dt, ",STATUS=AUDIT_FAILED,RETURN_CODE=-1")
				self.updateTrigger("AUDIT_FAILED", ingestion_dt)
				return -1
			
			audit_hivevar += " -hivevar PARTITION_VALUES=" + partition_value_list
			# legacy
			audit_hivevar += " -hivevar EVENT_DT_LIST=" + partition_value_list

		print ("INSERTING AUDIT COUNTS,ingestion_dt=" + ingestion_dt + ",command=hive -f " + self.audit_script + audit_hivevar + ",time=" + str(datetime.now()))
		self.updateTrigger("RUNNING_FINAL_AUDIT", ingestion_dt)
		
		# Insert counts into audit table
		audit_rc = os.system('hive -f ' + self.audit_script + audit_hivevar)
		
		# On failure, update trigger and log
		if audit_rc != 0:
			print ("ERROR,reason=audit script failed,time=" + str(datetime.now()))
			self.insertProcessLog("ERROR", ingestion_dt, ",STATUS=AUDIT_FAILED,RETURN_CODE=" + str(audit_rc))
			self.updateTrigger("AUDIT_FAILED", ingestion_dt)
			return audit_rc

		self.insertProcessLog("SUCCESS", ingestion_dt, "")
		return 0

		
	# returns an event date list that can be used to optimize a final table query		
	def getIngestionPartitionList(self, table, ingestion_dt):
		
		# build the hive query
		hql = "select distinct " + self.partition_conversion_syntax + " from " + self.ingestion_database + "." + table + " where ingestion_dt = '" + ingestion_dt + "';"
		
		# run the query
		results = self.getHiveQueryResults(hql)
		
		# check for error
		if results['rc'] != 0:
			print ("ERROR,reason=failed to run event date query,table=" + table + ",dt=" + ingestion_dt + ",hql=" + hql)
			return
				
		# build the sql list	
		sql_list = ""
		for record in results['records']:
			event_dt = record[0]
			if record[0].lower() != "null":
				sql_list += "\\'" + record[0] + "\\',"
		
		# return all but the last comma
		return sql_list[0:-1]

		
	# runs hive query specified and returns rows (as array of arrays) as well as status
	def getHiveQueryResults(self, hql):
		return_data = {}
		return_data['rc'] = ""
		return_data['records'] = []
		
		#run the query
		print ("RUNNING HIVE QUERY,hql=" + hql + ",time=" + str(datetime.now()))
		process = subprocess.Popen(["hive", "-e", '"' + hql + '"'], stdout=subprocess.PIPE)
		data = process.communicate()[0]
		return_data['rc'] = process.returncode
		
		# return if the query ended with an error
		if process.returncode != 0:
			return return_data
		
		# retrieve the data
		if data:
			for row in data.split('\n'):
				if row.strip():
					return_data['records'].append(row.split('\t'))
			
		return return_data

		
	# Insert row into DELV_PROCESS_LOG table	
	def insertProcessLog(self, status, ingestion_dt, additional_details):
		#hquery  = "SET hive.exec.dynamic.partition=true; "
		#hquery += "SET hive.exec.dynamic.partition.mode=nonstrict; "
		
		#hquery += "INSERT INTO TABLE " + self.audit_database + ".delv_process_log "
		#hquery += "PARTITION (log_dt) "
		#hquery += "select "
		#hquery += "  from_unixtime(unix_timestamp()) as log_dts, "
		#hquery += "  'FINAL_TABLE_INSERT' as process, "
		#hquery += "  '" + self.wfid + "/" + self.runid + "' as id, "
		#hquery += "  '" + status + "' as status, "
		#hquery += "  'FINAL_TABLENAME=" + self.final_table + ",INGESTION_TABLENAME=" + self.ingestion_table + ",INGESTION_DT=" + ingestion_dt + additional_details + "' as details, "
		#hquery += "  to_date(from_unixtime(unix_timestamp())) as log_dt "
		#hquery += "from "
		#hquery += "  " + self.audit_database + ".delv_process_log "
		#hquery += "limit 1;"

		#os.system('hive -e "' + hquery + '"')
		
		self.logfile_handle.write('\t'.join([self.start_time.strftime("%Y-%m-%d %H:%M:%S"),
			'FINAL_TABLE_INSERT', 
			self.wfid + "/" + self.runid, 
			status, 
			"FINAL_TABLENAME=" + self.final_table + ",INGESTION_TABLENAME=" + self.ingestion_table + ",INGESTION_DT=" + ingestion_dt + additional_details]) + "\n")
		
	def openLogFile(self):
		self.logfile_name = "logs_" + self.start_time.strftime("%Y%m%d%H%M%S") + ".dat" 
		self.logfile_handle=open(self.logfile_name,'w')
		
	def uploadLogFile(self):
		self.logfile_handle.close()		
		print ("POSTING RESULTS FILE,file=" + self.logfile_name + ",location=" + self.processlog_location + '/log_dt=' + self.start_time.strftime("%Y-%m-%d") + "/" + self.logfile_name)
		os.system('hadoop fs -put ' + self.logfile_name + ' ' + self.processlog_location + '/log_dt=' + self.start_time.strftime("%Y-%m-%d") + "/" + self.logfile_name) 
		
	
if __name__ == "__main__":
   main()
