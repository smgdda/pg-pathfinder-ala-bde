import sys, os, subprocess
import xml.etree.ElementTree as ET
from datetime import datetime, timedelta

def main():
	config=sys.argv[1]
	wfid=sys.argv[2]
	runid=sys.argv[3]
	
	tree = ET.parse(config)
	hdfs_path=tree.find('hdfs_path').text
	processlog_location="/user/delivery/tables/default/delv_process_log"
	if tree.find('processlog_location') != None:
		processlog_location=tree.find('processlog_location').text
	
	for source_params in tree.findall('source'):
		controller = PartitionController(wfid, runid, hdfs_path, source_params)
		controller.processlog_location=processlog_location
		controller.run()


class PartitionController:
	def __init__(self, wfid, runid, hdfs_path, params):
		self.wfid = wfid
		self.runid = runid
		self.hdfs_path=hdfs_path
		
		self.ingestion_table = params.find('ingestion_table').text
		self.trigger_location = params.find('trigger_location').text		
		self.landing_location = params.find('landing_location').text
		
		self.logfile_name = None
		self.logfile_handle = None
		
		self.start_time = datetime.now()
		
		# default parameters
		self.landing_success_file_name = "_PARTITION_ADDED"
				
		self.alter_location_only = False		
		if params.find('alter_location_only') != None:
			if params.find('alter_location_only').text.strip().lower() == 'true':
				self.alter_location_only = True				
		
		self.complete_trigger_prefix = None
		if params.find('add_complete_trigger') != None:
			if params.find('add_complete_trigger').text.strip().lower() == 'true':
				self.complete_trigger_prefix = "PARTITION_ADDED_"
				
		# database/schema containing the ingestion tables
		self.ingestion_database="default"
		if params.find('schema') != None:   #deprecated
			self.ingestion_database = params.find('schema').text.lower()
		if params.find('ingestion_database') != None:
			self.ingestion_database = params.find('ingestion_database').text.lower()
			
		# database/schema containing the audit tables
		self.audit_database="default"
		if params.find('audit_database') != None:
			self.audit_database = params.find('audit_database').text.lower()
			
		self.add_jar_from_local = None
		if params.find('add_jar_path_for_ingestion_table') != None:  #deprecated
			self.add_jar_from_local = params.find('add_jar_path_for_ingestion_table').text
		if params.find('add_jar_from_local') != None:
			self.add_jar_from_local = params.find('add_jar_from_local').text
			
		self.add_jar_from_hdfs = None
		if params.find('add_jar_from_hdfs') != None:
			self.add_jar_from_hdfs = params.find('add_jar_from_hdfs').text
			
		self.processlog_location = "/user/delivery/tables/default/delv_process_log/"
			
		self.hive_command = "hive "
		self.beeline_user = None
		if params.find('beeline_user') != None:
			self.beeline_user = params.find('beeline_user').text
			self.hive_command = "beeline -u jdbc:hive2// --outputformat=tsv -n " + self.beeline_user + " "
			
		# log paramaters
		print ("+++++++++ OPTIONS ++++++++++")
		print ("+ wfid=" + self.wfid)
		print ("+ runid=" + self.runid)
		print ("+ hdfs_path=" + str(self.hdfs_path))
		print ("+ ingestion_database=" + self.ingestion_database)
		print ("+ audit_database=" + self.audit_database)
		print ("+ ingestion_table=" + self.ingestion_table)
		print ("+ trigger_location=" + self.trigger_location)
		print ("+ landing_location=" + self.landing_location)
		print ("+ landing_file_succes_name=" + self.landing_success_file_name)
		print ("+ alter_location_only=" + str(self.alter_location_only))
		print ("+ complete_trigger_prefix=" + str(self.complete_trigger_prefix))
		print ("+ add_jar_from_local=" + str(self.add_jar_from_local))
		print ("+ add_jar_from_hdfs=" + str(self.add_jar_from_hdfs))
		print ("+ process_log_location=" + self.processlog_location)
		print ("++++++++++++++++++++++++++++")
		
		
	# master run steps	
	def run(self):
		self.openLogFile()
		self.prepare()
		self.addPartitions()
		self.rerunAudits()
		self.uploadLogFile()
	
	def prepare(self):
		if self.add_jar_from_hdfs != None:
			os.system("hadoop fs -get " + self.add_jar_from_hdfs)
			self.add_jar_from_local = self.add_jar_from_hdfs.split('/')[-1]
	
	# check for any new partitions that need to be added
	def addPartitions(self):
		print ("STARTING,table=" + self.ingestion_table + ",trigger_location=" + self.trigger_location + ",landing_location=" + self.landing_location)
		for ingestion_dt in self.findReadyDates("FILES_ADDED"):
			print ("TRIGGER FOUND,ingestion_dt=" + ingestion_dt)
			
			# set current trigger to start
			self.current_trigger_prefix = "FILES_ADDED_"
			
			# update trigger as adding partition
			self.updateTrigger("ADDING_PARTITION_", ingestion_dt)
			
			# alter location only
			if self.alter_location_only:
				print ("ALTERING LOCATION,table=" + self.ingestion_table + ",ingestion_dt=" + ingestion_dt)
				alter_rc = self.alterTableLocation(ingestion_dt)
				if alter_rc != 0:
					self.reportError(ingestion_dt, "alterLocation", alter_rc)
					continue
			# add new partition
			else:							
				print ("ADDING PARTITION,table=" + self.ingestion_table + ",ingestion_dt=" + ingestion_dt)
				add_rc = self.addPartition(ingestion_dt)	
				if add_rc != 0:
					self.reportError(ingestion_dt, "addPartition", add_rc)
					continue
					
			# add landing success file
			self.addLandingSuccessFile(ingestion_dt)
					
			# insert audit counts	
			print ("AUDIT COUNTS,table=" + self.ingestion_table + ",ingestion_dt=" + ingestion_dt)
			audit_rc = self.insertAuditCounts(ingestion_dt)
			if audit_rc != 0:
				self.reportError(ingestion_dt, "insertAuditCounts", audit_rc)
				continue
			
			# report success
			self.reportSuccess(ingestion_dt)

	# check for any audit counts rerun requests
	def rerunAudits(self):
		for ingestion_dt in self.findReadyDates("RERUN_INGESTION_AUDIT"):
			print ("AUDIT RERUN TRIGGER FOUND,ingestion_dt=" + ingestion_dt)
			self.current_trigger_prefix = "RERUN_INGESTION_AUDIT_"
			audit_rc = self.insertAuditCounts(ingestion_dt)
			self.removeCurrentTrigger(ingestion_dt.replace("-",""))
				
	# get a list of dates ready to be processed	
	def findReadyDates(self, trigger_type):
		readyList = []
		triggers = subprocess.Popen(["hadoop","fs","-ls", self.trigger_location], stdout=subprocess.PIPE).communicate()[0]
		
		for trigger in triggers.split('\n'):
			filename = trigger.split('/')[-1]
			if trigger_type in filename:
				ingestion_dt_ymd = filename.strip().replace(trigger_type + "_", "")
				ingestion_dt = ingestion_dt_ymd[0:4] + "-" + ingestion_dt_ymd[4:6] + "-" + ingestion_dt_ymd[6:8]
				readyList.append(ingestion_dt)
		
		return readyList
	
	# alter location only
	def alterTableLocation(self, ingestion_dt):
		hql  = "use " + self.ingestion_database + ";"
		if self.add_jar_from_local != None:
		  hql += "add jar " + self.add_jar_from_local + "; "
		hql += "alter table " + self.ingestion_table + " set location '" + self.hdfs_path + "/" + self.landing_location + ingestion_dt.replace('-','/') + "/';"
		print ("ALTER LOCATION QUERY," + hql)
		return os.system('hive -e "' + hql + '"')
	
	# add new partition to the table
	def addPartition(self, ingestion_dt):
		remove_hql  = "use " + self.ingestion_database + ";"
		if self.add_jar_from_local != None:
		  remove_hql += "add jar " + self.add_jar_from_local + "; "
		remove_hql += "alter table " + self.ingestion_table + " drop if exists partition (ingestion_dt='" + ingestion_dt + "');"
		os.system('hive -e "' + remove_hql + '"')
		
		
		add_hql  = "use " + self.ingestion_database + ";"
		if self.add_jar_from_local != None:
		  add_hql += "add jar " + self.add_jar_from_local + "; "
		add_hql += "alter table " + self.ingestion_table + " add partition (ingestion_dt='" + ingestion_dt + "') "
		add_hql += "location '" + self.landing_location + ingestion_dt.replace('-','/') + "/';"
		
		print ("ADD PARTITION QUERY," + add_hql)
		return os.system('hive -e "' + add_hql + '"')
		
	# update trigger
	def updateTrigger(self, prefix, ingestion_dt):
		self.removeCurrentTrigger(ingestion_dt.replace("-",""))
		self.current_trigger_prefix = prefix
		self.addCurrentTrigger(ingestion_dt.replace("-",""))
		return
		
	# add trigger file
	def addCurrentTrigger(self, ingestion_dt):
		# skip if current_trigger_prefix is empty
		if self.current_trigger_prefix == None:
			return
		
		os.system("hadoop fs -touchz " + self.trigger_location + self.current_trigger_prefix + ingestion_dt)
		return
		
	# remove existing trigger
	def removeCurrentTrigger(self, ingestion_dt):
		# skip if current_trigger_prefix is empty
		if self.current_trigger_prefix == None:
			return
			
		os.system("hadoop fs -rm " + self.trigger_location + self.current_trigger_prefix + ingestion_dt)
		return
				
	# add landing trigger on success
	def addLandingSuccessFile(self, ingestion_dt):
		# only add if landing trigger specified
		if self.landing_success_file_name:
			os.system("hadoop fs -touchz " + self.landing_location + ingestion_dt.replace("-","/") + "/" + self.landing_success_file_name)
		
		return
				
	# perform all reporting steps needed upon success
	def reportSuccess(self, ingestion_dt):
		report_data = "table=" + self.ingestion_table + ",ingestion_dt=" + ingestion_dt
		print ("SUCCESS," + report_data)
		self.insertProcessLog("SUCCESS", ingestion_dt, report_data)
		self.updateTrigger(self.complete_trigger_prefix, ingestion_dt)
	
	# perform all reporting steps needed when significant error occurs
	def reportError(self, ingestion_dt, step, rc):
		report_data = "table=" + self.ingestion_table + "ingestion_dt=" + ingestion_dt + ",step=" + step +",rc=" + str(rc)
		print ("PROCESS_FAILED," + report_data)
		self.insertProcessLog("PROCESS_FAILED", ingestion_dt, report_data)
		self.updateTrigger("ADD_PARTITION_FAILED_", ingestion_dt)
		return
	
	# insert counts into audit table
	def insertAuditCounts(self, ingestion_dt):
		# build the hive query
		hql = ""
		
		if self.add_jar_from_local != None:
		  hql += "add jar " + self.add_jar_from_local + "; "
		  
		hql += "INSERT INTO TABLE " + self.audit_database + ".delv_ingestion_table_audit "
		hql += "SELECT "  
		hql += "  FROM_UNIXTIME(UNIX_TIMESTAMP()) as insert_dts, "
		hql += "  '" + self.ingestion_table.upper() + "' as table_name, "
		if self.alter_location_only:
			hql += "  '" + ingestion_dt + "' as ingestion_dt,"
		else:
			hql += "  ingestion_dt, "
		hql += "  count(*) as rowcount "
		hql += "FROM "
		hql += "  " + self.ingestion_database + "." + self.ingestion_table + " "
		if self.alter_location_only:
			hql += ";"
		else:
			hql += "WHERE"
			hql += "  ingestion_dt='" + ingestion_dt + "' "
			hql += "GROUP BY"
			hql += "  ingestion_dt; "
		
		print ("AUDIT QUERY," + hql)
		return os.system('hive -e "' + hql + '"')


	# insert row into DELV_PROCESS_LOG table	
	def insertProcessLog(self, status, ingestion_dt, additional_details):	
		self.logfile_handle.write('\t'.join([self.start_time.strftime("%Y-%m-%d %H:%M:%S"),
			'ADD_PARTITION', 
			self.wfid + "/" + self.runid, 
			status, 
			"INGESTION_TABLENAME=" + self.ingestion_table + ",INGESTION_DT=" + ingestion_dt + additional_details]) + "\n")

	def openLogFile(self):
		self.logfile_name = "logs_" + self.start_time.strftime("%Y%m%d%H%M%S") + ".dat" 
		self.logfile_handle=open(self.logfile_name,'w')

	def uploadLogFile(self):
		self.logfile_handle.close()		
		print ("POSTING RESULTS FILE,file=" + self.logfile_name + ",location=" + self.processlog_location + '/log_dt=' + self.start_time.strftime("%Y-%m-%d") + "/" + self.logfile_name)
		os.system('hadoop fs -put ' + self.logfile_name + ' ' + self.processlog_location + '/log_dt=' + self.start_time.strftime("%Y-%m-%d") + "/" + self.logfile_name) 

if __name__ == "__main__":
   main()
   
