DATE_YMD=$1

for i in 2278296 3232122 3232123 3232125 3232132 3232138 3232142 3232143 3232144 3232146 3232149 3232182 3232185 3232203 3232206 3232211 3232215 3232216 3232220 3232222 3232230;
do
	hadoop fs -touchz /user/acxm_bdp/ingestion/dfa/media/activity/triggers/GETFILES_${i}_${DATE_YMD}
done

hadoop fs -touchz /user/acxm_bdp/ingestion/dfa/media/clicks/triggers/GETFILES_${DATE_YMD}
hadoop fs -touchz /user/acxm_bdp/ingestion/dfa/media/impressions/triggers/GETFILES_${DATE_YMD}
hadoop fs -touchz /user/acxm_bdp/ingestion/dfa/metadata/advertiser/triggers/GETFILES_${DATE_YMD}
hadoop fs -touchz /user/acxm_bdp/ingestion/dfa/metadata/campaign/triggers/GETFILES_${DATE_YMD}
hadoop fs -touchz /user/acxm_bdp/ingestion/dfa/metadata/page/triggers/GETFILES_${DATE_YMD}
hadoop fs -touchz /user/acxm_bdp/ingestion/dfa/metadata/site/triggers/GETFILES_${DATE_YMD}
hadoop fs -touchz /user/acxm_bdp/ingestion/dfa/metadata/creative/triggers/GETFILES_${DATE_YMD}
