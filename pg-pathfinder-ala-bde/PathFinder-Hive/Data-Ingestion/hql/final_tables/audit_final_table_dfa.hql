insert into table delv_final_table_audit
select
  FROM_UNIXTIME(UNIX_TIMESTAMP()) as insert_dts,
  '${WFID}/${RUNID}' as insert_oozie_wfid,
  '${FINAL_TABLE}' as table_name,
  ingestion_table,
  ingestion_dt,
  row_count,
  concat('unique_id_count=',unique_id_count) as other_counts
from
(
  select
    ingestion_table,
    ingestion_dt,
    count(*) as row_count,
    count(distinct dfa_user_id) as unique_id_count
  from
    ${FINAL_TABLE} f
  where
    ingestion_dt = '${INGESTION_DT}'
  and
    lower(ingestion_table) = lower('${INGESTION_TABLE}')
  and
    event_dt in (${EVENT_DT_LIST})
  group by
    ingestion_table,
    ingestion_dt
) sub;

