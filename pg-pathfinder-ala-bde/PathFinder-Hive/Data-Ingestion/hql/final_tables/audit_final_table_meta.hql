insert into table delv_final_table_audit
select
  FROM_UNIXTIME(UNIX_TIMESTAMP()) as insert_dts,
  '${WFID}/${RUNID}' as insert_oozie_wfid,
  '${FINAL_TABLE}' as table_name,
  ingestion_table,
  ingestion_dt,
  row_count,
  '' as other_counts
from
(
  select
    '${INGESTION_TABLE}' as ingestion_table,
    ingestion_dt,
    count(*) as row_count
  from
    ${FINAL_TABLE} f
  where
    ingestion_dt = '${INGESTION_DT}'
  group by
    ingestion_dt
) sub;

