ALTER TABLE dfa_activity DISABLE NO_DROP;

DROP TABLE IF EXISTS dfa_activity;

CREATE TABLE dfa_activity
(
  dfa_user_id  string,
  event_dts  timestamp,
  advertiser_id  bigint,
  buy_id  bigint,
  ad_id  bigint,
  creative_id  bigint,
  creative_version  bigint,
  creative_size_id  string,
  site_id  bigint,
  page_id  bigint,
  keyword  string,
  country_id  int,
  state_province  string,
  area_code  string,
  browser_id  bigint,
  browser_version  string,
  os_id  bigint,
  local_user_id  string,
  activity_type  string,
  activity_sub_type  string,
  quantity  int,
  revenue  float,
  transaction_id  bigint,
  other_data  string,
  ordinal  string,
  click_time  string,
  event_id  bigint,
  sv1 string,
  ingestion_dt  string,
  ingestion_table  string,
  insert_oozie_wfid  string,
  dlx_encrypted_dfa_user_id  string, 
  acxiom_encrypted_dfa_user_id string
)
PARTITIONED BY (event_dt string)
CLUSTERED BY (dfa_user_id) INTO 2 BUCKETS
STORED AS RCFILE
LOCATION '/user/delivery/tables/default/dfa_activity';

ALTER TABLE dfa_activity ENABLE NO_DROP;
