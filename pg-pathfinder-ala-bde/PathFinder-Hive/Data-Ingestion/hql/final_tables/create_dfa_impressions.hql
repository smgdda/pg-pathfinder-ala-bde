DROP TABLE IF EXISTS dfa_impressions;

CREATE TABLE dfa_impressions
(
  dfa_user_id  string,
  event_dts  timestamp,
  advertiser_id  bigint,
  buy_id  bigint,
  ad_id  bigint,
  creative_id  bigint,
  creative_version  bigint,
  creative_size_id  string,
  site_id  bigint,
  page_id  bigint,
  keyword  string,
  country_id  int,
  state_province  string,
  area_code  string,
  browser_id  bigint,
  browser_version  string,
  os_id  bigint,
  dma_id  bigint,
  city_id  bigint,
  zip_code  string,
  site_data  string,
  time_utc_sec  bigint,
  ingestion_dt  string,
  ingestion_table  string,
  insert_oozie_wfid  string,
  dlx_encrypted_dfa_user_id  string, 
  acxiom_encrypted_dfa_user_id string
)
PARTITIONED BY (event_dt string)
CLUSTERED BY (dfa_user_id) INTO 20 BUCKETS
STORED AS RCFILE
LOCATION '/user/delivery/tables/default/dfa_impressions';

--ALTER TABLE dfa_impressions ENABLE NO_DROP;
