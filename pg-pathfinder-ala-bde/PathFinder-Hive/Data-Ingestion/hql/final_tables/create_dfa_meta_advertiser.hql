
ALTER TABLE dfa_meta_advertiser DISABLE NO_DROP;
DROP TABLE IF EXISTS dfa_meta_advertiser;

CREATE TABLE dfa_meta_advertiser
(
  spot_id BIGINT,
  advertiser_id BIGINT,
  advertiser STRING,
  advertiser_group_id STRING,
  advertiser_group STRING,
  ingestion_dt  STRING,
  ingestion_table  STRING,
  insert_oozie_wfid  STRING
)
STORED AS RCFILE
LOCATION '/user/delivery/tables/default/dfa_meta_advertiser';

ALTER TABLE dfa_meta_advertiser ENABLE NO_DROP;
