
ALTER TABLE dfa_meta_campaign DISABLE NO_DROP;
DROP TABLE IF EXISTS dfa_meta_campaign;

CREATE TABLE dfa_meta_campaign
(
  advertiser_id BIGINT,
  buy_id BIGINT,
  buy STRING,
  start_dt STRING,
  end_dt STRING,
  creative_library_enabled STRING,
  billing_invoice_code STRING,
  ingestion_dt  STRING,
  ingestion_table  STRING,
  insert_oozie_wfid  STRING
)
STORED AS RCFILE
LOCATION '/user/delivery/tables/default/dfa_meta_campaign';

ALTER TABLE dfa_meta_campaign ENABLE NO_DROP;
