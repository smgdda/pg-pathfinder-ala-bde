ALTER TABLE dfa_meta_creative DISABLE NO_DROP;

DROP TABLE IF EXISTS dfa_meta_creative;

CREATE TABLE dfa_meta_creative
(
  advertiser_id BIGINT,
  creative_id BIGINT,
  ui_creative_id BIGINT,
  creative  STRING,
  last_modified_dt  STRING,
  creative_type  STRING,
  creative_sub_type  STRING,
  creative_size_id  STRING,
  image_url  STRING,
  creative_version  STRING,
  ingestion_dt  STRING,
  ingestion_table  STRING,
  insert_oozie_wfid  STRING
)
STORED AS RCFILE
LOCATION '/user/delivery/tables/default/dfa_meta_creative';

ALTER TABLE dfa_meta_creative ENABLE NO_DROP;
