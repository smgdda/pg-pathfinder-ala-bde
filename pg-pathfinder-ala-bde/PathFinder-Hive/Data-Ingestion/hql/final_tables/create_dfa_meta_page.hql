ALTER TABLE dfa_meta_page DISABLE NO_DROP;

DROP TABLE IF EXISTS dfa_meta_page;

CREATE TABLE dfa_meta_page
(
  buy_id BIGINT,
  site_id BIGINT,
  page_id BIGINT,
  page STRING,
  site_placement STRING,
  content_category STRING,
  strategy STRING,
  start_dt STRING,
  end_dt STRING,
  group_type STRING,
  group_parent_id BIGINT,
  pricing_type STRING,
  cap_cost STRING,
  purchase_quantity BIGINT,
  purchase_cost FLOAT,
  flighting_activated STRING,
  cpa_activity_id STRING,
  ingestion_dt  STRING,
  ingestion_table  STRING,
  insert_oozie_wfid  STRING
)
STORED AS RCFILE
LOCATION '/user/delivery/tables/default/dfa_meta_page';

ALTER TABLE dfa_meta_page ENABLE NO_DROP;
