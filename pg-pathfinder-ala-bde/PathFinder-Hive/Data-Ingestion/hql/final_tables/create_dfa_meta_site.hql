ALTER TABLE dfa_meta_site DISABLE NO_DROP;

DROP TABLE IF EXISTS dfa_meta_site;

CREATE TABLE dfa_meta_site
(
  site_id BIGINT,
  site STRING,
  directory_site_id BIGINT,
  directory_site  STRING,
  ingestion_dt  STRING,
  ingestion_table  STRING,
  insert_oozie_wfid  STRING
)
STORED AS RCFILE
LOCATION '/user/delivery/tables/default/dfa_meta_site';

ALTER TABLE dfa_meta_site ENABLE NO_DROP;
