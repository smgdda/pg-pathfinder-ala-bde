ALTER TABLE dlx_weekly_sales_signal DISABLE NO_DROP;

DROP TABLE IF EXISTS dlx_weekly_sales_signal;

CREATE TABLE dlx_weekly_sales_signal
(
  dfa_user_id STRING,
  ibtr INT,
  dlx_data_delivery_dt STRING,
  model_start_dt STRING,
  model_end_dt STRING,
  last_purchase_90d STRING,
  time_of_purchase STRING,
  level STRING,
  retailer_tracked STRING,
  sales_data_releasable STRING,
  sales_signal_projected STRING,
  ingestion_dt  STRING,
  ingestion_table  STRING,
  insert_oozie_wfid  STRING
)
PARTITIONED BY (pf_campaign string)
STORED AS RCFILE
LOCATION '/user/delivery/tables/default/dlx_weekly_sales_signal';

ALTER TABLE dlx_weekly_sales_signal ENABLE NO_DROP;
