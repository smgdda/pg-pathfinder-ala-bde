﻿ALTER TABLE dlx_weekly_sales_signal_v2 DISABLE NO_DROP;

DROP TABLE IF EXISTS dlx_weekly_sales_signal_v2;

CREATE TABLE dlx_weekly_sales_signal_v2
(
  dfa_user_id STRING,
  sales_signal_flag INT,
  model_start_dt STRING,
  model_end_dt STRING,
  ingestion_dt  STRING,
  ingestion_table  STRING,
  insert_oozie_wfid  STRING
)
PARTITIONED BY (pf_campaign string)
STORED AS RCFILE
LOCATION '/user/delivery/tables/default/dlx_weekly_sales_signal_v2';

ALTER TABLE dlx_weekly_sales_signal_v2 ENABLE NO_DROP;
