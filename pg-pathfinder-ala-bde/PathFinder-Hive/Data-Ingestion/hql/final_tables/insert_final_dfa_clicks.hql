SET hive.exec.dynamic.partition=true;
SET hive.exec.dynamic.partition.mode=nonstrict;
SET hive.exec.compress.output=true;
SET mapred.output.compression.type=BLOCK;
SET mapred.output.compression.codec=org.apache.hadoop.io.compress.SnappyCodec;
SET hive.enforce.bucketing = true;

INSERT INTO TABLE dfa_clicks
PARTITION(event_dt)
SELECT
  user_id as dfa_user_id,
  FROM_UNIXTIME(UNIX_TIMESTAMP(time, 'MM-dd-yyyy-HH:mm:ss')) as event_dts,
  advertiser_id,
  buy_id,
  ad_id,
  creative_id,
  creative_version,
  creative_size_id,
  site_id,
  page_id,
  keyword,
  country_id,
  state_province,
  area_code,
  browser_id,
  browser_version,
  os_id,
  dma_id,
  city_id,
  zip_code,
  site_data,
  time_utc_sec,
  sv1,
  ingestion_dt,
  '${INGESTION_TABLE}' as ingestion_table,
  '${WFID}/${RUNID}' as insert_oozie_wfid,
  dlx_encrypted_dfa_user_id,
  acxiom_encrypted_dfa_user_id,
  to_date(FROM_UNIXTIME(UNIX_TIMESTAMP(time, 'MM-dd-yyyy-HH:mm:ss'))) as event_dt
FROM
  ingestion_dfa_clicks
WHERE
  LOWER(time) != 'time'
AND
  user_id != '0'
AND
  ingestion_dt = '${INGESTION_DT}';
 