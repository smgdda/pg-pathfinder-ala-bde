SET hive.exec.dynamic.partition=true;
SET hive.exec.dynamic.partition.mode=nonstrict;
SET hive.exec.compress.output=true;
SET mapred.output.compression.type=BLOCK;
SET mapred.output.compression.codec=org.apache.hadoop.io.compress.SnappyCodec;

ADD JAR /usr/lib/hive/lib/hive-contrib-0.10.0-cdh4.4.0.jar;

INSERT OVERWRITE TABLE dfa_meta_advertiser
SELECT 
  ingestion_dfa_meta_advertiser.spot_id,
  ingestion_dfa_meta_advertiser.advertiser_id,
  ingestion_dfa_meta_advertiser.advertiser,
  ingestion_dfa_meta_advertiser.advertiser_group_id,
  ingestion_dfa_meta_advertiser.advertiser_group,
  most_recent_historical_ingestion.first_ingestion_dt,
  '${INGESTION_TABLE}' as ingestion_table,
  '${WFID}/${RUNID}' as insert_oozie_wfid
FROM 
  ingestion_dfa_meta_advertiser
JOIN (
  SELECT 
  advertiser_id
  , min(ingestion_dt) as first_ingestion_dt 
  , max(ingestion_dt) as last_ingestion_dt 
  FROM 
  ingestion_dfa_meta_advertiser 
  GROUP BY 
  advertiser_id
  ) most_recent_historical_ingestion
ON (
  most_recent_historical_ingestion.advertiser_id = ingestion_dfa_meta_advertiser.advertiser_id 
  and most_recent_historical_ingestion.last_ingestion_dt = ingestion_dfa_meta_advertiser.ingestion_dt
  )
WHERE
  ingestion_dfa_meta_advertiser.spot_id != 'Spot-ID'
;
