SET hive.exec.dynamic.partition=true;
SET hive.exec.dynamic.partition.mode=nonstrict;
SET hive.exec.compress.output=true;
SET mapred.output.compression.type=BLOCK;
SET mapred.output.compression.codec=org.apache.hadoop.io.compress.SnappyCodec;

ADD JAR /usr/lib/hive/lib/hive-contrib-0.10.0-cdh4.4.0.jar;

INSERT OVERWRITE TABLE dfa_meta_campaign
SELECT
  ingestion_dfa_meta_campaign.advertiser_id
, ingestion_dfa_meta_campaign.buy_id
, ingestion_dfa_meta_campaign.buy
, to_date(FROM_UNIXTIME(UNIX_TIMESTAMP(ingestion_dfa_meta_campaign.start_dt, 'MM-dd-yyyy'))) as start_dt
, to_date(FROM_UNIXTIME(UNIX_TIMESTAMP(ingestion_dfa_meta_campaign.end_dt, 'MM-dd-yyyy'))) as end_dt
, ingestion_dfa_meta_campaign.creative_library_enabled
, ingestion_dfa_meta_campaign.billing_invoice_code
, most_recent_historical_ingestion.first_ingestion_dt
, '${INGESTION_TABLE}' as ingestion_table
, '${WFID}/${RUNID}' as insert_oozie_wfid

FROM
ingestion_dfa_meta_campaign
JOIN (
  SELECT 
  buy_id
  , min(ingestion_dt) as first_ingestion_dt 
  , max(ingestion_dt) as last_ingestion_dt 
  FROM 
  ingestion_dfa_meta_campaign 
  GROUP BY 
  buy_id
  ) most_recent_historical_ingestion
ON (
  most_recent_historical_ingestion.buy_id = ingestion_dfa_meta_campaign.buy_id 
  and most_recent_historical_ingestion.last_ingestion_dt = ingestion_dfa_meta_campaign.ingestion_dt
  )

WHERE
ingestion_dfa_meta_campaign.buy_id != 'Buy-ID'
;

