SET hive.exec.compress.intermediate=true;
SET mapred.compress.map.output=true;
SET hive.exec.parallel=true;
SET hive.exec.dynamic.partition=true;
SET hive.exec.dynamic.partition.mode=nonstrict;
SET hive.exec.compress.output=true;
SET mapred.output.compression.type=BLOCK;
SET mapred.output.compression.codec=org.apache.hadoop.io.compress.SnappyCodec;

ADD JAR /usr/lib/hive/lib/hive-contrib-0.10.0-cdh4.4.0.jar;

INSERT OVERWRITE TABLE dfa_meta_creative
SELECT 
  advertiser_id,
  a.creative_id,
  ui_creative_id,
  regexp_replace(lower(creative),' ','') as creative,
  to_date(FROM_UNIXTIME(UNIX_TIMESTAMP(last_modified_dt, 'MM-dd-yyyy'))) as last_modified_dt,
  creative_type,
  creative_sub_type,
  creative_size_id,
  image_url,
  creative_version,
  b.first_ingestion_dt as ingestion_dt,
  '${INGESTION_TABLE}' as ingestion_table,
  '${WFID}/${RUNID}' as insert_oozie_wfid
FROM 
  ingestion_dfa_meta_creative a
JOIN
  (select creative_id, min(ingestion_dt) as first_ingestion_dt, max(ingestion_dt) as latest_ingestion_dt from ingestion_dfa_meta_creative group by creative_id) b
ON
  (a.creative_id = b.creative_id and a.ingestion_dt = b.latest_ingestion_dt)
WHERE
  advertiser_id != 'Advertiser-ID';
