SET hive.exec.compress.intermediate=true;
SET mapred.compress.map.output=true;
SET hive.exec.parallel=true;
SET hive.exec.dynamic.partition=true;
SET hive.exec.dynamic.partition.mode=nonstrict;
SET hive.exec.compress.output=true;
SET mapred.output.compression.type=BLOCK;
SET mapred.output.compression.codec=org.apache.hadoop.io.compress.SnappyCodec;

ADD JAR /usr/lib/hive/lib/hive-contrib-0.10.0-cdh4.4.0.jar;

INSERT OVERWRITE TABLE dfa_meta_page
SELECT 
  buy_id,
  site_id,
  a.page_id,
  page,
  regexp_replace(lower(site_placement), ' ', '') as site_placement,
  content_category,
  strategy,
  to_date(FROM_UNIXTIME(UNIX_TIMESTAMP(start_dt, 'MM-dd-yyyy'))) as start_dt,
  to_date(FROM_UNIXTIME(UNIX_TIMESTAMP(end_dt, 'MM-dd-yyyy'))) as end_dt,
  group_type,
  group_parent_id,
  pricing_type,
  cap_cost,
  purchase_quantity,
  purchase_cost,
  flighting_activated,
  cpa_activity_id,
  b.first_ingestion_dt as ingestion_dt,
  '${INGESTION_TABLE}' as ingestion_table,
  '${WFID}/${RUNID}' as insert_oozie_wfid
FROM 
  ingestion_dfa_meta_page a
JOIN
  (
  select 
  page_id
  , min(ingestion_dt) as first_ingestion_dt
  , max(ingestion_dt) as latest_ingestion_dt 
  from 
  ingestion_dfa_meta_page 
  group by 
  page_id) b
ON
  (a.page_id = b.page_id and a.ingestion_dt = b.latest_ingestion_dt)
WHERE
  buy_id != 'Buy-ID';
