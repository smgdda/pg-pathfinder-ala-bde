SET hive.exec.compress.intermediate=true;
SET mapred.compress.map.output=true;
SET hive.exec.parallel=true;
SET hive.exec.dynamic.partition=true;
SET hive.exec.dynamic.partition.mode=nonstrict;
SET hive.exec.compress.output=true;
SET mapred.output.compression.type=BLOCK;
SET mapred.output.compression.codec=org.apache.hadoop.io.compress.SnappyCodec;

ADD JAR /usr/lib/hive/lib/hive-contrib-0.10.0-cdh4.4.0.jar;

INSERT OVERWRITE TABLE dfa_meta_site
SELECT 
  a.site_id,
  regexp_replace(lower(site), ' ', '') as site,
  directory_site_id,
  directory_site,
  b.first_ingestion_dt as ingestion_dt,
  '${INGESTION_TABLE}' as ingestion_table,
  '${WFID}/${RUNID}' as insert_oozie_wfid
FROM 
  ingestion_dfa_meta_site a
JOIN
  (select site_id,max(ingestion_dt) as latest_ingestion_dt, min(ingestion_dt) as first_ingestion_dt from ingestion_dfa_meta_site group by site_id) b
ON
  (a.site_id = b.site_id and a.ingestion_dt = b.latest_ingestion_dt)
WHERE
  a.site_id != 'Site-ID';
