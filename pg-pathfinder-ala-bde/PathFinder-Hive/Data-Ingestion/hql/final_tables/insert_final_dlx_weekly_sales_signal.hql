SET hive.exec.dynamic.partition=true;
SET hive.exec.dynamic.partition.mode=nonstrict;
SET hive.exec.compress.output=true;
SET hive.exec.compress.intermediate=true;
SET mapred.compress.map.output=true;
SET mapred.output.compression.type=BLOCK;
SET mapred.output.compression.codec=org.apache.hadoop.io.compress.SnappyCodec;
SET hive.exec.parallel=true;
SET mapred.reduce.tasks=5;

INSERT OVERWRITE TABLE dlx_weekly_sales_signal
PARTITION(pf_campaign)
SELECT /*+ MAPJOIN(b) */
  dfa_id as dfa_user_id,
  ibtr,
  to_date(FROM_UNIXTIME(UNIX_TIMESTAMP(dlx_data_delivery_dt, 'MM/dd/yyyy'))) as dlx_data_delivery_dt,
  to_date(FROM_UNIXTIME(UNIX_TIMESTAMP(model_start_dt, 'MM/dd/yyyy'))) as model_start_dt,
  to_date(FROM_UNIXTIME(UNIX_TIMESTAMP(model_end_dt, 'MM/dd/yyyy'))) as model_end_dt,
  last_purchase_90d,
  time_of_purchase,
  level,
  retailer_tracked,
  sales_data_releasable,
  sales_signal_projected,
  a.ingestion_dt,
  '${INGESTION_TABLE}' as ingestion_table,
  '${WFID}/${RUNID}' as insert_oozie_wfid,
  a.pf_campaign
FROM
  ingestion_dlx_weekly_sales_signal a
JOIN
  (select pf_campaign, max(ingestion_dt) as latest_ingestion_dt from ingestion_dlx_weekly_sales_signal group by pf_campaign) b
ON
  (a.pf_campaign = b.pf_campaign and a.ingestion_dt = b.latest_ingestion_dt)
WHERE
  LOWER(a.pf_campaign) != 'pfcampaign';
 