DROP TABLE IF EXISTS ingestion_dfa_activity_${FILEID};

CREATE EXTERNAL TABLE ingestion_dfa_activity_${FILEID}
(
  time string,
  user_id string,
  advertiser_id string,
  buy_id string,
  ad_id string,
  creative_id string,
  creative_version string,
  creative_size_id string,
  site_id string,
  page_id string,
  keyword string,
  country_id string,
  state_province string,
  area_code string,
  browser_id string,
  browser_version string,
  os_id string,
  local_user_id string,
  activity_type string,
  activity_sub_type string,
  quantity string,
  revenue string,
  transaction_id string,
  other_data string,
  ordinal string,
  click_time string,
  event_id string,
  sv1 string,
  dlx_encrypted_dfa_user_id  string, 
  acxiom_encrypted_dfa_user_id string
)
PARTITIONED BY (ingestion_dt string)
ROW FORMAT DELIMITED FIELDS TERMINATED BY '\-2';

--alter table ingestion_dfa_activity_${FILEID} add partition (ingestion_dt='${INGESTION_YYYY}-${INGESTION_MM}-${INGESTION_DD}') location '/user/acxm_bdp/ingestion/dfa/media/activity_${FILEID}/landing/${INGESTION_YYYY}/${INGESTION_MM}/${INGESTION_DD}'