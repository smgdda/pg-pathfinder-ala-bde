DROP TABLE IF EXISTS ingestion_dfa_impressions;

CREATE EXTERNAL TABLE ingestion_dfa_impressions
(
  time string,
  user_id string,
  advertiser_id string,
  buy_id string,
  ad_id string,
  creative_id string,
  creative_version string,
  creative_size_id string,
  site_id string,
  page_id string,
  keyword string,
  country_id string,
  state_province string,
  area_code string,
  browser_id string,
  browser_version string,
  os_id string,
  dma_id string,
  city_id string,
  zip_code string,
  site_data string,
  time_utc_sec string,
  dlx_encrypted_dfa_user_id  string, 
  acxiom_encrypted_dfa_user_id string
)
PARTITIONED BY (ingestion_dt string)
ROW FORMAT DELIMITED FIELDS TERMINATED BY '\-2';

--alter table ingestion_dfa_impressions add partition (ingestion_dt='${INGESTION_YYYY}-${INGESTION_MM}-${INGESTION_DD}') location '/user/acxm_bdp/ingestion/dfa/media/NetworkImpression/landing/${INGESTION_YYYY}/${INGESTION_MM}/${INGESTION_DD}'