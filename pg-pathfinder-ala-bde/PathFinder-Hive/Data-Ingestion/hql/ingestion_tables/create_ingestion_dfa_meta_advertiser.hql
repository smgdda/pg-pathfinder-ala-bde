DROP TABLE IF EXISTS ingestion_dfa_meta_advertiser;

CREATE EXTERNAL TABLE ingestion_dfa_meta_advertiser
(
  spot_id string,
  advertiser_id string,
  advertiser string,
  advertiser_group_id string,
  advertiser_group string
)
PARTITIONED BY (ingestion_dt string)
ROW FORMAT SERDE 'org.apache.hadoop.hive.contrib.serde2.RegexSerDe'
WITH SERDEPROPERTIES (
  "input.regex" = "(.*?)Ã¾(.*?)Ã¾(.*?)Ã¾(.*?)Ã¾(.*?)"
);
