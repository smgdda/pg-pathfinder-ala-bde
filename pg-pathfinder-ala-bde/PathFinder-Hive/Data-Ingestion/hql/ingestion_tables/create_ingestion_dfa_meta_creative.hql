DROP TABLE IF EXISTS ingestion_dfa_meta_creative;

CREATE EXTERNAL TABLE ingestion_dfa_meta_creative
(
  advertiser_id string,
  buy_id string,
  buy string,
  start_dt string,
  end_dt string,
  creative_library_enabled string,
  billing_invoice_code string
)
PARTITIONED BY (ingestion_dt string)
ROW FORMAT SERDE 'org.apache.hadoop.hive.contrib.serde2.RegexSerDe'
WITH SERDEPROPERTIES (
  "input.regex" = "(.*?)Ã¾(.*?)Ã¾(.*?)Ã¾(.*?)Ã¾(.*?)Ã¾(.*?)Ã¾(.*?)"
);
