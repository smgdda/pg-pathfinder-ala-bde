DROP TABLE IF EXISTS ingestion_dfa_meta_page;

CREATE EXTERNAL TABLE ingestion_dfa_meta_page
(
  buy_id string,
  site_id string,
  page_id string,
  page string,
  site_placement string,
  content_category string,
  strategy string,
  start_dt string,
  end_dt string,
  group_type string,
  group_parent_id string,
  pricing_type string,
  cap_cost string,
  purchase_quantity string,
  purchase_cost string,
  flighting_activated string,
  cpa_activity_id string
)
PARTITIONED BY (ingestion_dt string)
ROW FORMAT SERDE 'org.apache.hadoop.hive.contrib.serde2.RegexSerDe'
WITH SERDEPROPERTIES (
  "input.regex" = "(.*?)Ã¾(.*?)Ã¾(.*?)Ã¾(.*?)Ã¾(.*?)Ã¾(.*?)Ã¾(.*?)Ã¾(.*?)Ã¾(.*?)Ã¾(.*?)Ã¾(.*?)Ã¾(.*?)Ã¾(.*?)Ã¾(.*?)Ã¾(.*?)Ã¾(.*?)Ã¾(.*?)"
);

