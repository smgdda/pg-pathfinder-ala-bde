DROP TABLE IF EXISTS ingestion_dfa_meta_site;

CREATE EXTERNAL TABLE ingestion_dfa_meta_site
(
  site_id string,
  site string,
  directory_site_id string,
  directory_site string
)
PARTITIONED BY (ingestion_dt string)
ROW FORMAT SERDE 'org.apache.hadoop.hive.contrib.serde2.RegexSerDe'
WITH SERDEPROPERTIES (
  "input.regex" = "(.*?)Ã¾(.*?)Ã¾(.*?)Ã¾(.*?)"
);
