﻿CREATE EXTERNAL TABLE ingestion_dlx_weekly_sales_signal(
  pf_campaign string, 
  dfa_id string, 
  ibtr string, 
  dlx_data_delivery_dt string, 
  model_start_dt string, 
  model_end_dt string, 
  last_purchase_90d string, 
  time_of_purchase string, 
  level string, 
  retailer_tracked string, 
  sales_data_releasable string, 
  sales_signal_projected string)
PARTITIONED BY ( 
  ingestion_dt string)
ROW FORMAT DELIMITED 
  FIELDS TERMINATED BY ',' 
STORED AS INPUTFORMAT 
  'org.apache.hadoop.mapred.TextInputFormat' 
OUTPUTFORMAT 
  'org.apache.hadoop.hive.ql.io.HiveIgnoreKeyTextOutputFormat'
;