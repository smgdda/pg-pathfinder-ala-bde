﻿CREATE EXTERNAL TABLE ingestion_dlx_weekly_sales_signal_v2(
  dfa_id string, 
  sales_signal_flag string, 
  model_start_dt string, 
  model_end_dt string, 
  pf_campaign string
)
PARTITIONED BY ( 
  ingestion_dt string)
ROW FORMAT DELIMITED 
  FIELDS TERMINATED BY ',' 
STORED AS INPUTFORMAT 
  'org.apache.hadoop.mapred.TextInputFormat' 
OUTPUTFORMAT 
  'org.apache.hadoop.hive.ql.io.HiveIgnoreKeyTextOutputFormat'
;