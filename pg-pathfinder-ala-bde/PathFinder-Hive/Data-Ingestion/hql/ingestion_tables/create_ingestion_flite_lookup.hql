ALTER TABLE ingestion_flite_lookup DISABLE NO_DROP;

DROP TABLE IF EXISTS ingestion_flite_lookup;

CREATE EXTERNAL TABLE ingestion_flite_lookup
(
  ad_name string,
  ad_id string
)
ROW FORMAT DELIMITED FIELDS TERMINATED BY ',';

ALTER TABLE ingestion_flite_lookup ENABLE NO_DROP;


