ALTER TABLE ingestion_spectra_cost DISABLE NO_DROP;

DROP TABLE IF EXISTS ingestion_spectra_cost;

CREATE EXTERNAL TABLE ingestion_spectra_cost
(
  franchise  STRING,
  brand  STRING,
  campaign  STRING,
  site  STRING,
  placement_group  STRING,
  placement  STRING,
  adserverplacementid  BIGINT,
  mbox_placementid  BIGINT,
  total_planned_value  FLOAT,
  total_plan_impressions  BIGINT,
  delivered_impressions  BIGINT,
  adserversitename  STRING,
  execution_type  STRING,
  targeting_type  STRING,
  planned_units  BIGINT,
  cost_type  STRING,
  event_date  STRING
)
ROW FORMAT DELIMITED FIELDS TERMINATED BY ',';

ALTER TABLE ingestion_spectra_cost ENABLE NO_DROP;
