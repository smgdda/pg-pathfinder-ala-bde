CREATE TABLE delv_final_table_audit
(
  insert_dts  TIMESTAMP,
  insert_oozie_wfid  STRING,
  table_name  STRING,
  ingestion_table  STRING,
  ingestion_dt  STRING,
  row_count  BIGINT,
  other_count  STRING
)
ROW FORMAT DELIMITED FIELDS TERMINATED BY '\t'
LOCATION '/user/delivery/tables/default/delv_final_table_audit';
