CREATE TABLE delv_ingestion_table_audit
(
  insert_dts  TIMESTAMP,
  table_name  STRING,
  ingestion_dt  STRING,
  row_count  BIGINT
)
ROW FORMAT DELIMITED FIELDS TERMINATED BY '\t'
LOCATION '/user/delivery/tables/default/delv_ingestion_table_audit';
