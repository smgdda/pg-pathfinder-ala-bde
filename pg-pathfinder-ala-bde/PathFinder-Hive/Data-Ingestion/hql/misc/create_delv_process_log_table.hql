CREATE TABLE delv_process_log
(
  log_dts  TIMESTAMP,
  process  STRING,
  id  STRING,
  level  STRING,
  details  STRING
)
PARTITIONED BY (log_dt STRING)
ROW FORMAT DELIMITED FIELDS TERMINATED BY '\t'
LOCATION '/user/delivery/tables/default/delv_process_log';


alter table delv_process_log enable no_drop;

--manually add this row into the folder
