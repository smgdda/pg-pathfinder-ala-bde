SET hive.exec.dynamic.partition.mode=nonstrict;
SET hive.exec.dynamic.partition=true;

INSERT INTO TABLE delv_process_log
PARTITION (log_dt)
select
  from_unixtime(unix_timestamp()) as log_dts,
  'DELETE_FROM_FINAL_TABLE' as process,
  '${INSERT_OOZIE_WFID}' as id,
  'INFO' as level,
  "final_table=${FINAL_TABLENAME},ingestion_table=${INGESTION_TABLENAME},ingestion_dt=${INGESTION_DT},insert_oozie_wfid=${INSERT_OOZIE_WFID},event_dt_list=${EVENT_DT_LIST}" as details,
  to_date(from_unixtime(unix_timestamp())) as log_dt
from 
  delv_process_log
limit 1;
  

SET hive.exec.compress.output=true;
SET mapred.output.compression.type=BLOCK;
SET mapred.output.compression.codec=org.apache.hadoop.io.compress.SnappyCodec;


SET mapred.reduce.tasks=20;

--pulls all records for each partition that has the data to be removed
-- into a temporary table; rows will be filtered on insert 
-- IT IS CRITICAL THAT ALL RECORDS BE COLLECTED FROM IMPACTED PARTITION

CREATE TABLE temp_${FINAL_TABLENAME}_delete as
select
  *
from
  ${FINAL_TABLENAME}
where
  event_dt in (${EVENT_DT_LIST});

  
--don't let it be dropped while processing
ALTER TABLE temp_${FINAL_TABLENAME}_delete ENABLE NO_DROP;
    
--now overwrite the production partitions filtering out
--  the rows to be removed
INSERT OVERWRITE TABLE ${FINAL_TABLENAME}
PARTITION (event_dt)
select 
  *
from
  temp_${FINAL_TABLENAME}_delete
where
  concat(ingestion_table,ingestion_dt,insert_oozie_wfid) !=  concat('${INGESTION_TABLENAME}','${INGESTION_DT}','${INSERT_OOZIE_WFID}');;


--drop temp table
ALTER TABLE temp_${FINAL_TABLENAME}_delete DISABLE NO_DROP;
DROP TABLE temp_${FINAL_TABLENAME}_delete;   