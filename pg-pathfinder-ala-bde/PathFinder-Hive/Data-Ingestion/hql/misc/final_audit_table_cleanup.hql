-- consolidates files and removes duplicate entries on final table audit table
insert overwrite table delv_final_table_audit
select
  a.*
from
  delv_final_table_audit a
join
(
select
  ingestion_dt,
  upper(ingestion_table) as ingestion_table,
  upper(table_name) as table_name,
  max(insert_dts) as latest_dts
from
  delv_final_table_audit
group by
  ingestion_dt,
  ingestion_table,
  table_name
) b
on
  a.ingestion_dt = b.ingestion_dt
and
  upper(a.ingestion_table) = upper(b.ingestion_table)
and
  upper(a.table_name) = upper(b.table_name)
and 
  a.insert_dts = b.latest_dts;
  