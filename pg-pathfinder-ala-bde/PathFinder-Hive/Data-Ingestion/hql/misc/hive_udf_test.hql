ADD JAR rank.jar;
CREATE TEMPORARY FUNCTION p_rank AS 'com.m6d.hiveudf.Rank';

SELECT
  insert_dts,
  table_name,
  ingestion_dt,
  row_count,
  p_rank(table_name)
FROM (
  SELECT
    insert_dts,
    table_name,
    ingestion_dt,
    row_count
  FROM
    delv_ingestion_table_audit
  DISTRIBUTE BY
    table_name, ingestion_dt
  SORT BY
    table_name, ingestion_dt, insert_dts) a;