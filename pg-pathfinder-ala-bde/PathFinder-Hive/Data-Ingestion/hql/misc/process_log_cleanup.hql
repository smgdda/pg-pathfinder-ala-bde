--------
-- clean up process log by combining all files for a day into a single file
--------


--these settings are needed for the process log insert
SET hive.exec.dynamic.partition=true;
SET hive.exec.dynamic.partition.mode=nonstrict;

insert overwrite table delv_process_log
partition (log_dt)
select
  *
from
  delv_process_log
where 
  log_dt = '${LOG_DATE}';