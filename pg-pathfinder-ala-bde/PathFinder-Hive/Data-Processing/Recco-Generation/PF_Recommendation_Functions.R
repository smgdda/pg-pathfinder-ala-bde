### LOAD REQUIRED LIBRARIES
require(dplyr)     # used for sequential dataframe functions
require(ggplot2)  # used to generate charts

### DEFINE HELPER FUNCTIONS
# Generic function to calculate based on groups
fun_calc <- function(dat, fun, new.name, grouping, nan.val = NaN) {
  dat <- dat %>% group_by_(.dots = grouping, add = FALSE) %>%
    mutate_(.dots = setNames(fun, new.name)) %>%
    mutate_(.dots = setNames(paste0("ifelse(is.nan(", new.name, "), ", nan.val, ", ", new.name, ")"), new.name)) %>%
    ungroup()
}

# Function to normalize by max abs based on groups, exclude values not included in the optimization from the denominator
fun_normalize <- function(dat, metric, new.name, optimize.metric, grouping, nan.val = NaN){
  suppressWarnings(dat <- dat %>% fun_calc(sprintf("%1$s/abs(ifelse(sum(%2$s, na.rm = TRUE) != 0, ifelse(max(%1$s * %2$s, na.rm = TRUE) == 0, max(abs(%1$s * %2$s), na.rm = TRUE), max(%1$s * %2$s, na.rm = TRUE)), ifelse(max(%1$s, na.rm = TRUE) == 0, max(abs(%1$s), na.rm = TRUE), max(%1$s, na.rm = TRUE))))", metric, optimize.metric), new.name, grouping = grouping, nan.val = nan.val) %>% 
     fun_calc(sprintf("%1$s + 1 - ifelse(sum(%2$s, na.rm = TRUE) != 0, max(%1$s * %2$s, na.rm = TRUE), max(%1$s, na.rm = TRUE))", new.name, optimize.metric), new.name, grouping = grouping, nan.val = nan.val))
  # suppressWarnings(dat <- dat %>% fun_calc(sprintf("%1$s/abs(ifelse(sum(%2$s, na.rm = TRUE) != 0, max(%1$s * %2$s, na.rm = TRUE), max(%1$s, na.rm = TRUE)))", metric, optimize.metric), new.name, grouping = grouping, nan.val = nan.val)
  # suppressWarnings(dat <- dat %>% fun_calc(sprintf("%1$s/max(abs(%1$s * %2$s), na.rm = TRUE)", metric, optimize.metric), new.name, grouping = grouping, nan.val = nan.val) %>% 
    # fun_calc(sprintf("%1$s + 1 - max(%1$s * %2$s, na.rm = TRUE)", new.name, optimize.metric), new.name, grouping = grouping, nan.val = nan.val))
  return(dat)
}

# Function to calculate group sums
fun_sum <- function(dat, metric, new.name, grouping){
  suppressWarnings(dat <- dat %>% fun_calc(sprintf("sum(abs(%s), na.rm = TRUE)", metric), new.name, grouping = grouping))
  return(dat)
}

# Generate text string based on vector
fun_c <- function(val, collapse = ", ", prePostChar = "" ){ 
  paste0("c(", paste0(prePostChar, val, prePostChar, collapse = collapse), ")")
}

# Function to replace key values by text
fun_key_to_text <- function(val, key, new.val, key.sep = "_", new.sep = " "){
  text <- strsplit(val, key.sep); new.val <- as.character(new.val)
  ans <- sapply(text, function(x){
    lookup <- match(x, key)
    val <- paste0(ifelse(is.na(lookup), toupper(x), new.val[lookup]), collapse = new.sep) # force upper case when there is no match
  })
  return(ans)
}

# Create blended efficiency metric
# Weights are dependent on campaign goal, e.g. Awareness, Consideration, Purchase Intent
# weights are adjusted based on data availability
fun_blend <- function(dat, norm.name, metric.name, optimize.metric, new.name, new.optimize, data.group.by, opt.group.by, campaign.group.by, tiebreaker.metric) {
  # Check data availability
  data.name <- paste0("dat_", norm.name)
  avail <- dat %>% group_by_(.dots = data.group.by) %>% slice(1) %>%
    select_(.dots = setNames(c(data.group.by, norm.name, optimize.metric), NULL)) %>%
    mutate_(.dots = setNames(paste0("ifelse(is.finite(", norm.name,"), 1, 0)"), data.name)) # check data available
  # Select weights by campaign based on objectives
  campaign.weights <- Campaigns %>% mutate_(.dots = setNames(sprintf("tolower(%1$s)", field$campaign.goal), field$campaign.goal)) %>% 
    merge(mutate_(Efficiency.Weights, .dots = setNames(sprintf("tolower(%1$s)", field$campaign.goal), field$campaign.goal)), by = field$campaign.goal) %>% # define campagin weights, ensure that names are all lower case.
    select_(.dots = c(campaign.group.by, metric.name)) 
  # Combine data available and campaign weights
  wgt.names <- paste0("wgt_", data.name)
  avail.wgt <- merge(avail, campaign.weights, by = campaign.group.by) %>%
    mutate_(.dots = setNames(paste0(data.name, "*", metric.name, "*", optimize.metric), wgt.names)) %>%  # Product of data, weight, and optimize flag
    mutate_(.dots = setNames(paste0("ifelse(is.na(", wgt.names, "), 0, ", wgt.names, ")", collapse = "+"), "sum")) %>%   # total weight with data avaialble
    mutate_(.dots = setNames(paste0(wgt.names, "/sum"), wgt.names)) %>%  # normalize weights by total available
    select_(.dots = c(data.group.by, wgt.names))
  dat <- dat %>% merge(avail.wgt, by = data.group.by, all.x = TRUE) %>%
    mutate_(.dots = setNames(c(paste0(sprintf("ifelse(is.na(%1$s), 0,  %1$s) * ifelse(is.na(%2$s), 0, %2$s)", wgt.names, norm.name), collapse = " + "), paste0("pmax(", paste0(optimize.metric, collapse = ", "), ", na.rm = TRUE)")), c(new.name, new.optimize))) %>%
    select_(.dots = paste0("-", wgt.names))
  # Check if there are ties geenrated by incomplete combination of metrics (due to optimize flag)
  ties <- dat %>% filter_(.dots = sprintf("%1$s >= 1", new.name)) %>% group_by_(.dots = opt.group.by) %>% summarize_(.dots = setNames(sprintf("length(%1$s)", field$dim), "count")) %>% filter(count > 1 ) %>% select(-count)
  if(nrow(ties) > 0){ # if there are ties break them using the tiebreaker.metric
    ties.dat <- dat %>% merge(ties , by = opt.group.by) # Select cases with ties 
    no.ties.dat <- dat %>% anti_join(ties.dat, by = data.group.by) # Select cases without ties
    ties.dat <- ties.dat %>% mutate_(.dots = setNames(sprintf("%1$s + replace(%2$s, is.na(%2$s), 0) / 1000000", new.name, tiebreaker.metric), new.name)) # Add the tiebreaker.metric (scaled to make it small) to the blended metric
    dat <- rbind(no.ties.dat, ties.dat) # combine data 
  }
  return(dat)
}

# Function to calculate base recommendations
fun_reco <- function(dat, pct.name, reco.name, reduce.name, optimize.metric, data.group.by){
  dat <- dat %>% mutate_(.dots = setNames(paste0(field$duration, " - ( ", field$month, " - ", field$start, " )"), field$months.to.end)) # calculate months left
  # Generate optimizations for records w/o na
  opt <- Optimization.Guidance %>% arrange_(.dots = c(field$months.to.end, field$norm.eff)) %>% # sort guidance by start of iterval
    mutate_(.dots = setNames(c(field$norm.eff), c("start"))) %>%
    within({end <- c(start[-1], Inf); end[is.na(end) | end == 0] <- Inf}) %>% # generate iterval for each recommendation record
    merge(dat %>% filter_(.dots = paste0(optimize.metric, " == 1")) # filter records to be optimized and merge with guidance by months left
          , by = field$months.to.end) %>% 
    filter_(.dots = sprintf("%1$s >= start & %1$s < end", pct.name)) %>% # filter records to the interval where the metric falls
    rename_(.dots = setNames(c(field$reco.guidance, field$reduce.guidance), c(reco.name, reduce.name))) %>% # rename reco and reduce fields
    mutate_(.dots = setNames(sprintf("ifelse(%1$s == 0, '%2$s', %3$s)", pct.name, reco$increase, reco.name), reco.name)) %>% # for max assign increase
    select_(.dots = c(data.group.by, reco.name, reduce.name)) # select fields to keep
  # merge back with data to attach NAs to records with NAs
  dat <- dat %>% merge(y = opt, by = data.group.by, all.x = TRUE) 
  dat <- dat %>% mutate_(.dots = setNames(c(sprintf("ifelse(is.na(%1$s), '%2$s', %1$s)", reco.name, reco$exclude), sprintf("ifelse(is.na(%1$s), 0, %1$s)", reduce.name)), c(reco.name, reduce.name))) # replace NAs with reco none and reduce 0
}

# Funtion to clean up format of raw input files
fun_process_input <- function(dat, delivery.output.prefix, default.month){
  delivery.name.output <- grep(delivery.output.prefix, group$delivery.metric, value = TRUE)
  if(is.null(dat)){
    return(as.data.frame(setNames(replicate(length(delivery.name.output) + 1, NA, simplify = FALSE), c(delivery.name.output, paste0(delivery.output.prefix, "_", field$optimize)))))
  }
  colnames(dat) <- tolower(colnames(dat)) # colunm names in lower case
  dat <- dat[, names(dat) %in% with(group, c(dim, input.metric))] # keep columns needed
  dim.in.data <- with(group, dim[dim %in% names(dat)]) # check dimensions in data
  opt.in.data <- with(group, opt[opt %in% names(dat)]) # check group variables in data
  campaign.in.data <- with(group, campaign.month[campaign.month %in% names(dat)]) # check campaign variables in data
  metrics.in.data <- group$input.metric[group$input.metric %in% names(dat)] # check metrics in data
  dat[, dim.in.data] <- dat[, dim.in.data] %>% apply(2, tolower) # lower case characters
  dat[, metrics.in.data] <- dat[,metrics.in.data, drop = FALSE] %>% apply(2, function(x) as.numeric(gsub("[$,]","",x))) # Force metrics to be numbers
  dat <- dat %>% mutate_(.dots = setNames(paste0("ifelse(", field$imp , " >= ", threshold$imp, " & ", field$cost , " >= ", threshold$cost, ", 1, 0)"), paste0(delivery.output.prefix, "_", field$optimize))) # Flag records with imp and cost below thresholds
  # Check if rollup is possible and pushdown is needed, rollup from type, pushdown to type
  opt.type <- type %>% filter_(.dots = field$optimize.type) %>% select_(.dots = sprintf("-c(%1$s)", field$optimize.type))# types to optimize
  data.groups <- dat %>% group_by_(.dots = opt.in.data) %>% 
    summarise_(.dots = setNames(sprintf("length(%1$s)", field$dim), "count")) %>% # find groups in data
    merge(type %>% select_(.dots = c(field$type, "has_site")), by = field$type) %>% # merge with type to track if data has site 
    merge(type %>% select_(.dots = sprintf("-c(%1$s, has_site)", field$type.text)) %>% rename_(.dots = setNames(field$type, "to" )), by.x = field$type, by.y = "from", all.x = TRUE) # get rollup / pushdown 
  rollup.groups <- data.groups %>% filter(to != '' & derive == "rollup") %>% 
    select_(.dots = c(opt.in.data, "to", "has_site")) # groups that should be rolled up 
  pushdown.groups <- data.groups %>% filter(to != '' & derive == "pushdown") %>% # groups that could be pushed down
    merge(data.groups %>% select_(.dots = opt.in.data) %>% mutate(no.pushdown = TRUE), by.x = replace(opt.in.data, opt.in.data == field$type, "to"), by.y = opt.in.data, all = TRUE) %>% # merge pushdown field with type to check if push down is needed
    filter(is.na(no.pushdown) & !is.na(to)) %>%
    select_(.dots = c(opt.in.data, "to", "has_site"))
  # filtered data to remove types not to optimize
  filtered.dat <- dat %>% merge(opt.type %>% select_(.dots = paste0("-c(derive, from, ", field$type.text, ")")), by = field$type)
  if(nrow(rollup.groups) > 0){  # remove data that will be replaced by a rollup
    filtered.dat <- filtered.dat %>% merge(rollup.groups %>% 
      within({rollup.replace <- TRUE; assign(field$type, to); to <- NULL; assign(field$has.site, NULL)}), by = opt.in.data, all.x = TRUE) %>% # merge rollup field with type to find records that will be replace by rollup
      filter(is.na(rollup.replace)) %>%
      select(-rollup.replace) 
  }
  # Rollup and push down data
  derive.dat <- dat %>% merge(rbind(rollup.groups, pushdown.groups) %>% merge(type %>% select_(.dots = c(field$type, field$need.site)), by.x = "to", by.y = field$type), by = opt.in.data) %>% 
    within({assign(field$type, to); to <- NULL})  # generate derived data
  dat <- rbind(filtered.dat, derive.dat) # update data with filtered and derived version  
  if(!(field$month %in% names(dat))) { 
    dat[[field$month]] <- default.month   # check if data has month column, if not assign default, if yes make number
  } else { dat[[field$month]] <- as.numeric(dat[[field$month]] ) }
  if(!(field$site %in% names(dat))) { dat[[field$site]] <- ifelse(dat[[field$has.site]], gsub("_.*[^_]$", "", dat[[field$dim]]), NA) } # check if data has site column, if not split it from dim
  missing.sites <- unique(dat[[field$site]])[!unique(dat[[field$site]]) %in% c(NA, site[[field$site]])] # identify missing sites, add NA to the list of valid sites as NAs are created during pushdown
  dat <- dat %>% merge(site %>% select_(.dots = c(field$site, field$site.type)), by.y = field$site, all.x = TRUE)  # merge with site to get site.type  
  fun_check_merge(dat[!is.na(dat[[field$site]]),], field$site.type, field$site, "Site") #  Check if sites are missing in the site table, use site.type as merge is all from dat, exclude site = NA as these are pushdown
  dat <- dat %>% mutate_(.dots = setNames(sprintf("ifelse(%1$s, %2$s, '')", field$need.site, field$site), field$site)) # check if site is needed, if not replace site with "SITE"
  dat <- dat %>% mutate_(.dots = setNames(c(field$dim, sprintf("ifelse(%1$s & %2$s, gsub('^[^_]*_', '', %3$s), %3$s)", field$has.site, field$need.site, field$dim), "NULL", "NULL"), c(paste0("old_", field$dim), field$dim, field$has.site, field$need.site))) # update DIM to remove site (to avoid duplicate site names) except for SITE type, remove flags
  dat <- dat %>% rename_(.dots = setNames(metrics.in.data, delivery.name.output))
  return(dat)
}

# Funtion to merge scores from multiple inputs, dat 1 is assumed to be the master input
fun_merge_input <- function(dat1, dat2){
  if (all(!group$dim %in% names(dat2))) {
    return(cbind(dat1, dat2))
  }
  by.key <- intersect(group$dim, intersect(names(dat1), names(dat2)))
  metrics.in.dat2 <- with(group, intersect(c(delivery.metric, optimize.metric), names(dat2))) # check metrics in data
  pushdownFields <- c(field$site, field$site.type) # pushdown based on site 
  if(any(is.na(dat2[, pushdownFields]))){
    by.key.no.pushdown <- by.key[!by.key %in% pushdownFields] # key without pushdown
    fields.no.pushdown <- names(dat2)[!names(dat2) %in% pushdownFields]
    key.dat1 <- dat1 %>% ungroup %>% select_(.dots = by.key) %>% unique # find all unique valid combinations of keys in dat1
    dat2.no.pushdown <- dat2 %>% filter_(.dots = paste0(paste0("!is.na(", pushdownFields, ")"), collapse = " & "))  # filter for records with data in pushDownFields
    dat2.no.pushdown.key <- dat2.no.pushdown %>% select_(.dots = by.key.no.pushdown) %>% unique  # unique keys that do not need pushdown
    dat2.pushdown <- dat2 %>% filter_(.dots = paste0(paste0("is.na(", pushdownFields, ")"), collapse = " & ")) %>% # filter for records with NA in pushDownFields
      anti_join(dat2.no.pushdown.key, by = by.key.no.pushdown) %>% # exclude keys that do not need pushdown
      select_(.dots = fields.no.pushdown) %>% # exclude pushDownFields
      merge(key.dat1, by = by.key.no.pushdown) # merge with keys to add them
    dat2 <- rbind(dat2.no.pushdown, dat2.pushdown)
  }   
  dat <- merge(dat1, dat2 %>% select_(.dots = c(by.key, metrics.in.dat2)), by = by.key, all = TRUE)
  return(dat)
}
# Function to push down site to records witht site = ''.  Values will be pushed down from other records with similar keys
fun_pushdown <- function(dat, pushDownFields){
  if(any(is.na(dat[, pushDownFields]))){
    fields.key <- group$dim[group$dim %in% names(dat)]
    fields.key.no.pushDownFields <- fields.key[!fields.key %in% pushDownFields]
    fields.no.pushDownFields <- names(dat)[!names(dat) %in% pushDownFields]
    ans.no.pushdown <- dat %>% filter_(.dots = paste0(paste0("!is.na(", pushDownFields, ")"), collapse = " & "))  # filter for records with data in pushDownFields
    key.no.pushdown <- ans.no.pushdown %>% select_(.dots = fields.key) %>% unique # find all unique valid combinations of keys with site
    ans.pushdown <- dat %>% filter_(.dots = paste0(paste0("is.na(", pushDownFields, ")"), collapse = " & ")) %>% select_(.dots = fields.no.pushDownFields) %>% # filter for records with NA in pushDownFields, exclude pushDownFields
      merge(key.no.pushdown, by = fields.key.no.pushDownFields) # merge with keys to add them
    key.pushdown <- ans.pushdown %>% select_(.dots = fields.key) %>% unique %>% within(drop <- TRUE)
    ans.no.pushdown <- ans.no.pushdown %>% merge(key.pushdown, by = fields.key) %>% filter(drup == FALSE) %>% select(-drop) # drop 
    ans <- rbind(ans.no.pushdown, ans.pushdown)
  } else{
    ans <- dat
  }
  return(ans)
  
}

fun_no_match_values <- function(dat1, dat2, fields){
  valid.fields <- fields[(fields %in% names(dat1)) & (fields %in% names(dat2))]
  ans <- list()
  for (f in valid.fields){
    d1 <- unique(dat1[[f]]); d2 <- unique(dat2[[f]])
    if (length(n1 <- d2[!d2 %in% d1]) > 0) ans[["not_in_1"]][[f]] <- list(n1)
    if (length(n2 <- d1[!d1 %in% d2]) > 0) ans[["not_in_2"]][[f]] <- list(n2)
  }
  return(ans)
}
# check if all keys exist use a second field (validation_field) as all keys are preserved in the data (left_joins), if not stop the process
fun_check_merge <- function(dat, validation.field, key.field, label){
  if (any(is.na(dat[[validation.field]]))){  
    stop(paste0(label, " missing: ", paste0(unique(dat[is.na(dat[[validation.field]]), key.field]), collapse = ", ")))
  }
}