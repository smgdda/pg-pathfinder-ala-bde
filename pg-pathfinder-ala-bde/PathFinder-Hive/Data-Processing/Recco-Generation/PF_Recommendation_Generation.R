# Written by: Kubilay Gursel
# Last updated: Oct 1, 2014

# Updated by Francisco Lippke October 18, 2014

########## IMPORTANT ##################################################################################################
# THIS CODE ASSUMES DATA INPUTs FOLLOW PARTICULAR FORMATs
# HELPER FUNCTIONS IN SEPARATE FILE

### DATA INPUTS:
### 1. Impression and Cost Data woth Behavioral Scalar KPI, e.g. PF_AGG_ALL CAMPAIGNS.csv
### 2. Optimization Guidance - Campaign Specific, e.g. Crest_Optimization_Guidance12.csv
### 3. Blended_Efficiency_Weights, e.g. Blended_Efficiency_Weights.csv
### 4. Prior_vs_Current_Recco_Adjustments, e.g. Prior_vs_Current_Recco_Adjustments.csv
### 5. Prior Recco file, PRIOR_RECCO_MONTH.csv
### 6. Campaign master, Campaign_master.csv, 
### 7. Site Lookup - Site.csv
### 9. Targeting Lookup - Targeting.csv
### 10. Exe_type Lookup - Exe_Type.csv
### 11. Creative to exclude - Creative.csv
### 12. Override table to create combinations

### DATA OUTPUTS:
### 1. Recommendation results
### 2. Chart data
### 3. Prior recommendation data (for next cycle)
### 4. Comprehensive Results Main data frame to save for recording purposes
#######################################################################################################################

### STEP 0: LOAD FUNCTIONS AND ENTER CAMPAIGN ANALYSIS PARAMETERS ####
# Constants
month <- 11 # 1 = July WOC, run in June
year <- 1415
reco <- list(increase = "increase", exclude = "none", monitor = "monitor") # recommendation text for increase, monitor and none
strengh <- list(values = c(0, 0.2, 0.3, 0.4, Inf), labels = c("80%", "85%", "90%", "95%")) # bins for strenght  c("80%", "85%", "90%", "95%"))
priority <- list(values = c(0, 0.01, 0.1, Inf), labels = c("3", "2", "1"), monitor = 4) # bins for priority
threshold <- list(imp = 50000, cost = 5000) # thresholds to ignore records due to low volumes
data.file.generic <- paste0("agg_all_", month, ".csv")
dimtype <- list(site = "site", sitetactic = "sitetactic", sitecreatie = "sitecreative") # type without exe_type
ssm <- "ssm" # SSM exe_type

# Load helper functions, assumed to be in working directory
source(file.path(getwd(), "PF_Recommendation_Functions.R"))

# Define data input and ouput directories
dir.data <- list(input = file.path(getwd(),"data"),
                output = file.path(getwd(),"output"))

# Define variable ames 
field <- list(month = "campaign_analysis_month",
              brand = "brand", brand.text = "brand_text", # field and user friendly text version
              campaign = "pfcampaign", campaign.text = "pfcampaign_text",
              rate.type = "rate_type",
              type = "dimtype", type.text = "dimtype_text", 
              exe.type = "exe_type", exe.type.text = "exe_type_text",
              site = "dim_site", site.text = "site_text",
              site.type = "site_type", 
              targeting = "targeting", targeting.text = "targeting_text",
              optimize.type = "optimize_type", # field to flag types to optimize
              dim = "dim", dim.text = "dim_text", 
              imp = "imps", 
              cost = "cost", 
              score = "score",
              sig = "p_val", 
              bhv = "pfscore", bhv.prefix = "bhv",
              att = "final_contr", att.prefix = "att",
              oss = "dlx_logreg", oss.prefix = "oss",
              optimize = "optimize", # Field to flag records to include in optimization
              has.site = "has_site", # Field to flag types that have site information
              need.site = "need_site", # Field to flag types that need site information 
              blend = "eff_blend",
              norm.blend = "norm_eff_blend",
              optimize.blend = "optimize_blend",
              duration = "campaign_duration", 
              start = "campaign_start",
              months.to.end = "months_to_end",
              campaign.goal = "campaign_goal",
              prior.month = "prior_analysis_month",
              adjust.reduce = "adjust_reduce_blend",
              reco = "final_reco_blend",
              initial.reco = "reco_blend",
              prior.reco.blend = "prior_reco_blend",
              reduce = "final_reduce_blend",
              initial.reduce = "reduce_blend",
              increase.dim = "increase",
              reduce.dim = "reduce", 
              monitor.dim = "monitor", 
              strength = "strengh",
              priority = "priority",
              reduce.amount = "reduce_amount", reduce.amount.text = "reduce_amount_text", 
              increase.text = "increase_text", reduce.text = "reduce_text", monitor.text = "monitor_text", 
              recnumber = "recnum",
              override.suffix = "_over",
              norm.eff = "norm_eff", # Optimization guidance efficience thresholds
              reco.guidance = "reco", # Optimization guidance reco levels
              reduce.guidance = "reduce") # Optimization guidance reduce ammount

# Define groups
group <- list()
group <- group %>% within({
  campaign <- with(field, c(brand, campaign)) # campaign level group
  campaign.month <- c(field$month, campaign) # campaign and month level group
  opt <- c(campaign.month, with(field, c(type, exe.type, site.type, site))) # optimization groups
  opt.text <- c(with(field, c(month, brand.text, campaign.text, type.text, exe.type.text, site.type, site.text))) # optimiztion groups with user friendly text 
  dim <- c(opt, field$dim) # group at the dimension level
  override <- setdiff(dim, field$month)
  type.total <- c(campaign.month, field$type)  # Used to calculate total campaign budget
  raw.score.metric <- with(field, c(bhv, att, oss)) # raw score metrics
  kpi.prefix <- with(field, c(bhv.prefix, att.prefix, oss.prefix))
  data.file <- setNames(paste0(kpi.prefix, "_", data.file.generic), kpi.prefix)
  score.metric <- setNames(paste0(kpi.prefix, "_", "score"), kpi.prefix) # processed score metricsgroup$data.file <- setNames(paste0(group$kpi.prefix, "_", data.file.generic, "_", month, ".csv"), group$kpi.prefix)
  optimize.kpi.metric <- setNames(paste0(kpi.prefix, "_", field$optimize), kpi.prefix)
  optimize.metric <- c(optimize.kpi.metric, field$optimize.blend)
  delivery.input <- with(field, c(imp, cost)) # impressions and cost input field names
  input.metric <- with(group, c(delivery.input, raw.score.metric)) # raw input delivery and metric names
  delivery.metric <- as.vector(t(outer(paste0(kpi.prefix, "_"), c(delivery.input, field$score), paste0))) # all elivery metrics across KPIs
#   scaled.delivery <- paste0("SCL_", delivery.metric) # scaled delivery metric, e.g., impressions, cost
#   scaled.imp.metric <- grep(field$imp, scaled.delivery, value = TRUE) # scaled impressions
#   scaled.cost.metric <- grep(field$cost, scaled.delivery, value = TRUE) # scaled cost
#   scaled.metric <- paste0("SCL_", score.metric) # scaled scores, e.g., behavioral score
  impact.metric <- paste0("imp_", score.metric) # impact metrics
  eff.metric <- paste0("eff_", score.metric) # efficiency metrics
  imp.metric <- grep(field$imp, delivery.metric, value = TRUE) # all impression metrics
  cost.metric <- grep(field$cost, delivery.metric, value = TRUE) # all cost metrics
  norm.kpi.metric <- paste0("norm_", eff.metric) # normalized efficiency scores w/o blended metric
  norm.metric <- c(norm.kpi.metric, field$norm.blend) # normalized efficience scores including blended metric
  norm.impact.metric <- paste0("norm_", impact.metric) # normalized impact scores, blended impact does not exist
  percent.metric <- gsub("^norm","percent_to_max", norm.metric) # percent to max efficiency metric by KPI
  percent.impact.metric <- gsub("^norm","percent_to_max", norm.impact.metric) # percent to max impact metric by KPI
  reco.metric <- gsub("^norm_eff","reco", norm.metric) # reco by KPI
  reduce.metric <- gsub("^norm_eff","reduce", norm.metric) # percent of reduction by KPI
  flag.metric <- paste0("flag_", score.metric) # flag of differece between blend and KPI reco
  opt.budget <- paste0("opt_bud_", cost.metric) # budget to optimize (initial before reco)
  budget.change <- paste0("bud_change_", cost.metric) # change in budget
  reduce.budget <- paste0("reduce_bud_", cost.metric) # budget reduction for reduce
  final.budget <- paste0("final_bud_", cost.metric) # final budget
  percent.reduce.budget <- paste0("percent_reduce_bud_", cost.metric) # percent of the total realloction allocated to one reco
  opt.total.metric <-  paste0("opt_", score.metric) # total score metric at the opt level
  total.metric <- paste0("total_", score.metric) # total score metric at the campaign level (max of opt for campaign) 
  estimate.metric <- paste0("opt_est_", score.metric) # impact of reco at the opt level
  inc.estimate.metric <- paste0("inc_opt_est_", score.metric) # increase in score on increase dimension (before substracting drop in reduce)
  campaign.estimate.metric <- paste0("campaign_est_", score.metric) # impact of reco at the campaign level
  campaign.estimate.metric.text <- paste0(campaign.estimate.metric, "_text") # impact of reco at the campaign level formated as text
  inc.eff <- paste0("inc_eff_", score.metric) # efficiency of the increase dimension
  percent.to.increase <- paste0("percent_to_inc_eff_", score.metric) # percent to the increase dimension at the opt level
  percent.to.increase.text <- paste0(percent.to.increase, "_text") # percent to the increase dimension at the opt level formated as text
  est.columns <- c(estimate.metric, campaign.estimate.metric) # estimates of impact at the opt ad campaing level by KPI
  reco.dim <- with(field, c(increase.dim, reduce.dim, monitor.dim)) # reco dim in final table
  site.reco.dim <- paste0("site_", reco.dim) # reco dims with site in final table
})

### STEP 1: LOAD PF.AGG AND TABLES REQUIRED FOR DECISION MAKING ALGORITHMS ####
# Import Campaign Master file
Campaigns <- read.csv(file.path(dir.data$input, paste0("Campaign_Master_", month, ".csv")), stringsAsFactors = FALSE)

# Import Blended Efficiency Weights
Efficiency.Weights <- read.csv(file.path(dir.data$input, "Blended_Efficiency_Weights.csv"), stringsAsFactors = FALSE)

# Import optimization Guidance table (Note that columns with NA will be imported as atomic type "Character")
Optimization.Guidance <- read.csv(file.path(dir.data$input, "Optimization_Guidance.csv"), stringsAsFactors = FALSE)

# Import key files for type, site, exe.type and targeting
type <- read.csv(file.path(dir.data$input, "Type_Key.csv"), stringsAsFactors = FALSE)
site <- read.csv(file.path(dir.data$input, "Site_Key.csv"), stringsAsFactors = FALSE)
exe.type <- read.csv(file.path(dir.data$input, "Exe_Type_Key.csv"), stringsAsFactors = FALSE)
targeting <- read.csv(file.path(dir.data$input, "Targeting_Key.csv"), stringsAsFactors = FALSE)
invalid.creatives <- read.csv(file.path(dir.data$input, "Creative_Key.csv"), stringsAsFactors = FALSE)

# Import Prior vs Current Reco adjustments guidelines
Reco.Adjustments <- read.csv(file.path(dir.data$input, "Prior_vs_Current_Recco_Adjustments.csv"), stringsAsFactors = FALSE)

# Import overrides
overrides <- tryCatch((function(x){x[is.na(x)] <- ""; x})(read.csv(file.path(dir.data$input, paste0("agg_override", ".csv")), stringsAsFactors = FALSE)), error = function(e) NULL) # replace NAs with ""

# Import Prior Reco for the campaign checking if file exists first
Prior.Reco <- tryCatch(read.csv(file.path(dir.data$input, paste0("prior_reco_", month, ".csv")), stringsAsFactors = FALSE), error = function(e) NULL)

# Import data tables 
raw.data <- lapply(group$data.file, function(x){tryCatch(read.csv(file.path(dir.data$input,x), stringsAsFactors = FALSE), error = function(e) NULL)})
if(all(as.logical(lapply(raw.data, is.null)))) { stop("No data") }

# Process raw data
kpi.data <- mapply(fun_process_input, dat = raw.data, delivery.output.prefix = group$kpi.prefix, MoreArgs = list(default.month = month)) 

## Aggregate across rate_type for BHV
kpi.data$bhv <- kpi.data$bhv %>% group_by_(.dots = c(group$opt, field$dim)) %>% summarize(bhv_imps = sum(bhv_imps), bhv_cost = sum(bhv_cost), bhv_score= sum(bhv_score), bhv_optimize = ifelse(bhv_imps >= threshold$imp & bhv_cost >= threshold$cost , 1, 0))

# Merge kpi.data into PF.AGG data.frame
PF.AGG <- Reduce(fun_merge_input, kpi.data)
no.match <- lapply(kpi.data, fun_no_match_values, dat2 = PF.AGG,  fields = group$dim)
PF.AGG <- PF.AGG %>% rbind(PF.AGG %>% merge(overrides, by = group$override) %>% # use override to generate new data
            mutate_(.dots = setNames(c(paste0(group$override, field$override.suffix)), group$override)) %>% # override old keys
            select_(.dots = c(paste0("-", paste0(group$override, field$override.suffix)))) %>% # drop override fields
            group_by_(.dots = group$dim) %>% # group by key to summaryze in case there are identical keys
            (function(d){summarize_(d, .dots = setNames(c(sprintf("sum(%1$s, na.rm = TRUE)", group$delivery.metric), sprintf("ifelse(%1$s >= %2$s & %3$s >= %4$s, 1, 0)", group$imp.metric, threshold$imp, group$cost.metric, threshold$cost)), c(group$delivery.metric, group$optimize.kpi.metric)))})(.)) # Summarize new data to combine identical keys, using anonimous function to avoid assignment 
PF.AGG <- PF.AGG %>% 
  merge(mutate(invalid.creatives, exclude = 1), by = field$dim, all = TRUE) %>% # merge with invalid creatives
  filter_(.dots = paste0(field$type, " != '", dimtype$sitecreatie, "' | is.na(exclude)")) %>% # Remove invalid creatives
  mutate(exclude = NULL) %>% # remove temp field
  merge(Campaigns %>% select_(.dots = c(group$campaign, field$duration, field$start, field$brand.text, field$campaign.text)), by = group$campaign, all.x = TRUE) %>% # Add campaign master data
  mutate_(.dots = setNames(paste0("ifelse(is.na(", field$site.type, "), '', ", field$site.type, ")"), field$site.type))
fun_check_merge(PF.AGG, field$duration, field$campaign, "Campaign") #  Check if campaigns are missing in the master table, use duration as merge is all from PF.AGG if (any(is.na(PF.AGG[[field$duration]]))){  
PF.AGG <- PF.AGG %>% 
  mutate_(.dots = setNames(paste0("ifelse(", field$type, " == '", dimtype$sitetactic ,"', '', ", field$exe.type, ")"), field$exe.type)) # remove exe.type from SITETACTIC to allow cross media optimization within sites
PF.AGG <- PF.AGG %>% rbind(PF.AGG %>% # add SSM data as a "separate" campaign
                             filter_(.dots = sprintf("%1$s == '%2$s'", field$exe.type, ssm)) %>% # Filter SSM data across campaigns
                             mutate_(.dots = setNames(c(sprintf("'%1$s'", ssm), sprintf("'%1$s'", ssm), sprintf("paste0(%1$s, ' - ', %2$s)", field$brand.text, field$campaign.text), 12, 1, sprintf("'%1$s'", ssm), sprintf("'%1$s'", ssm)), c(field$brand, field$campaign, field$exe.type, field$duration, field$start, field$campaign.text, field$brand.text)))) # Replace campaign brand and campaign with SSM, exe_type with brand and campaign text, duration to full year and start with beginning of fiscal, using sprintf("'%1$s'", ssm) to force evaluation as text string (not a field name).  Sitetactic recos not included as tactics these do not make sense - only possible if dim is updated to include brand-campaign and comparing across brands for the same site is not equivalent (conversions in one brand are not comparable to aother brand) - code for dim update if needed sprintf("ifelse(%1$s == '%2$s', %3$s, %4$s)", field$type, dimtype$sitetactic, field$exe.type, field$dim)

### STEP 2: NORMALIZE EACH EFFICIENCY SCALAR USING THE MAXIMUM VALUES BY TYPE, CAMPIGN, AND MONTH ####
# Calculate impact per 1000000 imps and efficiency per $1000000  
PF.AGG <- PF.AGG %>% 
  mutate_(.dots = setNames(sprintf("%1$s / %2$s * 1000000", group$score.metric, group$imp.metric), group$impact.metric)) %>% 
  mutate_(.dots = setNames(sprintf("%1$s / %2$s * 1000000", group$score.metric, group$cost.metric), group$eff.metric)) 
# Normalize metric
PF.AGG <- fun_normalize(PF.AGG, c(group$eff.metric, group$impact.metric), c(group$norm.kpi.metric, group$norm.impact.metric), optimize.metric = group$optimize.kpi.metric, grouping = group$opt) # exclude last norm.metric (blended) as it has ot been defined yet

# Calculate blended metric
PF.AGG <- fun_blend(PF.AGG, group$norm.kpi.metric, group$eff.metric, group$optimize.kpi.metric, field$blend, field$optimize.blend, group$dim, group$opt, group$campaign, group$norm.kpi.metric[1]) # exclude last norm.metric (blended) as it has ot been defined yet
PF.AGG <- fun_normalize(PF.AGG, field$blend, field$norm.blend, field$optimize.blend, grouping = group$opt)

### STEP 3: CREATE RECOMMENDATIONS BASED ON BLENDED EFFICIENCY METRIC ####
# Calcualte % to max
PF.AGG <- with(group, PF.AGG %>% mutate_(.dots = setNames(paste0("1-", c(norm.metric, norm.impact.metric)), c(percent.metric, percent.impact.metric))))

# Calculate base recommendations
for(m in seq_along(group$percent.metric)) PF.AGG <- fun_reco(PF.AGG, group$percent.metric[m], group$reco.metric[m], group$reduce.metric[m], group$optimize.metric[m], group$dim)

# create flags for diverging recos across KPIs and blended metric
PF.AGG <- PF.AGG %>% mutate_(.dots = setNames(paste0(head(group$reco.metric, -1)," != ", last(group$reco.metric)), group$flag.metric))

# append prior_reco for the campaign
if(!is.null(Prior.Reco)){
  PF.AGG <- PF.AGG %>% merge(merge(Prior.Reco, Campaigns %>%
      filter_(.dots = sprintf("!is.na(%1$s)", field$prior.month)) %>%
      select_(.dots = c(group$campaign.month, field$prior.month)), 
      by = c(group$campaign, field$prior.month)),
      by = group$dim, all.x = TRUE) %>% 
    mutate_(.dots = setNames(sprintf("ifelse(is.na(%1$s), '%2$s', %1$s)", field$prior.reco.blend, reco$exclude), field$prior.reco.blend)) # Replace NA with NONE
} else {
  PF.AGG <- PF.AGG %>% mutate_(.dots = setNames(reco$exclude, field$prior.reco.blend))
}

# append prior_reco vs current guidelines and assign dirrectional adjustment
PF.AGG <- PF.AGG %>% merge(Reco.Adjustments, by = c(field$initial.reco, field$prior.reco.blend), all.x = TRUE) %>%
  mutate_(.dots = setNames(sprintf("%1$s + ifelse(is.na(%2$s), 0, %2$s)", field$initial.reduce, field$adjust.reduce), field$reduce))

### STEP 4: ESTIMATE IMPACT ON CAMPAIGN ####
# Estimate impact of optimization on opt.dim e.g. AMAZON ADSIZE
# Assume campaign is flighted evenly for the entire campaign duration to forecast optimizable budget
# Assume WOC activations are in market by mid-month, e.g. Oct 2014 WOC is in market by Oct 15, 2014
# Updated to use historical budget, results are the same easier to check
PF.AGG <- PF.AGG %>% mutate_(.dots = setNames(c(group$cost.metric, paste0(group$opt.budget, " * ", field$reduce)), c(group$opt.budget, group$budget.change)))

# Identify the total sum to be moved from low to top performer and update final budget
# If BUD_CHANGE == 0 for INCREASE reco then no attribute is reducing so set reco to NONE
# Set to MONITOR recos that were increase before and are NONE now
PF.AGG <- PF.AGG %>% merge(PF.AGG %>% group_by_(.dots = group$opt ) %>% 
  summarise_(.dots = setNames(paste0("sum(abs(", group$budget.change , "), na.rm = TRUE)"), group$reduce.budget)), by = group$opt) %>% 
  mutate_(.dots = setNames(c(
    sprintf("ifelse(%1$s == '%2$s', %3$s, %4$s)", field$initial.reco, reco$increase, group$reduce.budget, group$budget.change), 
    sprintf("ifelse(%1$s != '%2$s', - %3$s / %4$s, 0)", field$initial.reco, reco$increase, group$budget.change, group$reduce.budget),
    sprintf("%1$s + %2$s", group$opt.budget, group$budget.change),
    sprintf("ifelse(pmax(%1$s, na.rm = TRUE) == 0, ifelse(%2$s %%in%% c('%3$s', '%4$s'), '%4$s', ifelse(%5$s == '%3$s', '%6$s', %5$s)), %5$s)", paste0(group$budget.change, collapse = ", "), field$prior.reco.blend, reco$increase, reco$monitor, field$initial.reco, reco$exclude)), c(group$budget.change, group$percent.reduce.budget, group$final.budget, field$reco))) %>% select_(.dots = paste0("c(", paste0("-", group$reduce.budget, collapse = ", " ), ")")) 

# Calculate impact across all KPIs at site and campaign level
total.score <- PF.AGG %>% fun_sum(group$score.metric, group$total.metric, grouping = group$type.total) %>%
    group_by_(.dots = group$campaign.month) %>%
    summarize_(.dots = setNames(paste0("max(abs(", group$total.metric, "), na.rm = TRUE)"), group$total.metric)) # calcualte total scores, max of total across all types per campaign month
PF.AGG <- PF.AGG %>% merge(total.score, by = group$campaign.month) %>%
  fun_sum(group$score.metric, group$opt.total.metric, grouping = group$opt) # calcualte optimization group score and merge with main table
PF.AGG <- PF.AGG %>% mutate_(.dots = setNames(paste0("(ifelse(is.na(", group$score.metric, "), 0, ", group$score.metric,") / ", group$opt.total.metric," ) * (", group$budget.change, " / ", group$opt.budget, " ) "), group$estimate.metric))
PF.AGG <- PF.AGG %>% merge(PF.AGG %>% 
          filter_(.dots = paste0(field$reco, " == '", reco$increase, "'")) %>% 
          select_(.dots = setNames(c(group$opt, group$estimate.metric, head(group$norm.metric,-1)), 
          c(group$opt, group$inc.estimate.metric, group$inc.eff))), by = group$opt, all.x = TRUE) %>% 
  mutate_(.dots = setNames(c(paste0("ifelse(", field$reco, " == '", reco$increase, "', 0, ", group$inc.estimate.metric, " * ", group$percent.reduce.budget, " + ", group$estimate.metric, ")"), paste0(group$estimate.metric, " * ", group$opt.total.metric," / ", group$total.metric), paste0("(", group$inc.eff, " - ", head(group$norm.metric,-1), ") / abs(", group$inc.eff, ")")), c(group$estimate.metric, group$campaign.estimate.metric, group$percent.to.increase)))  # Distribute impact across all attributes that are being reduced and calcualte campaign level improvement
# old veriosn of with removed error handling
# mutate_(.dots = setNames(c(paste0("ifelse(", field$reco, " == '", reco$increase, "', 0, ", group$inc.estimate.metric, " * ", group$percent.reduce.budget, " + ", group$estimate.metric, ")"), paste0(group$estimate.metric, " * ", group$opt.total.metric," / ", group$total.metric), paste0("ifelse(is.na(", group$estimate.metric,"), 0, ", group$estimate.metric, ")"), paste0("ifelse(is.na(", group$campaign.estimate.metric,"), 0, ", group$campaign.estimate.metric, ")"), paste0("ifelse(!is.na(", group$inc.eff, "), ifelse(", group$inc.eff , " != 0, ifelse(!is.na(", head(group$norm.metric,-1), "), 1 - ", head(group$norm.metric,-1), " / ", group$inc.eff , ", 0), 0), 0)")), c(group$estimate.metric, group$campaign.estimate.metric, group$estimate.metric, group$campaign.estimate.metric, group$percent.to.increase)))

### STEP 5: CREATE SUMMARY TABLE AND CHART DATA ####
# Generate key that combines with site, exe.type, and targeting
key <- data.frame(KEY = c(site[[field$site]], exe.type[[field$exe.type]], targeting[[field$targeting]]),
            TEXT = c(site[[field$site.text]], exe.type[[field$exe.type.text]], targeting[[field$targeting.text]]))
# Generate table and replace text for type, site and exe.type
rec.temp <- PF.AGG %>% within({
  assign(field$type.text, factor(fun_key_to_text(eval(as.name(field$type)), type[[field$type]], type[[field$type.text]]), type[[field$type.text]], ordered = TRUE))  # as factor to preserve sort 
  assign(field$exe.type.text, fun_key_to_text(eval(as.name(field$exe.type)), exe.type[[field$exe.type]], exe.type[[field$exe.type.text]]))  # as factor to preserve sort 
  assign(field$site.text, fun_key_to_text(eval(as.name(field$site)), key$KEY, key$TEXT))
  assign(field$dim.text, fun_key_to_text(eval(as.name(field$dim)), key$KEY, key$TEXT))
  assign(field$type, factor(eval(as.name(field$type)), type[[field$type]], ordered = TRUE))})  # as factor to preserve sort
# Generate summary table and chart data
CHT.PF <- rec.temp %>% select_(.dots = c(group$opt.text, field$dim.text, group$norm.impact.metric, group$norm.metric, group$dim, field$optimize.blend)) %>%
  group_by_(.dots = group$opt) %>% arrange_(.dots = group$dim) 

### SETP 6: GENERATE RECOMMENDATION SUMMARY TABLE ####
# Identify increase dim first
increase.reco <- rec.temp %>% filter_(.dots = paste0(field$reco, " == '", reco$increase, "'")) %>% 
  within({assign(field$increase.dim, eval(as.name(field$dim))); assign(field$increase.text, eval(as.name(field$dim.text)))}) %>%
  select_(.dots = c(group$opt.text, field$increase.text, field$increase.dim, group$opt))
# generate reduce strings and merge with reco table
reduce.reco <- rec.temp %>% filter_(.dots = paste0("!", field$reco, " %in% ", fun_c(c(reco$increase, reco$exclude, reco$monitor), prePostChar = "'"))) %>% group_by_(.dots = group$opt) %>% 
  arrange_(.dots = paste0("desc(", paste0(sprintf("ifelse(is.na(%1$s), 0, %1$s)", group$campaign.estimate.metric), collapse = " + "), ")")) %>% # sort for display
  mutate_(.dots = setNames(c(field$dim, field$dim.text, field$reduce, paste0("sprintf('%1.0f%%',-", field$reduce, " * 100)"), paste0("ifelse(!is.na(", group$percent.to.increase, "), sprintf('%1.0f%%',", group$percent.to.increase, " * 100), 'NA')"), paste0("cut(pmax(", last(group$percent.metric), ", na.rm = TRUE), ", fun_c(strengh$values), ", ", fun_c(strengh$labels, prePostChar = "'"),", right = FALSE)"), paste0("as.character(cut(pmax(", paste0(group$campaign.estimate.metric, collapse = ", "), ", na.rm = TRUE), ", fun_c(priority$values), ", ", fun_c(priority$labels),"))"), paste0("ifelse(!is.na(", group$campaign.estimate.metric, "), sprintf('%1.1f%%',", group$campaign.estimate.metric, " * 100), 'NA')")), c(field$reduce.dim, field$reduce.text, field$reduce.amount, field$reduce.amount.text, group$percent.to.increase.text, field$strength, field$strength.text, field$priority, group$campaign.estimate.metric.text))) %>%  
  select_(.dots = c(group$opt, field$reduce.dim, field$reduce.text,  field$reduce.amount, field$reduce.amount.text, group$percent.to.increase, group$percent.to.increase.text, group$campaign.estimate.metric, group$campaign.estimate.metric.text, field$strength, field$priority)) # opt.text not needed as this table will be merged with increase.reco that already has them
# old mutate that removed NAs
# mutate_(.dots = setNames(c(field$dim, field$dim.text, paste0("sprintf('%1.0f%%',-", field$reduce, " * 100)"), paste0("sprintf('%1.0f%%',", group$percent.to.increase, " * 100)"), paste0("cut(pmax(", last(group$percent.metric), ", na.rm = TRUE), ", fun_c(strengh$values), ", ", fun_c(strengh$labels, prePostChar = "'"),", right = FALSE)"), paste0("as.character(cut(pmax(", paste0(group$campaign.estimate.metric, collapse = ", "), ", na.rm = TRUE), ", fun_c(priority$values), ", ", fun_c(priority$labels),"))"), paste0("ifelse(!is.nan(", group$campaign.estimate.metric, "), sprintf('%1.1f%%',", group$campaign.estimate.metric, " * 100), '0.0%')")), c(field$reduce.dim, field$reduce.text,  field$reduce.amount, group$percent.to.increase, field$strength, field$priority, group$campaign.estimate.metric)))
# Monitor reco
monitor.reco <- rec.temp %>% filter_(.dots = paste0(field$reco, " == '", reco$monitor, "'"))
if(nrow(monitor.reco) > 0) {
    monitor.reco <- monitor.reco %>% mutate_(.dots = setNames(c("''", "''", field$dim, "''", "''",  field$dim.text, "''", "''", "''", priority$monitor, rep("''", times = 2 * length(c(group$percent.to.increase, group$campaign.estimate.metric)))), c(field$increase.dim, field$reduce.dim, field$monitor.dim, field$increase.text, field$reduce.text, field$monitor.text, field$reduce.amount, field$reduce.amount.text, field$strength, field$priority, group$percent.to.increase, group$percent.to.increase.text, group$campaign.estimate.metric, group$campaign.estimate.metric.text))) %>%
    select_(.dots = c(group$opt.text, field$increase.text, field$reduce.text, field$monitor.text, field$reduce.amount.text, group$percent.to.increase.text, field$priority, field$strength, group$campaign.estimate.metric.text, group$opt, field$increase.dim, field$reduce.dim, field$monitor.dim, field$reduce.amount, group$percent.to.increase , group$campaign.estimate.metric))
}
# Combine increase, reduce, format text and add monitor recos
REC.PF <- merge(increase.reco, reduce.reco, by = group$opt, all = TRUE) %>%
  within({assign(field$monitor.dim, ""); assign(field$monitor.text, "")}) %>%
  select_(.dots = c(group$opt.text, field$increase.text, field$reduce.text, field$monitor.text, field$reduce.amount.text, group$percent.to.increase.text, field$priority, field$strength, group$campaign.estimate.metric.text, group$opt, field$increase.dim, field$reduce.dim, field$monitor.dim, field$reduce.amount, group$percent.to.increase , group$campaign.estimate.metric)) %>%
  filter_(.dots = sprintf("!is.na(%1$s)", field$priority)) %>% # Filter recommendations with invalid priorities (negative impact)
  rbind(monitor.reco)
# Remove site level recos within the same site, these could be duplicates of sitetactic when sites (are the sime (site uses sitetactic data, and same site can have more than one, these should show only at sitetactic level)exeption is when site tactic focuses in cross exe_type
REC.PF <- REC.PF %>% filter_(.dots = sprintf("(%1$s != '%2$s') | ((gsub('_.*[^_]$', '', %3$s) != gsub('_.*[^_]$', '', %4$s)) | %5$s != '')", field$type, dimtype$site, field$increase.dim, field$reduce.dim, field$monitor.dim))
# Remove duplicates that may be caused by prio recos.
REC.PF <- REC.PF %>% mutate_(.dots = setNames(c(sprintf("ifelse(%2$s != '', sub('^_|_$', '', paste0(%1$s, '_', %2$s)), '')", field$site, group$reco.dim)), group$site.reco.dim)) %>% # Combine site and dim
 group_by_(.dots = c(group$campaign.month, group$site.reco.dim)) %>% # group by campaign and recos (site and dim combined)
 mutate_(.dots = setNames(sprintf("length(%1$s)", field$type), "c")) %>% # count occurrences
 filter_(.dots = sprintf("%1$s == '%2$s' & c > 1", field$type, dimtype$site)) %>%  # select records to drop, keep sitetactic over site
 anti_join(REC.PF, ., by = c(group$campaign.month, group$reco.dim)) 
# Add recnumbe
REC.PF <- REC.PF %>%
  arrange_(.dots = c(group$opt, paste0("desc(apply(cbind(", paste0(sprintf("as.numeric(%1$s)", group$campaign.estimate.metric), collapse = ", "), "), 1, sum, na.rm = TRUE))"))) %>% # sort for display
  mutate_(.dots = setNames(paste0("sprintf('%1$s_%2$02d_%3$s_%4$s_%5$s', '", year, "', ", field$month, ", ", field$brand, ", ", field$campaign, ", row_number())"), field$recnumber))
  REC.PF <- REC.PF[, c(field$recnumber, setdiff(names(REC.PF), field$recnumber))]  # reorder columns, renumer first

### Old REC.PF with one reco per row
# REC.PF <- merge(increase.reco, reduce.reco, by = group$opt) %>%
#   merge(SUM.PF, by = group$opt) %>%
#   mutate_(.dots = setNames(c(paste0("as.character(cut(pmax(", paste0(group$campaign.estimate.metric, collapse = ", "), ", na.rm = TRUE), ", fun_c(priority$values), ", ", fun_c(priority$labels),"))"), paste0("paste0(REDUCE, ' perform ', ", group$rationale.column, ", ' lower than ', INCREASE)"), paste0("sprintf('%1.1f%%', pmax(", group$campaign.estimate.metric, ", na.rm = TRUE) * 100)")), c("PRIORITY", group$rationale.text, group$campaign.estimate.metric))) %>%
#   within({RECOMMENDATON <- paste0("Increase ", INCREASE, " and reduce ", REDUCE)
#           SHIFT <- paste0(REDUCE_AMMOUNT, " from ", REDUCE, " to ", INCREASE)}) %>%
#   select_(.dots = c(group$opt, "RECOMMENDATON", group$rationale.text, "PRIORITY", "STRENGTH",  group$campaign.estimate.metric, "SHIFT")) %>%
#   rbind(monitor.reco) %>% 
#   arrange_(.dots = c(group$opt, last(group$campaign.estimate.metric)))

### STEP 7: OUTPUT RESULTS ####
# output files for review
# 1. Recommendation results
# 2. Chart data
# 3. Prior reco data (for next cycle)
# 4. Main data frame to save for recording purposes
write.csv(REC.PF, file.path(dir.data$output, paste0("PF_Recommendations_", month, "_", format(Sys.time(), "%Y%m%d-%H%M%S") , ".csv")), row.names = FALSE)
write.csv(CHT.PF, file.path(dir.data$output, paste0("PF_Chart_", month, "_", format(Sys.time(), "%Y%m%d-%H%M%S") , ".csv")), row.names = FALSE)

# Generate Prior reco table
PF.AGG %>% group_by_(.dots = group$dim) %>% select_(.dots = c(group$dim, field$reco)) %>%
  filter_(.dots = paste0(field$reco, " != '", reco$exclude, "'")) %>%
  rename_(.dots = setNames(c(field$month, field$reco), c(field$prior.month, field$prior.reco.blend))) %>%
  write.csv(file.path(dir.data$output, paste0("prior_reco_", month, "_", format(Sys.time(), "%Y%m%d-%H%M%S") , ".csv")), row.names = FALSE)

# Main data frame to save for recording purposes and for review
  PF.AGG %>% write.csv(file.path(dir.data$output, paste0("PF_Summary_Data_", month, "_", format(Sys.time(), "%Y%m%d-%H%M%S"), ".csv")), row.names = FALSE)

if (FALSE){ #  run manually to generate cross table of reco differences
  PF.AGG %>% select_(.dots = head(group$reco.metric,-1)) %>%
          lapply(X = ., FUN = function(x){xf <- as.factor(x)
                               levels(xf) <- c("REDUCE", "INCREASE", "REDUCE", "REDUCE", "NONE")
                               as.character(xf)}) %>%
          do.call(cbind, args = .) %>% as.data.frame %>%
    table
  REC.PF %>% select_(.dots = c(group$campaign.estimate.metric, field$month)) %>%
         lapply(X = ., FUN = function(x) {sign(as.numeric(gsub("%", "", x)))}) %>%
         do.call(cbind, args = .) %>% as.data.frame %>% 
    table 
}