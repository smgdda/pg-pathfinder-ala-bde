import sys, os, subprocess
import datetime
import time

def main():
  mbd_imp_process = mbd_imp_meta_process()
  mbd_imp_process.run()
  

class mbd_imp_meta_process:

  def __init__(self):
    print("mbd_imp_meta_process class init...")

  def run(self):
    print("starting mbd_imp_meta_process.run()...")

    self.run_hive_script('STEP-003-populate-prepost2014_mbd_user_imps_matched.sql', 'wetjet')
    self.run_hive_script('STEP-003-populate-prepost2014_mbd_user_imps_matched.sql', 'wygs')
    self.run_hive_script('STEP-003-populate-prepost2014_mbd_user_imps_matched.sql', 'wellness')
    self.run_hive_script('STEP-003-populate-prepost2014_mbd_user_imps_matched.sql', 'regimen')
    self.run_hive_script('STEP-003-populate-prepost2014_mbd_user_imps_matched.sql', 'discreet')
    self.run_hive_script('STEP-003-populate-prepost2014_mbd_user_imps_matched.sql', 'alwayson')
    self.run_hive_script('STEP-003-populate-prepost2014_mbd_user_imps_matched.sql', 'weeksestimator')

    print('completed mbd_imp_meta_process.run()...')

  def run_hive_script(self, sql_script_file, pf_campaign_name):
    print("starting mbd_imp_meta_process.run_hive_script(" + sql_script_file + ", " + pf_campaign_name + ")...")

    process = subprocess.Popen(["hive", "-f", sql_script_file, "-hiveconf", "pf_campaign_name=" + pf_campaign_name], stdout=subprocess.PIPE)
    data = process.communicate()[0]
    
    print("completed mbd_imp_meta_process.run_hive_script(" + sql_script_file + ")...\n\n")


if __name__ == "__main__":
  main()