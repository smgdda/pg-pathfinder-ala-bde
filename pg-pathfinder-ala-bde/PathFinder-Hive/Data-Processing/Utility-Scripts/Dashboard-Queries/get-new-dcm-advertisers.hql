﻿select
  advertiser_id
  , advertiser
  , ingestion_dt
  , spot_id
from
  dfa_meta_advertiser
where
  datediff(ingestion_dt, TO_DATE( FROM_UNIXTIME( UNIX_TIMESTAMP() ))) >= -90
order by
  datediff(ingestion_dt, TO_DATE( FROM_UNIXTIME( UNIX_TIMESTAMP() )))
limit 1000