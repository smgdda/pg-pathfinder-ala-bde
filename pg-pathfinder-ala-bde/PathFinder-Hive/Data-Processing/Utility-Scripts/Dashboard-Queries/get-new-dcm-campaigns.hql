﻿select
  dfa_meta_campaign.ingestion_dt
  , dfa_meta_campaign.buy_id
  , dfa_meta_campaign.buy
  , dfa_meta_campaign.advertiser_id
  , adv.advertiser
  , adv.advertiser_group
  , adv.advertiser_group_id
  , datediff(dfa_meta_campaign.ingestion_dt, TO_DATE( FROM_UNIXTIME( UNIX_TIMESTAMP() ))) as date_diff
from
  dfa_meta_campaign
  left outer join dfa_meta_advertiser adv on adv.advertiser_id = dfa_meta_campaign.advertiser_id
  join pf_advertisers on pf_advertisers.advertiser_id = adv.advertiser_id and pf_advertisers.in_pathfinder = 1
where
  datediff(dfa_meta_campaign.ingestion_dt, TO_DATE( FROM_UNIXTIME( UNIX_TIMESTAMP() ))) >= -30
order by
  advertiser
  , date_diff
  , buy
LIMIT 1500