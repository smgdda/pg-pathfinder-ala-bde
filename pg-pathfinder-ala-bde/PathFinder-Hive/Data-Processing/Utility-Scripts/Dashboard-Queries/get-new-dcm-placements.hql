﻿select
  adv.advertiser
  , dfa_meta_campaign.buy_id
  , dfa_meta_campaign.buy
  , dfa_meta_page.page_id
  , dfa_meta_page.site_placement
  , dfa_meta_page.ingestion_dt
  , dfa_meta_page.start_dt
  , dfa_meta_page.end_dt
from
  dfa_meta_page
  join dfa_meta_campaign on dfa_meta_campaign.buy_id = dfa_meta_page.buy_id
  join dfa_meta_advertiser adv on adv.advertiser_id = dfa_meta_campaign.advertiser_id
  join pf_advertisers on pf_advertisers.advertiser_id = adv.advertiser_id and pf_advertisers.in_pathfinder = 1
where
  datediff(dfa_meta_page.ingestion_dt, TO_DATE( FROM_UNIXTIME( UNIX_TIMESTAMP() ))) >= -30
order by
  advertiser
  , buy
  , site_placement
limit 1500