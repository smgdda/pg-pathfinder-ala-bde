select
	campaignid
	, siteid
	, placementid
	, creativeid
	, sum(impressions) as imp_sum
	, sum(clicks) as click_sum
	, sum(mediacost) as cost_sum

from
	agency_views_dcm.factimpressionsclickscost

join
	agency_views_dcm.dimadvertiser 
	on (
		agency_views_dcm.dimadvertiser.networkid = 5767
		and agency_views_dcm.dimadvertiser.advertiserid = agency_views_dcm.factimpressionsclickscost.advertiserid
	)

where
	agency_views_dcm.factimpressionsclickscost.dayseqnum >= TO_CHAR(date(current_date - cast('17 days' as interval)), 'YYYYMMDD')
--	and agency_views_dcm.factimpressionsclickscost.dayseqnum <= TO_CHAR(date(current_date - cast('1 days' as interval)), 'YYYYMMDD')
	and agency_views_dcm.factimpressionsclickscost.dayseqnum <= TO_CHAR(date(current_date), 'YYYYMMDD')

group by
	campaignid
	, siteid
	, placementid
	, creativeid

