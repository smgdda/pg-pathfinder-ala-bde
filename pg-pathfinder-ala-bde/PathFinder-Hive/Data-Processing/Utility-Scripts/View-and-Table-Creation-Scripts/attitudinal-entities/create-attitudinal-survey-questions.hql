﻿DROP TABLE IF EXISTS attitudinal_survey_questions;
CREATE EXTERNAL TABLE attitudinal_survey_questions
(
  survey_id BIGINT
  , survey_name STRING
  , question_id BIGINT
  , question_type INT
  , ordinal_position INT
  , is_required STRING
  , skips STRING
  , skip_unless STRING
  , question_text STRING
)
ROW FORMAT DELIMITED 
  FIELDS TERMINATED BY '\t' 
STORED AS INPUTFORMAT 
  'org.apache.hadoop.mapred.TextInputFormat' 
OUTPUTFORMAT 
  'org.apache.hadoop.hive.ql.io.HiveIgnoreKeyTextOutputFormat'
LOCATION
  'hdfs://vmcwy62059.prod.acxiom.net:8020/user/owatson/mbd_survey_data_via_api/attitudinal_survey_questions'
;