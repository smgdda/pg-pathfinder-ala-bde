﻿DROP TABLE IF EXISTS attitudinal_survey_responses;
CREATE EXTERNAL TABLE attitudinal_survey_responses
(
  question_id BIGINT
  , question_number STRING
  , respondent_id BIGINT
  , user_response_col_idx INT
  , user_response_col_text STRING
  , user_response_row_idx INT
  , user_response_row_text STRING
  , user_response_idx INT
  , user_response_text STRING
  , status INT
  , startTime STRING
  , completeTime STRING
)
ROW FORMAT DELIMITED 
  FIELDS TERMINATED BY '\t' 
STORED AS INPUTFORMAT 
  'org.apache.hadoop.mapred.TextInputFormat' 
OUTPUTFORMAT 
  'org.apache.hadoop.hive.ql.io.HiveIgnoreKeyTextOutputFormat'
LOCATION
  'hdfs://vmcwy62059.prod.acxiom.net:8020/user/owatson/mbd_survey_data_via_api/attitudinal_survey_responses'
;