﻿DROP TABLE IF EXISTS attitudinal_surveys;
CREATE EXTERNAL TABLE attitudinal_surveys
(
  survey_id BIGINT
  , survey_name STRING
  , survey_access_code STRING
  , survey_status INT
  , survey_start_date STRING
  , survey_complete_date STRING
  , survey_preview_url STRING
  , mbd_campaign_id BIGINT
  , mbd_campaign_name STRING
)
ROW FORMAT DELIMITED 
  FIELDS TERMINATED BY '\t' 
STORED AS INPUTFORMAT 
  'org.apache.hadoop.mapred.TextInputFormat' 
OUTPUTFORMAT 
  'org.apache.hadoop.hive.ql.io.HiveIgnoreKeyTextOutputFormat'
LOCATION
  'hdfs://vmcwy62059.prod.acxiom.net:8020/user/owatson/mbd_survey_data_via_api/attitudinal_surveys'
;