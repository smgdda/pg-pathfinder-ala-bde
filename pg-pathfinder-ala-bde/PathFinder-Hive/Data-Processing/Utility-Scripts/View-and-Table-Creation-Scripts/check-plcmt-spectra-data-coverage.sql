
-- TODO:  UPDATE TO USE spectra_mbox_rates instead of pf_plcmt_spectra_cpm

select
woc_plcmts.buy_id
, woc_plcmts.site_placement
, woc_plcmts.site_id
, woc_plcmts.page_id
, dfa_meta_page.pricing_type as dcm_rate_type
, dfa_meta_page.purchase_cost as dcm_cost
, dfa_meta_page.purchase_quantity as dcm_qty
, pf_plcmt_spectra_cpm.spectra_rate_type as spectra_rate_type
, pf_plcmt_spectra_cpm.spectra_rate as spectra_rate
, pf_plcmt_spectra_cpm.spectra_calculated_cpm as spectra_calculated_cpm

from

(
  select distinct
  pf_daily_impressions_per_placement.buy_id
  , pf_daily_impressions_per_placement.site_placement
  , pf_daily_impressions_per_placement.site_id
  , pf_daily_impressions_per_placement.page_id
  from
  pf_daily_impressions_per_placement
) woc_plcmts

join
dfa_meta_page
on (
  dfa_meta_page.buy_id = woc_plcmts.buy_id
  and dfa_meta_page.site_id = woc_plcmts.site_id
  and dfa_meta_page.page_id = woc_plcmts.page_id
  )

left outer join pf_plcmt_spectra_cpm
on
(
  pf_plcmt_spectra_cpm.buy_id = dfa_meta_page.buy_id
  and pf_plcmt_spectra_cpm.site_id = dfa_meta_page.site_id
  and pf_plcmt_spectra_cpm.page_id = dfa_meta_page.page_id
  )

;
