DROP TABLE IF EXISTS dcm_spectra_merge_agg_w_innovid_manual_update;
CREATE EXTERNAL TABLE dcm_spectra_merge_agg_w_innovid_manual_update
(
  brand string, 
  pfcampaign string, 
  site_id string, 
  site string, 
  siteadsize string, 
  sitecreative string, 
  sitetactic string, 
  ad_size string, 
  tactic string, 
  page_id string, 
  placement string, 
  exe_type string, 
  creative string, 
  dcm_rate_type string, 
  dcm_rate string, 
  dcm_qty string, 
  spectra_rate_type string, 
  spectra_rate string, 
  calculated_cpm string, 
  pfscore_plcmt double, 
  impressions double, 
  click_ct double, 
  activity_ct double, 
  calculated_actual_cost double, 
  ss_imp_sum double, 
  ss_click_sum double, 
  ss_cost_sum double
)

ROW FORMAT DELIMITED 
  FIELDS TERMINATED BY '\t' 
STORED AS INPUTFORMAT 
  'org.apache.hadoop.mapred.TextInputFormat' 
OUTPUTFORMAT 
  'org.apache.hadoop.hive.ql.io.HiveIgnoreKeyTextOutputFormat'
LOCATION
  'hdfs://vmcwy62059.prod.acxiom.net:8020/user/owatson/external-table-data/dcm_spectra_merge_agg_w_innovid-MANUAL-UPDATE'
;