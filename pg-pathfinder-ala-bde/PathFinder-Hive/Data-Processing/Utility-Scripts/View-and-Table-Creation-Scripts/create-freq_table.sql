CREATE TABLE freq_table
(
  pfcampaign string, 
  sitetactic string, 
  camp_tactic string, 
  freq bigint, 
  total_users bigint, 
  conversions bigint, 
  conv_rt double, 
  processing_date string
)
ROW FORMAT SERDE 
  'org.apache.hadoop.hive.serde2.columnar.ColumnarSerDe' 
STORED AS INPUTFORMAT 
  'org.apache.hadoop.hive.ql.io.RCFileInputFormat' 
OUTPUTFORMAT 
  'org.apache.hadoop.hive.ql.io.RCFileOutputFormat'
;
