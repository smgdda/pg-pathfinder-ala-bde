CREATE EXTERNAL TABLE ingestion_flite_lookup(
  ad_name string, 
  ad_id string)
ROW FORMAT DELIMITED 
  FIELDS TERMINATED BY ',' 
STORED AS INPUTFORMAT 
  'org.apache.hadoop.mapred.TextInputFormat' 
OUTPUTFORMAT 
  'org.apache.hadoop.hive.ql.io.HiveIgnoreKeyTextOutputFormat'
LOCATION
  'hdfs://vmcwy62059.prod.acxiom.net:8020//user/acxm_bdp/ingestion/smg/flite_lookup/landing/2015/02/23/'
;
