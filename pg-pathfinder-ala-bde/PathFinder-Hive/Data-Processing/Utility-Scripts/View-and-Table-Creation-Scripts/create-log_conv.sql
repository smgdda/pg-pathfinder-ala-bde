DROP TABLE IF EXISTS log_conv;

CREATE  TABLE log_conv(
  event_dts timestamp, 
  dfa_user_id string, 
  dlx_encrypted_dfa_user_id string, 
  brand string, 
  site string, 
  placement string, 
  page_id bigint, 
  creative_name string, 
  creative_id bigint, 
  ui_creative_id bigint, 
  tactic string, 
  sitetactic string, 
  siteadsize string, 
  sitecreative string, 
  exe_type string, 
  vendor_code string, 
  sfg_type string, 
  targeting_type string, 
  content_genre string, 
  ad_size string, 
  creative string, 
  current_date string, 
  sixty_days_ago string, 
  site_id bigint,
  dcm_rate_type string,
  dcm_rate float,
  dcm_qty bigint,
  freq bigint, 
  conv_user int, 
  t2c double)
PARTITIONED BY ( 
  pfcampaign string)
ROW FORMAT SERDE 
  'org.apache.hadoop.hive.serde2.columnar.ColumnarSerDe' 
STORED AS INPUTFORMAT 
  'org.apache.hadoop.hive.ql.io.RCFileInputFormat' 
OUTPUTFORMAT 
  'org.apache.hadoop.hive.ql.io.RCFileOutputFormat'
;
