DROP TABLE IF EXISTS logs_ordinal;
CREATE  TABLE logs_ordinal(
  sitetactic string, 
  dfa_user_id string, 
  dlx_encrypted_dfa_user_id string, 
  event_dts timestamp, 
  brand string, 
  site string, 
  placement string, 
  page_id bigint, 
  creative_name string, 
  creative_id bigint, 
  ui_creative_id bigint, 
  tactic string, 
  siteadsize string, 
  sitecreative string, 
  exe_type string, 
  vendor_code string, 
  sfg_type string, 
  targeting_type string, 
  content_genre string, 
  ad_size string, 
  creative string, 
  current_date string, 
  sixty_days_ago string, 
  site_id bigint,
  dcm_rate_type string,
  dcm_rate float,
  dcm_qty bigint,
  p_rank bigint)
PARTITIONED BY ( 
  pfcampaign string)
ROW FORMAT SERDE 
  'org.apache.hadoop.hive.serde2.columnar.ColumnarSerDe' 
STORED AS INPUTFORMAT 
  'org.apache.hadoop.hive.ql.io.RCFileInputFormat' 
OUTPUTFORMAT 
  'org.apache.hadoop.hive.ql.io.RCFileOutputFormat'
;
