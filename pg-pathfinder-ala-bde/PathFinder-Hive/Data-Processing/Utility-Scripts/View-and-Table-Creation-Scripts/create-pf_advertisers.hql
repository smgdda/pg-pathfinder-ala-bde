﻿CREATE EXTERNAL TABLE pf_advertisers 
(
  advertiser_id bigint, 
  advertiser string, 
  brand_ingestion_dt string, 
  in_PathFinder int, 
  in_add_daily_getfile_triggers int, 
  in_datadepot_clientdata int
)
ROW FORMAT DELIMITED 
  FIELDS TERMINATED BY '\t' 
STORED AS INPUTFORMAT 
  'org.apache.hadoop.mapred.TextInputFormat' 
OUTPUTFORMAT 
  'org.apache.hadoop.hive.ql.io.HiveIgnoreKeyTextOutputFormat'
LOCATION
  'hdfs://vmcwy62059.prod.acxiom.net:8020/user/owatson/pf_advertisers'

;