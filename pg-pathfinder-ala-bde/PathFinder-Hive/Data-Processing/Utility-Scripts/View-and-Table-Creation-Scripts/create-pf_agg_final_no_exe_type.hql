﻿DROP TABLE IF EXISTS pf_agg_final_no_exe_type;

CREATE TABLE pf_agg_final_no_exe_type
(
  brand string, 
  pfcampaign string, 
  rate_type string, 
  dimtype string, 
  dim string, 
  imps bigint, 
  cost double, 
  ss_cost double, 
  pfscore double, 
  scaled_imps double, 
  scaled_cost double, 
  scaled_ss_cost double, 
  scaled_pfscore double, 
  impact double, 
  efficiency double, 
  actual_efficiency double
)
PARTITIONED BY
(
  woc_spec string
)
ROW FORMAT SERDE 
  'org.apache.hadoop.hive.serde2.columnar.ColumnarSerDe' 
STORED AS INPUTFORMAT 
  'org.apache.hadoop.hive.ql.io.RCFileInputFormat' 
OUTPUTFORMAT 
  'org.apache.hadoop.hive.ql.io.RCFileOutputFormat'
;
