﻿DROP TABLE IF EXISTS pf_analysis_window;

CREATE EXTERNAL TABLE pf_analysis_window (
  woc_year int, 
  woc_month int, 
  analysis_start_dt string, 
  analysis_end_dt string
)

ROW FORMAT DELIMITED 
  FIELDS TERMINATED BY '\t'

STORED AS INPUTFORMAT 
  'org.apache.hadoop.mapred.TextInputFormat'

OUTPUTFORMAT 
  'org.apache.hadoop.hive.ql.io.HiveIgnoreKeyTextOutputFormat'

LOCATION
  'hdfs://vmcwy62059.prod.acxiom.net:8020/user/owatson/pf_analysis_window'

;