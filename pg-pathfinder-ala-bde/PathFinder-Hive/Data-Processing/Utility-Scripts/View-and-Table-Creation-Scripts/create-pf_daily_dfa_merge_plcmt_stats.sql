﻿DROP TABLE IF EXISTS pf_daily_dfa_merge_plcmt_stats;

CREATE TABLE pf_daily_dfa_merge_plcmt_stats
(
  buy_id bigint
  , site_id bigint
--  , site string
  , page_id bigint
--  , site_placement string
--  , dcm_rate_type string
--  , dcm_rate float
--  , dcm_qty bigint
  , creative_id bigint
--  , creative_name string
--  , ui_creative_id bigint
--  , site_data string

  , total_imps bigint
  , unusable_imps bigint
  , total_users bigint

  , total_clicks bigint
  , unusable_clicks bigint

  , total_activities bigint
  , unusable_activities bigint

--  , sitetactic string
--  , siteadsize string
--  , sitecreative string
)
PARTITIONED BY 
( 
  event_dt string
)
ROW FORMAT SERDE 
  'org.apache.hadoop.hive.serde2.columnar.ColumnarSerDe' 
STORED AS INPUTFORMAT 
  'org.apache.hadoop.hive.ql.io.RCFileInputFormat' 
OUTPUTFORMAT 
  'org.apache.hadoop.hive.ql.io.RCFileOutputFormat'
;
