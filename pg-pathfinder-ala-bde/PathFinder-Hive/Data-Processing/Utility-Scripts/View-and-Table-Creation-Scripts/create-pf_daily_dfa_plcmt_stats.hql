﻿CREATE  TABLE pf_daily_dfa_plcmt_stats
(
  buy_id bigint
  , site_id bigint
  , page_id bigint
  , creative_id bigint
  , imp_ct bigint
  , unusable_imp_ct bigint
  , user_ct bigint
  , click_ct bigint
  , unusable_click_ct bigint
  , activity_ct bigint
  , unusable_activity_ct bigint
)
PARTITIONED BY
( 
  event_dt string
)
ROW FORMAT SERDE 
  'org.apache.hadoop.hive.serde2.columnar.ColumnarSerDe' 
STORED AS INPUTFORMAT 
  'org.apache.hadoop.hive.ql.io.RCFileInputFormat' 
OUTPUTFORMAT 
  'org.apache.hadoop.hive.ql.io.RCFileOutputFormat'
;