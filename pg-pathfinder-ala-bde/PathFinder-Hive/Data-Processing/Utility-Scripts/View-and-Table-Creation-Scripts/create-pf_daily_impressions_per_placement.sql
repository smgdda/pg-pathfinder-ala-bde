﻿CREATE  TABLE pf_daily_impressions_per_placement
(
  buy_id bigint, 
  site_id bigint, 
  site string, 
  page_id bigint, 
  site_placement string, 
  creative_id bigint, 
  creative_name string, 
  creative string, 
  ui_creative_id bigint, 
  site_data string, 
  sitetactic string, 
  siteadsize string, 
  sitecreative string, 
  impression_ct bigint
)
PARTITIONED BY ( 
  event_dt string)
ROW FORMAT SERDE 
  'org.apache.hadoop.hive.serde2.columnar.ColumnarSerDe' 
STORED AS INPUTFORMAT 
  'org.apache.hadoop.hive.ql.io.RCFileInputFormat' 
OUTPUTFORMAT 
  'org.apache.hadoop.hive.ql.io.RCFileOutputFormat'
;
