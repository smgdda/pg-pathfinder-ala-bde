﻿DROP TABLE IF EXISTS pf_reccos;
create external table pf_reccos
(
recnum string
, month int
, brand_text string
, pfcampaign_text string
, dimtype_text string
, exe_type_text string
, site_type string
, site_text string
, increase_text string
, reduce_text string
, monitor_text string
, reduce_amount_text string
, percent_to_inc_eff_bhv_score_text string
, percent_to_inc_eff_att_score_text string
, percent_to_inc_eff_oss_score_text string
, priority int
, strengh string
, campaign_est_bhv_score_text string
, campaign_est_att_score_text string
, campaign_est_oss_score_text string
, brand string
, pfcampaign string
, dimtype string
, exe_type string
, dim_site string
, increase_dim string
, reduce_dim string
, monitor string
, reduce_amount double
, percent_to_inc_eff_bhv_score double
, percent_to_inc_eff_att_score double
, percent_to_inc_eff_oss_score double
, campaign_est_bhv_score double
, campaign_est_att_score double
, campaign_est_oss_score double
)

ROW FORMAT DELIMITED 
  FIELDS TERMINATED BY '\t' 
STORED AS INPUTFORMAT 
  'org.apache.hadoop.mapred.TextInputFormat' 
OUTPUTFORMAT 
  'org.apache.hadoop.hive.ql.io.HiveIgnoreKeyTextOutputFormat'
LOCATION
  'hdfs://vmcwy62059.prod.acxiom.net:8020/user/owatson/pf_reccos'
