DROP TABLE IF EXISTS pf_spectra_placements;

CREATE EXTERNAL TABLE pf_spectra_placements(
  adserver_name string, 
  adserver_network_id string, 
  adserver_campaign_id string, 
  adserver_placement_id string, 
  adserver_site_id string, 
  campaign_id string, 
  campaign_name string, 
  campaign_reporting_type string, 
  digital_type string, 
  client_id string, 
  placement_id string, 
  placement_name string, 
  placement_start_date string, 
  placement_end_date string, 
  placement_rate_type string, 
  grp_rate_type string, 
  rate string, 
  grp_rate string, 
  placement_reporting_type string, 
  placement_type string, 
  product_id string, 
  product_name string, 
  group_name string, 
  vendor_id string, 
  ordered_net string, 
  grp_ordered_net string, 
  units string, 
  grp_units string, 
  actual_impressions string,
  grp_actual_impressions string
  )
ROW FORMAT DELIMITED 
  FIELDS TERMINATED BY '\t' 
STORED AS INPUTFORMAT 
  'org.apache.hadoop.mapred.TextInputFormat' 
OUTPUTFORMAT 
  'org.apache.hadoop.hive.ql.io.HiveIgnoreKeyTextOutputFormat'
LOCATION
  'hdfs://vmcwy62059.prod.acxiom.net:8020/user/owatson/spectra_via_mbox'
;