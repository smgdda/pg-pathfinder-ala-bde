DROP TABLE IF EXISTS pf_spectra_placements_manual_updates;

CREATE EXTERNAL TABLE pf_spectra_placements_manual_updates(
  buy_id string, 
  site_id string, 
  page_id string, 
  site_placement string, 
  manual_placement_rate_type string, 
  manual_rate string
)

ROW FORMAT DELIMITED 
  FIELDS TERMINATED BY '\t' 
STORED AS INPUTFORMAT 
  'org.apache.hadoop.mapred.TextInputFormat' 
OUTPUTFORMAT 
  'org.apache.hadoop.hive.ql.io.HiveIgnoreKeyTextOutputFormat'
LOCATION
  'hdfs://vmcwy62059.prod.acxiom.net:8020/user/owatson/spectra_via_mbox_manual_updates'
;