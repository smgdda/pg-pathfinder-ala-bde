CREATE EXTERNAL TABLE pfcampaign_list
(
  brand string, 
  pfcampaign string, 
  channel string, 
  campaign_name string, 
  buy_id bigint, 
  dlx_campaign_name string, 
  date_dropped string, 
  date_added string, 
  status_notes string
  )
ROW FORMAT DELIMITED 
  FIELDS TERMINATED BY '\t' 
STORED AS INPUTFORMAT 
  'org.apache.hadoop.mapred.TextInputFormat' 
OUTPUTFORMAT 
  'org.apache.hadoop.hive.ql.io.HiveIgnoreKeyTextOutputFormat'
LOCATION
  'hdfs://vmcwy62059.prod.acxiom.net:8020/user/owatson/pfcampaign_list'
;
