﻿CREATE TABLE plcmt_delivery
(
  brand string, 
  pfcampaign string, 
  site string, 
  sitetactic string, 
  sitecreative string, 
  siteadsize string, 
  tactic string, 
  creative_id bigint, 
  creative string, 
  ad_size string, 
  placement string, 
  creative_name string, 
  page_id bigint, 
  site_id bigint, 
  dcm_rate_type string, 
  dcm_rate float, 
  dcm_qty bigint, 
  impressions bigint, 
  spectra_matchkey string
)
ROW FORMAT SERDE 
  'org.apache.hadoop.hive.serde2.columnar.ColumnarSerDe' 
STORED AS INPUTFORMAT 
  'org.apache.hadoop.hive.ql.io.RCFileInputFormat' 
OUTPUTFORMAT 
  'org.apache.hadoop.hive.ql.io.RCFileOutputFormat'
;
