CREATE  TABLE plcmt_pfscore(
  brand string, 
  pfcampaign string, 
  site string, 
  sitecreative string, 
  sitetactic string, 
  siteadsize string, 
  creative string, 
  creative_id bigint, 
  ui_creative_id bigint, 
  tactic string, 
  ad_size string, 
  placement string, 
  exe_type string, 
  page_id bigint, 
  creative_name string, 
  site_id bigint,
  dcm_rate_type string,
  dcm_rate float,
  dcm_qty bigint,
  pfscore_plcmt double)
ROW FORMAT SERDE 
  'org.apache.hadoop.hive.serde2.columnar.ColumnarSerDe' 
STORED AS INPUTFORMAT 
  'org.apache.hadoop.hive.ql.io.RCFileInputFormat' 
OUTPUTFORMAT 
  'org.apache.hadoop.hive.ql.io.RCFileOutputFormat'
;
