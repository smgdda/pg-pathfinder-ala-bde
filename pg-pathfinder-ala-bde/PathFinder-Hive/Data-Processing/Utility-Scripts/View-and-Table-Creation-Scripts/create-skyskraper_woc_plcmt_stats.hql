﻿CREATE EXTERNAL TABLE skyskraper_woc_plcmt_stats(
  campaignid bigint, 
  siteid bigint, 
  placementid bigint, 
  creativeid bigint, 
  imp_sum bigint, 
  click_sum bigint,
  cost_sum float
  )
ROW FORMAT DELIMITED 
  FIELDS TERMINATED BY '\t' 
STORED AS INPUTFORMAT 
  'org.apache.hadoop.mapred.TextInputFormat' 
OUTPUTFORMAT 
  'org.apache.hadoop.hive.ql.io.HiveIgnoreKeyTextOutputFormat'
LOCATION
  'hdfs://vmcwy62059.prod.acxiom.net:8020/user/owatson/skyskraper-woc-plcmt-stats'
;