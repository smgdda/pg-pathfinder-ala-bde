DROP TABLE IF EXISTS spectra_plcmt_mbox_rates;
CREATE TABLE spectra_plcmt_mbox_rates AS

SELECT DISTINCT
lower(campaign_name) as campaign_name
, adserver_campaign_id
, adserver_site_id
, adserver_placement_id
, split(regexp_replace(lower(case when pf_spectra_placements_manual_updates.site_placement is not null then pf_spectra_placements_manual_updates.site_placement else placement_name end), '[^a-z0-9_\\|]', ''), '\\|')[0] as placement_name
, placement_start_date
, placement_end_date
, lower(case when manual_placement_rate_type is not null then manual_placement_rate_type when grp_rate_type != '' then grp_rate_type else placement_rate_type end) as placement_rate_type
, case when manual_rate is not null then manual_rate when grp_rate != '' then grp_rate else rate end as rate
, case when grp_ordered_net != '' then grp_ordered_net else ordered_net end as ordered_net
, case when grp_units != '' then grp_units else units end as units
, case when grp_actual_impressions != '' then grp_actual_impressions else actual_impressions end as spectra_imps
, case
    when lower(manual_placement_rate_type) = 'flatrateimpressions' 
    and manual_rate >= 0 then
    
      cast(manual_rate as double)

    when lower(case
               when grp_rate_type != '' then grp_rate_type
               else placement_rate_type 
               end) in ('flat rate impressions', 'flatrateimpressions')
    and case when grp_units != '' then grp_units else units end > 0 
    and manual_placement_rate_type is null then
    
      (case when grp_ordered_net != '' then grp_ordered_net else ordered_net end * 1000) / case when grp_units != '' then grp_units else units end
    
    when lower(case 
               when manual_placement_rate_type is not null then manual_placement_rate_type
               when grp_rate_type != '' then grp_rate_type
               else placement_rate_type 
               end) = 'cpc' 
    and (case when manual_rate is not null then manual_rate when grp_rate != '' then grp_rate else rate end) >= 0 then
    
      case 
        when manual_rate is not null then cast(manual_rate as double)
        when grp_rate != '' then cast(grp_rate as double) 
        else cast(rate as double)
      end
    
    when lower(case 
               when manual_placement_rate_type is not null then manual_placement_rate_type
               when grp_rate_type != '' then grp_rate_type
               else placement_rate_type 
               end) = 'cpcv' 
    and (case when manual_rate is not null then manual_rate when grp_rate != '' then grp_rate else rate end) >= 0 then
    
      case 
        when manual_rate is not null then cast(manual_rate as double)
        when grp_rate != '' then cast(grp_rate as double) 
        else cast(rate as double)
      end
    
    when lower(case 
               when manual_placement_rate_type is not null then manual_placement_rate_type
               when grp_rate_type != '' then grp_rate_type
               else placement_rate_type 
               end) = 'cpa' 
    and (case when manual_rate is not null then manual_rate when grp_rate != '' then grp_rate else rate end) >= 0 then
    
      case 
        when manual_rate is not null then cast(manual_rate as double)
        when grp_rate != '' then cast(grp_rate as double) 
        else cast(rate as double)
      end
    
    when lower(case 
               when manual_placement_rate_type is not null then manual_placement_rate_type
               when grp_rate_type != '' then grp_rate_type
               else placement_rate_type 
               end) = 'cpm' 
    and (case when manual_rate is not null then manual_rate when grp_rate != '' then grp_rate else rate end) >= 0 then
    
      cast(case 
             when manual_rate is not null then manual_rate 
             when grp_rate != '' then grp_rate 
             else rate 
           end as double)
      
    else 
    
      null
      
  end as calculated_cpm

FROM
pf_spectra_placements

LEFT OUTER JOIN
pf_spectra_placements_manual_updates
on (
  pf_spectra_placements_manual_updates.buy_id = pf_spectra_placements.adserver_campaign_id
  and pf_spectra_placements_manual_updates.page_id = pf_spectra_placements.adserver_placement_id
  )

WHERE
lower(pf_spectra_placements.adserver_name) != 'adserver_name'
;
