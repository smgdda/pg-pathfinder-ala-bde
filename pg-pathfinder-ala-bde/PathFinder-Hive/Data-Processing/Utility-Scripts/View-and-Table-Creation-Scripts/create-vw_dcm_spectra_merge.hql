drop view if exists vw_dcm_spectra_merge;
create view vw_dcm_spectra_merge
as
select
*
from
(
select distinct
plcmt_delivery.brand
, plcmt_delivery.pfcampaign
, plcmt_delivery.site_id
, plcmt_delivery.site
, plcmt_delivery.siteadsize
, plcmt_delivery.sitecreative
, plcmt_delivery.sitetactic
, plcmt_delivery.ad_size
, plcmt_delivery.tactic
, plcmt_delivery.page_id
, plcmt_delivery.placement
, plcmt_pfscore.exe_type
, plcmt_delivery.creative_id
, plcmt_delivery.creative
, plcmt_delivery.creative_name
, regexp_replace(lower(plcmt_delivery.dcm_rate_type), '[^a-zA-Z0-9]', '') as dcm_rate_type
, plcmt_delivery.dcm_rate
, plcmt_delivery.dcm_qty
, regexp_replace(lower(spectra_plcmt_mbox_rates.placement_rate_type), '[^a-zA-Z0-9]', '') as spectra_rate_type
, spectra_plcmt_mbox_rates.rate as spectra_rate
, spectra_plcmt_mbox_rates.calculated_cpm
, pfscore_plcmt
, plcmt_delivery.impressions
, case when cpc_plcmt_clicks.click_ct is not null then cpc_plcmt_clicks.click_ct else cast(0 as BIGINT) end as click_ct
, case when cpa_plcmt_activities.activity_ct is not null then cpa_plcmt_activities.activity_ct else cast(0 as BIGINT) end as activity_ct
, CASE
  WHEN (regexp_replace(lower(case
    when spectra_plcmt_mbox_rates.placement_rate_type is not null then spectra_plcmt_mbox_rates.placement_rate_type
    else plcmt_delivery.dcm_rate_type
    end), '[^a-zA-Z0-9]', '') in ('cpm', 'flatrateimpressions')) THEN

    ((case 
    when spectra_plcmt_mbox_rates.calculated_cpm is not null 
    then cast(spectra_plcmt_mbox_rates.calculated_cpm as float) 
    else plcmt_delivery.dcm_rate 
    end * plcmt_delivery.impressions)/1000)

  WHEN (regexp_replace(lower(case
    when spectra_plcmt_mbox_rates.placement_rate_type is not null then spectra_plcmt_mbox_rates.placement_rate_type
    else plcmt_delivery.dcm_rate_type
    end), '[^a-zA-Z0-9]', '') = 'cpc') THEN

    cast((case 
    when spectra_plcmt_mbox_rates.calculated_cpm is not null 
    then cast(spectra_plcmt_mbox_rates.calculated_cpm as float) 
    else cast(plcmt_delivery.dcm_rate as float) 
    end * case when cpc_plcmt_clicks.click_ct is not null then cpc_plcmt_clicks.click_ct else cast(0 as BIGINT) end) as double)
  
  WHEN (regexp_replace(lower(case
    when spectra_plcmt_mbox_rates.placement_rate_type is not null then spectra_plcmt_mbox_rates.placement_rate_type
    else plcmt_delivery.dcm_rate_type
    end), '[^a-zA-Z0-9]', '') = 'cpa') THEN

    cast((case 
    when spectra_plcmt_mbox_rates.calculated_cpm is not null 
    then cast(spectra_plcmt_mbox_rates.calculated_cpm as float) 
    else cast(plcmt_delivery.dcm_rate as float) 
    end * case when cpa_plcmt_activities.activity_ct is not null then cpa_plcmt_activities.activity_ct else cast(0 as BIGINT) end) as double)
  
  END as calculated_actual_cost
, ss_woc_stats.imp_sum as ss_imp_sum
, ss_woc_stats.click_sum as ss_click_sum
, ss_woc_stats.cost_sum as ss_cost_sum

from
(
  select *
  from plcmt_delivery
  where placement not like 'pfv%') plcmt_delivery

left outer join cpc_plcmt_clicks 
on cpc_plcmt_clicks.page_id = plcmt_delivery.page_id and cpc_plcmt_clicks.creative_id = plcmt_delivery.creative_id
  
left outer join cpa_plcmt_activities 
on cpa_plcmt_activities.page_id = plcmt_delivery.page_id and cpa_plcmt_activities.creative_id = plcmt_delivery.creative_id
  
left outer join dfa_meta_creative 
on dfa_meta_creative.creative_id = plcmt_delivery.creative_id  

left outer join skyskraper_woc_plcmt_stats ss_woc_stats
on (
  ss_woc_stats.placementid = plcmt_delivery.page_id
  and ss_woc_stats.creativeid = dfa_meta_creative.ui_creative_id
)
  
left outer join (
  select *
  from plcmt_pfscore
  where placement not like 'pfv%') plcmt_pfscore
on (
  plcmt_pfscore.pfcampaign = plcmt_delivery.pfcampaign
  and plcmt_pfscore.site_id = plcmt_delivery.site_id
  and plcmt_pfscore.page_id = plcmt_delivery.page_id
  and plcmt_pfscore.placement = plcmt_delivery.placement
  and plcmt_pfscore.creative_id = plcmt_delivery.creative_id
  and plcmt_pfscore.creative = plcmt_delivery.creative
  and plcmt_pfscore.creative_name = plcmt_delivery.creative_name
  and plcmt_pfscore.dcm_rate = plcmt_delivery.dcm_rate
  and plcmt_pfscore.dcm_rate_type = plcmt_delivery.dcm_rate_type
  and plcmt_pfscore.dcm_qty = plcmt_delivery.dcm_qty
  )

left outer join (
  select
  lower(pfcampaign_list.pfcampaign) as spectra_campaign
  , spectra_plcmt_mbox_rates.*
  
  from
  (
    select *
    from spectra_plcmt_mbox_rates) spectra_plcmt_mbox_rates
  
  join (select distinct lower(campaign_name) as campaign_name, lower(pfcampaign) as pfcampaign from pfcampaign_list where date_dropped = '' and buy_id is not null) pfcampaign_list
  on (
      split(lower(spectra_plcmt_mbox_rates.campaign_name), '_')[0] = split(lower(pfcampaign_list.campaign_name), '_')[0]
      and split(lower(spectra_plcmt_mbox_rates.campaign_name), '_')[1] = split(lower(pfcampaign_list.campaign_name), '_')[1]
      and split(lower(spectra_plcmt_mbox_rates.campaign_name), '_')[2] = split(lower(pfcampaign_list.campaign_name), '_')[2]
      and split(lower(spectra_plcmt_mbox_rates.campaign_name), '_')[3] = split(lower(pfcampaign_list.campaign_name), '_')[3]
    )
  ) spectra_plcmt_mbox_rates
on (
  spectra_plcmt_mbox_rates.adserver_placement_id = plcmt_delivery.page_id
  and spectra_plcmt_mbox_rates.spectra_campaign = plcmt_delivery.pfcampaign
  )

union all

select distinct
plcmt_delivery.brand
, plcmt_delivery.pfcampaign
, plcmt_delivery.site_id
, plcmt_delivery.site
, plcmt_delivery.siteadsize
, plcmt_delivery.sitecreative
, plcmt_delivery.sitetactic
, plcmt_delivery.ad_size
, plcmt_delivery.tactic
, plcmt_delivery.page_id
, plcmt_delivery.placement
, plcmt_pfscore.exe_type
, plcmt_delivery.creative_id
, plcmt_delivery.creative
, plcmt_delivery.creative_name
, regexp_replace(lower(plcmt_delivery.dcm_rate_type), '[^a-zA-Z0-9]', '') as dcm_rate_type
, plcmt_delivery.dcm_rate
, plcmt_delivery.dcm_qty
, regexp_replace(lower(spectra_plcmt_mbox_rates.placement_rate_type), '[^a-zA-Z0-9]', '') as spectra_rate_type
, spectra_plcmt_mbox_rates.rate as spectra_rate
, spectra_plcmt_mbox_rates.calculated_cpm
, pfscore_plcmt
, plcmt_delivery.impressions
, case when cpc_plcmt_clicks.click_ct is not null then cpc_plcmt_clicks.click_ct else cast(0 as BIGINT) end as click_ct
, case when cpa_plcmt_activities.activity_ct is not null then cpa_plcmt_activities.activity_ct else cast(0 as BIGINT) end as activity_ct
, CASE
  WHEN (regexp_replace(lower(case
    when spectra_plcmt_mbox_rates.placement_rate_type is not null then spectra_plcmt_mbox_rates.placement_rate_type
    else plcmt_delivery.dcm_rate_type
    end), '[^a-zA-Z0-9]', '') in ('cpm', 'flatrateimpressions')) THEN

    ((case 
    when spectra_plcmt_mbox_rates.calculated_cpm is not null 
    then cast(spectra_plcmt_mbox_rates.calculated_cpm as float) 
    else plcmt_delivery.dcm_rate 
    end * plcmt_delivery.impressions)/1000)

  WHEN (regexp_replace(lower(case
    when spectra_plcmt_mbox_rates.placement_rate_type is not null then spectra_plcmt_mbox_rates.placement_rate_type
    else plcmt_delivery.dcm_rate_type
    end), '[^a-zA-Z0-9]', '') = 'cpc') and lower(plcmt_delivery.placement) not like 'pfv%' THEN

    cast((case 
    when spectra_plcmt_mbox_rates.calculated_cpm is not null 
    then cast(spectra_plcmt_mbox_rates.calculated_cpm as float) 
    else cast(plcmt_delivery.dcm_rate as float) 
    end * case when cpc_plcmt_clicks.click_ct is not null then cpc_plcmt_clicks.click_ct else cast(0 as BIGINT) end) as double)
  
  WHEN (regexp_replace(lower(case
    when spectra_plcmt_mbox_rates.placement_rate_type is not null then spectra_plcmt_mbox_rates.placement_rate_type
    else plcmt_delivery.dcm_rate_type
    end), '[^a-zA-Z0-9]', '') = 'cpc') and lower(plcmt_delivery.placement) like 'pfv%' THEN

    ((case 
    when spectra_plcmt_mbox_rates.calculated_cpm is not null 
    then cast(spectra_plcmt_mbox_rates.calculated_cpm as float) 
    else plcmt_delivery.dcm_rate 
    end * plcmt_delivery.impressions)/1000)
  
  WHEN (regexp_replace(lower(case
    when spectra_plcmt_mbox_rates.placement_rate_type is not null then spectra_plcmt_mbox_rates.placement_rate_type
    else plcmt_delivery.dcm_rate_type
    end), '[^a-zA-Z0-9]', '') = 'cpa') THEN

    cast((case 
    when spectra_plcmt_mbox_rates.calculated_cpm is not null 
    then cast(spectra_plcmt_mbox_rates.calculated_cpm as float) 
    else cast(plcmt_delivery.dcm_rate as float) 
    end * case when cpa_plcmt_activities.activity_ct is not null then cpa_plcmt_activities.activity_ct else cast(0 as BIGINT) end) as double)
  
  END as calculated_actual_cost
, ss_woc_stats.imp_sum as ss_imp_sum
, ss_woc_stats.click_sum as ss_click_sum
, ss_woc_stats.cost_sum as ss_cost_sum

from
(
  select 
  regexp_replace(regexp_replace(regexp_replace(placement, 'prerollcreative[a-z]', 'preroll'), 'pfv\\_', ''), 'preroll[a-z]', 'preroll') as spectra_plcmt_match_key
  , plcmt_delivery.*
  from plcmt_delivery
  where placement like 'pfv%') plcmt_delivery

left outer join cpc_plcmt_clicks 
on cpc_plcmt_clicks.page_id = plcmt_delivery.page_id and cpc_plcmt_clicks.creative_id = plcmt_delivery.creative_id
  
left outer join cpa_plcmt_activities 
on cpa_plcmt_activities.page_id = plcmt_delivery.page_id and cpa_plcmt_activities.creative_id = plcmt_delivery.creative_id
  
left outer join dfa_meta_creative 
on dfa_meta_creative.creative_id = plcmt_delivery.creative_id  

left outer join skyskraper_woc_plcmt_stats ss_woc_stats
on (
  ss_woc_stats.placementid = plcmt_delivery.page_id
  and ss_woc_stats.creativeid = dfa_meta_creative.ui_creative_id
)

left outer join (
  select *
  from plcmt_pfscore
  where placement like 'pfv%') plcmt_pfscore
on (
  plcmt_pfscore.pfcampaign = plcmt_delivery.pfcampaign
  and plcmt_pfscore.site_id = plcmt_delivery.site_id
  and plcmt_pfscore.page_id = plcmt_delivery.page_id
  and plcmt_pfscore.placement = plcmt_delivery.placement
  and plcmt_pfscore.creative_id = plcmt_delivery.creative_id
  and plcmt_pfscore.creative = plcmt_delivery.creative
  and plcmt_pfscore.creative_name = plcmt_delivery.creative_name
  and plcmt_pfscore.dcm_rate = plcmt_delivery.dcm_rate
  and plcmt_pfscore.dcm_rate_type = plcmt_delivery.dcm_rate_type
  and plcmt_pfscore.dcm_qty = plcmt_delivery.dcm_qty
  )

left outer join (
  select
  regexp_replace(regexp_replace(regexp_replace(placement_name, 'prerollcreative[a-z]', 'preroll'), 'pfv\\_', ''), 'preroll[a-z]', 'preroll') as spectra_plcmt_match_key
  , spectra_plcmt_mbox_rates.*
  
  from
  spectra_plcmt_mbox_rates
  ) spectra_plcmt_mbox_rates
on (
  spectra_plcmt_mbox_rates.spectra_plcmt_match_key = plcmt_delivery.spectra_plcmt_match_key
  and spectra_plcmt_mbox_rates.adserver_placement_id = plcmt_delivery.page_id
  )
) unioned_plcmt_info
;