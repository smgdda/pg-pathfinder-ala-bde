DROP VIEW IF EXISTS vw_pf_agg_final_no_exe_type;

CREATE VIEW vw_pf_agg_final_no_exe_type AS 
SELECT
  brand
  , pfcampaign
  , rate_type
  , dimtype
  , dim
  , imps
  , cost
  , ss_cost
  , pfscore
  , scaled_imps
  , scaled_cost
  , scaled_ss_cost
  , scaled_pfscore
  , scaled_pfscore/scaled_imps as impact
  , scaled_pfscore/scaled_cost as efficiency
  , scaled_pfscore/scaled_ss_cost as actual_efficiency
FROM
  vw_pf_scaled_no_exe_type
;