DROP VIEW IF EXISTS vw_pf_camp_totals_no_exe_type;

CREATE VIEW vw_pf_camp_totals_no_exe_type AS 
SELECT 
  lower(dcm_spectra_merge.brand) as brand, 
  lower(dcm_spectra_merge.pfcampaign) as pfcampaign,
  case when spectra_rate_type is not null then spectra_rate_type else dcm_rate_type end as rate_type,
  sum(impressions) as imps_tot,
  sum(calculated_actual_cost) as cost_tot,
  sum(ss_cost_sum) as ss_cost_tot,
  ((1000 * sum(calculated_actual_cost)) / sum(impressions)) as calc_cpm,
  ((1000 * sum(ss_cost_sum)) / sum(impressions)) as calc_ss_cpm,
  sum(pfscore_plcmt) as pfscore_tot
FROM
  (select distinct lower(brand) as brand, lower(pfcampaign) as pfcampaign from pfcampaign_list where date_dropped = '' and buy_id is not null) pfcampaign_list
join
  dcm_spectra_merge_agg_w_innovid dcm_spectra_merge
  on dcm_spectra_merge.brand = pfcampaign_list.brand
  and dcm_spectra_merge.pfcampaign = pfcampaign_list.pfcampaign
GROUP BY
  lower(dcm_spectra_merge.brand), 
  lower(dcm_spectra_merge.pfcampaign),
  case when spectra_rate_type is not null then spectra_rate_type else dcm_rate_type end
;
