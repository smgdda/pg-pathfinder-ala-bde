DROP VIEW IF EXISTS vw_pf_dimtotals_no_exe_type;
CREATE VIEW vw_pf_dimtotals_no_exe_type AS 
SELECT
  lower(dims.brand) as brand,
  dims.pfcampaign,
  dims.rate_type,
  dims.dimtype,
  dims.dim,
  dims.imps,
  dims.cost,
  dims.ss_cost,
  dims.calc_cpm,
  dims.calc_ss_cpm,
  dims.pfscore

FROM
  (
    SELECT 
      brand,
      pfcampaign,
      case when spectra_rate_type is not null then spectra_rate_type else dcm_rate_type end as rate_type,
      'site' as dimtype,
      lower(site) as dim,
      sum(impressions) as imps,
      sum(calculated_actual_cost) as cost,
      sum(ss_cost_sum) as ss_cost,
      ((1000 * sum(calculated_actual_cost)) / sum(impressions)) as calc_cpm,
      ((1000 * sum(ss_cost_sum)) / sum(impressions)) as calc_ss_cpm,
      sum(pfscore_plcmt) as pfscore
    FROM
      dcm_spectra_merge_agg_w_innovid dcm_spectra_merge
    GROUP BY 
      brand,
      pfcampaign,
      case when spectra_rate_type is not null then spectra_rate_type else dcm_rate_type end,
      lower(site)

    UNION ALL

    SELECT 
      brand,
      pfcampaign,
      case when spectra_rate_type is not null then spectra_rate_type else dcm_rate_type end as rate_type,
      'tactic' as dimtype,
      lower(tactic) as dim,
      sum(impressions) as imps,
      sum(calculated_actual_cost) as cost,
      sum(ss_cost_sum) as ss_cost,
      ((1000 * sum(calculated_actual_cost)) / sum(impressions)) as calc_cpm,
      ((1000 * sum(ss_cost_sum)) / sum(impressions)) as calc_ss_cpm,
      sum(pfscore_plcmt) as pfscore
    FROM
      dcm_spectra_merge_agg_w_innovid dcm_spectra_merge
    GROUP BY 
      brand,
      pfcampaign,
      case when spectra_rate_type is not null then spectra_rate_type else dcm_rate_type end,
      lower(tactic)

    UNION ALL

    SELECT 
      brand,
      pfcampaign,
      case when spectra_rate_type is not null then spectra_rate_type else dcm_rate_type end as rate_type,
      'adsize' as dimtype,
      lower(ad_size) as dim,
      sum(impressions) as imps,
      sum(calculated_actual_cost) as cost,
      sum(ss_cost_sum) as ss_cost,
      ((1000 * sum(calculated_actual_cost)) / sum(impressions)) as calc_cpm,
      ((1000 * sum(ss_cost_sum)) / sum(impressions)) as calc_ss_cpm,
      sum(pfscore_plcmt) as pfscore
    FROM
      dcm_spectra_merge_agg_w_innovid dcm_spectra_merge
    GROUP BY 
      brand,
      pfcampaign,
      case when spectra_rate_type is not null then spectra_rate_type else dcm_rate_type end,
      lower(ad_size)

    UNION ALL

    SELECT 
      brand,
      pfcampaign,
      case when spectra_rate_type is not null then spectra_rate_type else dcm_rate_type end as rate_type,
      'creative' as dimtype,
      lower(creative) as dim,
      sum(impressions) as imps,
      sum(calculated_actual_cost) as cost,
      sum(ss_cost_sum) as ss_cost,
      ((1000 * sum(calculated_actual_cost)) / sum(impressions)) as calc_cpm,
      ((1000 * sum(ss_cost_sum)) / sum(impressions)) as calc_ss_cpm,
      sum(pfscore_plcmt) as pfscore
    FROM
      dcm_spectra_merge_agg_w_innovid dcm_spectra_merge
    GROUP BY 
      brand,
      pfcampaign,
      case when spectra_rate_type is not null then spectra_rate_type else dcm_rate_type end,
      lower(creative)

    UNION ALL

    SELECT 
      brand,
      pfcampaign,
      case when spectra_rate_type is not null then spectra_rate_type else dcm_rate_type end as rate_type,
      'sitetactic' as dimtype,
      lower(sitetactic) as dim,
      sum(impressions) as imps,
      sum(calculated_actual_cost) as cost,
      sum(ss_cost_sum) as ss_cost,
      ((1000 * sum(calculated_actual_cost)) / sum(impressions)) as calc_cpm,
      ((1000 * sum(ss_cost_sum)) / sum(impressions)) as calc_ss_cpm,
      sum(pfscore_plcmt) as pfscore
    FROM
      dcm_spectra_merge_agg_w_innovid dcm_spectra_merge
    GROUP BY 
      brand,
      pfcampaign,
      case when spectra_rate_type is not null then spectra_rate_type else dcm_rate_type end,
      lower(sitetactic)

    UNION ALL

    SELECT 
      brand,
      pfcampaign,
      case when spectra_rate_type is not null then spectra_rate_type else dcm_rate_type end as rate_type,
      'siteadsize' as dimtype,
      lower(siteadsize) as dim,
      sum(impressions) as imps,
      sum(calculated_actual_cost) as cost,
      sum(ss_cost_sum) as ss_cost,
      ((1000 * sum(calculated_actual_cost)) / sum(impressions)) as calc_cpm,
      ((1000 * sum(ss_cost_sum)) / sum(impressions)) as calc_ss_cpm,
      sum(pfscore_plcmt) as pfscore
    FROM
      dcm_spectra_merge_agg_w_innovid dcm_spectra_merge
    GROUP BY 
      brand,
      pfcampaign,
      case when spectra_rate_type is not null then spectra_rate_type else dcm_rate_type end,
      lower(siteadsize)

    UNION ALL

    SELECT 
      brand,
      pfcampaign,
      case when spectra_rate_type is not null then spectra_rate_type else dcm_rate_type end as rate_type,
      'sitecreative' as dimtype,
      lower(sitecreative) as dim,
      sum(impressions) as imps,
      sum(calculated_actual_cost) as cost,
      sum(ss_cost_sum) as ss_cost,
      ((1000 * sum(calculated_actual_cost)) / sum(impressions)) as calc_cpm,
      ((1000 * sum(ss_cost_sum)) / sum(impressions)) as calc_ss_cpm,
      sum(pfscore_plcmt) as pfscore
    FROM
      dcm_spectra_merge_agg_w_innovid dcm_spectra_merge
    GROUP BY 
      brand,
      pfcampaign,
      case when spectra_rate_type is not null then spectra_rate_type else dcm_rate_type end,
      lower(sitecreative)
  ) dims
JOIN
  (
    select distinct 
      lower(brand) as brand
      , lower(pfcampaign) as pfcampaign 
    from 
      pfcampaign_list 
    where 
      date_dropped = '' 
      and buy_id is not null
  ) pfcampaign_list
on (
  pfcampaign_list.brand = dims.brand
  and pfcampaign_list.pfcampaign = dims.pfcampaign
)
;
