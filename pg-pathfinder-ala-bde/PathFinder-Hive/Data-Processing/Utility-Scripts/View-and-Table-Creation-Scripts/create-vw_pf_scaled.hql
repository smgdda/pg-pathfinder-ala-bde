DROP VIEW IF EXISTS vw_pf_scaled;

CREATE VIEW vw_pf_scaled AS 
SELECT
  a.brand
  ,a.pfcampaign
  ,a.rate_type
  ,lower(a.exe_type) as exe_type
  ,a.dimtype
  ,a.dim
  ,a.imps
  ,a.cost
  ,a.ss_cost
  ,a.pfscore
  ,a.imps/b.imps_tot as scaled_imps
  ,a.cost/b.cost_tot as scaled_cost
  ,a.ss_cost/b.ss_cost_tot as scaled_ss_cost
  ,a.pfscore/b.pfscore_tot as scaled_pfscore
FROM
  vw_pf_dimtotals a 

LEFT OUTER JOIN (
  SELECT
  brand
  ,pfcampaign
  ,rate_type
  ,lower(exe_type) as exe_type
  ,imps_tot
  ,cost_tot
  ,ss_cost_tot
  ,pfscore_tot
  FROM
  vw_pf_camp_totals
  ) b 
ON (
  a.pfcampaign = b.pfcampaign 
  and a.brand = b.brand
  and lower(a.exe_type) = lower(b.exe_type)
  and a.rate_type = b.rate_type
  )
;