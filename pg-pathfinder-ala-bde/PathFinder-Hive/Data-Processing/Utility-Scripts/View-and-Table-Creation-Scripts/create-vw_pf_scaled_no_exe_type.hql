DROP VIEW IF EXISTS vw_pf_scaled_no_exe_type;

CREATE VIEW vw_pf_scaled_no_exe_type AS
SELECT
  a.brand
  ,a.pfcampaign
  ,a.rate_type
  ,a.dimtype
  ,a.dim
  ,a.imps
  ,a.cost
  ,a.ss_cost
  ,a.pfscore
  ,a.imps/b.imps_tot as scaled_imps
  ,a.cost/b.cost_tot as scaled_cost
  ,a.ss_cost/b.ss_cost_tot as scaled_ss_cost
  ,a.pfscore/b.pfscore_tot as scaled_pfscore
FROM
  vw_pf_dimtotals_no_exe_type a 
    left outer join (
      select
        brand
        ,pfcampaign
        ,rate_type
        ,imps_tot
        ,cost_tot
        ,ss_cost_tot
        ,pfscore_tot
      from
        vw_pf_camp_totals_no_exe_type
      ) b 
      on (
      a.pfcampaign = b.pfcampaign
      and a.brand = b.brand
      and a.rate_type = b.rate_type
      )
;