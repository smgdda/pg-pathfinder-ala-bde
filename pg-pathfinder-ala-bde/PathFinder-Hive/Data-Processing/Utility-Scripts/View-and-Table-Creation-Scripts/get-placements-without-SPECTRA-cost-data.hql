-- select
--   buy_id
--   , site_id
--   , page_id
--   , site_placement
--   , placement_rate_type as manual_placement_rate_type
--   , calculated_cpm
--   , dcm_rate_type
--   , dcm_rate
--   , dcm_qty
--   , spectra_rate_type
--   , calculated_cpm
--   , pfscore_plcmt
--   , impressions
--   , calculated_actual_cost
-- 
-- from
-- (
--   select
--   dcm_spectra_merge.*
--   
--   from
--   dcm_spectra_merge
--   
--   where 
--   calculated_actual_cost IS  NULL
--   or calculated_actual_cost = 'NaN' 
--   or calculated_actual_cost = 'Infinity'
--   ) dcm_spectra_merge
-- 
-- left outer join (
--   select
--   dfa_meta_page.page_id
--   , regexp_replace(dfa_meta_page.site_placement, '[^a-zA-Z0-9_]', '') as site_placement
--   , dfa_meta_page.pricing_type
--   , dfa_meta_page.purchase_cost
--   , dfa_meta_page.purchase_quantity
--   , dfa_meta_page.cap_cost
--   from
--   dfa_meta_page
--   ) dfaMetaPage
-- on (
--   dfaMetaPage.site_placement = dcm_spectra_merge.placement
--   and dfaMetaPage.page_id = dcm_spectra_merge.page_id
--   )
-- 
-- order by
--   brand
--   , pfcampaign
-- ;



select
  dcm_spectra_merge.brand
  , dcm_spectra_merge.pfcampaign
  , dfa_meta_page.buy_id
  , dcm_spectra_merge.site_id
  , dcm_spectra_merge.site
  , dcm_spectra_merge.page_id
  , dcm_spectra_merge.placement
  , dcm_spectra_merge.dcm_qty
  , dcm_spectra_merge.dcm_rate_type
  , dcm_spectra_merge.dcm_rate
  , dcm_spectra_merge.spectra_rate_type
  , dcm_spectra_merge.spectra_rate
  , dcm_spectra_merge.calculated_cpm
  , sum(dcm_spectra_merge.impressions) as imp_sum

from
  dcm_spectra_merge

join
  dfa_meta_page
  on 
    dfa_meta_page.page_id = dcm_spectra_merge.page_id 
    and dfa_meta_page.site_id = dcm_spectra_merge.site_id

where
  dcm_spectra_merge.impressions > 1000
  and (
    dcm_spectra_merge.calculated_actual_cost = 0
    or dcm_spectra_merge.calculated_actual_cost is null
  )
  and dcm_spectra_merge.placement not like '%addedvalue%'

group by
  dcm_spectra_merge.brand
  , dcm_spectra_merge.pfcampaign
  , dfa_meta_page.buy_id
  , dcm_spectra_merge.site_id
  , dcm_spectra_merge.site
  , dcm_spectra_merge.page_id
  , dcm_spectra_merge.placement
  , dcm_spectra_merge.dcm_qty
  , dcm_spectra_merge.dcm_rate_type
  , dcm_spectra_merge.dcm_rate
  , dcm_spectra_merge.spectra_rate_type
  , dcm_spectra_merge.spectra_rate
  , dcm_spectra_merge.calculated_cpm

order by
  brand
  , pfcampaign
  , site
  , buy_id
  , placement