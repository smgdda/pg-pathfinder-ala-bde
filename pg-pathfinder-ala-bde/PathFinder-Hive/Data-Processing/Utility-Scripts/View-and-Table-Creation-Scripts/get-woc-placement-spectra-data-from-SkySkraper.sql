select distinct
  adserver_name
  , adserver_network_id
  , adserver_campaign_id
  , adserver_placement_id
  , adserver_site_id
  , campaign_id
  , campaign_name
  , campaign_reporting_type
  , digital_type
  , client_id
  , placement_id
  , placement_name
  , placement_start_date
  , placement_end_date
  , placement_rate_type
  , cast(NULL as varchar(50)) as grp_rate_type
  , rate
  , cast(NULL as FLOAT) as grp_rate
  , placement_reporting_type
  , placement_type
  , product_id
  , product_name
  , group_name
  , vendor_id
  , ordered_net
  , cast(NULL as decimal(12,2)) as grp_ordered_net
  , units
  , cast(NULL as BIGINT) as grp_units
  , actual_impressions
  , cast(NULL as BIGINT) as grp_actual_impressions

from 
  agency_views_mbox.digital_placement

where 
  CLIENT_ID = 'PG1'
  and lower(placement_name) not like '%cancel%'
  and placement_start_date >= '2015-05-01'
  and (group_name is NULL or group_name = '')

union all

select distinct
  agency_views_mbox.digital_placement.adserver_name
  , agency_views_mbox.digital_placement.adserver_network_id
  , agency_views_mbox.digital_placement.adserver_campaign_id
  , agency_views_mbox.digital_placement.adserver_placement_id
  , agency_views_mbox.digital_placement.adserver_site_id
  , agency_views_mbox.digital_placement.campaign_id
  , agency_views_mbox.digital_placement.campaign_name
  , agency_views_mbox.digital_placement.campaign_reporting_type
  , agency_views_mbox.digital_placement.digital_type
  , agency_views_mbox.digital_placement.client_id
  , agency_views_mbox.digital_placement.placement_id
  , agency_views_mbox.digital_placement.placement_name
  , agency_views_mbox.digital_placement.placement_start_date
  , agency_views_mbox.digital_placement.placement_end_date
  , agency_views_mbox.digital_placement.placement_rate_type
  , plcmt_group.placement_rate_type as grp_rate_type
  , agency_views_mbox.digital_placement.rate
  , plcmt_group.rate as grp_rate
  , agency_views_mbox.digital_placement.placement_reporting_type
  , agency_views_mbox.digital_placement.placement_type
  , agency_views_mbox.digital_placement.product_id
  , agency_views_mbox.digital_placement.product_name
  , agency_views_mbox.digital_placement.group_name
  , agency_views_mbox.digital_placement.vendor_id
  , agency_views_mbox.digital_placement.ordered_net
  , plcmt_group.ordered_net as grp_ordered_net
  , agency_views_mbox.digital_placement.units
  , plcmt_group.units as grp_units
  , agency_views_mbox.digital_placement.actual_impressions
  , plcmt_group.actual_impressions as grp_actual_impressions

from
  agency_views_mbox.digital_placement

left outer join
  (select * from agency_views_mbox.digital_placement where CLIENT_ID = 'PG1' and lower(placement_type) = 'placement group') plcmt_group
on
  (
  plcmt_group.group_name = agency_views_mbox.digital_placement.group_name
  and plcmt_group.campaign_id = agency_views_mbox.digital_placement.campaign_id
  )

where 
  agency_views_mbox.digital_placement.CLIENT_ID = 'PG1'
  and lower(agency_views_mbox.digital_placement.placement_name) not like '%cancel%'
  and agency_views_mbox.digital_placement.placement_start_date >= '2015-05-01'
  and (
    agency_views_mbox.digital_placement.group_name is not NULL 
    and agency_views_mbox.digital_placement.group_name != '' 
    and lower(agency_views_mbox.digital_placement.placement_type) != 'placement group'
  )
order by
  client_id
  , campaign_id
  , placement_id
