﻿DROP TABLE IF EXISTS innovid_report_data;
CREATE EXTERNAL TABLE innovid_report_data
(
  calendar_date string, 
  campaign_name string, 
  campaign_id string, 
  publisher string, 
  publisher_id string, 
  video_id string, 
  video_name string, 
  placement_name string, 
  impressions string, 
  full_completions string, 
  completion_rate string
)
ROW FORMAT DELIMITED 
FIELDS TERMINATED BY '\t' 
LINES TERMINATED BY '\n' 
STORED AS TEXTFILE
LOCATION
  'hdfs://vmcwy62059.prod.acxiom.net:8020/user/owatson/external-table-data/innovid-report-data'
;
