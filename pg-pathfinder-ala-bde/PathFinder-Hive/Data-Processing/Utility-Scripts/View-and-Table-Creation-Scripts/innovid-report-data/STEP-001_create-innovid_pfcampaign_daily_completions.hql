﻿DROP TABLE IF EXISTS innovid_pfcampaign_daily_completions;
CREATE TABLE innovid_pfcampaign_daily_completions AS
SELECT
  estimate_number
  , mbox_placement_id
  , regexp_replace(innovid_pfc_joined.cleaned_plcmt_name, '[^a-zA-Z0-9\\_]', '') as cleaned_plcmt_name
  , innovid_pfc_joined.video_name
  , regexp_replace(lower(innovid_pfc_joined.split_creative_name), '[^a-zA-Z0-9]', '') as derived_creative_name
  , innovid_pfc_joined.pfcampaign
  , concat_ws('_', split(innovid_pfc_joined.cleaned_plcmt_name,'_')[0], split(innovid_pfc_joined.cleaned_plcmt_name,'_')[3]) as tactic
  , concat_ws('_', publisher, split(innovid_pfc_joined.cleaned_plcmt_name,'_')[0], split(innovid_pfc_joined.cleaned_plcmt_name,'_')[3]) as sitetactic
  , concat_ws('_', publisher, split(innovid_pfc_joined.cleaned_plcmt_name,'_')[5]) as siteadsize
  , sum(innovid_pfc_joined.impressions) as innovid_imp_sum
  , sum(innovid_pfc_joined.full_completions) as innovid_completions_sum
FROM
  (
  select
    split(split(regexp_replace(lower(placement_name), '[^a-zA-Z0-9_\\|]', ''), '\\|')[0], '_')[0] as estimate_number
    , split(split(regexp_replace(lower(placement_name), '[^a-zA-Z0-9_\\|]', ''), '\\|')[0], '_')[1] as mbox_placement_id
    , regexp_replace(regexp_replace(split(regexp_replace(lower(placement_name), '[^a-zA-Z0-9_\\|]', ''), '\\|')[0], concat_ws('_', split(split(regexp_replace(lower(placement_name), '[^a-zA-Z0-9_\\|]', ''), '\\|')[0], '_')[0], split(split(regexp_replace(lower(placement_name), '[^a-zA-Z0-9_\\|]', ''), '\\|')[0], '_')[1], ''), ''), 'secure$', '') as cleaned_plcmt_name
    , innovid_report_data.video_name
    , split(innovid_report_data.video_name, '_PG')[0] as split_creative_name
    , size(split(innovid_report_data.video_name, '_PG')) as split_size
    , lower(pfcampaign_list.pfcampaign) as pfcampaign
    , regexp_replace(lower(innovid_report_data.publisher), '[^a-zA-Z0-9]', '') as publisher
    , innovid_report_data.impressions
    , innovid_report_data.full_completions
    , calendar_date as fixed_dt
  from
    innovid_report_data
  join
    pfcampaign_list
    on lower(pfcampaign_list.campaign_name) = regexp_replace(lower(innovid_report_data.campaign_name), '[^a-zA-Z0-9\\_]', '')
  ) innovid_pfc_joined
GROUP BY
  estimate_number
  , mbox_placement_id
  , regexp_replace(innovid_pfc_joined.cleaned_plcmt_name, '[^a-zA-Z0-9\\_]', '')
  , innovid_pfc_joined.video_name
  , regexp_replace(lower(innovid_pfc_joined.split_creative_name), '[^a-zA-Z0-9]', '')
  , innovid_pfc_joined.pfcampaign
  , concat_ws('_', split(innovid_pfc_joined.cleaned_plcmt_name,'_')[0], split(innovid_pfc_joined.cleaned_plcmt_name,'_')[3])
  , concat_ws('_', publisher,split(innovid_pfc_joined.cleaned_plcmt_name,'_')[0], split(innovid_pfc_joined.cleaned_plcmt_name,'_')[3])
  , concat_ws('_', publisher,split(innovid_pfc_joined.cleaned_plcmt_name,'_')[5])
;
