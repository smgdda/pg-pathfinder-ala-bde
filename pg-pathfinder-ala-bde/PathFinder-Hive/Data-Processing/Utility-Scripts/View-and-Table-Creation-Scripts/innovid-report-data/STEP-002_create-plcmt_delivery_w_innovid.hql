﻿DROP TABLE IF EXISTS plcmt_delivery_w_innovid;
CREATE TABLE plcmt_delivery_w_innovid AS
SELECT
  *
FROM
  (
    SELECT
      brand
      ,pfcampaign
      ,site
      ,sitetactic
      ,sitecreative
      ,siteadsize
      ,tactic
      ,creative
      ,ad_size
      ,placement
      ,page_id
      ,site_id
      ,dcm_rate_type
      ,dcm_rate
      ,dcm_qty
      ,spectra_matchkey
      ,sum(impressions) as impressions
    FROM
      plcmt_delivery
    GROUP BY
      brand
      ,pfcampaign
      ,site
      ,sitetactic
      ,sitecreative
      ,siteadsize
      ,tactic
      ,creative
      ,ad_size
      ,placement
      ,page_id
      ,site_id
      ,dcm_rate_type
      ,dcm_rate
      ,dcm_qty
      ,spectra_matchkey
  ) plcmt_delivery
LEFT OUTER JOIN
  (
    SELECT
      innovid_pfcampaign_daily_completions.pfcampaign as innovid_pfcampaign
      , innovid_pfcampaign_daily_completions.cleaned_plcmt_name as innovid_plcmt_name
      , innovid_pfcampaign_daily_completions.derived_creative_name as innovid_creative
      , sum(innovid_pfcampaign_daily_completions.innovid_imp_sum) as innovid_imp_sum
      , sum(innovid_pfcampaign_daily_completions.innovid_completions_sum) as innovid_completions_sum
    FROM
      innovid_pfcampaign_daily_completions
    GROUP BY
      innovid_pfcampaign_daily_completions.pfcampaign
      , innovid_pfcampaign_daily_completions.cleaned_plcmt_name
      , innovid_pfcampaign_daily_completions.derived_creative_name
  ) innovid_data
  on
  plcmt_delivery.pfcampaign = innovid_data.innovid_pfcampaign
  and regexp_replace(regexp_replace(plcmt_delivery.placement, '(no)?frq(no)?view\\_', ''), '^pfv\\_', '') = regexp_replace(regexp_replace(innovid_data.innovid_plcmt_name, '(15|30)$', ''), '(no)?frq(no)?view\\_', '')
  and plcmt_delivery.creative = innovid_data.innovid_creative