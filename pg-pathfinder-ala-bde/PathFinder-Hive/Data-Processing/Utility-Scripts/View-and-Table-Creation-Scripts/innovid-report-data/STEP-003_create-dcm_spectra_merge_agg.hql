﻿DROP TABLE IF EXISTS dcm_spectra_merge_agg;

CREATE TABLE dcm_spectra_merge_agg AS
SELECT
  brand, 
  pfcampaign, 
  site_id, 
  site, 
  siteadsize, 
  case 
    when pfcampaign = 'larryvi' and creative = 'po' then 
      concat_ws('_', site, split(creative_name, '_')[10]) 
    else sitecreative end as sitecreative, 
  sitetactic, 
  ad_size, 
  tactic, 
  page_id, 
  placement, 
  exe_type, 
  case
    when creative = 'po' and pfcampaign = 'larryvi' then 
      split(creative_name, '_')[10]
    else creative end as creative, 
  dcm_rate_type, 
  dcm_rate, 
  dcm_qty, 
  spectra_rate_type, 
  spectra_rate, 
  calculated_cpm, 
  sum(pfscore_plcmt) as pfscore_plcmt, 
  sum(impressions) as impressions, 
  sum(click_ct) as click_ct, 
  sum(activity_ct) as activity_ct, 
  sum(calculated_actual_cost) as calculated_actual_cost, 
  sum(ss_imp_sum) as ss_imp_sum, 
  sum(ss_click_sum) as ss_click_sum, 
  sum(ss_cost_sum) as ss_cost_sum
FROM
  dcm_spectra_merge
GROUP BY
  brand, 
  pfcampaign, 
  site_id, 
  site, 
  siteadsize, 
  case 
    when pfcampaign = 'larryvi' and creative = 'po' then 
      concat_ws('_', site, split(creative_name, '_')[10]) 
    else sitecreative end, 
  sitetactic, 
  ad_size, 
  tactic, 
  page_id, 
  placement, 
  exe_type, 
  case
    when creative = 'po' and pfcampaign = 'larryvi' then 
      split(creative_name, '_')[10]
    else creative end, 
  dcm_rate_type, 
  dcm_rate, 
  dcm_qty, 
  spectra_rate_type, 
  spectra_rate, 
  calculated_cpm
;