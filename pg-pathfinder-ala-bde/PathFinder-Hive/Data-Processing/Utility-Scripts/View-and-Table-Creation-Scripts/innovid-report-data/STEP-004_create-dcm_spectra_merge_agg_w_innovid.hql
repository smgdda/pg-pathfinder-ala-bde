﻿DROP TABLE IF EXISTS dcm_spectra_merge_agg_w_innovid;
CREATE TABLE dcm_spectra_merge_agg_w_innovid AS
SELECT
  dcm_spectra_merge_agg.brand
  , dcm_spectra_merge_agg.pfcampaign
  , dcm_spectra_merge_agg.site_id
  , dcm_spectra_merge_agg.site
  , dcm_spectra_merge_agg.siteadsize
  , dcm_spectra_merge_agg.sitecreative
  , dcm_spectra_merge_agg.sitetactic
  , dcm_spectra_merge_agg.ad_size
  , dcm_spectra_merge_agg.tactic
  , dcm_spectra_merge_agg.page_id
  , dcm_spectra_merge_agg.placement
  , dcm_spectra_merge_agg.exe_type
  , dcm_spectra_merge_agg.creative
  , dcm_spectra_merge_agg.dcm_rate_type
  , dcm_spectra_merge_agg.dcm_rate
  , dcm_spectra_merge_agg.dcm_qty
  , dcm_spectra_merge_agg.spectra_rate_type
  , dcm_spectra_merge_agg.spectra_rate
  , dcm_spectra_merge_agg.calculated_cpm
  , dcm_spectra_merge_agg.pfscore_plcmt
  , dcm_spectra_merge_agg.impressions
  , dcm_spectra_merge_agg.click_ct
  , dcm_spectra_merge_agg.activity_ct
  , case 
    when dcm_spectra_merge_agg.spectra_rate_type = 'cpcv' then
      ((plcmt_delivery_w_innovid.innovid_completions_sum / plcmt_delivery_w_innovid.innovid_imp_sum) * dcm_spectra_merge_agg.impressions) * dcm_spectra_merge_agg.spectra_rate
    else
      dcm_spectra_merge_agg.calculated_actual_cost 
    end as calculated_actual_cost
  , dcm_spectra_merge_agg.ss_imp_sum
  , dcm_spectra_merge_agg.ss_click_sum
  , dcm_spectra_merge_agg.ss_cost_sum
--  ,plcmt_delivery_w_innovid.innovid_completions_sum
--  ,plcmt_delivery_w_innovid.innovid_imp_sum
--  ,(plcmt_delivery_w_innovid.innovid_completions_sum / plcmt_delivery_w_innovid.innovid_imp_sum) as innovid_completion_rate,
--  ,((plcmt_delivery_w_innovid.innovid_completions_sum / plcmt_delivery_w_innovid.innovid_imp_sum) * dcm_spectra_merge_agg.impressions) as estimated_cv,
--  ,((plcmt_delivery_w_innovid.innovid_completions_sum / plcmt_delivery_w_innovid.innovid_imp_sum) * dcm_spectra_merge_agg.impressions) * dcm_spectra_merge_agg.spectra_rate as estimated_cv_cost
FROM
  dcm_spectra_merge_agg
LEFT OUTER JOIN
  plcmt_delivery_w_innovid
  ON
  dcm_spectra_merge_agg.spectra_rate_type = 'cpcv'
  AND plcmt_delivery_w_innovid.pfcampaign = dcm_spectra_merge_agg.pfcampaign
  AND plcmt_delivery_w_innovid.page_id = dcm_spectra_merge_agg.page_id
  AND plcmt_delivery_w_innovid.creative = dcm_spectra_merge_agg.creative
  AND plcmt_delivery_w_innovid.impressions = dcm_spectra_merge_agg.impressions
-- WHERE
--   dcm_spectra_merge_agg.spectra_rate_type = 'cpcv'