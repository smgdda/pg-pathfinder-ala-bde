﻿DROP TABLE IF EXISTS innovid_dcm_via_mbox;
CREATE TABLE innovid_dcm_via_mbox AS
SELECT DISTINCT
  innovid_pfcampaign_daily_completions.mbox_placement_id as innovid_mbox_plcmt_id
  , innovid_pfcampaign_daily_completions.estimate_number as innovid_estimate_number
  , innovid_pfcampaign_daily_completions.video_name as innovid_video_name
  , innovid_pfcampaign_daily_completions.derived_creative_name as innovid_derived_creative_name
  , innovid_pfcampaign_daily_completions.innovid_imp_sum as innovid_imp_sum
  , innovid_pfcampaign_daily_completions.innovid_completions_sum as innovid_completions_sum
  , pf_spectra_placements.placement_id as spectra_mbox_placement_id
  , pf_spectra_placements.adserver_campaign_id as spectra_mbox_adverser_campaign_id
  , pf_spectra_placements.adserver_placement_id as spectra_mbox_adverser_placement_id
  , pf_spectra_placements.adserver_site_id as spectra_mbox_adverser_site_id
  , regexp_replace(lower(pfcampaign_list.pfcampaign), '[^a-zA-Z0-9]', '') as pfcampaign
  
FROM
  innovid_pfcampaign_daily_completions
  
LEFT OUTER JOIN
  pf_spectra_placements
  ON
  pf_spectra_placements.placement_id = innovid_pfcampaign_daily_completions.mbox_placement_id

LEFT OUTER JOIN
  pfcampaign_list
  ON
  pfcampaign_list.buy_id = pf_spectra_placements.adserver_campaign_id

ORDER BY
  innovid_estimate_number
  , innovid_mbox_plcmt_id