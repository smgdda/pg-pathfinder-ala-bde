﻿-- GET THE BUY_ID
select * from
pfcampaign_list
where
pfcampaign_list.campaign_name like '%125362%'
;

-- FIND IN PLCMT_PFSCORE
select
plcmt_pfscore.*
from
plcmt_pfscore
where
creative_id in
(
65657365,
65657300,
65656672,
65657430,
65657492,
65657245,
65656682,
65657226,
65657241,
65657355,
65657216,
65656680,
65658011,
65657362
)
;

-- FIND IN PLCMT_DELIVERY
select
plcmt_delivery.*
from
plcmt_delivery
where
creative_id in
(
65657365,
65657300,
65656672,
65657430,
65657492,
65657245,
65656682,
65657226,
65657241,
65657355,
65657216,
65656680,
65658011,
65657362
)
;

-- FIND IN PF_DAILY_DFA_PLCMT_STATS
select
*

from
pf_daily_dfa_plcmt_stats

where
creative_id in (
  65657365,
  65657300
)
;

select
*

from
pf_daily_dfa_plcmt_stats

where
creative_id = 65657362
;

select
*

from
pf_daily_dfa_plcmt_stats

where
creative_id in (
  65656672,
  65657430,
  65657492
)
;

select
*
from
pf_daily_dfa_plcmt_stats
where
creative_id in (65656680,65658011)
;

select
*
from
pf_daily_dfa_plcmt_stats
where
creative_id in (65657355,65657216)
;

select
*
from
pf_daily_dfa_plcmt_stats
where
creative_id in (65657245,65656682)
;

select
*
from
pf_daily_dfa_plcmt_stats
where
creative_id in (65657226,65657241)
;


-- LOOK IN THE LOG FILE FOR A DATE WITHIN THE WINDOW, 8-11 - 10-10
select
count(*) as imp_ct_for_missing_creatives
, count(distinct user_id) as user_ct_for_missing_creatives

from
ingestion_dfa_impressions

where
ingestion_dt = '2015-09-15'
and buy_id = 8834696
and site_id = 1071747
and creative_id in
(
65657365,
65657300,
65656672,
65657430,
65657492,
65657245,
65656682,
65657226,
65657241,
65657355,
65657216,
65656680,
65658011,
65657362
)
;
