﻿CREATE EXTERNAL TABLE pf_bhv_kpi_urls
(
  brand string, 
  pfcampaign string, 
  bhv_kpi_url string, 
  live_or_not string, 
  sent_to_gbs string
)
ROW FORMAT DELIMITED 
FIELDS TERMINATED BY '\t' 
LINES TERMINATED BY '\n' 
STORED AS TEXTFILE
LOCATION
  'hdfs://vmcwy62059.prod.acxiom.net:8020/user/owatson/external-table-data/bhv-kpi-urls'