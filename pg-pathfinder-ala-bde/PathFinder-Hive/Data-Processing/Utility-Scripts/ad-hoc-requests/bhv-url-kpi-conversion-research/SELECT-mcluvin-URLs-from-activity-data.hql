﻿select
-- buy_id
dfa_activity.advertiser_id
, advertiser
, activity_type
-- , other_data
-- , str_to_map(other_data, ';', '=')['u1'] as u1_from_other_data
, str_to_map(other_data, ';', '=')['~oref'] as oref_from_other_data
, count(*) as instance_ct

from
dfa_activity

join 
dfa_meta_advertiser
on
dfa_activity.event_dt > '2015-10-20'
-- and dfa_activity.buy_id = 8783524
and dfa_activity.advertiser_id = 3232123
and dfa_meta_advertiser.advertiser_id = dfa_activity.advertiser_id

group by
-- buy_id
dfa_activity.advertiser_id
, advertiser
, activity_type
-- , other_data
-- , str_to_map(other_data, ';', '=')['u1']
, str_to_map(other_data, ';', '=')['~oref']