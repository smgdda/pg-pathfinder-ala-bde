﻿DROP TABLE IF EXISTS dcm_spectra_merge_agg_w_innovid_and_bounty_segments;
CREATE TABLE dcm_spectra_merge_agg_w_innovid_and_bounty_segments AS
SELECT
  dcm_spectra_merge_agg_w_innovid.*
  , CASE WHEN bounty_segment_map.bounty_target IS NOT NULL then bounty_segment_map.bounty_target ELSE 'no_segment_specified' END as bounty_target

FROM
  dcm_spectra_merge_agg_w_innovid

LEFT OUTER JOIN
  (
    SELECT DISTINCT
      regexp_replace(lower(brand), '[^a-zA-Z0-9]', '') as brand
      , regexp_replace(lower(pfcampaign), '[^a-zA-Z0-9]', '') as pfcampaign
      , page_id
      , regexp_replace(lower(bounty_target), '[\\ ]', '_') as bounty_target
    
    FROM
      pf_placement_segment_map
    WHERE
      bounty_target IS NOT NULL AND bounty_target != ''
  ) bounty_segment_map
  ON
  bounty_segment_map.brand = 'bounty'
  AND bounty_segment_map.pfcampaign = dcm_spectra_merge_agg_w_innovid.pfcampaign
  AND bounty_segment_map.page_id = dcm_spectra_merge_agg_w_innovid.page_id

WHERE
  dcm_spectra_merge_agg_w_innovid.brand = 'bounty'
;