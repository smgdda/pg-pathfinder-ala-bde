﻿CREATE EXTERNAL TABLE pf_placement_segment_map
(
  brand string, 
  pfcampaign string, 
  buy_id bigint, 
  buy string, 
  page_id bigint, 
  site_placement string, 
  last string, 
  extracted_segment string, 
  bounty_target string
)
ROW FORMAT DELIMITED 
  FIELDS TERMINATED BY '\t' 
STORED AS INPUTFORMAT 
  'org.apache.hadoop.mapred.TextInputFormat' 
OUTPUTFORMAT 
  'org.apache.hadoop.hive.ql.io.HiveIgnoreKeyTextOutputFormat'
LOCATION
  'hdfs://vmcwy62059.prod.acxiom.net:8020/user/owatson/external-table-data/pf-placement-segment-map'
;