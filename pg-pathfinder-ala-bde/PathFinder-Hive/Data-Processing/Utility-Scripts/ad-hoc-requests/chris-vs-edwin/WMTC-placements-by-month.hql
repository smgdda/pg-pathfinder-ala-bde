﻿select 
date_part(month, agency_views_mbox.digital_placement.placement_created_date) as mth
, date_part(year, agency_views_mbox.digital_placement.placement_created_date) as yr
, count(*)
from
agency_views_mbox.digital_placement
where
agency_views_mbox.digital_placement.client_id = 'WMTC'
group by
date_part(month, agency_views_mbox.digital_placement.placement_created_date)
, date_part(year, agency_views_mbox.digital_placement.placement_created_date)
order by
date_part(month, agency_views_mbox.digital_placement.placement_created_date)
, date_part(year, agency_views_mbox.digital_placement.placement_created_date)
