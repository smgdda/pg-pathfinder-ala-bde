﻿select
*
from

(
	select
		dayseqnum as chris_date
		, count(distinct vendor_name) as unique_chris_vendors
		, count(distinct placementID) as unique_chris_placements
		, count(distinct creativeid) as unique_chris_creatives
		, sum(convert(bigint, impressions)) as chris_imps
		, count(*) as day_records_chris
	from
		Chris_table
	group by
		dayseqnum
) Chris_dates

left outer join
(
	select distinct
		CONVERT(varchar(50), CONVERT(DATE, DayDate), 112) as edwin_date
		, count(distinct Vendor_Name) as unique_edwin_vendors
		, count(distinct Placement) as unique_edwin_placements
		, count(distinct CreativeID) as unique_edwin_creatives
		, sum(convert(bigint, impressions)) as edwin_imps
		, count(*) as day_records_edwin
	from
		Edwin_table
	group by
		CONVERT(varchar(50), CONVERT(DATE, DayDate), 112)
) Edwin_dates
on
	Edwin_dates.edwin_date = Chris_dates.chris_date

order by 
	chris_date
