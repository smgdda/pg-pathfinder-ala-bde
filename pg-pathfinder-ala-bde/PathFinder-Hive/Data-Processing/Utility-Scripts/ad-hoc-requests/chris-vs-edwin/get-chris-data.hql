﻿--select
--sum(chris_results.impressions)
--, sum(chris_results.net_cost)
--from
--(

select 
  --left(dcm.dayseqnum,4) as year, 
  --extract(month from to_date(dcm.dayseqnum::varchar(10), 'yyyymmdd')) as month, 
  dcm.dayseqnum,
  mbox.vendor_name,
  mbox.placement_reporting_type,
  mbox.adserver_placement_id,
  dcm.placementid, 
  ctv.creative,
  dcm.creativeid,
  sum(dcm.impressions) as impressions, 
  cast(sum(dcm.mediacost) as integer) as net_cost

from 
  agency_views_dcm.factimpressionsclickscost dcm

join 
  agency_views_dcm.dimsite site 
  on dcm.siteid = site.siteid

join 
  agency_views_dcm.dimcreative ctv
  on  ctv.creativeid = dcm.creativeid

join 
  agency_views_mbox.digital_placement mbox
  on mbox.adserver_placement_id = dcm.placementid

where 
  dcm.dayseqnum between 20150601 and 20150831  
  and dcm.advertiserid = 1559799
  --And mbox.Vendor_Name Not In ('FACEBOOK INC','FACEBOOK, INC., THE','KENSHOO INC')
  --And mbox.Placement_Reporting_Type <>'Non-Working Media'

  --remove facebook and kenshoo counts
  --and site.site

group by 
  dcm.dayseqnum, mbox.vendor_name, mbox.placement_reporting_type, mbox.adserver_placement_id, dcm.placementid, ctv.creative, dcm.creativeid

order by 
  dcm.placementid DESC


--) chris_results