﻿        Select
          mbox.Vendor_Name,
          d.Site As Publication,
          mbox.Placement_Name As Placement,
          f.Creative,
          f.CreativeID,
          c.Campaign,
          a.PlacementID,
          b.DayDate,
          Case
      	    When mbox.Placement_Rate_Type='CPC' Then 
              cast ((mbox.Rate*cast( a.Clicks As Decimal (18,2)) ) as Decimal (18,2))
      	    When mbox.Placement_Rate_Type='CPM' Then 
              cast((mbox.Rate*(cast(a.Impressions As Decimal (18,4))/1000.0000)) as Decimal(18,2))
      	    When mbox.Placement_Rate_Type In ('Flat Rate Clicks','Flat Rate Impressions') Then 
              (CAST((CAST(CASE WHEN SYSDATE > e.PlacementEndDate THEN e.PlacementEndDate ELSE SYSDATE END as DATE) - e.PlacementStartDate) + 1 as Decimal(18,2))/cast(((e.PlacementEndDate - e.PlacementStartDate)+1) As Decimal(18,2)))* mbox.Planned_Cost
      	    Else 0
          End As Net_Cost,
          sum(a.MEDIACOST) As DFA_Media_Cost,
          sum(a.Impressions) As Impressions,
          sum(a.Clicks) As Clicks

        From  
          AGENCY_VIEWS_DCM.FACTIMPRESSIONSCLICKSCOST a

        Inner Join 
          AGENCY_VIEWS_DCM.DIM_DAY b
          On a.DAYSEQNUM=b.DATENUM

        Join 
          AGENCY_VIEWS_DCM.DIMCAMPAIGN c
    	    On a.CampaignID=c.CampaignID
        Join 
          AGENCY_VIEWS_DCM.DIMSITE d
    	    On a.SiteID=d.SiteID
        Join 
          AGENCY_VIEWS_DCM.DIMPLACEMENT e
    	    On a.PlacementID=e.PlacementID
        Join 
          AGENCY_VIEWS_DCM.DIMCREATIVE f
    	    On a.CreativeID=f.CreativeID
        Join 
          AGENCY_VIEWS_DCM.DIMAD g
    	    On a.ADID=g.ADID

        Join
          (
  		    select Distinct 
            a.Campaign_ID
            ,a.Placement_ID
            ,a.Placement_Name
            ,b.Placement_Reporting_Type
            ,a.Vendor_Name
            ,a.DFA_SITE_NAME
            ,b.Product_Name
            ,b.Product_ID
            ,a.Group_Name
            ,a.Package_Details
            ,b.UDF3
            ,a.Placement_Rate_Type
            ,a.Rate
            ,a.Planned_Cost
        
  		    from
            AGENCY_VIEWS_MBOX.SMG_PACING_V2 a
        
  		    Left Join
            (
  				    Select Distinct 
                cast(a.ADSERVER_PLACEMENT_ID AS BIGINT) as Placement_ID
                ,a.Placement_Name
                ,a.Placement_Reporting_Type
                ,a.Product_Name
                ,a.Product_ID
                ,a.Group_Name
                ,b.UDF3
  				    From
                AGENCY_VIEWS_MBOX.DIGITAL_PLACEMENT a
  				    Left Join
                AGENCY_VIEWS_MBOX.DIGITAL_DETAIL b
  					    On  (
                  a.CLIENT_ID = b.CLIENT_ID
  						    and a.PRODUCT_ID = b.PRODUCT_ID
  						    and a.VENDOR_ID =b.VENDOR_ID
  						    and a.loc_id = b.loc_id
  						    and a.INSERTION_ORDER_ID = b.INSERTION_ORDER_ID
                )
  				    Where
                regexp_count(a.ADSERVER_PLACEMENT_ID::text, '(^-?[0-9]+)'::character varying::text) =1
                And a.Client_ID In ('WMS','WMH','WMT','WMC','WMTC')
  				    Group By
                a.ADSERVER_PLACEMENT_ID
                ,a.Placement_Name
                ,a.Placement_Reporting_Type
                ,a.Product_Name
                ,a.Product_ID
                ,a.Group_Name
                ,b.UDF3
  			    ) b
            On a.Placement_ID=b.Placement_ID
  		    Where
            a.Client_ID In ('WMS','WMH','WMT','WMC','WMTC')
  			    And a.Package_Details In ('Non-Package')
  		    Group By
  			    a.Campaign_ID,a.Placement_ID,a.Placement_Name,b.Placement_Reporting_Type,a.Vendor_Name,a.DFA_SITE_NAME,b.Product_Name,b.Product_ID,a.Group_Name,a.Package_Details,b.UDF3,
  			    a.Placement_Rate_Type,a.Rate,a.Planned_Cost

          Union All

  		    select Distinct 
            a.Campaign_ID
            ,a.Placement_ID
            ,a.Placement_Name
            ,b.Placement_Reporting_Type
            ,a.Vendor_Name
            ,a.DFA_SITE_NAME
            ,b.Product_Name
            ,b.Product_ID
            ,a.Group_Name
            ,a.Package_Details
            ,b.UDF3
            ,c.Placement_Rate_Type
            ,c.Rate,c.Planned_Cost
  		
          from
            AGENCY_VIEWS_MBOX.SMG_PACING_V2 a
        
  		    Left Join
            (
  				    Select Distinct 
                cast(a.ADSERVER_PLACEMENT_ID AS BIGINT) as Placement_ID
                ,a.Placement_Name
                ,a.Placement_Reporting_Type
                ,a.Product_Name
                ,a.Product_ID
                ,a.Group_Name
                ,b.UDF3
            
  				    From
                AGENCY_VIEWS_MBOX.DIGITAL_PLACEMENT a
            
  				    Left Join
                AGENCY_VIEWS_MBOX.DIGITAL_DETAIL b
  					    On  (
                  a.CLIENT_ID = b.CLIENT_ID
  						    and a.PRODUCT_ID = b.PRODUCT_ID
  						    and a.VENDOR_ID =b.VENDOR_ID
  						    and a.loc_id = b.loc_id
  						    and a.INSERTION_ORDER_ID = b.INSERTION_ORDER_ID)
  				    Where
                regexp_count(a.ADSERVER_PLACEMENT_ID::text, '(^-?[0-9]+$)'::character varying::text) =1
  					    And a.Client_ID In ('WMS','WMH','WMT','WMC','WMTC')
  		        Group By
                a.ADSERVER_PLACEMENT_ID
                ,a.Placement_Reporting_Type
                ,a.Placement_Name
                ,a.Product_Name
                ,a.Product_ID
                ,a.Group_Name
                ,b.UDF3
  			    ) b
            On a.Placement_ID=b.Placement_ID

  		    Left Join
            (
  				    select Distinct
                Campaign_ID
                ,Group_Name
                ,Placement_Rate_Type
                ,Rate
                ,Planned_Cost
  				    from 
                AGENCY_VIEWS_MBOX.SMG_PACING_V2 a
  				    Where
                Client_ID In ('WMS','WMH','WMT','WMC','WMTC')
  					    And Package_Details ='Parent'
  			    ) c
            On (
              a.Campaign_ID= c.Campaign_ID
  				    And a.Group_Name = c.Group_Name
            )

  		    Where
            a.Client_ID In ('WMS','WMH','WMT','WMC','WMTC')
  			    And a.Package_Details In ('Child')
          
  		    Group By
            a.Campaign_ID
            ,a.Placement_ID
            ,a.Placement_Name
            ,b.Placement_Reporting_Type
            ,a.Vendor_Name
            ,a.DFA_SITE_NAME
            ,b.Product_Name
            ,b.Product_ID
            ,a.Group_Name
            ,a.Package_Details
            ,b.UDF3
            ,c.Placement_Rate_Type
            ,c.Rate
            ,c.Planned_Cost
  		    ) mbox
          On a.PlacementID = mbox.Placement_ID

  	    Where 
          a.dayseqnum between 20150601 and 20150831
  		    And a.AdvertiserID = 1559799
  	    Group By
  		    mbox.Vendor_Name,
  		    d.Site,
  		    mbox.Placement_Name,
  		    f.Creative,
  		    f.CreativeID,
  		    c.Campaign,
  		    a.PlacementID,
  		    b.DayDate,
  		    Case
  			     When mbox.Placement_Rate_Type='CPC' Then 
  				    cast ((mbox.Rate*cast( a.Clicks As Decimal (18,2)) ) as Decimal (18,2))
  			     When mbox.Placement_Rate_Type='CPM' Then 
  				    cast((mbox.Rate*(cast(a.Impressions As Decimal (18,4))/1000.0000)) as Decimal(18,2))
  			     When mbox.Placement_Rate_Type In ('Flat Rate Clicks','Flat Rate Impressions') Then 
             (CAST((CAST(CASE WHEN SYSDATE > e.PlacementEndDate THEN e.PlacementEndDate ELSE SYSDATE END as DATE) - e.PlacementStartDate) + 1 as Decimal(18,2))/cast(((e.PlacementEndDate - e.PlacementStartDate)+1) As Decimal(18,2)))* mbox.Planned_Cost
  			     Else 0
  		    End
