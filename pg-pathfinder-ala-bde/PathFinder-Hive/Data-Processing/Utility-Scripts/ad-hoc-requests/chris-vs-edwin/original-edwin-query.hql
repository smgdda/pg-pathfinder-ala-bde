﻿--select
--count(*) as record_ct
--, sum(edwin_results.Impressions) as imp_sum
--, sum(edwin_results.Net_Cost) as cost_net_sum
--, sum(edwin_results.DFA_Media_Cost) as cost_dcm_sum
--from
--(

Select 
  --'WAL-MART STORES, INC' As Client_Name,
  --'Digital' As Media_Group,
  --'Display' As Media_Type,
  --Case 
  --  When mbox.Placement_Name iLike '%hispanic%' and mbox.Placement_Reporting_Type In ('Other Display','Display Direct From Publisher','Display Ad Network','Display Ad Exchange') Then 'Display Hispanic MBox'
  --  When mbox.Placement_Name iLike '%hispanic%' and mbox.Placement_Reporting_Type In ('Other Digital') Then 'Display Hispanic Other'
  --  When mbox.Placement_Name iLike '%hispanic%' and mbox.Placement_Reporting_Type In ('Paid Search','Mobile Search') Then 'Search Hispanic'
  --  When mbox.Placement_Name iLike '%hispanic%' and mbox.Placement_Reporting_Type In ('Social Direct From Publisher','Social Ad Network','Social Ad Exchange','Other Social') Then 'Display Hispanic Social'
  --  When mbox.Placement_Name iLike '%hispanic%' and mbox.Placement_Reporting_Type In ('Video Direct From Publisher','Video Ad Network','Video Ad Exchange','Video (Upfront)','Video (Scatter)') Then 'Display Hispanic Digital Video'
  --  When mbox.Placement_Name iLike '%hispanic%' and mbox.Placement_Reporting_Type In ('Other Mobile','Mobile Direct From Publisher','Mobile Ad Network') Then 'Display Hispanic Mobile'
  --  When mbox.Placement_Name iLike '%hispanic%' and mbox.Placement_Reporting_Type = 'Email Marketing' Then 'Hispanic Email'
  --  When mbox.Placement_Name iLike '%hispanic%' and d.Site = 'WLMRTCOMOX14400' Then 'Display Hispanic Dotcom'  
  --  When mbox.Placement_Reporting_Type In ('Other Display','Display Direct From Publisher','Display Ad Network','Display Ad Exchange') Then 'Display MBox'
  --  When mbox.Placement_Reporting_Type In ('Other Digital') Then 'Display Other'
  --  When mbox.Placement_Reporting_Type In ('Paid Search','Mobile Search') Then 'Search'
  --  When mbox.Placement_Reporting_Type In ('Social Direct From Publisher','Social Ad Network','Social Ad Exchange','Other Social') Then 'Display Social'
  --  When mbox.Placement_Reporting_Type In ('Video Direct From Publisher','Video Ad Network','Video Ad Exchange','Video (Upfront)','Video (Scatter)') Then 'Display Digital Video'
  --  When mbox.Placement_Reporting_Type In ('Other Mobile','Mobile Direct From Publisher','Mobile Ad Network') Then 'Display Mobile'
  --  When mbox.Placement_Reporting_Type = 'Email Marketing' Then 'Email'
  --  When d.Site = 'WLMRTCOMOX14400' Then 'Display Dotcom'
  --  Else 'Display MBox'
  -- End As  Media_SubType,
  -- '' As DMA_Code,
  --'' As Market,
  --'' As ISCI_Code,
  --'' As AFFID_ISCI,
  --'' As AFFID_ISCI_Name,
  --'' As HH_Ratings,
  --'' As WM1849_Ratings,
  --'' As MN1834_Ratings,
  --'' As PP914_Ratings,
  --'' As A1849_Ratings,
  --'' As HH_HISPANIC_Ratings,
  --'' As Supplier,
  --'' As Account,
  --'' As Search_Campaign,
  --'' As Account_ID,
  --'' As Daypart,
  --'' As Length,
  --'' As Network_Name,
  mbox.Placement_Reporting_Type,
  mbox.Vendor_Name,
  d.Site As Publication,
  mbox.Placement_Name As Placement,
  --mbox.UDF3 As IPP,
  --mbox.Product_Name As Event_Code,
  --mbox.Product_ID As Product_Code,
  f.Creative,
  f.CreativeID,
  --g.ADID,
  c.Campaign,
  a.PlacementID,
  --To_Char(b.DayDate, 'WW') As Week,
  b.DayDate,
  Case 
  		When mbox.Placement_Rate_Type= 'CPC' Then cast ((mbox.Rate*cast( a.Clicks As Decimal (18,2)) ) as Decimal (18,2))
  		When mbox.Placement_Rate_Type= 'CPM' Then cast((mbox.Rate*(cast(a.Impressions As Decimal (18,4))/1000.0000)) as Decimal(18,2)) 
  		When mbox.Placement_Rate_Type In ('Flat Rate Clicks','Flat Rate Impressions') 
      Then (CAST((CAST(CASE WHEN SYSDATE > e.PlacementEndDate THEN e.PlacementEndDate ELSE SYSDATE END as DATE) - e.PlacementStartDate) + 1 as Decimal(18,2))/cast(((e.PlacementEndDate - e.PlacementStartDate)+1) As Decimal(18,2)))* mbox.Planned_Cost 
  		Else 0 
  End As Net_Cost,
  sum(a.MEDIACOST) As DFA_Media_Cost,
  sum(a.Impressions) As Impressions,
  sum(a.Clicks) As Clicks

From
  AGENCY_VIEWS_DCM.FACTIMPRESSIONSCLICKSCOST a

Inner Join AGENCY_VIEWS_DCM.DIM_DAY b 
  On a.DAYSEQNUM=b.DATENUM

Join AGENCY_VIEWS_DCM.DIMCAMPAIGN c
  On a.CampaignID=c.CampaignID

Join AGENCY_VIEWS_DCM.DIMSITE d
  On a.SiteID=d.SiteID

Join AGENCY_VIEWS_DCM.DIMPLACEMENT e
  On a.PlacementID=e.PlacementID

Join AGENCY_VIEWS_DCM.DIMCREATIVE f
  On a.CreativeID=f.CreativeID

Join AGENCY_VIEWS_DCM.DIMAD g
  On a.ADID=g.ADID

Join 
  (
-- region
    select Distinct 
      a.Campaign_ID,a.Placement_ID,a.Placement_Name,b.Placement_Reporting_Type,a.Vendor_Name,
      a.DFA_SITE_NAME,b.Product_Name,b.Product_ID,a.Group_Name,a.Package_Details,b.UDF3,
      a.Placement_Rate_Type,a.Rate,a.Planned_Cost
    from 
      AGENCY_VIEWS_MBOX.SMG_PACING_V2 a
    Left Join
      (
-- region
        Select Distinct cast(a.ADSERVER_PLACEMENT_ID AS BIGINT) as Placement_ID,a.Placement_Name,a.Placement_Reporting_Type,a.Product_Name,a.Product_ID,a.Group_Name,b.UDF3
        From AGENCY_VIEWS_MBOX.DIGITAL_PLACEMENT a
        Left Join 
          AGENCY_VIEWS_MBOX.DIGITAL_DETAIL b
         	On  (
            a.CLIENT_ID = b.CLIENT_ID
            and a.PRODUCT_ID = b.PRODUCT_ID
            and a.VENDOR_ID =b.VENDOR_ID
            and a.loc_id = b.loc_id
            and a.INSERTION_ORDER_ID = b.INSERTION_ORDER_ID)
        Where
          regexp_count(a.ADSERVER_PLACEMENT_ID::text, '(^-?[0-9]+$)'::character varying::text) = 1
          -- WMTC added for 2015
          And a.Client_ID In ('WMS','WMH','WMT','WMC', 'WMTC')
        Group By
          a.ADSERVER_PLACEMENT_ID,a.Placement_Name,a.Placement_Reporting_Type,a.Product_Name,a.Product_ID,a.Group_Name,b.UDF3
-- endregion
      ) b  On a.Placement_ID=b.Placement_ID

    Where
      -- WMTC added for 2015
      a.Client_ID In ('WMS','WMH','WMT','WMC', 'WMTC')
      And a.Package_Details In ('Non-Package')
    Group By
      a.Campaign_ID,a.Placement_ID,a.Placement_Name,b.Placement_Reporting_Type,a.Vendor_Name,a.DFA_SITE_NAME,b.Product_Name,b.Product_ID,a.Group_Name,a.Package_Details,b.UDF3,
      a.Placement_Rate_Type,a.Rate,a.Planned_Cost
-- endregion
                          
    Union All
                          
-- region
    select Distinct 
      a.Campaign_ID,a.Placement_ID,a.Placement_Name,b.Placement_Reporting_Type,a.Vendor_Name,a.DFA_SITE_NAME,b.Product_Name,b.Product_ID,a.Group_Name,a.Package_Details,b.UDF3,
      c.Placement_Rate_Type,c.Rate,c.Planned_Cost
    from 
      AGENCY_VIEWS_MBOX.SMG_PACING_V2 a
    Left Join
      (
-- region
        Select Distinct
          cast(a.ADSERVER_PLACEMENT_ID AS BIGINT) as Placement_ID,a.Placement_Name,a.Placement_Reporting_Type,a.Product_Name,a.Product_ID,a.Group_Name,b.UDF3
        From
          AGENCY_VIEWS_MBOX.DIGITAL_PLACEMENT a
        Left Join
          AGENCY_VIEWS_MBOX.DIGITAL_DETAIL b
          On  (
            a.CLIENT_ID = b.CLIENT_ID
            and a.PRODUCT_ID = b.PRODUCT_ID
            and a.VENDOR_ID =b.VENDOR_ID
            and a.loc_id = b.loc_id
            and a.INSERTION_ORDER_ID = b.INSERTION_ORDER_ID)
        Where
          regexp_count(a.ADSERVER_PLACEMENT_ID::text, '(^-?[0-9]+$)'::character varying::text) = 1
          -- WMTC added for 2015
          And a.Client_ID In ('WMS','WMH','WMT','WMC','WMTC')
        Group By
          a.ADSERVER_PLACEMENT_ID,a.Placement_Reporting_Type,a.Placement_Name,a.Product_Name,a.Product_ID,a.Group_Name,b.UDF3
-- endregion
      ) b  On a.Placement_ID=b.Placement_ID

    Left Join 
      (
        select Distinct Campaign_ID,Group_Name,Placement_Rate_Type,Rate,Planned_Cost
        from AGENCY_VIEWS_MBOX.SMG_PACING_V2 a
        -- WMTC added for 2015
          Where Client_ID In ('WMS','WMH','WMT','WMC','WMTC')
        And Package_Details = 'Parent'
      ) c 
      On (
        a.Campaign_ID= c.Campaign_ID
        And a.Group_Name = c.Group_Name)

    Where
      -- WMTC added for 2015
      a.Client_ID In ('WMS','WMH','WMT','WMC','WMTC')
      And a.Package_Details In ('Child')
    Group By
      a.Campaign_ID,a.Placement_ID,a.Placement_Name,b.Placement_Reporting_Type,a.Vendor_Name,a.DFA_SITE_NAME,b.Product_Name,b.Product_ID,a.Group_Name,a.Package_Details,b.UDF3,
      c.Placement_Rate_Type,c.Rate,c.Planned_Cost
-- endregion
  ) mbox 
  On
  -- the following 2 conditions on the mbox dataset were moved from the where clause because right table conditions in where clauses force inner joins
  mbox.Vendor_Name Not In ('FACEBOOK INC','FACEBOOK, INC., THE','KENSHOO INC')
  And mbox.Placement_Reporting_Type not in ('Non-Working Media')
  And a.PlacementID = mbox.Placement_ID
 
Where 
  a.dayseqnum between 20150601 and 20150831 
  And a.AdvertiserID=1559799
  
Group By 
  --  1,2,3,4,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,
  mbox.Placement_Reporting_Type,
  mbox.Vendor_Name,
  d.Site,
  mbox.Placement_Name,
  --  mbox.UDF3,
  --  mbox.Product_Name,
  --  mbox.Product_ID,
  f.Creative,
  f.CreativeID,
  --  g.ADID,
  c.Campaign,
  a.PlacementID,
  --  To_Char(b.DayDate, 'WW'),
  b.DayDate,
  Case 
		When mbox.Placement_Rate_Type= 'CPC' Then cast ((mbox.Rate*cast( a.Clicks As Decimal (18,2)) ) as Decimal (18,2))
		When mbox.Placement_Rate_Type= 'CPM' Then cast((mbox.Rate*(cast(a.Impressions As Decimal (18,4))/1000.0000)) as Decimal(18,2)) 
		When mbox.Placement_Rate_Type In ('Flat Rate Clicks','Flat Rate Impressions') Then (CAST((CAST(CASE WHEN SYSDATE > e.PlacementEndDate THEN e.PlacementEndDate ELSE SYSDATE END as DATE) - e.PlacementStartDate) + 1 as Decimal(18,2))/cast(((e.PlacementEndDate - e.PlacementStartDate)+1) As Decimal(18,2)))* mbox.Planned_Cost 
		Else 0 
  End

--) edwin_results