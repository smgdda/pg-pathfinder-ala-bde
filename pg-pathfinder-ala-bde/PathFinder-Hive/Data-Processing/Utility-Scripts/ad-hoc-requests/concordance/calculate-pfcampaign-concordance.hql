﻿insert into table pf_concordance_log
select
  '${hiveconf:pf_campaign_name}' as pfcampaign
  , '${hiveconf:woc_spec}' as woc_spec
--  , set_a_conv
--  , set_a_score > set_b_score as apdf_gt_bpdf
--  , set_a_score < set_b_score as apdf_lt_bpdf
--  , set_a_score = set_b_score as apdf_eq_bpdf
--  , set_a_t2c > set_b_t2c as at2c_gt_bt2c
--  , set_a_t2c < set_b_t2c as at2c_lt_bt2c
--  , set_a_t2c = set_b_t2c as at2c_eq_bt2c
  , sum(case 
        when set_a_conv = 0 and set_a_t2c > set_b_t2c and set_a_score < set_b_score then 1 
        when set_a_conv = 1 and set_a_t2c > set_b_t2c and set_a_score < set_b_score then 1 
        when set_a_conv = 1 and set_a_t2c < set_b_t2c and set_a_score > set_b_score then 1 
        else 0 end) as concordant
  , sum(case 
        when set_a_conv = 0 and set_a_t2c > set_b_t2c and set_a_score > set_b_score then 1 
        when set_a_conv = 1 and set_a_t2c > set_b_t2c and set_a_score > set_b_score then 1 
        when set_a_conv = 1 and set_a_t2c < set_b_t2c and set_a_score < set_b_score then 1 
        else 0 end) as discordant
  , sum(case
        when set_a_score = set_b_score then 1 
        else 0 end) as pdf_equal_disqual
  , sum(case 
        when set_a_t2c = set_b_t2c then 1
        else 0 end) as t2c_equal_disqual
  , sum(case
        when set_a_conv = 0 and set_a_t2c < set_b_t2c then 1
        else 0 end) as censor_disqual
from
  (
  select
    pf_concordance_input.time_weighted_pdf_sum as set_a_score
    , pf_concordance_input.max_t2c as set_a_t2c
    , pf_concordance_input.conv_user as set_a_conv
  from
    pf_concordance_input
  where
    pfcampaign = '${hiveconf:pf_campaign_name}'
  ) set_a
cross join
  (
  select
    time_weighted_pdf_sum as set_b_score
    , max_t2c as set_b_t2c
  from
    pf_concordance_input
  where
    conv_user = 1
    and pfcampaign = '${hiveconf:pf_campaign_name}'
  ) set_b

-- group by
--   set_a_conv
--   , set_a_score > set_b_score
--   , set_a_score < set_b_score
--   , set_a_score = set_b_score
--   , set_a_t2c > set_b_t2c
--   , set_a_t2c < set_b_t2c
--   , set_a_t2c = set_b_t2c
;
