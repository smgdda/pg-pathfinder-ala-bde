﻿SET hive.enforce.bucketing=true; 
SET hive.optimize.bucketmapjoin=true; 
SET hive.exec.compress.output=true;
SET hive.exec.compress.intermediate=true;
SET hive.exec.dynamic.partition=true; 
SET hive.exec.dynamic.partition.mode=nonstrict; 
SET hive.exec.parallel=true;
SET hive.auto.convert.join=true;
SET mapred.compress.map.output=true;
SET mapred.output.compression.type=BLOCK;
SET mapred.output.compression.codec=org.apache.hadoop.io.compress.SnappyCodec;

DROP TABLE IF EXISTS pf_concordance_input;
CREATE  TABLE pf_concordance_input
(
  dfa_user_id string, 
  conv_user int, 
  max_freq int, 
  max_t2c double, 
  pdf_sum double, 
  time_weighted_pdf_sum double
)
PARTITIONED BY
(
  pfcampaign string
)
ROW FORMAT SERDE 
  'org.apache.hadoop.hive.serde2.columnar.ColumnarSerDe' 
STORED AS INPUTFORMAT 
  'org.apache.hadoop.hive.ql.io.RCFileInputFormat' 
OUTPUTFORMAT 
  'org.apache.hadoop.hive.ql.io.RCFileOutputFormat'
;

INSERT OVERWRITE TABLE pf_concordance_input PARTITION(pfcampaign)
SELECT
  dfa_user_id
  , conv_user
  , max(freq) as max_freq
  , max(t2c) as max_t2c
  , sum(freq_pdf) as pdf_sum
  , sum(pfscore_log) as time_weighted_pdf_sum
  , pfcampaign

FROM
  logs_product

GROUP BY
  pfcampaign
  , dfa_user_id
  , conv_user
;
