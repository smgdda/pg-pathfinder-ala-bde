﻿create table pf_concordance_log
(
  pfcampaign string
  , woc_spec string
  , concordant bigint
  , discordant bigint
  , pdf_equal_disqual bigint
  , t2c_equal_disqual bigint
  , censor_disqual bigint
)
;
