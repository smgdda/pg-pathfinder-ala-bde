﻿CREATE TABLE MOAT_DATA_REVIEW
(
  buy_id string
  , event_dt string
  , site_id string
  , page_id string
  , site_placement string
  , U_from_site_data string
  , m_from_site_data string
  , iv_from_site_data string
  , v_from_site_data string
  , um_from_site_data string
  , tuv_from_site_data string
  , tet_from_site_data string
  , h_from_site_data string
  , th_from_site_data string
  , bfa_from_site_data string
  , apd_from_site_data string
  , level_1_id_from_site_data string
  , level_2_id_from_site_data string
  , level_3_id_from_site_data string
  , level_4_id_from_site_data string
  , slicer_1_id_from_site_data string
  , slicer_2_id_from_site_data string
  , d_from_site_data string
  , sp_from_site_data string
  , t_from_site_data string
)
ROW FORMAT SERDE 
  'org.apache.hadoop.hive.serde2.columnar.ColumnarSerDe' 
STORED AS INPUTFORMAT 
  'org.apache.hadoop.hive.ql.io.RCFileInputFormat' 
OUTPUTFORMAT 
  'org.apache.hadoop.hive.ql.io.RCFileOutputFormat'
;