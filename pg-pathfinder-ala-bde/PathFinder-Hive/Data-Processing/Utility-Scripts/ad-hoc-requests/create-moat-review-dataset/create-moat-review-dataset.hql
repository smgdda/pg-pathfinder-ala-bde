﻿CREATE TABLE IF NOT EXISTS MOAT_DATA_REVIEW AS
SELECT
  i.buy_id
  , to_date(FROM_UNIXTIME(UNIX_TIMESTAMP(i.time, 'MM-dd-yyyy-HH:mm:ss'))) as event_dt
  , i.site_id
  , i.page_id
  , p.site_placement
  , str_to_map(i.site_data, '&', '=')["U"] as U_from_site_data
  , str_to_map(i.site_data, '&', '=')["m"] as m_from_site_data
  , str_to_map(i.site_data, '&', '=')["iv"] as iv_from_site_data
  , str_to_map(i.site_data, '&', '=')["v"] as v_from_site_data
  , str_to_map(i.site_data, '&', '=')["um"] as um_from_site_data
  , str_to_map(i.site_data, '&', '=')["tuv"] as tuv_from_site_data
  , str_to_map(i.site_data, '&', '=')["tet"] as tet_from_site_data
  , str_to_map(i.site_data, '&', '=')["h"] as h_from_site_data
  , str_to_map(i.site_data, '&', '=')["th"] as th_from_site_data
  , str_to_map(i.site_data, '&', '=')["bfa"] as bfa_from_site_data
  , str_to_map(i.site_data, '&', '=')["apd"] as apd_from_site_data
  , str_to_map(i.site_data, '&', '=')["L1id"] as L1id_from_site_data
  , str_to_map(i.site_data, '&', '=')["L2id"] as L2id_from_site_data
  , str_to_map(i.site_data, '&', '=')["L3id"] as L3id_from_site_data
  , str_to_map(i.site_data, '&', '=')["L4id"] as L4id_from_site_data
  , str_to_map(i.site_data, '&', '=')["S1id"] as S1id_from_site_data
  , str_to_map(i.site_data, '&', '=')["S2id"] as S2id_from_site_data
  , str_to_map(i.site_data, '&', '=')["d"] as d_from_site_data
  , str_to_map(i.site_data, '&', '=')["sp"] as sp_from_site_data
  , str_to_map(i.site_data, '&', '=')["t"] as t_from_site_data

FROM
  (
    SELECT
      page_id
      , site_placement
      , buy_id
    FROM
      dfa_meta_page 
    WHERE
      buy_id in (8612634, 8609548, 8612570, 8610435)
  ) p

JOIN
  (
  select buy_id, site_id, page_id, site_data, ingestion_dt, time
  from
  ingestion_dfa_impressions
  ) i
on (i.ingestion_dt > '${hiveconf:begin_dt}' and i.ingestion_dt <= '${hiveconf:end_dt}' and i.page_id = p.page_id)

GROUP BY
  i.buy_id
  , to_date(FROM_UNIXTIME(UNIX_TIMESTAMP(i.time, 'MM-dd-yyyy-HH:mm:ss')))
  , i.site_id
  , i.page_id
  , p.site_placement
  , str_to_map(i.site_data, '&', '=')["U"]
  , str_to_map(i.site_data, '&', '=')["m"]
  , str_to_map(i.site_data, '&', '=')["iv"]
  , str_to_map(i.site_data, '&', '=')["v"]
  , str_to_map(i.site_data, '&', '=')["um"]
  , str_to_map(i.site_data, '&', '=')["tuv"]
  , str_to_map(i.site_data, '&', '=')["tet"]
  , str_to_map(i.site_data, '&', '=')["h"]
  , str_to_map(i.site_data, '&', '=')["th"]
  , str_to_map(i.site_data, '&', '=')["bfa"]
  , str_to_map(i.site_data, '&', '=')["apd"]
  , str_to_map(i.site_data, '&', '=')["L1id"]
  , str_to_map(i.site_data, '&', '=')["L2id"]
  , str_to_map(i.site_data, '&', '=')["L3id"]
  , str_to_map(i.site_data, '&', '=')["L4id"]
  , str_to_map(i.site_data, '&', '=')["S1id"]
  , str_to_map(i.site_data, '&', '=')["S2id"]
  , str_to_map(i.site_data, '&', '=')["d"]
  , str_to_map(i.site_data, '&', '=')["sp"]
  , str_to_map(i.site_data, '&', '=')["t"]
;