﻿drop table if exists pf_dlx_campaign_names;

create table pf_dlx_campaign_names as
select distinct
pfcampaign
, dlx_campaign_name
, advertiser_id
from
pfcampaign_list
join dfa_meta_campaign
on dfa_meta_campaign.buy_id = pfcampaign_list.buy_id

where
dlx_campaign_name != ''
and date_dropped = ''
;
