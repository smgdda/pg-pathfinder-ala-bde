﻿-- create list of distinct users per pfcampaign, for campaigns with DLX

drop table if exists distinct_pfcampaign_users;

create table distinct_pfcampaign_users as
select distinct
logs_product.pfcampaign
, pf_dlx_campaign_names.advertiser_id
, logs_product.dfa_user_id as dcm_log_dfa_user_id
from
logs_product
join
pf_dlx_campaign_names
on lower(pf_dlx_campaign_names.pfcampaign) = lower(logs_product.pfcampaign)
;
