﻿drop table if exists cookies_in_recryption_table;

create table cookies_in_recryption_table as
select
  distinct_pfcampaign_users.pfcampaign
  , count(*) as total_dcm_log_pf_user_ct
  , sum(case when dlx_recrypted_cookies.newly_encrypted is not null then 1 else 0 end) as recrypted_cookie_ct
from
  distinct_pfcampaign_users
left outer join
  dlx_recrypted_cookies
  on dlx_recrypted_cookies.newly_encrypted = distinct_pfcampaign_users.dcm_log_dfa_user_id
  and dlx_recrypted_cookies.advertiser_id = distinct_pfcampaign_users.advertiser_id
group by
  distinct_pfcampaign_users.pfcampaign
;
