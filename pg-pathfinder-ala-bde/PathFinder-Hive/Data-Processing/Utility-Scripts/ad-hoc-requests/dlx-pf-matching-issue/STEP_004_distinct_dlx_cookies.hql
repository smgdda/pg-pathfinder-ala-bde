﻿create table distinct_dlx_cookies as
select distinct
  pf_dlx_campaign_names.advertiser_id
  , ingestion_dlx_weekly_sales_signal.dfa_id as dfa_macro_user_id_from_dlx
from
  pf_dlx_campaign_names
join
  ingestion_dlx_weekly_sales_signal
  on
  ingestion_dlx_weekly_sales_signal.ingestion_dt >= '2015-07-11'
  and lower(ingestion_dlx_weekly_sales_signal.pf_campaign) = lower(pf_dlx_campaign_names.dlx_campaign_name)