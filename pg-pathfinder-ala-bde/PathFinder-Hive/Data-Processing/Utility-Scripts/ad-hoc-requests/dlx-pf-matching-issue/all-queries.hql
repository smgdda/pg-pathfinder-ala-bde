﻿CREATE TABLE distinct_dlx_cookies
(
  advertiser_id BIGINT
  , dfa_Macro_user_id_from_dlx STRING
)
;

-- select distinct 
-- advertiser_id
-- , dfa_macro_user_id_from_dlx 
-- from 
-- distinct_dlx_cookies
-- where advertiser_id = 3232230;

