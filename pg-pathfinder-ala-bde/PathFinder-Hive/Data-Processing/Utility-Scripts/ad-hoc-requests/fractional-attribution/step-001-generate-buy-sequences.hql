﻿--drop table buy_sequences;
use smgdda.pf_kb_home;
create table pf_kb_home.kb_buy_sequences as
  
  select
    buy_id_1
    , 'creative_id' as dimension_type
    , sitetactic_1
    , sitetactic_2
    , sitetactic_3
    , sitetactic_4
    , sitetactic_5
    , sum(case when pf_score_1 is not null then pf_score_1 else 0 end) as pf_score_1
    , sum(case when pf_score_2 is not null then pf_score_2 else 0 end) as pf_score_2
    , sum(case when pf_score_3 is not null then pf_score_3 else 0 end) as pf_score_3
    , sum(case when pf_score_4 is not null then pf_score_4 else 0 end) as pf_score_4
    , sum(case when pf_score_5 is not null then pf_score_5 else 0 end) as pf_score_5
    , count(*) as seq_freq
  from
  (
    select
    user_id as uid_1
    , buy_id as buy_id_1
    , creative_id as sitetactic_1
    , case when pfscore_log is not null then pfscore_log else 0 end as pf_score_1

    from
    pf_kb_home.kbh_contentscale_pfscore
    
    where
    pfrank = 1
    and activity_flag = 1
    
  ) freq_1

  left outer join
  (
    select
    user_id as uid_2
    , buy_id as buy_id_2
    , creative_id as sitetactic_2
    , case when pfscore_log is not null then pfscore_log else 0 end as pf_score_2

    from
    pf_kb_home.kbh_contentscale_pfscore
    
    where
    pfrank = 2
    and activity_flag = 1
  ) freq_2
  on
  freq_2.uid_2 = freq_1.uid_1
  and freq_2.buy_id_2 = freq_1.buy_id_1

  left outer join
  (
    select
    user_id as uid_3
    , buy_id as buy_id_3
    , creative_id as sitetactic_3
    , case when pfscore_log is not null then pfscore_log else 0 end as pf_score_3

    from
    pf_kb_home.kbh_contentscale_pfscore
    
    where
    pfrank = 3
    and activity_flag = 1
  ) freq_3
  on
  freq_3.uid_3 = freq_1.uid_1
  and freq_3.buy_id_3 = freq_1.buy_id_1

  left outer join
  (
    select
    user_id as uid_4
    , buy_id as buy_id_4
    , creative_id as sitetactic_4
    , case when pfscore_log is not null then pfscore_log else 0 end as pf_score_4

    from
    pf_kb_home.kbh_contentscale_pfscore
    
    where
    pfrank = 4
    and activity_flag = 1
  ) freq_4
  on
  freq_4.uid_4 = freq_1.uid_1
  and freq_4.buy_id_4 = freq_1.buy_id_1

  left outer join
  (
    select
    user_id as uid_5
    , buy_id as buy_id_5
    , creative_id as sitetactic_5
    , case when pfscore_log is not null then pfscore_log else 0 end as pf_score_5

    from
    pf_kb_home.kbh_contentscale_pfscore
    
    where
    pfrank = 5
    and activity_flag = 1
  ) freq_5
  on
  freq_5.uid_5 = freq_1.uid_1
  and freq_5.buy_id_5 = freq_1.buy_id_1

group by
  buy_id_1
  , sitetactic_1
  , sitetactic_2
  , sitetactic_3
  , sitetactic_4
  , sitetactic_5
