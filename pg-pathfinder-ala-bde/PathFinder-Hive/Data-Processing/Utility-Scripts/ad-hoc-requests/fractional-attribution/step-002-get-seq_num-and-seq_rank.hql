﻿use smgdda.pf_kb_home;
create table pf_kb_home.kb_buy_sequences_ranked as
  
select 
rank() over (order by seq_freq desc, seq_score desc) as seq_rank
, * 
, (pf_score_1+pf_score_2+pf_score_3+pf_score_4+pf_score_5) as seq_score

from
pf_kb_home.kb_buy_sequences 

order by 
seq_rank asc
