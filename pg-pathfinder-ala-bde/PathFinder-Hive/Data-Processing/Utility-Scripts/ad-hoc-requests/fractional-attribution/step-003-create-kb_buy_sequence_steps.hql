﻿use smgdda.pf_kb_home;
drop table pf_kb_home.kb_buy_sequence_steps;
create table pf_kb_home.kb_buy_sequence_steps as
  
select 
seq_rank
, buy_id_1 as buy_id
, dimension_type
, sitetactic_1 as sitetactic
, pf_score_1 as pf_score
, (pf_score_1 / seq_score) as step_indexed_seq_score
, seq_freq * (pf_score_1 / seq_score) as step_conversions
, 1 as step_ordinal

from
pf_kb_home.kb_buy_sequences_ranked

where 
sitetactic_1 is not null

union all

select 
seq_rank
, buy_id_1 as buy_id
, dimension_type
, sitetactic_2 as sitetactic
, pf_score_2 as pf_score
, (pf_score_2 / seq_score) as step_indexed_seq_score
, seq_freq * (pf_score_2 / seq_score) as step_conversions
, 2 as step_ordinal

from
pf_kb_home.kb_buy_sequences_ranked

where 
sitetactic_2 is not null

union all

select 
seq_rank
, buy_id_1 as buy_id
, dimension_type
, sitetactic_3 as sitetactic
, pf_score_3 as pf_score
, (pf_score_3 / seq_score) as step_indexed_seq_score
, seq_freq * (pf_score_3 / seq_score) as step_conversions
, 3 as step_ordinal

from
pf_kb_home.kb_buy_sequences_ranked

where 
sitetactic_3 is not null

union all

select 
seq_rank
, buy_id_1 as buy_id
, dimension_type
, sitetactic_4 as sitetactic
, pf_score_4 as pf_score
, (pf_score_4 / seq_score) as step_indexed_seq_score
, seq_freq * (pf_score_4 / seq_score) as step_conversions
, 4 as step_ordinal

from
pf_kb_home.kb_buy_sequences_ranked

where 
sitetactic_4 is not null

union all

select 
seq_rank
, buy_id_1 as buy_id
, dimension_type
, sitetactic_5 as sitetactic
, pf_score_5 as pf_score
, (pf_score_5 / seq_score) as step_indexed_seq_score
, seq_freq * (pf_score_5 / seq_score) as step_conversions
, 5 as step_ordinal

from
pf_kb_home.kb_buy_sequences_ranked

where 
sitetactic_5 is not null

order by 
seq_rank asc
;
