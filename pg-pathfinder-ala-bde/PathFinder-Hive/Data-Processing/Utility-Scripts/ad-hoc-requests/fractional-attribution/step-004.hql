﻿use smgdda.pf_kb_home;
drop table pf_kb_home.kb_buy_seq_step_fract_conv;
create table pf_kb_home.kb_buy_seq_step_fract_conv as
  
SELECT 
  buy_id
  , sitetactic
--  , sum(pf_score) as sum_pf_score
  , sum(step_conversions) as fractional_conversions
--  , step_indexed_seq_score
--  , dimension_type
FROM
  smgdda.pf_kb_home.kb_buy_sequence_steps 
GROUP BY
  buy_id
  , sitetactic
;