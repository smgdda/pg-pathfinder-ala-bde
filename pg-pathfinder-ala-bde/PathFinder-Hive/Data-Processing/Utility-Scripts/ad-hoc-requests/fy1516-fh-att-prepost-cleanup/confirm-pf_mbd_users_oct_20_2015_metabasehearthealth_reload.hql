﻿select
survey_users.respondent_id
, pf_mbd_survey_users.dfa_user_id

from
(select distinct respondent_id from attitudinal_survey_responses_oct_20_2015 where question_id in (9321819,9321865,9321873,9321927,9321931)) survey_users

join
(select distinct dl_member_id, dfa_user_id, mbd_survey_id from pf_mbd_users_oct_20_2015_metabasehearthealth_reload) pf_mbd_survey_users
on 
  pf_mbd_survey_users.mbd_survey_id = 262634
  and
  pf_mbd_survey_users.dl_member_id = survey_users.respondent_id