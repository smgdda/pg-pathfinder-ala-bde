﻿create table att_responses_all as
select 
  * 
from
  (
    select
      dfa_user_id
      , att_response
      , survey_dt
      , exposed_control
      , 'ambrosia' as pfcampaign
    from
      att_responses_ambrosia
    
    union all
    
    select
      dfa_user_id
      , att_response
      , survey_dt
      , exposed_control
      , 'andros' as pfcampaign
    from
      att_responses_andros
    
    union all
    
    select
      dfa_user_id
      , att_response
      , survey_dt
      , exposed_control
      , 'larryvi' as pfcampaign
    from
      att_responses_larryvi
    
    union all
    
    select
      dfa_user_id
      , att_response
      , survey_dt
      , exposed_control
      , 'metabasehearthealth' as pfcampaign
    from
      att_responses_metabasehearthealth_oct_20_2015
    
    union all
    
    select
      dfa_user_id
      , att_response
      , survey_dt
      , exposed_control
      , 'metahealthbaryr2' as pfcampaign
    from
      att_responses_metahealthbaryr2
    
    union all
    
    select
      dfa_user_id
      , att_response
      , survey_dt
      , exposed_control
      , 'darla' as pfcampaign
    from
      att_responses_secret
    
    union all
    
    select
      dfa_user_id
      , att_response
      , survey_dt
      , exposed_control
      , 'supersizer' as pfcampaign
    from
      att_responses_supersizer
    
    union all
    
    select
      dfa_user_id
      , att_response
      , survey_dt
      , exposed_control
      , 'springsteendiamond' as pfcampaign
    from
      att_responses_tide
    
    union all
    
    select
      dfa_user_id
      , att_response
      , survey_dt
      , exposed_control
      , 'springsteen' as pfcampaign
    from
      att_responses_tide
    
    union all
    
    select
      dfa_user_id
      , att_response
      , survey_dt
      , exposed_control
      , 'diamond' as pfcampaign
    from
      att_responses_tide
    
    union all
    
    select
      dfa_user_id
      , att_response
      , survey_dt
      , exposed_control
      , 'zzzquil' as pfcampaign
    from
      att_responses_zzzquil
  ) att_responses_all