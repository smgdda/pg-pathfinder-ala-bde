﻿CREATE TABLE att_responses_metabasehearthealth_oct_20_2015
ROW FORMAT SERDE 
  'org.apache.hadoop.hive.serde2.columnar.ColumnarSerDe' 
STORED AS INPUTFORMAT 
  'org.apache.hadoop.hive.ql.io.RCFileInputFormat' 
OUTPUTFORMAT 
  'org.apache.hadoop.hive.ql.io.RCFileOutputFormat'
as
select
survey_users.respondent_id
, pf_mbd_survey_users.dfa_user_id
, to_date(age_q.starttime) as survey_dt
, age_q.user_response_text as age
, exposed_control.user_response_text as exposed_control
, dseen.user_response_text as dseen
, case when dseen.user_response_text is null then 0 else 1 end as derived_dseen
, dheard.user_response_text as dheard
, case when dheard.user_response_text is null then 0 else 1 end as derived_dheard
, dcons.user_response_text as dcons
, case when dcons.user_response_text is null then 0 else 1 end as derived_dcons
, case when dcons.user_response_text is null then 0 else 1 end as att_response

from
(select distinct respondent_id from attitudinal_survey_responses_oct_20_2015 where question_id in (9321819,9321865,9321873,9321927,9321931)) survey_users

join
(select distinct dl_member_id, dfa_user_id, mbd_survey_id from pf_mbd_users_oct_20_2015_metabasehearthealth_reload) pf_mbd_survey_users
on 
  pf_mbd_survey_users.mbd_survey_id = 262634
  and
  pf_mbd_survey_users.dl_member_id = survey_users.respondent_id

left outer join
(
  select *
  from attitudinal_survey_responses_oct_20_2015
  where attitudinal_survey_responses_oct_20_2015.question_id = 9321819
) exposed_control
on exposed_control.respondent_id = survey_users.respondent_id

left outer join
(
  select *
  from attitudinal_survey_responses_oct_20_2015
  where attitudinal_survey_responses_oct_20_2015.question_id = 9321865
) age_q
on age_q.respondent_id = survey_users.respondent_id

left outer join
(
  select *
  from attitudinal_survey_responses_oct_20_2015
  where attitudinal_survey_responses_oct_20_2015.question_id = 9321927
  and trim(user_response_text) = 'I Have Seen Metamucil'
) dseen
on dseen.respondent_id = survey_users.respondent_id

left outer join
(
  select *
  from attitudinal_survey_responses_oct_20_2015
  where attitudinal_survey_responses_oct_20_2015.question_id = 9321873
  and trim(user_response_text) = 'I Have Heard Of Metamucil'
) dheard
on dheard.respondent_id = survey_users.respondent_id

left outer join
(
  select *
  from attitudinal_survey_responses_oct_20_2015
  where attitudinal_survey_responses_oct_20_2015.question_id = 9321931
  and trim(user_response_text) in ('Very Likely Metamucil','Somewhat Likely Metamucil')
) dcons
on dcons.respondent_id = survey_users.respondent_id
;