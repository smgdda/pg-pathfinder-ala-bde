﻿create table pf_mbd_users_oct_20_2015_metabasehearthealth_reload
as
SELECT DISTINCT
      split(str_to_map(dfa_activity.other_data, '\\;', '=')['u3'], '-')[0] as dl_member_id
      , split(str_to_map(dfa_activity.other_data, '\\;', '=')['u3'], '-')[1] as mbd_survey_id
      , dfa_user_id
      , dlx_encrypted_dfa_user_id
    
    FROM
      dfa_activity
    
    WHERE
      event_dt >= '2015-07-01'
      and lower(activity_type) = 'vindid'
      and lower(activity_sub_type) = 'us_13324'
      and dfa_activity.other_data like '%http://www.insightexpress.com/ix/Survey.aspx%'
      and dfa_user_id != '0'