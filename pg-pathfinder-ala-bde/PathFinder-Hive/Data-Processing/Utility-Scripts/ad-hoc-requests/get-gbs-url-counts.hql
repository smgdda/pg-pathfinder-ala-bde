﻿select
  str_to_map(lower(ingestion_dfa_activity.other_data), '&', '=')['u1'] as gbs_url
  , min(to_date(FROM_UNIXTIME(UNIX_TIMESTAMP(time, 'MM-dd-yyyy-HH:mm:ss')))) as min_date
  , max(to_date(FROM_UNIXTIME(UNIX_TIMESTAMP(time, 'MM-dd-yyyy-HH:mm:ss')))) as max_date
  , count(*) as total_activity_ct
  , count(distinct user_id) as unique_user_ct
from
  ingestion_dfa_activity
where
  ingestion_dt >= '2015-06-01'
  and lower(ingestion_dfa_activity.other_data) like '%u1=%'
  and lower(ingestion_dfa_activity.other_data) not like '%[url%'
group by
  str_to_map(lower(ingestion_dfa_activity.other_data), '&', '=')['u1']
order by
  gbs_url