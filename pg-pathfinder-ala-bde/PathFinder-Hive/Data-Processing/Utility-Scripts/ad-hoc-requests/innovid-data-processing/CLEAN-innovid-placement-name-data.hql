﻿select
  pf_campaign_data.pfcampaign
  , innovid_placement.estimate_number
  , pf_campaign_data.buy_id
  , innovid_placement.mbox_placement_id
  , innovid_placement.placement_name_wo_prefix

from
  (
    select distinct
    split(split(regexp_replace(lower(placement_name), '[^a-zA-Z0-9_\\|]', ''), '\\|')[0], '_')[0] as estimate_number
    , split(split(regexp_replace(lower(placement_name), '[^a-zA-Z0-9_\\|]', ''), '\\|')[0], '_')[1] as mbox_placement_id
    , concat_ws('_', split(split(regexp_replace(lower(placement_name), '[^a-zA-Z0-9_\\|]', ''), '\\|')[0], '_')[0], split(split(regexp_replace(lower(placement_name), '[^a-zA-Z0-9_\\|]', ''), '\\|')[0], '_')[1], '') as prefix_to_remove
    , regexp_replace(split(regexp_replace(lower(placement_name), '[^a-zA-Z0-9_\\|]', ''), '\\|')[0], concat_ws('_', split(split(regexp_replace(lower(placement_name), '[^a-zA-Z0-9_\\|]', ''), '\\|')[0], '_')[0], split(split(regexp_replace(lower(placement_name), '[^a-zA-Z0-9_\\|]', ''), '\\|')[0], '_')[1], ''), '') as placement_name_wo_prefix
    , placement_name

    from
    innovid_report_data
  ) innovid_placement

join
  (
    select
    pfcampaign
    , campaign_name
    , split(campaign_name, '_')[2] as estimate_number
    , buy_id

    from
    pfcampaign_list

    where
    buy_id is not null
    and
    date_dropped = ''
  ) pf_campaign_data
  on pf_campaign_data.estimate_number = innovid_placement.estimate_number
;
