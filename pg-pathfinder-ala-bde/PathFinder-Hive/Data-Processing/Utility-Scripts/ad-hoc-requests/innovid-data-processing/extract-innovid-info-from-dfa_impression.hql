﻿select
ivcampaignid
, ivplacementid
, ivcreativeid
, ivpublisherid
, count(*) as item_ct

from
(
select distinct
  regexp_replace(lower(site_data), '^(u\\=)', '') as parsed_site_data
  , str_to_map(regexp_replace(lower(site_data), '^(u\\=)', ''), '\\|', '\\_')['ivcampaignid'] as ivcampaignid
  , str_to_map(regexp_replace(lower(site_data), '^(u\\=)', ''), '\\|', '\\_')['ivclientid'] as ivclientid
  , str_to_map(regexp_replace(lower(site_data), '^(u\\=)', ''), '\\|', '\\_')['ivbrandid'] as ivbrandid
  , str_to_map(regexp_replace(lower(site_data), '^(u\\=)', ''), '\\|', '\\_')['ivverticalid'] as ivverticalid
  , str_to_map(regexp_replace(lower(site_data), '^(u\\=)', ''), '\\|', '\\_')['ivagencyid'] as ivagencyid
  , str_to_map(regexp_replace(lower(site_data), '^(u\\=)', ''), '\\|', '\\_')['ivplacementid'] as ivplacementid
  , str_to_map(regexp_replace(lower(site_data), '^(u\\=)', ''), '\\|', '\\_')['ivchannelid'] as ivchannelid
  , str_to_map(regexp_replace(lower(site_data), '^(u\\=)', ''), '\\|', '\\_')['ivadid'] as ivadid
  , str_to_map(regexp_replace(lower(site_data), '^(u\\=)', ''), '\\|', '\\_')['ivcreativeid'] as ivcreativeid
  , str_to_map(regexp_replace(lower(site_data), '^(u\\=)', ''), '\\|', '\\_')['ivpublisherid'] as ivpublisherid
from
ingestion_dfa_impressions
where
ingestion_dt = '2015-12-05'
and site_data like '%campaign%'
) dfa_innovid_feed

group by
ivcampaignid
, ivplacementid
, ivcreativeid
, ivpublisherid

order by
ivcampaignid
, ivplacementid
, ivcreativeid
, ivpublisherid
