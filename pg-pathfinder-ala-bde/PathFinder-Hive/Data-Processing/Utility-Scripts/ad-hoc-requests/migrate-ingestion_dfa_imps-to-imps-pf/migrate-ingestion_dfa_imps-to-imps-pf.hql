﻿SET hive.exec.compress.output=TRUE;
SET io.seqfile.compression.type=BLOCK;
SET mapred.map.output.compression.codec=org.apache.hadoop.io.compress.SnappyCodec;
SET hive.exec.dynamic.partition=TRUE;
SET hive.exec.dynamic.partition.mode=NONSTRICT;

INSERT INTO TABLE ingestion_dfa_impressions_pf PARTITION (ingestion_dt)
SELECT /*+ mapjoin(pfcampaign_list) */
  ingestion_dfa_impressions.*
FROM
  (SELECT DISTINCT buy_id FROM pfcampaign_list WHERE buy_id IS NOT NULL) pfcampaign_list
JOIN
  ingestion_dfa_impressions
ON
  ingestion_dfa_impressions.ingestion_dt='${hiveconf:ingestion_dt}' 
  and pfcampaign_list.buy_id = ingestion_dfa_impressions.buy_id
;

ALTER TABLE ingestion_dfa_impressions DROP IF EXISTS PARTITION (ingestion_dt='${hiveconf:ingestion_dt}');

