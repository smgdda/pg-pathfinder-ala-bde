﻿ALTER TABLE log_conv_oss 
DROP IF EXISTS PARTITION (pfcampaign='${hiveconf:pf_campaign_name}')
;

FROM (
  select
    * 
  from
    pfcampaign_reportingwindow
  where
    pfcampaign = '${hiveconf:pf_campaign_name}'
  ) campaign

JOIN (
  select
  event_dts
  , dfa_user_id
  , dlx_encrypted_dfa_user_id
  , brand
  , site
  , placement
  , page_id
  , creative_name
  , creative_id
  , ui_creative_id
  , tactic
  , sitetactic
  , siteadsize
  , sitecreative
  , exe_type
  , vendor_code
  , sfg_type
  , targeting_type
  , content_genre
  , ad_size
  , creative
  , site_id
  , dcm_rate_type
  , dcm_rate
  , dcm_qty
  , freq
  , conv_user as conv_user_bhv
  , t2c as t2c_bhv
  , freq_pdf as freq_pdf_bhv
  , t2c_pdf as t2c_pdf_bhv
  , pfscore_log as pfscore_log_bhv
  , user_seq_num
  , pfcampaign
  from
    logs_product 
  where
    pfcampaign = '${hiveconf:pf_campaign_name}'
  ) user_imp 
ON (user_imp.pfcampaign = campaign.pfcampaign)

LEFT OUTER JOIN (
  SELECT
    dlx_converted_bhv_users.dfa_user_id
    , regexp_replace(lower(pfcampaign_list.pfcampaign), '[^a-zA-Z0-9]', '') as oss_activity_campaign
    , max(dlx_converted_bhv_users.max_oss_conv_date) as event_dts 

  FROM
    pfcampaign_list
 
  JOIN
    dlx_converted_bhv_users
    ON (
      pfcampaign_list.date_dropped = ''
      and pfcampaign_list.buy_id is not null
      and pfcampaign_list.dlx_campaign_name != ''
      and regexp_replace(lower(pfcampaign_list.pfcampaign), '[^a-zA-Z0-9]', '') = '${hiveconf:pf_campaign_name}'
      and dlx_converted_bhv_users.pfcampaign = regexp_replace(lower(pfcampaign_list.pfcampaign), '[^a-zA-Z0-9]', '')
    )

  GROUP BY
    dlx_converted_bhv_users.dfa_user_id
    , regexp_replace(lower(pfcampaign_list.pfcampaign), '[^a-zA-Z0-9]', '')
  ) user_activity 
  ON (
    user_activity.dfa_user_id = user_imp.dfa_user_id
    and user_activity.oss_activity_campaign = user_imp.pfcampaign 
  )

INSERT INTO TABLE log_conv_oss PARTITION(pfcampaign='${hiveconf:pf_campaign_name}') 
SELECT 
  user_imp.event_dts
  , user_imp.dfa_user_id
  , user_imp.dlx_encrypted_dfa_user_id
  , user_imp.brand
  , user_imp.site
  , user_imp.placement
  , user_imp.page_id
  , user_imp.creative_name
  , user_imp.creative_id
  , user_imp.ui_creative_id
  , user_imp.tactic
  , user_imp.sitetactic
  , user_imp.siteadsize
  , user_imp.sitecreative
  , user_imp.exe_type
  , user_imp.vendor_code
  , user_imp.sfg_type
  , user_imp.targeting_type
  , user_imp.content_genre
  , user_imp.ad_size
  , user_imp.creative
  , user_imp.site_id
  , user_imp.dcm_rate_type
  , user_imp.dcm_rate
  , user_imp.dcm_qty
  , user_imp.freq
  , user_imp.conv_user_bhv
  , user_imp.t2c_bhv
  , user_imp.freq_pdf_bhv
  , user_imp.t2c_pdf_bhv
  , user_imp.pfscore_log_bhv
  , case when user_activity.dfa_user_id is not null then 1 else 0 end as conv_user_oss
  , case 
    when user_activity.dfa_user_id is not null and user_activity.event_dts > user_imp.event_dts then 
      ( unix_timestamp(user_activity.event_dts) - unix_timestamp(user_imp.event_dts)  ) / 3600 
    else
      ( unix_timestamp(campaign.enddate,  'yyyy-MM-dd') - unix_timestamp(user_imp.event_dts) )/3600 
    end as t2c_oss
  , user_imp.user_seq_num
  
WHERE
  user_imp.pfcampaign = '${hiveconf:pf_campaign_name}'
;