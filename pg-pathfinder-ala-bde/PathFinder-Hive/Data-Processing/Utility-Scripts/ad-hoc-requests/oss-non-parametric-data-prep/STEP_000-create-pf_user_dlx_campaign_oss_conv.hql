﻿DROP TABLE IF EXISTS pf_user_dlx_campaign_oss_conv;

CREATE TABLE pf_user_dlx_campaign_oss_conv AS
SELECT
  pf_campaign AS dlx_campaign
  , pf_dlx_campaign_names.advertiser_id
  , CASE 
      WHEN dlx_recrypted_cookies.newly_encrypted IS NOT NULL THEN 
        dlx_recrypted_cookies.newly_encrypted 
      ELSE 
        dfa_id 
    END AS dlx_bridged_dfa_user_id
  , max(to_date(from_unixtime(unix_timestamp(model_end_dt, 'MM/dd/yyyy')))) AS max_oss_conv_date

FROM
  ingestion_dlx_weekly_sales_signal

JOIN
  (
    SELECT DISTINCT
      pf_dlx_campaign_names.dlx_campaign_name
      , pf_dlx_campaign_names.advertiser_id
    FROM
      pf_dlx_campaign_names
  ) pf_dlx_campaign_names
  ON
  pf_dlx_campaign_names.dlx_campaign_name = ingestion_dlx_weekly_sales_signal.pf_campaign

LEFT OUTER JOIN
  dlx_recrypted_cookies
  ON
  dlx_recrypted_cookies.encrypted = ingestion_dlx_weekly_sales_signal.dfa_id
  AND dlx_recrypted_cookies.advertiser_id = pf_dlx_campaign_names.advertiser_id
WHERE
  ingestion_dt >= '2015-09-08'
  AND ingestion_dt <= '2015-11-07'
  AND sales_signal_projected = 1
GROUP BY
  pf_campaign
  , pf_dlx_campaign_names.advertiser_id
  , CASE 
      WHEN dlx_recrypted_cookies.newly_encrypted IS NOT NULL THEN 
        dlx_recrypted_cookies.newly_encrypted 
      ELSE 
        dfa_id 
    END
;
