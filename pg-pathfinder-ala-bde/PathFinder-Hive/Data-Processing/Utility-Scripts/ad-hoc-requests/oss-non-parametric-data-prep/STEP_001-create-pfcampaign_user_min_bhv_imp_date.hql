﻿DROP TABLE IF EXISTS pfcampaign_user_min_bhv_imp_date;
CREATE TABLE pfcampaign_user_min_bhv_imp_date AS
SELECT
  pfcampaign
  , dfa_user_id
  , min(to_date(from_unixtime(unix_timestamp(event_dts)))) as min_bhv_imp_dt
FROM
  logs_product
GROUP BY
  pfcampaign
  , dfa_user_id
;
