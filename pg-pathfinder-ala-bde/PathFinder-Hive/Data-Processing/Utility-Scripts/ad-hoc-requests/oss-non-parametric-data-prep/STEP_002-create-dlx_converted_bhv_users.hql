﻿DROP TABLE IF EXISTS dlx_converted_bhv_users;
CREATE TABLE dlx_converted_bhv_users AS

SELECT DISTINCT
  pf_dlx_campaign_names.pfcampaign
  , pfcampaign_user_min_bhv_imp_date.dfa_user_id
  , pf_user_dlx_campaign_oss_conv.max_oss_conv_date
FROM
  (
    SELECT DISTINCT
      lower(pfcampaign) as pfcampaign
      , dlx_campaign_name
    FROM
      pf_dlx_campaign_names
    WHERE
      dlx_campaign_name IS NOT NULL
      AND dlx_campaign_name != ''
  ) pf_dlx_campaign_names

JOIN
  pfcampaign_user_min_bhv_imp_date
  ON 
  pfcampaign_user_min_bhv_imp_date.pfcampaign = pf_dlx_campaign_names.pfcampaign

JOIN
  pf_user_dlx_campaign_oss_conv
  ON 
  pf_user_dlx_campaign_oss_conv.dlx_campaign = pf_dlx_campaign_names.dlx_campaign_name
  AND pf_user_dlx_campaign_oss_conv.dlx_bridged_dfa_user_id = pfcampaign_user_min_bhv_imp_date.dfa_user_id

WHERE
  pfcampaign_user_min_bhv_imp_date.min_bhv_imp_dt <= pf_user_dlx_campaign_oss_conv.max_oss_conv_date
;
