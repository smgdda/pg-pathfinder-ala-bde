﻿set hive.optimize.bucketmapjoin = true;
set hive.exec.compress.intermediate = true;
set hive.exec.compress.output = true;
set mapred.output.compression.type = BLOCK;
set mapred.output.compression.codec = org.apache.hadoop.io.compress.SnappyCodec;

DROP TABLE IF EXISTS campaign_t2c_oss;

CREATE TABLE campaign_t2c_oss
ROW FORMAT SERDE 
  'org.apache.hadoop.hive.serde2.columnar.ColumnarSerDe' 
STORED AS INPUTFORMAT 
  'org.apache.hadoop.hive.ql.io.RCFileInputFormat' 
OUTPUTFORMAT 
  'org.apache.hadoop.hive.ql.io.RCFileOutputFormat'
AS
SELECT
  brand
  , pfcampaign
  , avg(t2c_oss) as lambda_oss
FROM
  log_conv_oss
GROUP BY
  brand
  , pfcampaign
;
