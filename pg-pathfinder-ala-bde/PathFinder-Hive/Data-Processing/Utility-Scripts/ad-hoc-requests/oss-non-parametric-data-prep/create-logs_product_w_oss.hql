﻿SET hive.enforce.bucketing=true; 
SET hive.optimize.bucketmapjoin=true; 
SET hive.exec.compress.output=true;
SET hive.exec.compress.intermediate=true;
SET hive.exec.dynamic.partition=true; 
SET hive.exec.dynamic.partition.mode=nonstrict; 
SET hive.exec.parallel=true;
SET hive.auto.convert.join=true;
SET mapred.compress.map.output=true;
SET mapred.output.compression.type=BLOCK;
SET mapred.output.compression.codec=org.apache.hadoop.io.compress.SnappyCodec;

DROP TABLE IF EXISTS logs_product_w_oss;
CREATE  TABLE logs_product_w_oss(
  event_dts timestamp, 
  dfa_user_id string, 
  dlx_encrypted_dfa_user_id string, 
  brand string, 
  site string, 
  placement string, 
  page_id bigint, 
  creative_name string, 
  creative_id bigint, 
  ui_creative_id bigint, 
  tactic string, 
  sitetactic string, 
  siteadsize string, 
  sitecreative string, 
  exe_type string, 
  vendor_code string, 
  sfg_type string, 
  targeting_type string, 
  content_genre string, 
  ad_size string, 
  creative string, 
  site_id bigint,
  dcm_rate_type string,
  dcm_rate float,
  dcm_qty bigint,
  freq bigint, 
  conv_user_bhv int, 
  conv_user_oss int, 
  t2c_bhv double, 
  t2c_pdf double, 
  freq_pdf_bhv double, 
  freq_pdf_oss double, 
  t2c_pdf_bhv double, 
  t2c_pdf_oss double, 
  pfscore_log_bhv double,
  pfscore_log_oss double,
  user_seq_num bigint
  )
PARTITIONED BY (pfcampaign string)
ROW FORMAT SERDE 
  'org.apache.hadoop.hive.serde2.columnar.ColumnarSerDe' 
STORED AS INPUTFORMAT 
  'org.apache.hadoop.hive.ql.io.RCFileInputFormat' 
OUTPUTFORMAT 
  'org.apache.hadoop.hive.ql.io.RCFileOutputFormat'
;

INSERT OVERWRITE TABLE logs_product_w_oss PARTITION(pfcampaign)
select /*+ MAPJOIN(b,c) */ 
a.event_dts
, a.dfa_user_id
, a.dlx_encrypted_dfa_user_id
, a.brand
, a.site
, a.placement
, a.page_id
, a.creative_name
, a.creative_id
, a.ui_creative_id
, a.tactic
, a.sitetactic
, a.siteadsize
, a.sitecreative
, a.exe_type
, a.vendor_code
, a.sfg_type
, a.targeting_type
, a.content_genre
, a.ad_size
, a.creative
, a.site_id
, a.dcm_rate_type
, a.dcm_rate
, a.dcm_qty
, a.freq
, a.conv_user_bhv
, a.conv_user_oss
, a.t2c_bhv
, a.t2c_oss
, a.freq_pdf_bhv
, b.value as freq_pdf_oss
, a.t2c_pdf_bhv
, (1/c.lambda_oss) * exp(-1 * a.t2c_oss / c.lambda_oss ) as t2c_pdf_oss
, a.pfscore_log_bhv
, (b.value) * ((1/c.lambda_oss) * exp(-1 * a.t2c_oss / c.lambda_oss )) as pfscore_log_oss
, a.user_seq_num
, a.pfcampaign
from
  log_conv_oss a
left outer join
  freq_modelled_oss b
  on
  (b.freq_matchkey = concat(a.pfcampaign, '_', a.sitetactic, '.', a.freq) ) 
left outer join
  campaign_t2c_oss c
  on ( a.pfcampaign = c.pfcampaign and a.brand = c.brand )
;
