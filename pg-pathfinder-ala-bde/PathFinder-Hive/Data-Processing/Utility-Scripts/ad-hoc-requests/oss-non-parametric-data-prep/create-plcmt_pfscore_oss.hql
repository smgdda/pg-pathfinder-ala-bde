﻿SET hive.exec.compress.output=true;
SET hive.exec.compress.intermediate=true;
SET mapred.compress.map.output=true;
SET mapred.output.compression.type=BLOCK;
SET mapred.output.compression.codec=org.apache.hadoop.io.compress.SnappyCodec;

DROP TABLE IF EXISTS plcmt_pfscore_oss;

CREATE TABLE plcmt_pfscore_oss
ROW FORMAT SERDE 
  'org.apache.hadoop.hive.serde2.columnar.ColumnarSerDe' 
STORED AS INPUTFORMAT 
  'org.apache.hadoop.hive.ql.io.RCFileInputFormat' 
OUTPUTFORMAT 
  'org.apache.hadoop.hive.ql.io.RCFileOutputFormat'
AS
select
  brand,
  pfcampaign,
  site,
  sitecreative,
  sitetactic,
  siteadsize,
  creative,
  creative_id,
  ui_creative_id,
  tactic,
  ad_size,
  placement,
  exe_type,
  page_id,
  creative_name,
  site_id,
  dcm_rate_type,
  dcm_rate,
  dcm_qty,
  sum(case when pfscore_log_bhv is not null then pfscore_log_bhv else CAST(0 as double) end) as pfscore_plcmt_bhv,
  sum(case when pfscore_log_oss is not null then pfscore_log_oss else CAST(0 as double) end) as pfscore_plcmt_oss
from
  logs_product_w_oss
group by
  brand,
  pfcampaign,
  site,
  sitecreative,
  sitetactic,
  siteadsize,
  creative,
  creative_id,
  ui_creative_id,
  tactic,
  ad_size,
  placement,
  exe_type,
  page_id,
  creative_name,
  site_id,
  dcm_rate_type,
  dcm_rate,
  dcm_qty