﻿DROP VIEW IF EXISTS vw_pf_agg_final_oss;

CREATE VIEW vw_pf_agg_final_oss AS 
SELECT
  brand
  , pfcampaign
  , rate_type
  , exe_type
  , dimtype
  , dim
  , imps
  , cost
  , ss_cost
  , pfscore_bhv
  , pfscore_oss
  , scaled_imps
  , scaled_cost
  , scaled_ss_cost
  , scaled_pfscore_bhv
  , scaled_pfscore_oss
  , scaled_pfscore_bhv/scaled_imps as impact_bhv
  , scaled_pfscore_oss/scaled_imps as impact_oss
  , scaled_pfscore_bhv/scaled_cost as efficiency_bhv
  , scaled_pfscore_oss/scaled_cost as efficiency_oss
  , scaled_pfscore_bhv/scaled_ss_cost as actual_efficiency_bhv
  , scaled_pfscore_oss/scaled_ss_cost as actual_efficiency_oss
from
  vw_pf_scaled_oss
;