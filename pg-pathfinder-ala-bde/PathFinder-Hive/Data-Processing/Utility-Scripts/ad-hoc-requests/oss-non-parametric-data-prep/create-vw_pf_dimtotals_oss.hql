﻿DROP VIEW IF EXISTS vw_pf_dimtotals_oss;
CREATE VIEW vw_pf_dimtotals_oss AS 
SELECT
  lower(dims.brand) as brand,
  dims.pfcampaign,
  dims.exe_type,
  dims.rate_type,
  dims.dimtype,
  dims.dim,
  dims.imps,
  dims.cost,
  dims.ss_cost,
  dims.calc_cpm,
  dims.calc_ss_cpm,
  dims.pfscore_bhv,
  dims.pfscore_oss
FROM
  (
    SELECT 
      brand,
      pfcampaign,
      exe_type,
      case when spectra_rate_type is not null then spectra_rate_type else dcm_rate_type end as rate_type,
      'site' as dimtype,
      lower(site) as dim,
      sum(impressions) as imps,
      sum(calculated_actual_cost) as cost,
      sum(ss_cost_sum) as ss_cost,
      ((1000 * sum(calculated_actual_cost)) / sum(impressions)) as calc_cpm,
      ((1000 * sum(ss_cost_sum)) / sum(impressions)) as calc_ss_cpm,
      sum(pfscore_plcmt_bhv) as pfscore_bhv,
      sum(pfscore_plcmt_oss) as pfscore_oss
    FROM
      dcm_spectra_merge_agg_w_innovid_oss dcm_spectra_merge
    GROUP BY 
      brand,
      pfcampaign,
      exe_type,
      case when spectra_rate_type is not null then spectra_rate_type else dcm_rate_type end,
      lower(site)

    union all

    SELECT 
      brand,
      pfcampaign,
      exe_type,
      case when spectra_rate_type is not null then spectra_rate_type else dcm_rate_type end as rate_type,
      'tactic' as dimtype,
      lower(tactic) as dim,
      sum(impressions) as imps,
      sum(calculated_actual_cost) as cost,
      sum(ss_cost_sum) as ss_cost,
      ((1000 * sum(calculated_actual_cost)) / sum(impressions)) as calc_cpm,
      ((1000 * sum(ss_cost_sum)) / sum(impressions)) as calc_ss_cpm,
      sum(pfscore_plcmt_bhv) as pfscore_bhv,
      sum(pfscore_plcmt_oss) as pfscore_oss
    FROM
      dcm_spectra_merge_agg_w_innovid_oss dcm_spectra_merge
    GROUP BY 
      brand,
      pfcampaign,
      exe_type,
      case when spectra_rate_type is not null then spectra_rate_type else dcm_rate_type end,
      lower(tactic)

    union all

    SELECT 
      brand,
      pfcampaign,
      exe_type,
      case when spectra_rate_type is not null then spectra_rate_type else dcm_rate_type end as rate_type,
      'adsize' as dimtype,
      lower(ad_size) as dim,
      sum(impressions) as imps,
      sum(calculated_actual_cost) as cost,
      sum(ss_cost_sum) as ss_cost,
      ((1000 * sum(calculated_actual_cost)) / sum(impressions)) as calc_cpm,
      ((1000 * sum(ss_cost_sum)) / sum(impressions)) as calc_ss_cpm,
      sum(pfscore_plcmt_bhv) as pfscore_bhv,
      sum(pfscore_plcmt_oss) as pfscore_oss
    FROM
      dcm_spectra_merge_agg_w_innovid_oss dcm_spectra_merge
    GROUP BY 
      brand,
      pfcampaign,
      exe_type,
      case when spectra_rate_type is not null then spectra_rate_type else dcm_rate_type end,
      lower(ad_size)

    union all

    SELECT 
      brand,
      pfcampaign,
      exe_type,
      case when spectra_rate_type is not null then spectra_rate_type else dcm_rate_type end as rate_type,
      'creative' as dimtype,
      lower(creative) as dim,
      sum(impressions) as imps,
      sum(calculated_actual_cost) as cost,
      sum(ss_cost_sum) as ss_cost,
      ((1000 * sum(calculated_actual_cost)) / sum(impressions)) as calc_cpm,
      ((1000 * sum(ss_cost_sum)) / sum(impressions)) as calc_ss_cpm,
      sum(pfscore_plcmt_bhv) as pfscore_bhv,
      sum(pfscore_plcmt_oss) as pfscore_oss
    FROM
      dcm_spectra_merge_agg_w_innovid_oss dcm_spectra_merge
    GROUP BY 
      brand,
      pfcampaign,
      exe_type,
      case when spectra_rate_type is not null then spectra_rate_type else dcm_rate_type end,
      lower(creative)

    union all

    SELECT 
      brand,
      pfcampaign,
      exe_type,
      case when spectra_rate_type is not null then spectra_rate_type else dcm_rate_type end as rate_type,
      'sitetactic' as dimtype,
      lower(sitetactic) as dim,
      sum(impressions) as imps,
      sum(calculated_actual_cost) as cost,
      sum(ss_cost_sum) as ss_cost,
      ((1000 * sum(calculated_actual_cost)) / sum(impressions)) as calc_cpm,
      ((1000 * sum(ss_cost_sum)) / sum(impressions)) as calc_ss_cpm,
      sum(pfscore_plcmt_bhv) as pfscore_bhv,
      sum(pfscore_plcmt_oss) as pfscore_oss
    FROM
      dcm_spectra_merge_agg_w_innovid_oss dcm_spectra_merge
    GROUP BY 
      brand,
      pfcampaign,
      exe_type,
      case when spectra_rate_type is not null then spectra_rate_type else dcm_rate_type end,
      lower(sitetactic)

    union all

    SELECT 
      brand,
      pfcampaign,
      exe_type,
      case when spectra_rate_type is not null then spectra_rate_type else dcm_rate_type end as rate_type,
      'siteadsize' as dimtype,
      lower(siteadsize) as dim,
      sum(impressions) as imps,
      sum(calculated_actual_cost) as cost,
      sum(ss_cost_sum) as ss_cost,
      ((1000 * sum(calculated_actual_cost)) / sum(impressions)) as calc_cpm,
      ((1000 * sum(ss_cost_sum)) / sum(impressions)) as calc_ss_cpm,
      sum(pfscore_plcmt_bhv) as pfscore_bhv,
      sum(pfscore_plcmt_oss) as pfscore_oss
    FROM
      dcm_spectra_merge_agg_w_innovid_oss dcm_spectra_merge
    GROUP BY 
      brand,
      pfcampaign,
      exe_type,
      case when spectra_rate_type is not null then spectra_rate_type else dcm_rate_type end,
      lower(siteadsize)

    union all

    SELECT 
      brand,
      pfcampaign,
      exe_type,
      case when spectra_rate_type is not null then spectra_rate_type else dcm_rate_type end as rate_type,
      'sitecreative' as dimtype,
      lower(sitecreative) as dim,
      sum(impressions) as imps,
      sum(calculated_actual_cost) as cost,
      sum(ss_cost_sum) as ss_cost,
      ((1000 * sum(calculated_actual_cost)) / sum(impressions)) as calc_cpm,
      ((1000 * sum(ss_cost_sum)) / sum(impressions)) as calc_ss_cpm,
      sum(pfscore_plcmt_bhv) as pfscore_bhv,
      sum(pfscore_plcmt_oss) as pfscore_oss
    FROM
      dcm_spectra_merge_agg_w_innovid_oss dcm_spectra_merge
    GROUP BY 
      brand,
      pfcampaign,
      exe_type,
      case when spectra_rate_type is not null then spectra_rate_type else dcm_rate_type end,
      lower(sitecreative)
  ) dims
JOIN
  (
    select distinct 
      lower(brand) as brand
      , lower(pfcampaign) as pfcampaign 
    from 
      pfcampaign_list 
    where 
      date_dropped = '' 
      and buy_id is not null
  ) pfcampaign_list
on (
  pfcampaign_list.brand = dims.brand
  and pfcampaign_list.pfcampaign = dims.pfcampaign
)
;