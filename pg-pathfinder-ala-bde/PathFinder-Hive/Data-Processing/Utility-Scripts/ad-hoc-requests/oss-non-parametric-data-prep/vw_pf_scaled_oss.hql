﻿DROP VIEW IF EXISTS vw_pf_scaled_oss;

CREATE VIEW vw_pf_scaled_oss AS 
SELECT
  a.brand
  ,a.pfcampaign
  ,a.rate_type
  ,lower(a.exe_type) as exe_type
  ,a.dimtype
  ,a.dim
  ,a.imps
  ,a.cost
  ,a.ss_cost
  ,a.pfscore_bhv
  ,a.pfscore_oss
  ,a.imps/b.imps_tot as scaled_imps
  ,a.cost/b.cost_tot as scaled_cost
  ,a.ss_cost/b.ss_cost_tot as scaled_ss_cost
  ,a.pfscore_bhv/b.pfscore_tot_bhv as scaled_pfscore_bhv
  ,a.pfscore_oss/b.pfscore_tot_oss as scaled_pfscore_oss
FROM
  vw_pf_dimtotals_oss a 

LEFT OUTER JOIN (
  SELECT
  brand
  ,pfcampaign
  ,rate_type
  ,lower(exe_type) as exe_type
  ,imps_tot
  ,cost_tot
  ,ss_cost_tot
  ,pfscore_tot_bhv
  ,pfscore_tot_oss
  FROM
  vw_pf_camp_totals_oss
  ) b 
ON (
  a.pfcampaign = b.pfcampaign 
  and a.brand = b.brand
  and lower(a.exe_type) = lower(b.exe_type)
  and a.rate_type = b.rate_type
  )
;