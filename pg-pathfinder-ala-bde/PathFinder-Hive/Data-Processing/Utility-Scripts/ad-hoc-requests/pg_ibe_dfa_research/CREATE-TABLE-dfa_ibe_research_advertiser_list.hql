﻿drop table if exists pg_default.dfa_ibe_research_advertiser_list;

create table pg_default.dfa_ibe_research_advertiser_list as
select
  advertiser_data.advertiser_id
  , advertiser_data.advertiser
  , most_recent_ingestion.min_ingestion_dt

from
  pg_default.pg_dfa_ma_advertiser advertiser_data

join
  (
    select
      advertiser_id
      , max(ingestion_dt) as max_ingestion_dt
      , min(ingestion_dt) as min_ingestion_dt
    from
      pg_default.pg_dfa_ma_advertiser
    group by
      advertiser_id
  ) most_recent_ingestion
  on most_recent_ingestion.advertiser_id = advertiser_data.advertiser_id
  and most_recent_ingestion.max_ingestion_dt = advertiser_data.ingestion_dt
;
