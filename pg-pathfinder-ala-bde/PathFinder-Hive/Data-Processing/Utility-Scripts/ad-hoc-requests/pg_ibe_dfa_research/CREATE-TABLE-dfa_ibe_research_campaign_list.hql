﻿drop table if exists pg_default.dfa_ibe_research_campaign_list;

create table pg_default.dfa_ibe_research_campaign_list as
select
  campaign_data.advertiser_id
  , campaign_data.buy_id
  , campaign_data.buy
  , campaign_data.start_date
  , campaign_data.end_date
  , most_recent_ingestion.min_ingestion_dt

from
  pg_default.pg_dfa_ma_campaign campaign_data

join
  (
    select
      buy_id
      , max(ingestion_dt) as max_ingestion_dt
      , min(ingestion_dt) as min_ingestion_dt
    from
      pg_default.pg_dfa_ma_campaign
    group by
      buy_id
  ) most_recent_ingestion
  on most_recent_ingestion.buy_id = campaign_data.buy_id
  and most_recent_ingestion.max_ingestion_dt = campaign_data.ingestion_dt
