﻿drop table if exists pg_default.dfa_ibe_research_creative_list;

create table pg_default.dfa_ibe_research_creative_list as
select
pg_dfa_ma_creative.*
, split(pg_dfa_ma_creative.creative,'_')[2] as derived_creative

from
  pg_default.pg_dfa_ma_creative

join
(
  select
  creative_id
  , max(ingestion_dt) as max_ingestion_dt
  from
  pg_default.pg_dfa_ma_creative
  group by
  creative_id
) max_ingestion
on
max_ingestion.creative_id = pg_dfa_ma_creative.creative_id
and max_ingestion.max_ingestion_dt = pg_dfa_ma_creative.ingestion_dt