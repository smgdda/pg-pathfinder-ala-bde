﻿drop table if exists pg_default.dfa_ibe_research_page_list;

create table pg_default.dfa_ibe_research_page_list as
select
  pg_dfa_ma_page.buy_id
  , pg_dfa_ma_page.site_id
  , pg_dfa_ma_page.page_id
  , upper(regexp_replace(pg_dfa_ma_site.site, '[^a-zA-Z0-9\\_]', '')) as site
  , case 
      when substr(lower(site_placement),0,2) = 'pf' or lower(site_placement) like 'cancel%'
      then concat_ws('_',split(upper(site_placement),'_')[1],split(upper(site_placement),'_')[4])
    else concat_ws('_',split(upper(site_placement),'_')[0],split(upper(site_placement),'_')[3])
  end as tactic
  , case
      when substr(lower(site_placement),0,2) = 'pf'
        then split(upper(site_placement),'_')[1]
    else 
      split(upper(site_placement),'_')[0] 
  end as exe_type
  , case
      when substr(lower(site_placement),0,2) = 'pf' or lower(site_placement) like 'cancel%'
        then split(upper(site_placement),'_')[2]
    else split(upper(site_placement),'_')[1] 
  end as vendor_code
  , case
      when substr(lower(site_placement),0,2) = 'pf' or lower(site_placement) like 'cancel%'
        then split(upper(site_placement),'_')[3]
    else split(upper(site_placement),'_')[2] 
  end as sfg_type
  , case
      when substr(lower(site_placement),0,2) = 'pf' or lower(site_placement) like 'cancel%'
        then split(upper(site_placement),'_')[4]
    else split(upper(site_placement),'_')[3] 
  end as targeting_type
  , case
     when substr(lower(site_placement),0,2) = 'pf' or lower(site_placement) like 'cancel%'
      then split(upper(site_placement),'_')[6] 
    else split(upper(site_placement),'_')[5] 
  end as ad_size
from
  pg_default.pg_dfa_ma_page
join
  (
    select
    page_id as pg_id
    , max(ingestion_dt) as max_ingestion_dt
    from
    pg_default.pg_dfa_ma_page
    group by
    page_id
  ) page_id_max
  on
    pg_dfa_ma_page.page_id = page_id_max.pg_id 
    and pg_dfa_ma_page.ingestion_dt = page_id_max.max_ingestion_dt
join
  (select site_id, site from pg_default.dfa_ibe_research_site_list) pg_dfa_ma_site
  on pg_dfa_ma_site.site_id = pg_dfa_ma_page.site_id
;
