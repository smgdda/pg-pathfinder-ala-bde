﻿drop table if exists pg_default.dfa_ibe_research_site_list;

create table pg_default.dfa_ibe_research_site_list as
select
  site_data.site_id
  , upper(site_data.site) as site
  , most_recent_ingestion.min_ingestion_dt

from
  pg_default.pg_dfa_ma_site site_data

join
  (
    select
      site_id
      , max(ingestion_dt) as max_ingestion_dt
      , min(ingestion_dt) as min_ingestion_dt
    from
      pg_default.pg_dfa_ma_site
    group by
      site_id
  ) most_recent_ingestion
  on most_recent_ingestion.site_id = site_data.site_id
  and most_recent_ingestion.max_ingestion_dt = site_data.ingestion_dt
;
