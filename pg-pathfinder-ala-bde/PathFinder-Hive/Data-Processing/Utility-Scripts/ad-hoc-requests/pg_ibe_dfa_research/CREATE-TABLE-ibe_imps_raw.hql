﻿

CREATE TABLE pg_default.ibe_imps_raw
ROW FORMAT SERDE 
  'org.apache.hadoop.hive.serde2.columnar.ColumnarSerDe' 
STORED AS INPUTFORMAT 
  'org.apache.hadoop.hive.ql.io.RCFileInputFormat' 
OUTPUTFORMAT 
  'org.apache.hadoop.hive.ql.io.RCFileOutputFormat'
AS
SELECT /*+ MAPJOIN (clicks) */
  imps.pel
  , imps.event_wk
  , imps.event_mth
  , imps.advertiser_id
  , imps.buy_id
  , imps.site_id
  , CASE WHEN pel_ibe.age_rng IS NULL THEN 'na' ELSE pel_ibe.age_rng END as age_rng
  , CASE WHEN pel_ibe.gender IS NULL THEN 'na' ELSE pel_ibe.gender END AS gender
  , CASE WHEN clicks.pel IS NOT NULL THEN 1 ELSE 0 END as conv_user

FROM
  (
    select
      ibe.pel
      , trim(regexp_replace(max(ibe.ibe8627_age_in_two_year_increments_input_individual_b05), 'Age in Two-Year Increments Input Individual - ', '')) as age_rng
      , max(ibe.ibe8688_gender_input_individual_b05) as gender

    from
      pg_default.ibe

    group by
      ibe.pel
  ) pel_ibe

JOIN
  (
    SELECT
    weekofyear(event_dt) as event_wk
    , month(event_dt) as event_mth
    , pel
    , advertiser_id
    , buy_id
    , site_id
    FROM
    pg_default.dfa_impressions
    WHERE
    event_dt >= '${hiveconf:week_start_dt}'
    and event_dt <= '${hiveconf:week_end_dt}'
    and pel IS NOT NULL
    ) imps
  ON 
  imps.pel = pel_ibe.pel

LEFT OUTER JOIN
  pg_default.dfa_ibe_research_user_buy_clicks clicks
  ON
  clicks.pel = imps.pel
  and clicks.advertiser_id = imps.advertiser_id
;