﻿DROP TABLE IF EXISTS pg_default.ibe_stats_by_wk_of_yr;

CREATE TABLE pg_default.ibe_stats_by_wk_of_yr
(
  event_wk int,
  advertiser_id int,
  buy_id int,
  site_id int,
  page_id bigint,
  creative_id int,
  age_rng string,
  gender string,
  total_imp_ct bigint,
  total_user_ct bigint,
  total_clicking_imps bigint
)
;
