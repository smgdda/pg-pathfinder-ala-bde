﻿select
  week_data.*
  , advertiser_data.advertiser
  , campaign_data.buy
  , page_data.site
  , page_data.tactic
  , page_data.targeting_Type
  , page_data.sfg_type
  , page_data.ad_size

from
  pg_default.ibe_stats_by_wk_of_yr week_data

join 
  (
    select 
      advertiser
      , advertiser_id as dfa_advertiser_id 
    from 
      pg_default.dfa_ibe_research_advertiser_list
  ) advertiser_data 
  on advertiser_data.dfa_advertiser_id = week_data.advertiser_id

join 
  (
    select 
      buy
      , buy_id as dfa_buy_id 
    from 
      pg_default.dfa_ibe_research_campaign_list
  ) campaign_data
  on campaign_data.dfa_buy_id = week_data.buy_id

join 
  (
    select
      page_id as pl_page_id
      , site_id as pl_site_id
      , buy_id as pl_buy_id
      , site
      , tactic
      , targeting_type
      , sfg_type
      , ad_size
    from
      pg_default.dfa_ibe_research_page_list
  ) page_data
  on 
  page_data.pl_page_id = week_data.page_id 
  and page_data.pl_site_id = week_data.site_id 
  and page_data.pl_buy_id = week_data.buy_id
