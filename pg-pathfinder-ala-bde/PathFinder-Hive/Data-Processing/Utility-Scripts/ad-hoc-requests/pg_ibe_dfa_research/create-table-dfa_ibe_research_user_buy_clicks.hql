﻿create table dfa_ibe_research_user_buy_clicks as
select
dfa_clicks.pel
, dfa_clicks.advertiser_id
, dfa_clicks.buy_id
, max(dfa_clicks.event_dts) as max_click_dt

from
dfa_clicks

where
dfa_clicks.pel is not null

group by
dfa_clicks.pel
, dfa_clicks.advertiser_id
, dfa_clicks.buy_id
;