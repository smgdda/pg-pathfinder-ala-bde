﻿drop table if exists dfa_ibe_research_merged_data;

create table dfa_ibe_research_merged_data as
select
  week_data.*
  , case when week_converts.unique_converters is not null then week_converts.unique_converters else cast(-1 as bigint) end as unique_converters
  , advertiser_data.advertiser
  , campaign_data.buy
  , page_data.site
  , page_data.tactic
  , page_data.targeting_Type
  , page_data.sfg_type
  , page_data.ad_size
  , creative_data.creative
  , creative_data.derived_creative
  , creative_data.creative_size_id
  

from
pg_default.ibe_stats_by_wk_of_yr week_data

join 
(select advertiser, advertiser_id as dfa_advertiser_id from pg_default.dfa_ibe_research_advertiser_list) advertiser_data 
on advertiser_data.dfa_advertiser_id = week_data.advertiser_id

join 
(select buy, buy_id as dfa_buy_id from pg_default.dfa_ibe_research_campaign_list) campaign_data
on campaign_data.dfa_buy_id = week_data.buy_id

join 
(
  select
  page_id as pl_page_id
  , site_id as pl_site_id
  , buy_id as pl_buy_id
  , site
  , tactic
  , targeting_type
  , sfg_type
  , ad_size
  from
  pg_default.dfa_ibe_research_page_list
) page_data
  on 
  page_data.pl_page_id = week_data.page_id 
  and page_data.pl_site_id = week_data.site_id 
  and page_data.pl_buy_id = week_data.buy_id

join
(select * from pg_default.dfa_ibe_research_creative_list) creative_data
on creative_data.creative_id = week_data.creative_id

left outer join
pg_default.ibe_conv_users_by_wk_of_yr week_converts
on
  week_converts.event_wk = week_data.event_wk
  and week_converts.advertiser_id = week_data.advertiser_id
  and week_converts.buy_id = week_data.buy_id
  and week_converts.site_id = week_data.site_id
  and week_converts.page_id = week_data.page_id
  and week_converts.creative_id = week_data.creative_id
  and week_converts.gender = week_data.gender
  and week_converts.age_rng = week_data.age_rng

;
