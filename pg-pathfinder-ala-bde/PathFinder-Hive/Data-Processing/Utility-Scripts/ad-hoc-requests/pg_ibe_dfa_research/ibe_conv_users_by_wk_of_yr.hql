﻿SET hive.exec.compress.intermediate=TRUE;
SET hive.exec.compress.output=TRUE;
SET mapred.output.compression.type=BLOCK;
SET mapred.output.compression.codec=org.apache.hadoop.io.compress.SnappyCodec;

-- INSERT INTO TABLE pg_default.ibe_conv_users_by_wk_of_yr
SELECT
  *

FROM
  (
    select /*+ MAPJOIN (clicks) */
      weekofyear(event_dt) as event_wk
      , imps.advertiser_id
      , imps.buy_id
      , imps.site_id
      , imps.page_id
      , imps.creative_id
      , pel_ibe.age_rng
      , pel_ibe.gender
      , count(distinct imps.pel) as unique_converters
    from
      pg_default.dfa_impressions imps

    join
      (
        select
          ibe.pel
          , trim(regexp_replace(max(ibe.ibe8627_age_in_two_year_increments_input_individual_b05), 'Age in Two-Year Increments Input Individual - ', '')) as age_rng
          , max(ibe.ibe8688_gender_input_individual_b05) as gender

        from
          pg_default.ibe

        group by
          ibe.pel
      ) pel_ibe
      on 
      imps.event_dt >= '${hiveconf:week_start_dt}'
      and imps.event_dt <= '${hiveconf:week_end_dt}'
      and pel_ibe.pel = imps.pel
    
    join
      pg_default.dfa_ibe_research_user_buy_clicks clicks
      on
      clicks.pel = imps.pel
      and clicks.advertiser_id = imps.advertiser_id
    
    group by
      weekofyear(event_dt)
      , imps.advertiser_id
      , imps.buy_id
      , imps.site_id
      , imps.page_id
      , imps.creative_id
      , pel_ibe.age_rng
      , pel_ibe.gender
  ) imp_traffic
ORDER BY
  imp_traffic.advertiser_id
  , imp_traffic.buy_id
  , imp_traffic.site_id
  , imp_traffic.page_id
;
