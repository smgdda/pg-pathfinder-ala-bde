﻿select
  table_list.table_name 
  , case when usage0615.size_in_gb is not null then usage0615.size_in_gb else cast(0 as double) end as size_0615
  , case when usage0616.size_in_gb is not null then usage0616.size_in_gb else cast(0 as double) end as size_0616
  , case when usage0618.size_in_gb is not null then usage0618.size_in_gb else cast(0 as double) end as size_0618
  , case when usage0619.size_in_gb is not null then usage0619.size_in_gb else cast(0 as double) end as size_0619
  , case when (case when usage0615.size_in_gb is not null then usage0615.size_in_gb else cast(0 as double) end) != 0 then
      (case when usage0619.size_in_gb is not null then usage0619.size_in_gb else cast(0 as double) end - case when usage0615.size_in_gb is not null then usage0615.size_in_gb else cast(0 as double) end) / case when usage0615.size_in_gb is not null then usage0615.size_in_gb else cast(0 as double) end
    else
      cast(1 as double)
    end as percent_delta
  
from 
  (
  select distinct
    table_name
  from
    space_usage_by_table
  where
    table_name != 'tab_name'
    and create_dt >= '2015-06-15'
  ) table_list

left outer join
  (
    select
      table_name
      , size_in_gb
    
    from
      space_usage_by_table

    where
      create_dt = '2015-06-15'
      and table_name != 'tab_name'
  ) usage0615
  on (usage0615.table_name = table_list.table_name) 

left outer join
  (
    select
      table_name
      , size_in_gb
    
    from
      space_usage_by_table

    where
      create_dt = '2015-06-16'
      and table_name != 'tab_name'
  ) usage0616
  on (usage0616.table_name = table_list.table_name) 

left outer join
  (
    select
      table_name
      , size_in_gb
    
    from
      space_usage_by_table

    where
      create_dt = '2015-06-18'
      and table_name != 'tab_name'
  ) usage0618
  on (usage0618.table_name = table_list.table_name) 

left outer join
  (
    select
      table_name
      , size_in_gb
    
    from
      space_usage_by_table

    where
      create_dt = '2015-06-19'
      and table_name != 'tab_name'
  ) usage0619
  on (usage0619.table_name = table_list.table_name) 

order by
  table_name 
;
