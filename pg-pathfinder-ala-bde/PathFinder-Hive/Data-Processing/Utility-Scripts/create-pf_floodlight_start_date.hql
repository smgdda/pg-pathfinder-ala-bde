﻿create table pf_floodlight_start_date as
select
  pfcampaign
  , min(min_activity_dt) as min_pf_activity_dt
from
  (
    select
      pfcampaign
      , min(min_activity_dts) as min_activity_dt
    from
      (
        select
          buy_id
          , pfcampaign
        from
          pfcampaign_list
        where
          date_dropped = ''
          and buy_id is not null
      ) pfcampaign
    join
      dfa_user_buy_activity
      on dfa_user_buy_activity.buy_id = pfcampaign.buy_id
    group by
      pfcampaign
  ) buy_min_activity_dt
group by
  pfcampaign
order by
  pfcampaign
