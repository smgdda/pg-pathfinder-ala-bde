﻿SET hive.exec.compress.intermediate=true;
SET hive.exec.compress.output=true;
SET mapred.output.compression.type=BLOCK;
SET mapred.output.compression.codec=org.apache.hadoop.io.compress.SnappyCodec;
SET hive.exec.dynamic.partition=true;
SET hive.exec.dynamic.partition.mode=nonstrict;
SET hive.exec.max.dynamic.partitions.pernode=1500;

from
  (
  select
    to_date(FROM_UNIXTIME(UNIX_TIMESTAMP(time, 'MM-dd-yyyy-HH:mm:ss'))) as event_dt
    , ingestion_dfa_impressions.buy_id
    , ingestion_dfa_impressions.site_id
    , ingestion_dfa_impressions.page_id
    , ingestion_dfa_impressions.creative_id
    , count(*) as imp_ct
    , count(distinct user_id) as user_ct
    , sum(case when user_id = '0' then 1 else 0 end) as unusable_imp_ct
  from
    ingestion_dfa_impressions
  where
    ingestion_dt  = '${ingestion_dt}'
    and buy_id != 'Buy-ID'
  group by
    to_date(FROM_UNIXTIME(UNIX_TIMESTAMP(time, 'MM-dd-yyyy-HH:mm:ss')))
    , ingestion_dfa_impressions.buy_id
    , ingestion_dfa_impressions.site_id
    , ingestion_dfa_impressions.page_id
    , ingestion_dfa_impressions.creative_id
   ) i

left outer join (
  SELECT
    dfa_clicks.buy_id
    , dfa_clicks.page_id
    , dfa_clicks.creative_id
    , count(*) as click_ct
    , sum(case when user_id = '0' then 1 else 0 end) as unusable_click_ct
  FROM
    ingestion_dfa_clicks dfa_clicks
  WHERE
    ingestion_dt = '${ingestion_dt}'
    and buy_id != 'Buy-ID'
  GROUP BY
    dfa_clicks.buy_id
    , dfa_clicks.page_id
    , dfa_clicks.creative_id
) clicks
ON (
  clicks.buy_id = i.buy_id
  and clicks.page_id = i.page_id
  and clicks.creative_id = i.creative_id
)

left outer join (
  SELECT
    dfa_activity.buy_id
    , dfa_activity.page_id
    , dfa_activity.creative_id
    , count(*) as activity_ct
    , sum(case when user_id = '0' then 1 else 0 end) as unusable_activity_ct
  FROM
    ingestion_dfa_activity dfa_activity
  WHERE
    ingestion_dt = '${ingestion_dt}'
    and buy_id != 'Buy-ID'
  GROUP BY
    dfa_activity.buy_id
    , dfa_activity.page_id
    , dfa_activity.creative_id
) activities
ON (
  activities.buy_id = i.buy_id
  and activities.page_id = i.page_id
  and activities.creative_id = i.creative_id
)
insert into table pf_daily_dfa_plcmt_stats partition (event_dt)
select
i.buy_id
, i.site_id
, i.page_id
, i.creative_id

, i.imp_ct
, i.unusable_imp_ct
, i.user_ct

, case when clicks.click_ct is null then cast(0 as bigint) else clicks.click_ct end as click_ct
, case when clicks.unusable_click_ct is null then cast(0 as bigint) else clicks.unusable_click_ct end as unusable_click_ct

, case when activities.activity_ct is null then cast(0 as bigint) else activities.activity_ct end as activity_ct
, case when activities.unusable_activity_ct is null then cast(0 as bigint) else activities.unusable_activity_ct end as unusable_activity_ct

, i.event_dt
;