﻿insert into dlx_dfa_matching_sample
select distinct 
  dlx_campaign_name
  , user_id as dfa_user_id

from
  pfcampaign_list

join
  ingestion_dfa_impressions
  on 
    pfcampaign_list.dlx_campaign_name = '${hiveconf:dlx_campaign_name}'
    and pfcampaign_list.buy_id is not null
    and pfcampaign_list.date_dropped = ''
    and ingestion_dfa_impressions.ingestion_dt = '2015-08-09'
    and ingestion_dfa_impressions.user_id != '0'
    and ingestion_dfa_impressions.buy_id = pfcampaign_list.buy_id

limit 5000
;
