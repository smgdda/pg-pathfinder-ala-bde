﻿select
  table_name
  , min_create_dt
  , max_create_dt
  , last_size_in_gb
  , current_size_in_gb
  , cast(format_number(percent_delta, 4) as float) as percent_delta
from
  (
  select
      table_name
      , min_create_dt
      , max_create_dt
      , last_size_in_gb
      , current_size_in_gb
      , case 
        when last_size_in_gb = 0 and current_size_in_gb > 0 then cast(1 as double) 
        when last_size_in_gb = 0 and current_size_in_gb = 0 then cast(0 as double) 
        else (current_size_in_gb - last_size_in_gb) / last_size_in_gb end * 100 as percent_delta
  from
    (
    select
      table_name
      , min_create_dt
      , max_create_dt
      , case when last_size_in_gb is null then cast(0 as double) else last_size_in_gb end as last_size_in_gb
      , case when current_size_in_gb is null then cast(0 as double) else current_size_in_gb end as current_size_in_gb

    from
      (
      select
        final_table_set.table_name
        , min(date_range.create_dt) as min_create_dt
        , max(date_range.create_dt) as max_create_dt

      from
        (
        select distinct
          tables_in_date_range.table_name
        from
          (
          select
            table_list.table_name
            , table_list.create_dt
          from
            space_usage_by_table table_list
          join
            (select distinct create_dt from space_usage_by_table order by create_dt desc limit 2) date_range
            on (
              date_range.create_dt = table_list.create_dt
            )
          where
            table_list.table_name != 'tab_name'
          ) tables_in_date_range
        ) final_table_set

      cross join
        (select distinct create_dt from space_usage_by_table order by create_dt desc limit 2) date_range

      group by
        final_table_set.table_name
      ) date_tables

    left outer join
      (
        select 
          last_tables.table_name as last_table_name
          , last_tables.size_in_gb as last_size_in_gb
          , last_tables.create_dt as last_create_dt
        from
          space_usage_by_table last_tables
        join
          (select distinct create_dt from space_usage_by_table order by create_dt desc limit 2) date_range
          on (
            date_range.create_dt = last_tables.create_dt
          )
      ) last_size_data
      on (last_size_data.last_table_name = date_tables.table_name and last_size_data.last_create_dt = date_tables.min_create_dt)

    left outer join
      (
        select 
          current_tables.table_name as current_table_name
          , current_tables.size_in_gb as current_size_in_gb
          , current_tables.create_dt as current_create_dt
        from
          space_usage_by_table current_tables
        join
          (select distinct create_dt from space_usage_by_table order by create_dt desc limit 2) date_range
          on (
            date_range.create_dt = current_tables.create_dt
          )
      ) current_size_data
      on (current_size_data.current_table_name = date_tables.table_name and current_size_data.current_create_dt = date_tables.max_create_dt)
    ) size_data
  ) size_data_w_delta
order by
  percent_delta desc
;
