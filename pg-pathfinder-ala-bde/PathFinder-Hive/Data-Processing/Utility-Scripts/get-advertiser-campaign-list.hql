﻿select
a.advertiser_id
, a.advertiser
, c.buy_id as campaign_id
, c.buy as campaign_name
, c.start_dt as campaign_start_date
, c.end_dt as campaign_end_date
, case
    when pfcampaign_list.buy_id is null then 'not in pathfinder'
    when pfcampaign_list.buy_id is not null and pfcampaign_list.date_dropped != '' then 'dropped'
    when pfcampaign_list.buy_id is not null and pfcampaign_list.date_dropped = '' then 'live'
  end as pathfinder_status

from
dfa_meta_advertiser a

join (
  select * from dfa_meta_campaign where dfa_meta_campaign.end_dt > '2014-12-31'
) c
on c.advertiser_id = a.advertiser_id

left outer join pfcampaign_list
on pfcampaign_list.buy_id = c.buy_id

order by
advertiser
, campaign_start_date
, campaign_end_date
;
