﻿select
*
from

(
  select 'site' as dim, site as dimValue, count(*) as dimInstanceCt 
  from logs_pfcamp_matched
  group by site

  union all

  select 'tactic' as dim, tactic as dimValue, count(*) as dimInstanceCt 
  from logs_pfcamp_matched
  group by tactic

  union all

  select 'creative' as dim, creative as dimValue, count(*) as dimInstanceCt 
  from logs_pfcamp_matched
  group by creative

  union all

  select 'sfg_type' as dim, sfg_type as dimValue, count(*) as dimInstanceCt 
  from logs_pfcamp_matched
  group by sfg_type

  union all

  select 'targeting_type' as dim, targeting_type as dimValue, count(*) as dimInstanceCt 
  from logs_pfcamp_matched
  group by targeting_type

  union all

  select 'ad_size' as dim, ad_size as dimValue, count(*) as dimInstanceCt  
  from logs_pfcamp_matched
  group by ad_size

  union all

  select 'exe_type' as dim, exe_type as dimValue, count(*) as dimInstanceCt  
  from logs_pfcamp_matched
  group by exe_type

  union all

  select 'dcm_rate_type' as dim, dcm_rate_type as dimValue, count(*) as dimInstanceCt  
  from logs_pfcamp_matched
  group by dcm_rate_type
) dims
