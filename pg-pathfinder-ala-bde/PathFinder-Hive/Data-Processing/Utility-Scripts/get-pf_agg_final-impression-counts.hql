SET hive.auto.convert.join=true;
SET hive.exec.parallel=true;
SET io.sort.mb=512;

select
pfcampaign
, max(impressions) pf_agg_imp_ct_max
, min(impressions) pf_agg_imp_ct_min

from
(
select 
pfcampaign
, dimtype
, sum(imps) as impressions

from
pf_agg_final_no_exe_type_2015_02_WoC_w_campaign

group by
pfcampaign
, dimtype

order by
pfcampaign
, dimtype
) pf_agg_totals

group by
pfcampaign

order by
pfcampaign

;

