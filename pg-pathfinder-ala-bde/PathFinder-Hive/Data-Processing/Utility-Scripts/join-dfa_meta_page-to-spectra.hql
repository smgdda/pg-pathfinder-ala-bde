﻿select
*
from
(

  select
  pfcampaign_list.brand
  , pfcampaign_list.pfcampaign
  , p.buy_id
  , p.site_id
  , s.site
  , p.page_id
  , p.site_placement
  , regexp_replace(lower(p.dcm_rate_type), '[^a-zA-Z0-9]', '') as dcm_rate_type
  , p.dcm_rate
  , p.dcm_qty
  , regexp_replace(lower(spectra_plcmt_mbox_rates.placement_rate_type), '[^a-zA-Z0-9]', '') as spectra_rate_type
  , spectra_plcmt_mbox_rates.rate as spectra_rate
  , spectra_plcmt_mbox_rates.calculated_cpm

  FROM
  (select * from pfcampaign_list where date_dropped = '') pfcampaign_list

  JOIN (
    select 
      page_id
      , buy_id
      , site_id
      , split(regexp_replace(lower(site_placement), '[^a-zA-Z0-9_\\|]', ''), '\\|')[0] as site_placement 
      , regexp_replace(lower(pricing_type), '[^a-zA-Z0-9\\_]', '') as dcm_rate_type
      , purchase_cost as dcm_rate
      , purchase_quantity as dcm_qty
    from 
      dfa_meta_page 
    where 
      page_id is not null
      and split(regexp_replace(lower(site_placement), '[^a-zA-Z0-9_\\|]', ''), '\\|')[0] not like '%cancel%'
      and split(regexp_replace(lower(site_placement), '[^a-zA-Z0-9_\\|]', ''), '\\|')[0] not like 'pfv%'
    ) p 
  ON pfcampaign_list.buy_id = p.buy_id

  LEFT OUTER JOIN (select 
      site_id
      , regexp_replace(lower(site), '[^a-zA-Z0-9_]', '') as site 
    from 
      dfa_meta_site) s
  ON (s.site_id = p.site_id)

  LEFT OUTER JOIN spectra_plcmt_mbox_rates
  ON (
    spectra_plcmt_mbox_rates.adserver_placement_id = p.page_id
    and spectra_plcmt_mbox_rates.campaign_name = pfcampaign_list.campaign_name
    )

  union all

  select
  pfcampaign_list.brand
  , pfcampaign_list.pfcampaign
  , p.buy_id
  , p.site_id
  , s.site
  , p.page_id
  , p.site_placement
  , regexp_replace(lower(p.dcm_rate_type), '[^a-zA-Z0-9]', '') as dcm_rate_type
  , p.dcm_rate
  , p.dcm_qty
  , regexp_replace(lower(spectra_plcmt_mbox_rates.placement_rate_type), '[^a-zA-Z0-9]', '') as spectra_rate_type
  , spectra_plcmt_mbox_rates.rate as spectra_rate
  , spectra_plcmt_mbox_rates.calculated_cpm

  FROM
  (select * from pfcampaign_list where date_dropped = '') pfcampaign_list

  JOIN (
    select 
      page_id
      , buy_id
      , site_id
      , split(regexp_replace(lower(site_placement), '[^a-zA-Z0-9_\\|]', ''), '\\|')[0] as site_placement 
      , regexp_replace(regexp_replace(regexp_replace(split(regexp_replace(lower(site_placement), '[^a-zA-Z0-9_\\|]', ''), '\\|')[0], 'prerollcreative[a-z]', 'preroll'), 'pfv\\_', ''), 'preroll[a-z]', 'preroll') as spectra_plcmt_name
      , regexp_replace(lower(pricing_type), '[^a-zA-Z0-9\\_]', '') as dcm_rate_type
      , purchase_cost as dcm_rate
      , purchase_quantity as dcm_qty
    from 
      dfa_meta_page 
    where 
      page_id is not null
      and split(regexp_replace(lower(site_placement), '[^a-zA-Z0-9_\\|]', ''), '\\|')[0] not like '%cancel%'
      and split(regexp_replace(lower(site_placement), '[^a-zA-Z0-9_\\|]', ''), '\\|')[0] like 'pfv%'
    ) p 
  ON pfcampaign_list.buy_id = p.buy_id

  LEFT OUTER JOIN (select 
      site_id
      , regexp_replace(lower(site), '[^a-zA-Z0-9_]', '') as site 
    from 
      dfa_meta_site) s
  ON (s.site_id = p.site_id)

  LEFT OUTER JOIN spectra_plcmt_mbox_rates
  ON (
    spectra_plcmt_mbox_rates.placement_name = p.spectra_plcmt_name
    )

) unioned_dcm_spectra_plcmt_rates
;