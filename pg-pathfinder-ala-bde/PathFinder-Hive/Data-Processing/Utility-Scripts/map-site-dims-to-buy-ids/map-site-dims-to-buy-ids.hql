﻿from
  plcmt_pfscore
join
  dfa_meta_page
  on dfa_meta_page.page_id = plcmt_pfscore.page_id
join
  pfcampaign_list
  on pfcampaign_list.buy_id = dfa_meta_page.buy_id

select distinct
  pfcampaign_list.pfcampaign
  , dfa_meta_page.buy_id
  , pfcampaign_list.campaign_name
  , plcmt_pfscore.sitetactic
  , plcmt_pfscore.siteadsize
  , plcmt_pfscore.sitecreative

order by
  pfcampaign
  , buy_id
  , campaign_name
  , sitetactic
  , siteadsize
  , sitecreative