SET hive.exec.compress.intermediate=true;
SET hive.exec.compress.output=true;
SET mapred.output.compression.type=BLOCK;
SET mapred.output.compression.codec=org.apache.hadoop.io.compress.SnappyCodec;
SET hive.optimize.bucketmapjoin=true;
SET hive.exec.parallel=true;
SET hive.auto.convert.join=true;
SET hive.exec.dynamic.partition=true;
SET hive.exec.dynamic.partition.mode=nonstrict;

from
(select * from pfcampaign_list) pfc

join ingestion_dfa_impressions i
on (
    i.ingestion_dt >= concat_ws('-', substring('${hiveconf:imp_begin_dt}', 1, 4), substring('${hiveconf:imp_begin_dt}', 5, 2), substring('${hiveconf:imp_begin_dt}', 7, 2))
    and i.ingestion_dt < concat_ws('-', substring('${hiveconf:imp_end_dt}', 1, 4), substring('${hiveconf:imp_end_dt}', 5, 2), substring('${hiveconf:imp_end_dt}', 7, 2))
    and i.buy_id = pfc.buy_id 
    and i.user_id != 0
  )

join (
  select
  page_id
  , buy_id
  , split(regexp_replace(lower(site_placement), '[^a-z0-9_\\|]', ''), '\\|')[0] as site_placement
  , regexp_replace(lower(pricing_type), '[^a-zA-Z0-9\\_]', '') as dcm_rate_type
  , purchase_cost as dcm_rate
  , purchase_quantity as dcm_qty
  from
  dfa_meta_page
  where
  page_id is not null
  and split(regexp_replace(lower(site_placement), '[^a-zA-Z0-9_\\|]', ''), '\\|')[0] not like '%cancel%'
  ) p
on (
  p.page_id = i.page_id
  and p.buy_id = i.buy_id
  )

join (
  select 
    site_id
    , regexp_replace(lower(site), '[^a-zA-Z0-9_]', '') as site 
  from 
    dfa_meta_site
  ) s
on (
  s.site_id = i.site_id
  )

left outer join (
  select 
    creative_id
    , ui_creative_id
    , regexp_replace(lower(creative),'[^a-zA-Z0-9_]','') as creative
  from 
    dfa_meta_creative 
  where 
    creative_id is not null
    and split(regexp_replace(lower(creative),'[^a-zA-Z0-9_]',''),'_')[2] <> 'flite') c
on (
  c.creative_id = i.creative_id
  )

left outer join (
  select 
    ad_id
    , regexp_replace(lower(ad_name),'[^a-zA-Z0-9_]','') as ad_name 
  from 
    ingestion_flite_lookup
  )  f
on (
  f.ad_id = substr(i.site_data, 3, length(i.site_data))
  )



insert into table pf_daily_dfa_merge_plcmt_stats partition (event_dt)
select
i.buy_id
, i.site_id
, s.site
, i.page_id
, p.site_placement
, i.creative_id
, c.creative as creative_name
, case
    when substr(p.site_placement,0,3) = 'pff' 
      then f.ad_name       
    else split(c.creative,'_')[2] 
  end as creative
, c.ui_creative_id
, case when p.site_placement like 'pff_%' then split(lower(i.site_data), 'u=')[1] else null end as site_data
, case
    when substr(p.site_placement,0,2) = 'pf' 
      then concat_ws('_',s.site,split(p.site_placement,'_')[1],split(p.site_placement,'_')[4])
    else concat_ws('_',s.site,split(p.site_placement,'_')[0],split(p.site_placement,'_')[3])
  end as sitetactic
, case
    when substr(p.site_placement,0,2) = 'pf'
      then concat_ws('_',s.site,split(p.site_placement,'_')[6])
    else concat_ws('_',s.site,split(p.site_placement,'_')[5])
  end as siteadsize
, case
    when substr(p.site_placement,0,3) = 'pff' 
      then concat_ws('_',s.site,f.ad_name)
    else concat_ws('_',s.site,split(c.creative,'_')[2])
  end as sitecreative
, count(*) as total_imps
, count(distinct i.user_id) as total_users
, sum(case when i.user_id <> '0' then 1 else 0 end) as total_identifiable_users
, p.dcm_rate_type
, p.dcm_rate
, p.dcm_qty
, to_date(FROM_UNIXTIME(UNIX_TIMESTAMP(time, 'MM-dd-yyyy-HH:mm:ss'))) as event_dt

group by
 to_date(FROM_UNIXTIME(UNIX_TIMESTAMP(time, 'MM-dd-yyyy-HH:mm:ss')))
, i.buy_id
, i.site_id
, s.site
, i.page_id
, p.site_placement
, i.creative_id
, c.creative
, case
    when substr(p.site_placement,0,3) = 'pff' 
      then f.ad_name       
    else split(c.creative,'_')[2] 
  end
, c.ui_creative_id
, case
    when substr(p.site_placement,0,2) = 'pf' 
      then concat_ws('_',s.site,split(p.site_placement,'_')[1],split(p.site_placement,'_')[4])
    else concat_ws('_',s.site,split(p.site_placement,'_')[0],split(p.site_placement,'_')[3])
  end
, case
    when substr(p.site_placement,0,2) = 'pf'
      then concat_ws('_',s.site,split(p.site_placement,'_')[6])
    else concat_ws('_',s.site,split(p.site_placement,'_')[5])
  end
, case
    when substr(p.site_placement,0,3) = 'pff' 
      then concat_ws('_',s.site,f.ad_name)
    else concat_ws('_',s.site,split(c.creative,'_')[2])
  end
, case when p.site_placement like 'pff_%' then split(lower(i.site_data), 'u=')[1] else null end
, p.dcm_rate_type
, p.dcm_rate
, p.dcm_qty
;

