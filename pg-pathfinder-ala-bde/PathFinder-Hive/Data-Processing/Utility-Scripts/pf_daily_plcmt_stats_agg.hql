﻿SET hive.exec.compress.intermediate=true;
SET hive.exec.compress.output=true;
SET mapred.output.compression.type=BLOCK;
SET mapred.output.compression.codec=org.apache.hadoop.io.compress.SnappyCodec;
SET hive.exec.dynamic.partition=true;
SET hive.exec.dynamic.partition.mode=nonstrict;
SET hive.exec.max.dynamic.partitions.pernode=1500;

drop table if exists pf_daily_impressions_per_placement;
create table pf_daily_impressions_per_placement like pf_daily_dfa_merge_plcmt_stats;

insert into table pf_daily_impressions_per_placement partition (event_dt)
select
buy_id
, site_id
, site
, page_id
, site_placement
, creative_id
, creative_name
, creative
, ui_creative_id
, site_data
, sitetactic
, siteadsize
, sitecreative
, sum(total_imps) as impression_ct
, sum(total_users) as user_ct
, dcm_rate_type
, dcm_rate
, dcm_qty
, event_dt

from
pf_daily_dfa_merge_plcmt_stats

group by
buy_id
, site_id
, site
, page_id
, site_placement
, creative_id
, creative_name
, creative
, ui_creative_id
, site_data
, sitetactic
, siteadsize
, sitecreative
, dcm_rate_type
, dcm_rate
, dcm_qty
, event_dt
;
