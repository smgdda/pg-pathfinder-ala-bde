﻿DROP TABLE IF EXISTS survival_analysis_data_setup_step_004;
CREATE TABLE survival_analysis_data_setup_step_004
(
  dfa_user_id string
  , ud_random_var double
)
PARTITIONED BY (
  pfcampaign string
)
ROW FORMAT SERDE 
  'org.apache.hadoop.hive.serde2.columnar.ColumnarSerDe' 
STORED AS INPUTFORMAT 
  'org.apache.hadoop.hive.ql.io.RCFileInputFormat' 
OUTPUTFORMAT 
  'org.apache.hadoop.hive.ql.io.RCFileOutputFormat'
;

