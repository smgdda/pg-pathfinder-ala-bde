﻿DROP TABLE IF EXISTS survival_analysis_data_setup_final_output;

CREATE TABLE survival_analysis_data_setup_final_output
(
  dfa_user_id string
  , sitetactic string
  , freq_count string
  , conv_user string
  , ud_random_var string
  , time_to_event string
)
PARTITIONED BY
(
  pfcampaign string
)
ROW FORMAT SERDE 
  'org.apache.hadoop.hive.serde2.columnar.ColumnarSerDe' 
STORED AS INPUTFORMAT 
  'org.apache.hadoop.hive.ql.io.RCFileInputFormat' 
OUTPUTFORMAT 
  'org.apache.hadoop.hive.ql.io.RCFileOutputFormat'
;