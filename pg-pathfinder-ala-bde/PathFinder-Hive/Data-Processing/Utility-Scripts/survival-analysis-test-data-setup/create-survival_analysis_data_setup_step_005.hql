﻿DROP TABLE IF EXISTS survival_analysis_data_setup_step_005;

CREATE TABLE survival_analysis_data_setup_step_005
(
  dfa_user_id string, 
  dlx_encrypted_dfa_user_id string, 
  sitetactic string, 
  freq_count bigint, 
  conv_user int,
  ud_random_var double
)
PARTITIONED BY 
(
  pfcampaign string
)
CLUSTERED BY
(
  dfa_user_id
)
INTO 20 BUCKETS
ROW FORMAT SERDE 
  'org.apache.hadoop.hive.serde2.columnar.ColumnarSerDe' 
STORED AS INPUTFORMAT 
  'org.apache.hadoop.hive.ql.io.RCFileInputFormat' 
OUTPUTFORMAT 
  'org.apache.hadoop.hive.ql.io.RCFileOutputFormat'
;
