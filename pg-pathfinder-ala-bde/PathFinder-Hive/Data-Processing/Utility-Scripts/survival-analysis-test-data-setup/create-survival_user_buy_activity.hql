﻿SET hive.exec.compress.intermediate=TRUE;
SET hive.exec.compress.output=TRUE;
SET mapred.output.compression.type=BLOCK;
SET mapred.output.compression.codec=org.apache.hadoop.io.compress.SnappyCodec;

DROP TABLE IF EXISTS survival_user_buy_activity;

CREATE TABLE survival_user_buy_activity
ROW FORMAT SERDE 
  'org.apache.hadoop.hive.serde2.columnar.ColumnarSerDe' 
STORED AS INPUTFORMAT 
  'org.apache.hadoop.hive.ql.io.RCFileInputFormat' 
OUTPUTFORMAT 
  'org.apache.hadoop.hive.ql.io.RCFileOutputFormat'
as
SELECT
  buy_id
  , user_id as dfa_user_id
  , case when dlx_encrypted_dfa_user_id is NULL then user_id else dlx_encrypted_dfa_user_id end as dlx_encrypted_dfa_user_id
  , min(FROM_UNIXTIME(UNIX_TIMESTAMP(time, 'MM-dd-yyyy-HH:mm:ss'))) as min_activity_dts
  , max(FROM_UNIXTIME(UNIX_TIMESTAMP(time, 'MM-dd-yyyy-HH:mm:ss'))) as max_activity_dts
  , datediff(max(FROM_UNIXTIME(UNIX_TIMESTAMP(time, 'MM-dd-yyyy-HH:mm:ss'))), min(FROM_UNIXTIME(UNIX_TIMESTAMP(time, 'MM-dd-yyyy-HH:mm:ss')))) as days_between_min_max_activities
  , count(*) as activity_instance_ct

FROM 
  ingestion_dfa_activity

WHERE
  ingestion_dt >= '2015-06-30'
  and ingestion_dt <= '2015-08-09'
  and activity_type like 'pfind%'
  and user_id != '0'
  and (buy_id != '' and buy_id is not null)
  
GROUP BY
  buy_id
  , user_id
  , case when dlx_encrypted_dfa_user_id is NULL then user_id else dlx_encrypted_dfa_user_id end

ORDER BY
  buy_id
  , days_between_min_max_activities DESC
;
