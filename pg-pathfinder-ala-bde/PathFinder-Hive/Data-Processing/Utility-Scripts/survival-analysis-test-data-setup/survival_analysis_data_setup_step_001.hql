﻿SET hive.exec.dynamic.partition=true; 
SET hive.exec.dynamic.partition.mode=nonstrict; 
SET hive.exec.compress.output=true; 
SET hive.exec.compress.intermediate=true; 
SET mapred.output.compression.type=BLOCK; 
SET mapred.output.compression.codec=org.apache.hadoop.io.compress.SnappyCodec; 

DROP TABLE IF EXISTS survival_analysis_data_setup_step_001;

CREATE TABLE survival_analysis_data_setup_step_001 
(
  dfa_user_id STRING
)
PARTITIONED BY
(
  pfcampaign string
)
ROW FORMAT SERDE 
  'org.apache.hadoop.hive.serde2.columnar.ColumnarSerDe' 
STORED AS INPUTFORMAT 
  'org.apache.hadoop.hive.ql.io.RCFileInputFormat' 
OUTPUTFORMAT 
  'org.apache.hadoop.hive.ql.io.RCFileOutputFormat'
;

INSERT INTO TABLE survival_analysis_data_setup_step_001 PARTITION(pfcampaign)
SELECT DISTINCT
  dfa_user_id
  , pfcampaign
FROM 
  logs_product
;
