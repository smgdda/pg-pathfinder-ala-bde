﻿SET hive.exec.dynamic.partition=true; 
SET hive.exec.dynamic.partition.mode=nonstrict; 
SET hive.exec.compress.intermediate=true;
SET hive.exec.compress.output=true;
SET mapred.output.compression.type=BLOCK;
SET mapred.output.compression.codec=org.apache.hadoop.io.compress.SnappyCodec;

INSERT INTO TABLE survival_analysis_data_setup_step_002 PARTITION(pfcampaign='${hiveconf:pfcampaign}')
SELECT  DISTINCT
  dfa_user_id
FROM
  (
    SELECT
      buy_id
    FROM
      pfcampaign_list
    WHERE
      lower(pfcampaign) = '${hiveconf:pfcampaign}'
      and buy_id is not NULL
      and date_dropped = ''
  ) related_buy_ids
  JOIN
  (
    SELECT
      DISTINCT user_id as dfa_user_id, buy_id
    FROM
      ingestion_dfa_impressions
    WHERE
      ingestion_dfa_impressions.ingestion_dt >= concat_ws('-', substring('${hiveconf:imp_begin_dt}', 1, 4), substring('${hiveconf:imp_begin_dt}', 5, 2), substring('${hiveconf:imp_begin_dt}', 7, 2))
      and ingestion_dfa_impressions.ingestion_dt < concat_ws('-', substring('${hiveconf:imp_end_dt}', 1, 4), substring('${hiveconf:imp_end_dt}', 5, 2), substring('${hiveconf:imp_end_dt}', 7, 2))
      and user_id != '0'
  ) ingestion_dfa_impressions
  ON ingestion_dfa_impressions.buy_id = related_buy_ids.buy_id
;
