﻿SET hive.exec.compress.intermediate=true;
SET hive.exec.compress.output=true;
SET mapred.output.compression.type=BLOCK;
SET mapred.output.compression.codec=org.apache.hadoop.io.compress.SnappyCodec;

DROP TABLE IF EXISTS survival_analysis_data_setup_step_003;

CREATE TABLE survival_analysis_data_setup_step_003 
ROW FORMAT SERDE 
  'org.apache.hadoop.hive.serde2.columnar.ColumnarSerDe' 
STORED AS INPUTFORMAT 
  'org.apache.hadoop.hive.ql.io.RCFileInputFormat' 
OUTPUTFORMAT 
  'org.apache.hadoop.hive.ql.io.RCFileOutputFormat'
AS
SELECT DISTINCT
  dfa_user_id
  , pfcampaign
FROM
  survival_analysis_data_setup_step_002
WHERE
  dfa_user_id != '0'
;
