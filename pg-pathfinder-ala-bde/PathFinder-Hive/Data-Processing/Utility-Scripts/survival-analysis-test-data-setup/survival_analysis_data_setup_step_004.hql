﻿SET hive.exec.dynamic.partition=true; 
SET hive.exec.dynamic.partition.mode=nonstrict; 
SET hive.exec.compress.intermediate=true;
SET hive.exec.compress.output=true;
SET mapred.output.compression.type=BLOCK;
SET mapred.output.compression.codec=org.apache.hadoop.io.compress.SnappyCodec;

INSERT INTO TABLE survival_analysis_data_setup_step_004 PARTITION (pfcampaign = '${hiveconf:pfcampaign}')
select 
  survival_analysis_data_setup_step_001.dfa_user_id
  , rand() as ud_random_var
from
  (
    select distinct 
      dfa_user_id 
    from 
      survival_analysis_data_setup_step_001 
    where 
      pfcampaign = '${hiveconf:pfcampaign}'
  ) survival_analysis_data_setup_step_001
left outer join
  survival_analysis_data_setup_step_003 
  on (
    survival_analysis_data_setup_step_003.pfcampaign = '${hiveconf:pfcampaign}'
    and survival_analysis_data_setup_step_003.dfa_user_id = survival_analysis_data_setup_step_001.dfa_user_id
  )
where
  survival_analysis_data_setup_step_003.dfa_user_id is null
;
