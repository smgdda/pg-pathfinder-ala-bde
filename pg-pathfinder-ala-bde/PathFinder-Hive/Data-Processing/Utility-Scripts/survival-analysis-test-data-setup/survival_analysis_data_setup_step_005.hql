﻿SET hive.exec.dynamic.partition=true; 
SET hive.exec.dynamic.partition.mode=nonstrict; 
SET hive.exec.compress.intermediate=true;
SET hive.exec.compress.output=true;
SET mapred.output.compression.type=BLOCK;
SET mapred.output.compression.codec=org.apache.hadoop.io.compress.SnappyCodec;

INSERT INTO TABLE survival_analysis_data_setup_step_005 PARTITION (pfcampaign = '${hiveconf:pfcampaign}')
SELECT
  woc_lr_sitetactic.dfa_user_id
  , woc_lr_sitetactic.dlx_encrypted_dfa_user_id
  , woc_lr_sitetactic.sitetactic
  , woc_lr_sitetactic.freq_count
  , woc_lr_sitetactic.conv_user
  , survival_analysis_data_setup_step_004.ud_random_var
  
FROM
(
  SELECT 
    * 
  FROM
    lr_sitetactic_freq 
  WHERE
    pfcampaign = '${hiveconf:pfcampaign}'
) woc_lr_sitetactic
JOIN 
  survival_analysis_data_setup_step_004 
  ON (
    survival_analysis_data_setup_step_004.pfcampaign = '${hiveconf:pfcampaign}'
    and survival_analysis_data_setup_step_004.dfa_user_id = woc_lr_sitetactic.dfa_user_id
  )
;