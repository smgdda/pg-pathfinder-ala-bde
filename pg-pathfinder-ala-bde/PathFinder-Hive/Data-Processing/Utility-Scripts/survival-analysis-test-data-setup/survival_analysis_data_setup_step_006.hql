﻿SET hive.exec.dynamic.partition=true; 
SET hive.exec.dynamic.partition.mode=nonstrict; 
SET hive.exec.compress.intermediate=true;
SET hive.exec.compress.output=true;
SET mapred.output.compression.type=BLOCK;
SET mapred.output.compression.codec=org.apache.hadoop.io.compress.SnappyCodec;

ALTER TABLE survival_analysis_data_setup_step_006 DROP IF EXISTS PARTITION (pfcampaign = '${hiveconf:pfcampaign}');

INSERT INTO TABLE survival_analysis_data_setup_step_006 PARTITION (pfcampaign = '${hiveconf:pfcampaign}')
SELECT
  survival_analysis_data_setup_step_005.dfa_user_id
  , survival_analysis_data_setup_step_005.dlx_encrypted_dfa_user_id
  , survival_analysis_data_setup_step_005.sitetactic
  , survival_analysis_data_setup_step_005.freq_count
  , survival_analysis_data_setup_step_005.conv_user
  , survival_analysis_data_setup_step_005.ud_random_var
  , case
      WHEN max_activity_dts is NULL THEN (unix_timestamp('2015-07-17 23:59:59') - unix_timestamp(min_exposure_dt)  ) / 86400
      WHEN max_activity_dts > min_exposure_dt THEN (unix_timestamp(max_activity_dts) - unix_timestamp(min_exposure_dt)) / 86400
      ELSE (unix_timestamp('2015-07-17 23:59:59') - unix_timestamp(min_exposure_dt)  ) / 86400
    end as time_to_event
  
FROM
  survival_analysis_data_setup_step_005

JOIN (
  select
    dfa_user_id
    , min(event_dts) as min_exposure_dt
  from
    logs_product 
  where
    pfcampaign = '${hiveconf:pfcampaign}'
    and logs_product.freq = 1
  group by
    dfa_user_id
  ) logs_product 
on (
  survival_analysis_data_setup_step_005.pfcampaign = '${hiveconf:pfcampaign}'
  and logs_product.dfa_user_id = survival_analysis_data_setup_step_005.dfa_user_id
  )

left outer join (
  select
    dfa_user_id
    , max_activity_dts
  from
    (select buy_id from pfcampaign_list where pfcampaign = '${hiveconf:pfcampaign}' and date_dropped = '' and buy_id is not null) pfcampaign_list
  join
    dfa_user_buy_activity
    on dfa_user_buy_activity.buy_id = pfcampaign_list.buy_id
  ) dfa_activity
on (
  dfa_activity.dfa_user_id = survival_analysis_data_setup_step_005.dfa_user_id
  )
;
