﻿SET hive.exec.dynamic.partition=true; 
SET hive.exec.dynamic.partition.mode=nonstrict; 
SET hive.exec.compress.intermediate=true;
SET hive.exec.compress.output=true;
SET mapred.output.compression.type=BLOCK;
SET mapred.output.compression.codec=org.apache.hadoop.io.compress.SnappyCodec;

ALTER TABLE survival_analysis_data_setup_final_output 
  DROP IF EXISTS PARTITION (pfcampaign = '${hiveconf:pfcampaign}');

INSERT INTO TABLE survival_analysis_data_setup_final_output 
  PARTITION (pfcampaign = '${hiveconf:pfcampaign}')
select
  dfa_user_id
  , sitetactic
  , freq_count
  , conv_user
  , ud_random_var
  , time_to_event
from
  survival_analysis_data_setup_step_006
WHERE
  pfcampaign = '${hiveconf:pfcampaign}'
;
