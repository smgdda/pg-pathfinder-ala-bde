﻿-- THIS PROCESS HAS BEEN SUPERCEDED BY PYTHON-BASED PROCESS.
-- PYTHON SCRIPT TO GENERATE SINGLE TSV's FROM HIVE DATA
-- CAN BE FOUND IN \PathFinder-Python\Utility-Scripts\query-hive-write-single-tsv\tsv-generator.py

SET hive.merge.mapredfiles = TRUE;
SET hive.exec.dynamic.partition = TRUE;
SET hive.exec.dynamic.partition.mode = nonstrict;

INSERT INTO TABLE csv_outputs PARTITION(pfcampaign='${hiveconf:pf_campaign_name}')
SELECT
  dfa_user_id
  , sitetactic
  , freq_count
  , conv_user
  , ud_random_var
  , time_to_event
FROM
  survival_analysis_data_setup_final_output 
WHERE
  pfcampaign = '${hiveconf:pf_campaign_name}'
;
