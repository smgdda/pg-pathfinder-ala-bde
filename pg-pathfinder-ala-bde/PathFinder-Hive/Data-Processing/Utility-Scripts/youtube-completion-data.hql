﻿DROP TABLE IF EXISTS youtube_daily_completions;
CREATE EXTERNAL TABLE youtube_daily_completions
(
  date string, 
  campaign_name string, 
  campaign_id string, 
  publisher string, 
  publisher_id string, 
  video_id string, 
  video_name string, 
  placement_name string, 
  impressions string, 
  full_completions string, 
  completion_rate string
)
ROW FORMAT DELIMITED 
FIELDS TERMINATED BY '\t' 
LINES TERMINATED BY '\n' 
STORED AS TEXTFILE
LOCATION
  'hdfs://vmcwy62059.prod.acxiom.net:8020/user/owatson/youtube-daily-placement-completions'
;

-- youtube_daily_completions_cleaned needs to be joined to dcm_spectra_merge to get appropriate cost data

DROP TABLE IF EXISTS youtube_daily_completions_cleaned;
CREATE TABLE youtube_daily_completions_cleaned AS
SELECT
  youtube_pfc_joined.cleaned_plcmt_name
  , youtube_pfc_joined.video_name
  , regexp_replace(lower(youtube_pfc_joined.split_creative_name), '[^a-zA-Z0-9]', '') as derived_creative_name
  , youtube_pfc_joined.campaign_name
  , youtube_pfc_joined.buy_id
  , youtube_pfc_joined.pfcampaign
  , sum(youtube_pfc_joined.impressions) as innovid_imp_sum
  , sum(youtube_pfc_joined.full_completions) as innovid_completions_sum
FROM
  (
  select
    lower(regexp_replace(concat_ws(
      '_'
      , split(youtube_daily_completions.placement_name, '_')[2]
      , split(youtube_daily_completions.placement_name, '_')[3]
      , split(youtube_daily_completions.placement_name, '_')[4]
      , split(youtube_daily_completions.placement_name, '_')[5]
      , split(youtube_daily_completions.placement_name, '_')[6]
      , split(youtube_daily_completions.placement_name, '_')[7]
      , split(youtube_daily_completions.placement_name, '_')[8]
      , split(youtube_daily_completions.placement_name, '_')[9]
      ), ' \\(secure\\)$', '')) as cleaned_plcmt_name
    , youtube_daily_completions.video_name
    , split(youtube_daily_completions.video_name, '_PG')[0] as split_creative_name
    , size(split(youtube_daily_completions.video_name, '_PG')) as split_size
    , regexp_replace(lower(youtube_daily_completions.campaign_name), '[^a-zA-Z0-9\\_]', '') as campaign_name
    , pfcampaign_list.buy_id
    , lower(pfcampaign_list.pfcampaign) as pfcampaign
    , youtube_daily_completions.impressions
    , youtube_daily_completions.full_completions
    , from_unixtime(unix_timestamp(calendar_date, 'M/d/yyyy'), 'yyyy-MM-dd') as fixed_dt
  from
    youtube_daily_completions
  join
    pfcampaign_list
    on lower(pfcampaign_list.campaign_name) = regexp_replace(lower(youtube_daily_completions.campaign_name), '[^a-zA-Z0-9\\_]', '')
  ) youtube_pfc_joined
WHERE
  fixed_dt >= '2015-07-07'
  and fixed_dt <= '2015-09-05'
GROUP BY
  youtube_pfc_joined.cleaned_plcmt_name
  , youtube_pfc_joined.video_name
  , regexp_replace(lower(youtube_pfc_joined.split_creative_name), '[^a-zA-Z0-9]', '')
  , youtube_pfc_joined.campaign_name
  , youtube_pfc_joined.buy_id
  , youtube_pfc_joined.pfcampaign
;


-- works, but not confident that the rate * the completed views aligns with reality.  getting info from RChin about a specific placement to confirm.
-- this needs to be merged with dcm_spectra_merge to overwrite cost data for relevant placements
DROP TABLE IF EXISTS youtube_completed_costs;

CREATE TABLE youtube_completed_costs AS
SELECT DISTINCT
  youtube_daily_completions_cleaned.buy_id
  , youtube_daily_completions_cleaned.campaign_name
  , youtube_daily_completions_cleaned.cleaned_plcmt_name
  , youtube_daily_completions_cleaned.pfcampaign
  , youtube_daily_completions_cleaned.completed_views
  , youtube_daily_completions_cleaned.impressions_served
  , dcm_spectra_merge.dcm_rate_type
  , dcm_spectra_merge.dcm_rate
  , dcm_spectra_merge.spectra_rate_type
  , dcm_spectra_merge.spectra_rate
  , dcm_spectra_merge.calculated_cpm
  , dcm_spectra_merge.site
  , CASE
      WHEN dcm_spectra_merge.site = 'youtube'
        THEN dcm_spectra_merge.calculated_cpm * youtube_daily_completions_cleaned.completed_views
      WHEN dcm_spectra_merge.site != 'youtube' and dcm_spectra_merge.dcm_rate_type = 'cpm'
        THEN dcm_spectra_merge.calculated_cpm * youtube_daily_completions_cleaned.impressions_served / 1000
    END AS calculated_cost

FROM
  (
    SELECT
      buy_id
      , campaign_name
      , lower(cleaned_plcmt_name) as cleaned_plcmt_name
      , lower(pfcampaign) as pfcampaign
      , sum(full_completion) as completed_views
      , sum(impressions) as impressions_served
    FROM
      youtube_daily_completions_cleaned 
    WHERE
      fixed_dt >= '2015-07-07' 
      and fixed_dt <= '2015-09-05'
    GROUP BY
      buy_id
      , campaign_name
      , cleaned_plcmt_name
      , pfcampaign
  ) youtube_daily_completions_cleaned

JOIN
  dcm_spectra_merge
  ON
  lower(dcm_spectra_merge.pfcampaign) = youtube_daily_completions_cleaned.pfcampaign
  AND
  lower(regexp_replace(dcm_spectra_merge.placement, '^pfv\\_', '')) = youtube_daily_completions_cleaned.cleaned_plcmt_name

ORDER BY
  buy_id
  , campaign_name
  , cleaned_plcmt_name
  , pfcampaign
  , dcm_rate
  , spectra_rate
  , calculated_cpm
;
