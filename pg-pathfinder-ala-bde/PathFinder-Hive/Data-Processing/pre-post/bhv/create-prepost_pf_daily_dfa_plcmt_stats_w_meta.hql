﻿DROP TABLE IF EXISTS prepost_pf_daily_dfa_plcmt_stats_w_meta;
CREATE TABLE prepost_pf_daily_dfa_plcmt_stats_w_meta
ROW FORMAT SERDE 
  'org.apache.hadoop.hive.serde2.columnar.ColumnarSerDe' 
STORED AS INPUTFORMAT 
  'org.apache.hadoop.hive.ql.io.RCFileInputFormat' 
OUTPUTFORMAT 
  'org.apache.hadoop.hive.ql.io.RCFileOutputFormat'
AS
select 
  regexp_replace(lower(pfcampaign_list.pfcampaign), '[^a-zA-Z0-9]', '') as pfcampaign
  , pf_daily_dfa_plcmt_stats.event_dt
  , pf_daily_dfa_plcmt_stats.buy_id
  , pf_daily_dfa_plcmt_stats.page_id
  , dfa_meta_page.site_placement
  , pf_daily_dfa_plcmt_stats.site_id
  , pf_daily_dfa_plcmt_stats.creative_id
  , case 
      when substr(dfa_meta_page.site_placement,0,2) = 'pf'
        then concat_ws('_',case when dfa_meta_site.site = 'audiencescience' then concat_ws('', dfa_meta_site.site, dfa_meta_page.sfg_type) else dfa_meta_site.site end,split(dfa_meta_page.site_placement,'_')[1],split(dfa_meta_page.site_placement,'_')[4])
      else concat_ws('_',case when dfa_meta_site.site = 'audiencescience' then concat_ws('', dfa_meta_site.site, dfa_meta_page.sfg_type) else dfa_meta_site.site end,split(dfa_meta_page.site_placement,'_')[0],split(dfa_meta_page.site_placement,'_')[3])
    end as site_tactic
  , case
      when substr(dfa_meta_page.site_placement,0,2) = 'pf' 
        then concat_ws('_',case when dfa_meta_site.site = 'audiencescience' then concat_ws('', dfa_meta_site.site, dfa_meta_page.sfg_type) else dfa_meta_site.site end,split(dfa_meta_page.site_placement,'_')[6])
      else concat_ws('_',case when dfa_meta_site.site = 'audiencescience' then concat_ws('', dfa_meta_site.site, dfa_meta_page.sfg_type) else dfa_meta_site.site end,split(dfa_meta_page.site_placement,'_')[5])
    end as site_ad_size
  , case
      when substr(dfa_meta_page.site_placement,0,3) = 'pff' 
        then concat_ws('_',case when dfa_meta_site.site = 'audiencescience' then concat_ws('', dfa_meta_site.site, dfa_meta_page.sfg_type) else dfa_meta_site.site end,'need_flite_ad_name') -- f.ad_name      
      else concat_ws('_',case when dfa_meta_site.site = 'audiencescience' then concat_ws('', dfa_meta_site.site, dfa_meta_page.sfg_type) else dfa_meta_site.site end,split(dfa_meta_creative.creative,'_')[2])
    end as site_creative
  , sum(pf_daily_dfa_plcmt_stats.imp_ct) as imp_sum
  , sum(pf_daily_dfa_plcmt_stats.click_ct) as click_sum
  , sum(pf_daily_dfa_plcmt_stats.activity_ct) as act_sum
  , avg(pf_daily_dfa_plcmt_stats.user_ct) as avg_daily_users


from
  pf_daily_dfa_plcmt_stats

join 
  (
  select 
      creative_id
      , ui_creative_id
      , regexp_replace(lower(creative),'[^a-zA-Z0-9_]','') as creative
    from 
      dfa_meta_creative 
    where 
      creative_id is not null
  --    and lower(creative) like '%mrgrimm%'
  ) dfa_meta_creative
  on
  pf_daily_dfa_plcmt_stats.event_dt >= '2015-07-01'
  and dfa_meta_creative.creative_id = pf_daily_dfa_plcmt_stats.creative_id

join
  (
    select 
      site_id
      , regexp_replace(lower(site), '[^a-zA-Z0-9_]', '') as site 
    from 
      dfa_meta_site
  ) dfa_meta_site
  on
  dfa_meta_site.site_id = pf_daily_dfa_plcmt_stats.site_id

join 
  (
    select 
      page_id
      , split(regexp_replace(lower(site_placement), '[^a-zA-Z0-9_\\|]', ''), '\\|')[0] as site_placement 
      , regexp_replace(lower(pricing_type), '[^a-zA-Z0-9\\_]', '') as dcm_rate_type
      , case
        when substr(site_placement,0,2) = 'pf' 
          then split(site_placement,'_')[3]
        else split(site_placement,'_')[2] 
      end as sfg_type
      , purchase_cost as dcm_rate
      , purchase_quantity as dcm_qty
    from 
      dfa_meta_page 
    where 
      page_id is not null
      and split(regexp_replace(lower(site_placement), '[^a-zA-Z0-9_\\|]', ''), '\\|')[0] not like '%cancel%'
  ) dfa_meta_page
  on
  dfa_meta_page.page_id = pf_daily_dfa_plcmt_stats.page_id

join
  pfcampaign_list
  on
  pfcampaign_list.buy_id = pf_daily_dfa_plcmt_stats.buy_id

group by
  regexp_replace(lower(pfcampaign_list.pfcampaign), '[^a-zA-Z0-9]', '')
  , pf_daily_dfa_plcmt_stats.event_dt
  , pf_daily_dfa_plcmt_stats.buy_id
  , pf_daily_dfa_plcmt_stats.page_id
  , dfa_meta_page.site_placement
  , pf_daily_dfa_plcmt_stats.site_id
  , pf_daily_dfa_plcmt_stats.creative_id
  , case 
      when substr(dfa_meta_page.site_placement,0,2) = 'pf'
        then concat_ws('_',case when dfa_meta_site.site = 'audiencescience' then concat_ws('', dfa_meta_site.site, dfa_meta_page.sfg_type) else dfa_meta_site.site end,split(dfa_meta_page.site_placement,'_')[1],split(dfa_meta_page.site_placement,'_')[4])
      else concat_ws('_',case when dfa_meta_site.site = 'audiencescience' then concat_ws('', dfa_meta_site.site, dfa_meta_page.sfg_type) else dfa_meta_site.site end,split(dfa_meta_page.site_placement,'_')[0],split(dfa_meta_page.site_placement,'_')[3])
    end
  , case
      when substr(dfa_meta_page.site_placement,0,2) = 'pf' 
        then concat_ws('_',case when dfa_meta_site.site = 'audiencescience' then concat_ws('', dfa_meta_site.site, dfa_meta_page.sfg_type) else dfa_meta_site.site end,split(dfa_meta_page.site_placement,'_')[6])
      else concat_ws('_',case when dfa_meta_site.site = 'audiencescience' then concat_ws('', dfa_meta_site.site, dfa_meta_page.sfg_type) else dfa_meta_site.site end,split(dfa_meta_page.site_placement,'_')[5])
    end
  , case
      when substr(dfa_meta_page.site_placement,0,3) = 'pff' 
        then concat_ws('_',case when dfa_meta_site.site = 'audiencescience' then concat_ws('', dfa_meta_site.site, dfa_meta_page.sfg_type) else dfa_meta_site.site end,'need_flite_ad_name') -- f.ad_name      
      else concat_ws('_',case when dfa_meta_site.site = 'audiencescience' then concat_ws('', dfa_meta_site.site, dfa_meta_page.sfg_type) else dfa_meta_site.site end,split(dfa_meta_creative.creative,'_')[2])
    end
