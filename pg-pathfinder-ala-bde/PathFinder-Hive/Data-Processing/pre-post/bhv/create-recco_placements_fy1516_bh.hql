﻿SET hive.exec.compress.intermediate=TRUE;
SET hive.exec.compress.output=TRUE;
SET mapred.output.compression.type=BLOCK;
SET mapred.output.compression.codec=org.apache.hadoop.io.compress.SnappyCodec;

drop table if exists recco_placements_fy1516_bh;
create table recco_placements_fy1516_bh as
-- insert into table recco_placements_fy1516_bh
select
  prepost2014_optimizations.recco_num
  , prepost2014_optimizations.pre_start_date_final
  , prepost2014_optimizations.pre_end_date_final
  , prepost2014_optimizations.post_start_date_final
  , prepost2014_optimizations.post_end_date_final
  , prepost2014_optimizations.dimtype
  , prepost2014_optimizations.decrease
  , prepost2014_optimizations.increase
  , pfcampaign_list.buy_id
  , prepost_pf_daily_dfa_plcmt_stats_w_meta.site_placement
  , prepost_pf_daily_dfa_plcmt_stats_w_meta.imp_sum as impression_ct
  , prepost_pf_daily_dfa_plcmt_stats_w_meta.act_sum as activity_ct
  , prepost_pf_daily_dfa_plcmt_stats_w_meta.click_sum as click_ct
  , prepost_pf_daily_dfa_plcmt_stats_w_meta.event_dt
  , prepost_pf_daily_dfa_plcmt_stats_w_meta.site_creative as sitecreative
  , prepost_pf_daily_dfa_plcmt_stats_w_meta.site_tactic as sitetactic
  , prepost_pf_daily_dfa_plcmt_stats_w_meta.site_ad_size as siteadsize
  , prepost_pf_daily_dfa_plcmt_stats_w_meta.site_id
  , prepost_pf_daily_dfa_plcmt_stats_w_meta.page_id
  , prepost_pf_daily_dfa_plcmt_stats_w_meta.creative_id
  , case
      when prepost_pf_daily_dfa_plcmt_stats_w_meta.event_dt >= prepost2014_optimizations.pre_start_date_final
        and prepost_pf_daily_dfa_plcmt_stats_w_meta.event_dt < prepost2014_optimizations.pre_end_date_final
        and (
          (
            prepost2014_optimizations.dimtype = 'sitecreative' and
            prepost_pf_daily_dfa_plcmt_stats_w_meta.site_creative = prepost2014_optimizations.decrease
          )
          or (
            prepost2014_optimizations.dimtype = 'siteadsize' and
            prepost_pf_daily_dfa_plcmt_stats_w_meta.site_ad_size = prepost2014_optimizations.decrease
          )
          or (
            prepost2014_optimizations.dimtype in ('sitetactic','site') and
            prepost_pf_daily_dfa_plcmt_stats_w_meta.site_tactic = prepost2014_optimizations.decrease
          )
        )
      then 1
      else 0
    end as in_dec_pre
  , case
      when prepost_pf_daily_dfa_plcmt_stats_w_meta.event_dt >= prepost2014_optimizations.post_start_date_final
        and prepost_pf_daily_dfa_plcmt_stats_w_meta.event_dt < prepost2014_optimizations.post_end_date_final
        and (
          (
            prepost2014_optimizations.dimtype = 'sitecreative' and
            prepost_pf_daily_dfa_plcmt_stats_w_meta.site_creative = prepost2014_optimizations.decrease
          )
          or (
            prepost2014_optimizations.dimtype = 'siteadsize' and
            prepost_pf_daily_dfa_plcmt_stats_w_meta.site_ad_size = prepost2014_optimizations.decrease
          )
          or (
            prepost2014_optimizations.dimtype in ('sitetactic','site') and
            prepost_pf_daily_dfa_plcmt_stats_w_meta.site_tactic = prepost2014_optimizations.decrease
          )
        )
      then 1
      else 0
    end as in_dec_post
  , case
      when prepost_pf_daily_dfa_plcmt_stats_w_meta.event_dt >= prepost2014_optimizations.pre_start_date_final
        and prepost_pf_daily_dfa_plcmt_stats_w_meta.event_dt < prepost2014_optimizations.pre_end_date_final
        and (
          (
            prepost2014_optimizations.dimtype = 'sitecreative' and
            prepost_pf_daily_dfa_plcmt_stats_w_meta.site_creative = prepost2014_optimizations.increase
          )
          or (
            prepost2014_optimizations.dimtype = 'siteadsize' and
            prepost_pf_daily_dfa_plcmt_stats_w_meta.site_ad_size = prepost2014_optimizations.increase
          )
          or (
            prepost2014_optimizations.dimtype in ('sitetactic','site') and
            prepost_pf_daily_dfa_plcmt_stats_w_meta.site_tactic = prepost2014_optimizations.increase
          )
        )
      then 1
      else 0
    end as in_inc_pre
  , case
      when prepost_pf_daily_dfa_plcmt_stats_w_meta.event_dt >= prepost2014_optimizations.post_start_date_final
        and prepost_pf_daily_dfa_plcmt_stats_w_meta.event_dt < prepost2014_optimizations.post_end_date_final
        and (
          (
            prepost2014_optimizations.dimtype = 'sitecreative' and
            prepost_pf_daily_dfa_plcmt_stats_w_meta.site_creative = prepost2014_optimizations.increase
          )
          or (
            prepost2014_optimizations.dimtype = 'siteadsize' and
            prepost_pf_daily_dfa_plcmt_stats_w_meta.site_ad_size = prepost2014_optimizations.increase
          )
          or (
            prepost2014_optimizations.dimtype in ('sitetactic','site') and
            prepost_pf_daily_dfa_plcmt_stats_w_meta.site_tactic = prepost2014_optimizations.increase
          )
        )
      then 1
      else 0
    end as in_inc_post

from
(
  select
  *
  from
  prepost2014_optimizations
  where lower(recco_num) != 'recco_num'
  and analysis_completed != 1
  and priority_updated in ('PRIORITY1')
--  and recco_num in (
--    '1516_03_gain_ambrosiaush_63',
--    '1516_03_tide_jupilerush_101',
--    '1516_04_tide_springsteen_213',
--    '1516_04_tide_springsteendiamond_240'
--  )
) prepost2014_optimizations

join pfcampaign_list
on (regexp_replace(lower(pfcampaign_list.pfcampaign), '[^a-zA-Z0-9]', '') = prepost2014_optimizations.pfcampaign)

join prepost_pf_daily_dfa_plcmt_stats_w_meta 
on (
  prepost_pf_daily_dfa_plcmt_stats_w_meta.buy_id = pfcampaign_list.buy_id
  )

where
prepost_pf_daily_dfa_plcmt_stats_w_meta.event_dt >= prepost2014_optimizations.pre_start_date_final
and prepost_pf_daily_dfa_plcmt_stats_w_meta.event_dt <= prepost2014_optimizations.post_end_date_final
and (
  (
  prepost2014_optimizations.dimtype in ('sitetactic','site')
  and (
    prepost_pf_daily_dfa_plcmt_stats_w_meta.site_tactic = prepost2014_optimizations.increase
    or prepost_pf_daily_dfa_plcmt_stats_w_meta.site_tactic = prepost2014_optimizations.decrease
    )
    )
  or (
  prepost2014_optimizations.dimtype = 'siteadsize'
  and (
    prepost_pf_daily_dfa_plcmt_stats_w_meta.site_ad_size = prepost2014_optimizations.increase
    or prepost_pf_daily_dfa_plcmt_stats_w_meta.site_ad_size = prepost2014_optimizations.decrease
    )
    )
  or (
  prepost2014_optimizations.dimtype = 'sitecreative'
  and (
    prepost_pf_daily_dfa_plcmt_stats_w_meta.site_creative = prepost2014_optimizations.increase
    or prepost_pf_daily_dfa_plcmt_stats_w_meta.site_creative = prepost2014_optimizations.decrease
    )
    )
)
;

