SET hive.exec.dynamic.partition=true;
SET hive.exec.dynamic.partition.mode=nonstrict;
SET hive.exec.max.dynamic.partitions.pernode=2500;
SET hive.exec.max.dynamic.partitions=4500;
SET hive.exec.compress.intermediate=true;
SET hive.exec.compress.output=true;
SET mapred.output.compression.type=BLOCK;
SET mapred.output.compression.codec=org.apache.hadoop.io.compress.SnappyCodec;
SET hive.auto.convert.join=true;

insert into table prepost_bhv_recco_users PARTITION (recco_num)
select
dfa_impressions.dfa_user_id
, max(case
  when
    dfa_impressions.event_dt >= prepost2014_bhv_recco_plcmts.pre_start_date_final
    and dfa_impressions.event_dt < prepost2014_bhv_recco_plcmts.pre_end_date_final 
    then 1 
  else 0 
  end) as in_pre
, max(case
  when
    dfa_impressions.event_dt >= prepost2014_bhv_recco_plcmts.post_start_date_final 
    and dfa_impressions.event_dt < prepost2014_bhv_recco_plcmts.post_end_date_final 
    then 1 
  else 0 
  end) as in_post
, prepost2014_bhv_recco_plcmts.recco_num

from
prepost_fy1516_fh_bhv_recco_plcmts prepost2014_bhv_recco_plcmts

join (
  select
  user_id as dfa_user_id
  , to_date(FROM_UNIXTIME(UNIX_TIMESTAMP(time, 'MM-dd-yyyy-HH:mm:ss'))) as event_dt
  , buy_id
  , site_id
  , page_id
  , creative_id
  from
  ingestion_dfa_impressions
  where
  ingestion_dfa_impressions.ingestion_dt >= concat_ws('-', substring('${hiveconf:imp_begin_dt}', 1, 4), substring('${hiveconf:imp_begin_dt}', 5, 2), substring('${hiveconf:imp_begin_dt}', 7, 2))
  and ingestion_dfa_impressions.ingestion_dt < concat_ws('-', substring('${hiveconf:imp_end_dt}', 1, 4), substring('${hiveconf:imp_end_dt}', 5, 2), substring('${hiveconf:imp_end_dt}', 7, 2))
     ) dfa_impressions
on (
  dfa_impressions.buy_id = prepost2014_bhv_recco_plcmts.buy_id
  and dfa_impressions.site_id = prepost2014_bhv_recco_plcmts.site_id
  and dfa_impressions.page_id = prepost2014_bhv_recco_plcmts.page_id
  and dfa_impressions.creative_id = prepost2014_bhv_recco_plcmts.creative_id
  )

group by
  prepost2014_bhv_recco_plcmts.recco_num
  , dfa_impressions.dfa_user_id
;