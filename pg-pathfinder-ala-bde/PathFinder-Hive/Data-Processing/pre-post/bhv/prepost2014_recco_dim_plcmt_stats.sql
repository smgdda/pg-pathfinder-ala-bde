drop table if exists prepost2014_recco_dim_plcmt_stats;

create table prepost2014_recco_dim_plcmt_stats as
select
prepost2014_optimizations.recco_num
, prepost2014_optimizations.increase
, prepost2014_optimizations.decrease
, case
    when prepost2014_optimizations.dimtype in ('site', 'sitetactic') then pf_daily_impressions_per_placement.sitetactic
    when prepost2014_optimizations.dimtype in ('siteadsize') then pf_daily_impressions_per_placement.siteadsize
    when prepost2014_optimizations.dimtype in ('sitecreative') then pf_daily_impressions_per_placement.sitecreative
  end as plcmt_recco_dim
, prepost2014_optimizations.pre_start_date_final
, prepost2014_optimizations.pre_end_date_final
, prepost2014_optimizations.post_start_date_final
, prepost2014_optimizations.post_end_date_final
, prepost2014_bhv_plcmt_level_cpm.buy_id
, prepost2014_bhv_plcmt_level_cpm.site_placement
, prepost2014_bhv_plcmt_level_cpm.calculated_cpm
, prepost2014_bhv_plcmt_level_cpm.placement_rate_type
, sum(case
      when pf_daily_impressions_per_placement.event_dt >= prepost2014_optimizations.pre_start_date_final 
      and pf_daily_impressions_per_placement.event_dt < prepost2014_optimizations.pre_end_date_final 
      then pf_daily_impressions_per_placement.impression_ct 
      else cast(0 as bigint) end) as imps_pre
, (sum(case
      when pf_daily_impressions_per_placement.event_dt >= prepost2014_optimizations.pre_start_date_final 
      and pf_daily_impressions_per_placement.event_dt < prepost2014_optimizations.pre_end_date_final 
      then pf_daily_impressions_per_placement.impression_ct 
      else cast(0 as bigint) end) * prepost2014_bhv_plcmt_level_cpm.calculated_cpm)/1000 as spend_pre
, sum(case
      when pf_daily_impressions_per_placement.event_dt >= prepost2014_optimizations.post_start_date_final 
      and pf_daily_impressions_per_placement.event_dt < prepost2014_optimizations.post_end_date_final 
      then pf_daily_impressions_per_placement.impression_ct 
      else cast(0 as bigint) end) as imps_post
, (sum(case
      when pf_daily_impressions_per_placement.event_dt >= prepost2014_optimizations.post_start_date_final 
      and pf_daily_impressions_per_placement.event_dt < prepost2014_optimizations.post_end_date_final 
      then pf_daily_impressions_per_placement.impression_ct 
      else cast(0 as bigint) end) * prepost2014_bhv_plcmt_level_cpm.calculated_cpm)/1000 as spend_post
, sum(case
      when pf_daily_impressions_per_placement.event_dt >= prepost2014_optimizations.pre_start_date_final
      and pf_daily_impressions_per_placement.event_dt < prepost2014_optimizations.post_end_date_final
      then pf_daily_impressions_per_placement.impression_ct 
      else cast(0 as bigint) end) as imps_total_for_recco
, (sum(case
      when pf_daily_impressions_per_placement.event_dt >= prepost2014_optimizations.pre_start_date_final
      and pf_daily_impressions_per_placement.event_dt < prepost2014_optimizations.post_end_date_final
      then pf_daily_impressions_per_placement.impression_ct 
      else cast(0 as bigint) end) * prepost2014_bhv_plcmt_level_cpm.calculated_cpm)/1000 as spend_total
from
(
  select
  *
  from
  prepost2014_optimizations
  where lower(recco_num) != 'recco_num'
  and analysis_completed != 1
  and priority_updated in ('PRIORITY2', 'PRIORITY3', 'PRIORITY4', 'PRIORITY5')
  ) prepost2014_optimizations

join prepost2014_bhv_plcmt_level_cpm_round_2 prepost2014_bhv_plcmt_level_cpm
on prepost2014_bhv_plcmt_level_cpm.recco_num = prepost2014_optimizations.recco_num

join pf_daily_impressions_per_placement
on (
  pf_daily_impressions_per_placement.buy_id = prepost2014_bhv_plcmt_level_cpm.buy_id
  and pf_daily_impressions_per_placement.site_placement = prepost2014_bhv_plcmt_level_cpm.site_placement
  )

where
(
  (
    prepost2014_optimizations.dimtype = 'siteadsize' and (
      pf_daily_impressions_per_placement.siteadsize = prepost2014_optimizations.increase
      or
      pf_daily_impressions_per_placement.siteadsize = prepost2014_optimizations.decrease
      )
    )
  or
  (
    prepost2014_optimizations.dimtype = 'sitecreative' and (
      pf_daily_impressions_per_placement.sitecreative = prepost2014_optimizations.increase
      or
      pf_daily_impressions_per_placement.sitecreative = prepost2014_optimizations.decrease
      )
    )
  or
  (
    prepost2014_optimizations.dimtype in ('sitetactic', 'site') and (
      pf_daily_impressions_per_placement.sitetactic = prepost2014_optimizations.increase
      or
      pf_daily_impressions_per_placement.sitetactic = prepost2014_optimizations.decrease
      )
    )
  )

group by
prepost2014_bhv_plcmt_level_cpm.buy_id
, prepost2014_bhv_plcmt_level_cpm.site_placement
, prepost2014_bhv_plcmt_level_cpm.calculated_cpm
, prepost2014_bhv_plcmt_level_cpm.placement_rate_type
, prepost2014_optimizations.recco_num
, prepost2014_optimizations.increase
, prepost2014_optimizations.decrease
, case
    when prepost2014_optimizations.dimtype in ('site', 'sitetactic') then pf_daily_impressions_per_placement.sitetactic
    when prepost2014_optimizations.dimtype in ('siteadsize') then pf_daily_impressions_per_placement.siteadsize
    when prepost2014_optimizations.dimtype in ('sitecreative') then pf_daily_impressions_per_placement.sitecreative
  end
, prepost2014_optimizations.pre_start_date_final
, prepost2014_optimizations.pre_end_date_final
, prepost2014_optimizations.post_start_date_final
, prepost2014_optimizations.post_end_date_final

order by
recco_num
, plcmt_recco_dim
;
