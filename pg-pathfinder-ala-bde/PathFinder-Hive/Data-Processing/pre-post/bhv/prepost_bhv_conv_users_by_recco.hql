SET hive.exec.compress.intermediate=true;
SET hive.exec.compress.output=true;
SET mapred.output.compression.type=BLOCK;
SET mapred.output.compression.codec=org.apache.hadoop.io.compress.SnappyCodec;
SET hive.auto.convert.join=true;

DROP TABLE IF EXISTS prepost_bhv_conv_users_by_recco;

CREATE TABLE prepost_bhv_conv_users_by_recco AS
SELECT
  user_conv_stats.pfcampaign
  , user_conv_stats.recco_num
  , sum(user_conv_stats.conv_pre) as conv_users_pre
  , sum(user_conv_stats.conv_post) as conv_users_post

FROM
  (
    SELECT
      reccos.pfcampaign
      , reccos.recco_num
      , recco_users.dfa_user_id
      , max(case
          when 
            buy_user_activity.event_dt >= reccos.pre_start_date_final 
            and buy_user_activity.event_dt < reccos.pre_end_date_final 
            and recco_users.in_pre = 1 
            then 1
          else 0
        end) as conv_pre
      , max(case
          when 
            buy_user_activity.event_dt >= reccos.post_start_date_final 
            and buy_user_activity.event_dt < reccos.post_end_date_final 
            and recco_users.in_post = 1 
            then 1
          else 0
        end) as conv_post
  
    FROM
      (
        SELECT
          recco_num
          , pfcampaign
          , pre_start_date_final
          , pre_end_date_final
          , post_start_date_final
          , post_end_date_final
        FROM
          prepost2014_optimizations
        WHERE
          lower(recco_num) != 'recco_num'
          AND analysis_completed != 1
      ) reccos

    JOIN
      (
        select
          buy_id
          , regexp_replace(lower(pfcampaign), '[^a-zA-Z0-9]', '') as pfcampaign
        from
          pfcampaign_list
      ) recco_buy_ids
      ON (recco_buy_ids.pfcampaign = reccos.pfcampaign)

    JOIN
      (
        SELECT
          dfa_user_id
          , buy_id
          , event_dt
        FROM
          prepost_recco_user_activity
      ) buy_user_activity
      ON (buy_user_activity.buy_id = recco_buy_ids.buy_id)

    JOIN
      (
        SELECT
          prepost_bhv_recco_users_agg.recco_num
          , prepost_bhv_recco_users_agg.dfa_user_id
          , prepost_bhv_recco_users_agg.in_pre
          , prepost_bhv_recco_users_agg.in_post
        FROM
          prepost_bhv_recco_users_agg
        WHERE
          (
            (prepost_bhv_recco_users_agg.in_pre = 1 and prepost_bhv_recco_users_agg.in_post = 0)
            or (prepost_bhv_recco_users_agg.in_pre = 0 and prepost_bhv_recco_users_agg.in_post = 1)
          )
      ) recco_users
      ON (
        recco_users.recco_num = reccos.recco_num
        AND recco_users.dfa_user_id = buy_user_activity.dfa_user_id
      )

    GROUP BY
      reccos.pfcampaign
      , reccos.recco_num
      , recco_users.dfa_user_id
  ) user_conv_stats
GROUP BY
  user_conv_stats.pfcampaign
  , user_conv_stats.recco_num
;
