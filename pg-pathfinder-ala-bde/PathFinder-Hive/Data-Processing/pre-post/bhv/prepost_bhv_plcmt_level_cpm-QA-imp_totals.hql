﻿select
  *
  , (plcmt_imp_sum = cpm_imp_sum) as imps_match

from
  (
    select
      prepost_fy1516_fh_bhv_recco_plcmts.recco_num as plcmt_recco_num
      , prepost_fy1516_fh_bhv_recco_plcmts.buy_id as plcmt_buy_id
      , prepost_fy1516_fh_bhv_recco_plcmts.site_id as plcmt_site_id
      , prepost_fy1516_fh_bhv_recco_plcmts.page_id as plcmt_page_id
      , sum(prepost_fy1516_fh_bhv_recco_plcmts.imps) as plcmt_imp_sum
    from
      prepost_fy1516_fh_bhv_recco_plcmts
    group by
      prepost_fy1516_fh_bhv_recco_plcmts.recco_num
      , prepost_fy1516_fh_bhv_recco_plcmts.buy_id
      , prepost_fy1516_fh_bhv_recco_plcmts.site_id
      , prepost_fy1516_fh_bhv_recco_plcmts.page_id
  ) plcmt_imps

left outer join
  (
    select
      prepost_bhv_plcmt_level_cpm.recco_num as cpm_recco_num
      , prepost_bhv_plcmt_level_cpm.buy_id as cpm_buy_id
      , prepost_bhv_plcmt_level_cpm.site_id as cpm_site_id
      , prepost_bhv_plcmt_level_cpm.page_id as cpm_page_id
      , sum(prepost_bhv_plcmt_level_cpm.imp_sum) as cpm_imp_sum
    from
      prepost_bhv_plcmt_level_cpm
    group by
      prepost_bhv_plcmt_level_cpm.recco_num
      , prepost_bhv_plcmt_level_cpm.buy_id
      , prepost_bhv_plcmt_level_cpm.site_id
      , prepost_bhv_plcmt_level_cpm.page_id
  ) cpm_imps
  on
    cpm_imps.cpm_buy_id = plcmt_imps.plcmt_buy_id
    and cpm_imps.cpm_site_id = plcmt_imps.plcmt_site_id
    and cpm_imps.cpm_page_id = plcmt_imps.plcmt_page_id
    and cpm_imps.cpm_recco_num = plcmt_imps.plcmt_recco_num
;
