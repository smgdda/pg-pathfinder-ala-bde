SET hive.exec.compress.intermediate=true;
SET hive.exec.compress.output=true;
SET mapred.output.compression.type=BLOCK;
SET mapred.output.compression.codec=org.apache.hadoop.io.compress.SnappyCodec;
SET hive.auto.convert.join=true;


drop table if exists prepost2014_bhv_conv_users_by_recco;

create table prepost2014_bhv_conv_users_by_recco as
select
user_conv_stats.pfcampaign
, user_conv_stats.recco_num
, sum(user_conv_stats.conv_pre) as conv_users_pre
, sum(user_conv_stats.conv_post) as conv_users_post
from
(
  select 
  reccos.pfcampaign
  , reccos.recco_num
  , recco_users.dfa_user_id
  , max(case
      when buy_user_activity.event_dt >= reccos.pre_start_date_final and buy_user_activity.event_dt < reccos.pre_end_date_final then 1
      else 0
    end) as conv_pre
  , max(case
      when buy_user_activity.event_dt >= reccos.post_start_date_final and buy_user_activity.event_dt < reccos.post_end_date_final then 1
      else 0
    end) as conv_post
  from
  (
    select
    recco_num
    , pfcampaign
    , pre_start_date_final
    , pre_end_date_final
    , post_start_date_final
    , post_end_date_final
    from
    prepost2014_optimizations

    where lower(recco_num) != 'recco_num'
    and analysis_completed != 1
    ) reccos

  join (
    select
    buy_id
    , regexp_replace(lower(pfcampaign), '[^a-zA-Z0-9]', '') as pfcampaign
    from
    pfcampaign_list
    ) recco_buy_ids
  on (recco_buy_ids.pfcampaign = reccos.pfcampaign)

  join (
    select
    dfa_user_id
    , buy_id
    , event_dt
    from
    prepost_recco_user_activity
    ) buy_user_activity
  on (buy_user_activity.buy_id = recco_buy_ids.buy_id)

  join (
    select
    prepost_bhv_recco_users_agg.recco_num
    , prepost_bhv_recco_users_agg.dfa_user_id
    from
    prepost_bhv_recco_users_agg
    where
      (prepost_bhv_recco_users_agg.in_pre=1 or prepost_bhv_recco_users_agg.in_post=1)
    ) recco_users
  on (
    recco_users.recco_num = reccos.recco_num
    and recco_users.dfa_user_id = buy_user_activity.dfa_user_id
     )

  group by
  reccos.pfcampaign
  , reccos.recco_num
  , recco_users.dfa_user_id
) user_conv_stats
group by
user_conv_stats.pfcampaign
, user_conv_stats.recco_num
;

