set hive.exec.dynamic.partition=true;
set hive.exec.dynamic.partition.mode=nonstrict;
set hive.exec.compress.output=true;
set hive.exec.compress.intermediate=true;
set mapred.compress.map.output=true;
set mapred.output.compression.type=BLOCK;
set mapred.output.compression.codec=org.apache.hadoop.io.compress.SnappyCodec;

ALTER TABLE prepost_bhv_exposed_users_by_recco DROP IF EXISTS PARTITION (recco_num='${hiveconf:recco_num}');
INSERT INTO TABLE prepost_bhv_exposed_users_by_recco PARTITION (recco_num='${hiveconf:recco_num}')
SELECT
prepost2014_optimizations.pfcampaign
, sum(prepost_bhv_recco_users_agg.in_pre) AS exposed_users_pre
, sum(prepost_bhv_recco_users_agg.in_post) AS exposed_users_post

FROM
(
  SELECT
    *
  FROM
    prepost2014_optimizations
  WHERE
    lower(recco_num) != 'recco_num'
    and analysis_completed != 1
) prepost2014_optimizations

JOIN (
  SELECT
    recco_num
    , in_pre
    , in_post 
  FROM
  prepost_bhv_recco_users_agg
  WHERE
  recco_num = '${hiveconf:recco_num}'
) prepost_bhv_recco_users_agg
ON (
prepost2014_optimizations.recco_num = '${hiveconf:recco_num}'
and prepost_bhv_recco_users_agg.recco_num = prepost2014_optimizations.recco_num
)

GROUP BY
prepost2014_optimizations.pfcampaign
;
