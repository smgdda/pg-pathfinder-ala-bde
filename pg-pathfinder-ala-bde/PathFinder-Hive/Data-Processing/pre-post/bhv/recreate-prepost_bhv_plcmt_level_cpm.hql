drop table if exists prepost_bhv_plcmt_level_cpm;

create table prepost_bhv_plcmt_level_cpm as
select
*
from
(
  select
  recco_num
  , buy_id
  , site_id
  , page_id
  , site_placement
  , lower(case 
    when manual_mbox_plcmt_info.manual_placement_rate_type is null then placement_rate_type 
    else manual_mbox_plcmt_info.manual_placement_rate_type end) as placement_rate_type
  , case 
    when manual_mbox_plcmt_info.manual_rate is null then rate 
    else manual_mbox_plcmt_info.manual_rate end as rate
  , calculated_cpm
  , sum(ordered_net) as ordered_net_sum
  , sum(units) as unit_sum
  , sum(imps) as imp_sum

  from
  (
    select
    recco_num
    , buy_id
    , site_id
    , site_placement
    , regexp_replace(regexp_replace(regexp_replace(site_placement, 'prerollcreative[a-z]', 'preroll'), 'pfv\\_', ''), 'pff\\_', '') as spectra_plcmt_name
    , page_id
    , sum(imps) as imps

    from
    prepost_fy1516_fh_bhv_recco_plcmts

    where
    site_placement not like 'pff_%'
    and site_placement not like 'pfv_%'

    group by
    recco_num
    , buy_id
    , site_id
    , site_placement
    , regexp_replace(regexp_replace(regexp_replace(site_placement, 'prerollcreative[a-z]', 'preroll'), 'pfv\\_', ''), 'pff\\_', '')
    , page_id
  ) recco_plcmt_info

  left outer join (
    select distinct
    adserver_campaign_id
    , adserver_site_id
    , adserver_placement_id
    , placement_name
    , regexp_replace(lower(placement_rate_type), '\\s', '') as placement_rate_type
    , rate
    , ordered_net
    , units
    , calculated_cpm

    from 
    spectra_plcmt_mbox_rates
    where
    placement_name not like 'pff_%'
    and placement_name not like 'pfv_%'
  ) mbox_plcmt_info
  on
  (
    (
    mbox_plcmt_info.adserver_campaign_id = recco_plcmt_info.buy_id
    and mbox_plcmt_info.adserver_placement_id = recco_plcmt_info.page_id
    )
  )

  left outer join (
    select distinct
    buy_id as manual_buy_id
    , site_id as manual_site_id
    , page_id as manual_page_id
    , site_placement as manual_site_placement
    , lower(manual_placement_rate_type) as manual_placement_rate_type
    , manual_rate

    from 
    pf_spectra_placements_manual_updates
  ) manual_mbox_plcmt_info
  on
  (
    (
    manual_mbox_plcmt_info.manual_buy_id = recco_plcmt_info.buy_id
    and manual_mbox_plcmt_info.manual_page_id = recco_plcmt_info.page_id
    and manual_mbox_plcmt_info.manual_site_id = recco_plcmt_info.site_id
    and manual_mbox_plcmt_info.manual_site_placement = recco_plcmt_info.site_placement
    )
  )

  group by
  recco_num
  , buy_id
  , site_id
  , page_id
  , site_placement
  , lower(case 
    when manual_mbox_plcmt_info.manual_placement_rate_type is null then placement_rate_type 
    else manual_mbox_plcmt_info.manual_placement_rate_type end)
  , case 
    when manual_mbox_plcmt_info.manual_rate is null then rate 
    else manual_mbox_plcmt_info.manual_rate end
  , calculated_cpm

  union all

  select
  recco_num
  , buy_id
  , site_id
  , page_id
  , site_placement
  , lower(case 
    when manual_mbox_plcmt_info.manual_placement_rate_type is null then placement_rate_type 
    else manual_mbox_plcmt_info.manual_placement_rate_type end) as placement_rate_type
  , case 
    when manual_mbox_plcmt_info.manual_rate is null then rate 
    else manual_mbox_plcmt_info.manual_rate end as rate
  , calculated_cpm
  , sum(ordered_net) as ordered_net_sum
  , sum(units) as unit_sum
  , sum(imps) as imp_sum

  from
  (
    select
    recco_num
    , buy_id
    , site_id
    , site_placement
    , regexp_replace(regexp_replace(regexp_replace(site_placement, 'prerollcreative[a-z]', 'preroll'), 'pfv\\_', ''), 'pff\\_', '') as spectra_plcmt_name
    , page_id
    , sum(imps) as imps

    from
    prepost_fy1516_fh_bhv_recco_plcmts

    where
    site_placement like 'pff_%'

    group by
    recco_num
    , buy_id
    , site_id
    , site_placement
    , regexp_replace(regexp_replace(regexp_replace(site_placement, 'prerollcreative[a-z]', 'preroll'), 'pfv\\_', ''), 'pff\\_', '')
    , page_id
  ) recco_plcmt_info

  left outer join (
    select distinct
    adserver_campaign_id
    , adserver_site_id
    , adserver_placement_id
    --, regexp_replace(placement_name, '^(pf[f|v]\\_)', '') as placement_name
    , placement_name
    , regexp_replace(lower(placement_rate_type), '\\s', '') as placement_rate_type
    , rate
    , ordered_net
    , units
    , calculated_cpm

    from 
    spectra_plcmt_mbox_rates
  ) mbox_plcmt_info
  on
  (
    (
    mbox_plcmt_info.adserver_campaign_id = recco_plcmt_info.buy_id
    and mbox_plcmt_info.placement_name = recco_plcmt_info.spectra_plcmt_name
    )
  )

  left outer join (
    select distinct
    buy_id as manual_buy_id
    , site_id as manual_site_id
    , page_id as manual_page_id
    , site_placement as manual_site_placement
    , lower(manual_placement_rate_type) as manual_placement_rate_type
    , manual_rate

    from 
    pf_spectra_placements_manual_updates
  ) manual_mbox_plcmt_info
  on
  (
    (
    manual_mbox_plcmt_info.manual_buy_id = recco_plcmt_info.buy_id
    and manual_mbox_plcmt_info.manual_page_id = recco_plcmt_info.page_id
    and manual_mbox_plcmt_info.manual_site_id = recco_plcmt_info.site_id
    and manual_mbox_plcmt_info.manual_site_placement = recco_plcmt_info.site_placement
    )
  )

  group by
  recco_num
  , buy_id
  , site_id
  , page_id
  , site_placement
  , lower(case 
    when manual_mbox_plcmt_info.manual_placement_rate_type is null then placement_rate_type 
    else manual_mbox_plcmt_info.manual_placement_rate_type end)
  , case 
    when manual_mbox_plcmt_info.manual_rate is null then rate 
    else manual_mbox_plcmt_info.manual_rate end
  , calculated_cpm

  union all

select
  recco_num
  , buy_id
  , site_id
  , page_id
  , site_placement
  , lower(case 
    when manual_mbox_plcmt_info.manual_placement_rate_type is null then placement_rate_type 
    else manual_mbox_plcmt_info.manual_placement_rate_type end) as placement_rate_type
  , case 
    when manual_mbox_plcmt_info.manual_rate is null then rate 
    else manual_mbox_plcmt_info.manual_rate end as rate
  , case 
        when  
          (calculated_cpm is null or calculated_cpm != manual_mbox_plcmt_info.manual_rate)
          and lower(case when manual_mbox_plcmt_info.manual_placement_rate_type is null then placement_rate_type else manual_mbox_plcmt_info.manual_placement_rate_type end) = 'cpm' then 
          case when manual_mbox_plcmt_info.manual_rate is null then rate else manual_mbox_plcmt_info.manual_rate end
        when 
          (calculated_cpm is null or calculated_cpm != manual_mbox_plcmt_info.manual_rate)
          and lower(case when manual_mbox_plcmt_info.manual_placement_rate_type is null then placement_rate_type else manual_mbox_plcmt_info.manual_placement_rate_type end) = 'cpc' then 
          case when manual_mbox_plcmt_info.manual_rate is null then rate else manual_mbox_plcmt_info.manual_rate end
        when 
          (calculated_cpm is null or calculated_cpm != manual_mbox_plcmt_info.manual_rate)
          and lower(case when manual_mbox_plcmt_info.manual_placement_rate_type is null then placement_rate_type else manual_mbox_plcmt_info.manual_placement_rate_type end) = 'cpa' then 
          case when manual_mbox_plcmt_info.manual_rate is null then rate else manual_mbox_plcmt_info.manual_rate end
        when 
          (calculated_cpm is null or calculated_cpm != manual_mbox_plcmt_info.manual_rate)
          and lower(case when manual_mbox_plcmt_info.manual_placement_rate_type is null then placement_rate_type else manual_mbox_plcmt_info.manual_placement_rate_type end) = 'cpcv' then 
          case when manual_mbox_plcmt_info.manual_rate is null then rate else manual_mbox_plcmt_info.manual_rate end
        when 
          (calculated_cpm is null or calculated_cpm != manual_mbox_plcmt_info.manual_rate)
          and lower(case when manual_mbox_plcmt_info.manual_placement_rate_type is null then placement_rate_type else manual_mbox_plcmt_info.manual_placement_rate_type end) in ('flat rate impressions','flatrateimpressions') then 
          cast((1000 * ordered_net / units) as string)
        else cast(calculated_cpm as string)
      end as calculated_cpm
  , sum(ordered_net) as ordered_net_sum
  , sum(units) as unit_sum
  , sum(imps) as imp_sum

  from
  (
    select
    recco_num
    , buy_id
    , site_id
    , site_placement
    , regexp_replace(regexp_replace(regexp_replace(site_placement, 'prerollcreative[a-z]', 'preroll'), 'pfv\\_', ''), 'preroll[a-z]', 'preroll') as spectra_plcmt_name
    , page_id
    , sum(imps) as imps

    from
    prepost_fy1516_fh_bhv_recco_plcmts

    where
    site_placement like 'pfv_%'

    group by
    recco_num
    , buy_id
    , site_id
    , site_placement
    , regexp_replace(regexp_replace(regexp_replace(site_placement, 'prerollcreative[a-z]', 'preroll'), 'pfv\\_', ''), 'preroll[a-z]', 'preroll')
    , page_id
  ) recco_plcmt_info

  left outer join (
    select distinct
    adserver_campaign_id
    , adserver_site_id
    , adserver_placement_id
    , placement_name
    , regexp_replace(lower(placement_rate_type), '\\s', '') as placement_rate_type
    , rate
    , ordered_net
    , units
    , calculated_cpm

    from 
    spectra_plcmt_mbox_rates
  ) mbox_plcmt_info
  on
  (
    (
    mbox_plcmt_info.adserver_campaign_id = recco_plcmt_info.buy_id
    and mbox_plcmt_info.placement_name = recco_plcmt_info.spectra_plcmt_name
    )
  )

  left outer join (
    select distinct
    buy_id as manual_buy_id
    , site_id as manual_site_id
    , page_id as manual_page_id
    , site_placement as manual_site_placement
    , lower(manual_placement_rate_type) as manual_placement_rate_type
    , manual_rate

    from 
    pf_spectra_placements_manual_updates
  ) manual_mbox_plcmt_info
  on
  (
    (
    manual_mbox_plcmt_info.manual_buy_id = recco_plcmt_info.buy_id
    and manual_mbox_plcmt_info.manual_page_id = recco_plcmt_info.page_id
    and manual_mbox_plcmt_info.manual_site_id = recco_plcmt_info.site_id
--    and manual_mbox_plcmt_info.manual_site_placement = recco_plcmt_info.site_placement
    )
  )

  group by
  recco_num
  , buy_id
  , site_id
  , page_id
  , site_placement
  , lower(case 
    when manual_mbox_plcmt_info.manual_placement_rate_type is null then placement_rate_type 
    else manual_mbox_plcmt_info.manual_placement_rate_type end)
  , case 
    when manual_mbox_plcmt_info.manual_rate is null then rate 
    else manual_mbox_plcmt_info.manual_rate end
  , case 
        when  
          (calculated_cpm is null or calculated_cpm != manual_mbox_plcmt_info.manual_rate)
          and lower(case when manual_mbox_plcmt_info.manual_placement_rate_type is null then placement_rate_type else manual_mbox_plcmt_info.manual_placement_rate_type end) = 'cpm' 
          then case when manual_mbox_plcmt_info.manual_rate is null then rate else manual_mbox_plcmt_info.manual_rate end
        when 
          (calculated_cpm is null or calculated_cpm != manual_mbox_plcmt_info.manual_rate)
          and lower(case when manual_mbox_plcmt_info.manual_placement_rate_type is null then placement_rate_type else manual_mbox_plcmt_info.manual_placement_rate_type end) = 'cpc' then 
          case when manual_mbox_plcmt_info.manual_rate is null then rate else manual_mbox_plcmt_info.manual_rate end
        when 
          (calculated_cpm is null or calculated_cpm != manual_mbox_plcmt_info.manual_rate)
          and lower(case when manual_mbox_plcmt_info.manual_placement_rate_type is null then placement_rate_type else manual_mbox_plcmt_info.manual_placement_rate_type end) = 'cpa' then 
          case when manual_mbox_plcmt_info.manual_rate is null then rate else manual_mbox_plcmt_info.manual_rate end
        when 
          (calculated_cpm is null or calculated_cpm != manual_mbox_plcmt_info.manual_rate)
          and lower(case when manual_mbox_plcmt_info.manual_placement_rate_type is null then placement_rate_type else manual_mbox_plcmt_info.manual_placement_rate_type end) = 'cpcv' then 
          case when manual_mbox_plcmt_info.manual_rate is null then rate else manual_mbox_plcmt_info.manual_rate end
        when 
          (calculated_cpm is null or calculated_cpm != manual_mbox_plcmt_info.manual_rate)
          and lower(case when manual_mbox_plcmt_info.manual_placement_rate_type is null then placement_rate_type else manual_mbox_plcmt_info.manual_placement_rate_type end) in ('flat rate impressions','flatrateimpressions') then 
          cast((1000 * ordered_net / units) as string)
        else cast(calculated_cpm as string)
      end
) all_plcmts
;