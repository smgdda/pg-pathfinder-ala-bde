SET hive.exec.compress.intermediate=true;
SET hive.exec.compress.output=true;
SET mapred.output.compression.type=BLOCK;
SET mapred.output.compression.codec=org.apache.hadoop.io.compress.SnappyCodec;

drop table if exists prepost_fy1516_fh_bhv_recco_plcmts;

create table prepost_fy1516_fh_bhv_recco_plcmts as
select
  recco_placements.recco_num
  , recco_placements.dimtype
  , recco_placements.decrease
  , recco_placements.increase
  , recco_placements.site_placement
  , recco_placements.siteadsize
  , recco_placements.sitecreative
  , recco_placements.sitetactic
  , recco_placements.buy_id
  , recco_placements.site_id
  , recco_placements.page_id
  , recco_placements.creative_id
  , recco_placements.pre_start_date_final
  , recco_placements.pre_end_date_final
  , recco_placements.post_start_date_final
  , recco_placements.post_end_date_final
  , sum(recco_placements.impression_ct) as imps

from
  recco_placements_fy1516_fh recco_placements

group by
  recco_placements.recco_num
  , recco_placements.dimtype
  , recco_placements.decrease
  , recco_placements.increase
  , recco_placements.site_placement
  , recco_placements.siteadsize
  , recco_placements.sitecreative
  , recco_placements.sitetactic
  , recco_placements.buy_id
  , recco_placements.site_id
  , recco_placements.page_id
  , recco_placements.creative_id
  , recco_placements.pre_start_date_final
  , recco_placements.pre_end_date_final
  , recco_placements.post_start_date_final
  , recco_placements.post_end_date_final
;
