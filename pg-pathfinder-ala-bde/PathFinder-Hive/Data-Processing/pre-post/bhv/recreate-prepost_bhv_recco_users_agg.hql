set hive.exec.dynamic.partition=true;
set hive.exec.dynamic.partition.mode=nonstrict;
set hive.exec.compress.output=true;
set hive.exec.compress.intermediate=true;
set mapred.compress.map.output=true;
set mapred.output.compression.type=BLOCK;
set mapred.output.compression.codec=org.apache.hadoop.io.compress.SnappyCodec;

ALTER TABLE prepost_bhv_recco_users_agg DROP IF EXISTS PARTITION (recco_num='${hiveconf:recco_num}');

INSERT INTO TABLE prepost_bhv_recco_users_agg PARTITION (recco_num='${hiveconf:recco_num}')
SELECT
dfa_user_id
, max(in_pre) as in_pre
, max(in_post) as in_post
, count(*) as impression_ct

FROM
prepost_bhv_recco_users

WHERE
recco_num = '${hiveconf:recco_num}'

GROUP BY
dfa_user_id

;

