﻿drop table if exists prepost_bhv_spend_by_recco;
create table prepost_bhv_spend_by_recco as
select
recco_num
, sum(imps_pre_sum) as pre_imps
, sum(imps_post_sum) as post_imps
, sum(imps_total_sum) as total_imps
, sum(spend_pre) as pre_spend
, sum(spend_post) as post_spend
, sum(spend_total) as total_spend

from
(
  select
  recco_plcmt_imps_w_cpm.recco_num
  , recco_plcmt_imps_w_cpm.placement_rate_type
  , recco_plcmt_imps_w_cpm.calculated_cpm
  , sum(recco_plcmt_imps_w_cpm.imps_pre) as imps_pre_sum
  , sum(recco_plcmt_imps_w_cpm.imps_pre) * recco_plcmt_imps_w_cpm.calculated_cpm/1000 as spend_pre

  , sum(recco_plcmt_imps_w_cpm.imps_post) as imps_post_sum
  , sum(recco_plcmt_imps_w_cpm.imps_post) * recco_plcmt_imps_w_cpm.calculated_cpm/1000 as spend_post

  , sum(recco_plcmt_imps_w_cpm.imps_total_for_recco) as imps_total_sum
  , sum(recco_plcmt_imps_w_cpm.imps_total_for_recco) * recco_plcmt_imps_w_cpm.calculated_cpm/1000 as spend_total

  from
  recco_plcmt_imps_w_cpm

  group by
  recco_plcmt_imps_w_cpm.recco_num
  , recco_plcmt_imps_w_cpm.placement_rate_type
  , recco_plcmt_imps_w_cpm.calculated_cpm
) recco_plcmt_spend_data

group by
recco_num