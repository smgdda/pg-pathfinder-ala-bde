SET hive.exec.dynamic.partition=true;
SET hive.exec.dynamic.partition.mode=nonstrict;
SET hive.exec.compress.output=true;
SET hive.exec.compress.intermediate=true;
SET mapred.compress.map.output=true;
SET mapred.output.compression.type=BLOCK;
SET mapred.output.compression.codec=org.apache.hadoop.io.compress.SnappyCodec;

ALTER TABLE prepost_recco_activity_users DROP IF EXISTS PARTITION (recco_num='${hiveconf:recco_num}');

INSERT INTO TABLE prepost_recco_activity_users PARTITION (recco_num='${hiveconf:recco_num}')
SELECT DISTINCT
dfa_activity_users.dfa_user_id

FROM
(
  SELECT DISTINCT 
    dfa_user_id 
  FROM 
    dfa_activity 
  WHERE 
    dfa_user_id != '0' 
    and activity_type like 'pfind%' 
    and event_dt >= '2015-07-01' 
    and event_dt <= '2015-11-04'
) dfa_activity_users

JOIN
(
  SELECT DISTINCT
    dfa_user_id 
  FROM
    prepost_bhv_recco_users_agg
  WHERE
    recco_num = '${hiveconf:recco_num}'
    and (
        (in_pre = 1 and in_post = 0)
        or (in_pre = 0 and in_post = 1)
      )
) prepost_bhv_recco_users
ON (prepost_bhv_recco_users.dfa_user_id = dfa_activity_users.dfa_user_id)
;