SET hive.exec.compress.intermediate=true;
SET hive.exec.compress.output=true;
SET mapred.output.compression.type=BLOCK;
SET mapred.output.compression.codec=org.apache.hadoop.io.compress.SnappyCodec;
SET hive.auto.convert.join=true;

drop table if exists prepost_recco_user_activity;

create table prepost_recco_user_activity as
select
dfa_activity.buy_id
, dfa_activity.dfa_user_id
, dfa_activity.event_dt
, count(*) activities_for_day

from
prepost_recco_activity_users
join dfa_activity on (
  lower(dfa_activity.activity_type) like 'pfind%'
  and dfa_activity.buy_id is not null
  and dfa_activity.event_dt >= '2015-07-01' 
  and dfa_activity.event_dt <= '2015-11-04'
  and dfa_activity.dfa_user_id = prepost_recco_activity_users.dfa_user_id
  )

group by
dfa_activity.buy_id
, dfa_activity.dfa_user_id
, dfa_activity.event_dt;
