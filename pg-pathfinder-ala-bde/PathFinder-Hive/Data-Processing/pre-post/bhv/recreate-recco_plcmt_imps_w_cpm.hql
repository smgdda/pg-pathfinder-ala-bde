drop table if exists recco_plcmt_imps_w_cpm;

create table recco_plcmt_imps_w_cpm as
select
  prepost2014_optimizations.recco_num
  , prepost2014_optimizations.increase
  , prepost2014_optimizations.decrease
  , prepost2014_optimizations.pre_start_date_final
  , prepost2014_optimizations.pre_end_date_final
  , prepost2014_optimizations.post_start_date_final
  , prepost2014_optimizations.post_end_date_final
  , prepost_bhv_plcmt_level_cpm.buy_id
  , prepost_bhv_plcmt_level_cpm.site_placement
  , case
      when prepost_bhv_plcmt_level_cpm.calculated_cpm is not null then prepost_bhv_plcmt_level_cpm.calculated_cpm 
      when prepost_bhv_plcmt_level_cpm.calculated_cpm is null and prepost_bhv_plcmt_level_cpm.placement_rate_type = 'cpm' then prepost_bhv_plcmt_level_cpm.rate 
      when prepost_bhv_plcmt_level_cpm.calculated_cpm is null and prepost_bhv_plcmt_level_cpm.placement_rate_type != 'cpm' then NULL 
    end as calculated_cpm
  , prepost_bhv_plcmt_level_cpm.placement_rate_type
  , sum(case
        when prepost_pf_daily_dfa_plcmt_stats_w_meta.event_dt >= prepost2014_optimizations.pre_start_date_final 
        and prepost_pf_daily_dfa_plcmt_stats_w_meta.event_dt < prepost2014_optimizations.pre_end_date_final 
        then prepost_pf_daily_dfa_plcmt_stats_w_meta.imp_sum 
        else cast(0 as bigint) end) as imps_pre
  , sum(case
        when prepost_pf_daily_dfa_plcmt_stats_w_meta.event_dt >= prepost2014_optimizations.post_start_date_final 
        and prepost_pf_daily_dfa_plcmt_stats_w_meta.event_dt < prepost2014_optimizations.post_end_date_final 
        then prepost_pf_daily_dfa_plcmt_stats_w_meta.imp_sum 
        else cast(0 as bigint) end) as imps_post
  , sum(case
        when prepost_pf_daily_dfa_plcmt_stats_w_meta.event_dt >= prepost2014_optimizations.pre_start_date_final
        and prepost_pf_daily_dfa_plcmt_stats_w_meta.event_dt < prepost2014_optimizations.post_end_date_final
        then prepost_pf_daily_dfa_plcmt_stats_w_meta.imp_sum 
        else cast(0 as bigint) end) as imps_total_for_recco

from
  (
    select
    *
    from
    prepost2014_optimizations
    where lower(recco_num) != 'recco_num'
    and analysis_completed != 1
  ) prepost2014_optimizations

join 
  prepost_bhv_plcmt_level_cpm
  on prepost_bhv_plcmt_level_cpm.recco_num = prepost2014_optimizations.recco_num

join 
  prepost_pf_daily_dfa_plcmt_stats_w_meta
  on (
    prepost_pf_daily_dfa_plcmt_stats_w_meta.buy_id = prepost_bhv_plcmt_level_cpm.buy_id
    and prepost_pf_daily_dfa_plcmt_stats_w_meta.site_placement = prepost_bhv_plcmt_level_cpm.site_placement
  )

where
(
  (
    prepost2014_optimizations.dimtype = 'siteadsize' and (
      prepost_pf_daily_dfa_plcmt_stats_w_meta.site_ad_size = prepost2014_optimizations.increase
      or
      prepost_pf_daily_dfa_plcmt_stats_w_meta.site_ad_size = prepost2014_optimizations.decrease
      )
    )
  or
  (
    prepost2014_optimizations.dimtype = 'sitecreative' and (
      prepost_pf_daily_dfa_plcmt_stats_w_meta.site_creative = prepost2014_optimizations.increase
      or
      prepost_pf_daily_dfa_plcmt_stats_w_meta.site_creative = prepost2014_optimizations.decrease
      )
    )
  or
  (
    prepost2014_optimizations.dimtype in ('sitetactic', 'site') and (
      prepost_pf_daily_dfa_plcmt_stats_w_meta.site_tactic = prepost2014_optimizations.increase
      or
      prepost_pf_daily_dfa_plcmt_stats_w_meta.site_tactic = prepost2014_optimizations.decrease
      )
    )
  )

group by
  prepost_bhv_plcmt_level_cpm.buy_id
  , prepost_bhv_plcmt_level_cpm.site_placement
  , case
      when prepost_bhv_plcmt_level_cpm.calculated_cpm is not null then prepost_bhv_plcmt_level_cpm.calculated_cpm 
      when prepost_bhv_plcmt_level_cpm.calculated_cpm is null and prepost_bhv_plcmt_level_cpm.placement_rate_type = 'cpm' then prepost_bhv_plcmt_level_cpm.rate 
      when prepost_bhv_plcmt_level_cpm.calculated_cpm is null and prepost_bhv_plcmt_level_cpm.placement_rate_type != 'cpm' then NULL 
    end
  , prepost_bhv_plcmt_level_cpm.placement_rate_type
  , prepost2014_optimizations.recco_num
  , prepost2014_optimizations.increase
  , prepost2014_optimizations.decrease
  , prepost2014_optimizations.pre_start_date_final
  , prepost2014_optimizations.pre_end_date_final
  , prepost2014_optimizations.post_start_date_final
  , prepost2014_optimizations.post_end_date_final

;

