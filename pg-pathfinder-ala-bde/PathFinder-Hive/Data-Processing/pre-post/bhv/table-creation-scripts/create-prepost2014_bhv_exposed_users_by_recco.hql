﻿DROP TABLE IF EXISTS prepost2014_bhv_exposed_users_by_recco;
CREATE TABLE prepost2014_bhv_exposed_users_by_recco
(
  pfcampaign string, 
  exposed_users_pre bigint, 
  exposed_users_post bigint
)
PARTITIONED BY
(
  recco_num string
)
ROW FORMAT SERDE 
  'org.apache.hadoop.hive.serde2.columnar.ColumnarSerDe' 
STORED AS INPUTFORMAT 
  'org.apache.hadoop.hive.ql.io.RCFileInputFormat' 
OUTPUTFORMAT 
  'org.apache.hadoop.hive.ql.io.RCFileOutputFormat'
;
