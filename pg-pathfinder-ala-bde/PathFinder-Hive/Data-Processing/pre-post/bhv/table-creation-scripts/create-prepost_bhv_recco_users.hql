﻿DROP TABLE IF EXISTS prepost_bhv_recco_users;

CREATE  TABLE prepost_bhv_recco_users (
  dfa_user_id string, 
  in_pre int, 
  in_post int
)
PARTITIONED BY (
  recco_num string
)
ROW FORMAT SERDE 
  'org.apache.hadoop.hive.serde2.columnar.ColumnarSerDe' 
STORED AS INPUTFORMAT 
  'org.apache.hadoop.hive.ql.io.RCFileInputFormat' 
OUTPUTFORMAT 
  'org.apache.hadoop.hive.ql.io.RCFileOutputFormat'
;