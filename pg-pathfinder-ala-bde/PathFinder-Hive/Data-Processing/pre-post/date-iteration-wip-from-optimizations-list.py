import sys, os, subprocess
import datetime
import time

def main():
  optimization_data_process = get_optimization_process()
  optimization_data_process.run()
  

class get_optimization_process:

  def __init__(self):
    print("get_optimization_process class init...")

  def run(self):
    print("starting get_optimization_process.run()...")

    print("starting get_optimization_process.gather_recco_data_for_processing()...")
    optimization_dict = self.gather_recco_data_for_processing()
    print("completed get_optimization_process.gather_recco_data_for_processing()...")

    for optimization in optimization_dict['records']:
      print('In process loop.  Processing optimization recco: # ' + optimization[18])
      print(' - brand: ' + optimization[1])
      print(' - dimtype: ' + optimization[8])
      print(' - increase: ' + optimization[10])
      print(' - decrease: ' + optimization[11])
      print(' - pre_start: ' + optimization[26])
      print(' - pre_end: ' + optimization[27])
      print(' - post_start: ' + optimization[28])
      print(' - post_end: ' + optimization[29] + '\n\n\n')

      self.process_campaign_data_by_week(optimization[18], optimization[1], optimization[8], optimization[10], optimization[11], optimization[26], optimization[27], optimization[28], optimization[29])

    print('completed get_optimization_process.run()...')

  def gather_recco_data_for_processing(self):
    print("starting get_optimization_process.gather_recco_data_for_processing()...")

    hql = "select * from prepost2014_optimizations;"
    optimization_dict_list_dict = self.getHiveQueryResults(hql)
    print("completed get_optimization_process.gather_recco_data_for_processing()...\n\n")
    return optimization_dict_list_dict

  def process_campaign_data_by_week(self, recco_num, brand, dimtype, increase, decrease, pre_start, pre_end, post_start, post_end):
    #print("starting get_optimization_process.process_campaign_data_by_week(" + recco_num + ", " + brand + ", " + dimtype + ", " + increase + ", " + decrease + ", " + pre_start + ", " + pre_end + ", " + post_start + ", " + post_end + ")...")

    #today = datetime.date(2014,7,1)
    #startDate = today.replace(2014,7,1)
    startDate = datetime.date(int(pre_start[0:4]), int(pre_start[5:7]), int(pre_start[8:10]))
    currentEndDate = startDate + datetime.timedelta(days=7)

    while startDate < datetime.date(2014,12,31):
      print(recco_num, datetime.date.isoformat(startDate), datetime.date.isoformat(currentEndDate), sep="\t")

      #reset start and end dates for next iteration of processing loop
      startDate = currentEndDate
      currentEndDate = startDate + datetime.timedelta(days=7)

    print("completed get_optimization_process.process_campaign_data_by_week(" + recco_num + ")...\n\n")

  def getHiveQueryResults(self, hql):
    print("get_optimization_process.getHiveQueryResults(hql=" + hql + ")...")

    return_data = {}
    return_data['rc'] = ""
    return_data['records'] = []
    
    #run the query
    print ("RUNNING HIVE QUERY, hql=" + hql)
    #process = subprocess.Popen(["hive", "-e", '"' + hql + '"'], stdout=subprocess.PIPE)
    cmd = '%HIVE_HOME%\\bin\\hive -e "' + hql + '"'
    process = subprocess.Popen(cmd, stdin=None, stdout=subprocess.PIPE, shell=True)

    data = process.communicate()[0]
    return_data['rc'] = process.returncode
    
    # return if the query ended with an error
    if process.returncode != 0:
      return return_data
    
    # retrieve the data, if any exists
    if data:
      for row in data.split('\n'):
      #for row in data.decode('utf-8').split('\n'):
        if row.strip():
          return_data['records'].append(row.split('\t'))

    return return_data

  def execute_hive_script(self, sql_script_file, recco_num):
    print ('starting execute_hive_script: ' + recco_num)

    print ('processing optimization: ', recco_num)
    print ('executing sql script==========>')
    #run the query
    process = subprocess.Popen(["hive", "-f", sql_script_file, "-hiveconf", "recco_num=" + recco_num], stdout=subprocess.PIPE)
    data = process.communicate()[0]
    print ('completed running sql script==========>')

if __name__ == "__main__":
  main()
