SET mapred.output.compression.type=BLOCK;
SET mapred.output.compression.codec=org.apache.hadoop.io.compress.SnappyCodec;
SET hive.exec.compress.output=true;
SET hive.exec.compress.intermediate=true;
SET hive.auto.convert.join=true;
SET hive.exec.dynamic.partition=true;
SET hive.exec.dynamic.partition.mode=nonstrict;
SET hive.exec.max.dynamic.partitions.pernode=2500;
SET hive.exec.max.dynamic.partitions=4500;

from
(select * from pfcampaign_list where pfcampaign = '${hiveconf:pf_campaign_name}') pfc

join (
  select 
  dfa_user_id
  ,event_dts
  ,buy_id
  ,creative_id
  ,site_id
  ,page_id
  ,site_data
  ,dlx_encrypted_dfa_user_id
  ,acxiom_encrypted_dfa_user_id
  ,event_dt
  from 
  prepost_dlx_dfa_user_impressions_v2
  where 
  pfcampaign = '${hiveconf:pf_campaign_name}'
) i
on (
    i.buy_id = pfc.buy_id 
    and 
    i.dfa_user_id != '0'
  )

join (
  select
  page_id
  , split(regexp_replace(lower(site_placement), '[^a-z0-9_\\|]', ''), '\\|')[0] as site_placement
  from
  dfa_meta_page
  where
  site_placement not like '%cancel%'
  ) p
on (
  p.page_id = i.page_id
  )

join (
  select 
    site_id
    , regexp_replace(lower(site), '[^a-zA-Z0-9_]', '') as site 
  from 
    dfa_meta_site
  ) s
on (
  s.site_id = i.site_id
  )

join (
  select 
    creative_id
    , ui_creative_id
    , regexp_replace(lower(creative),'[^a-zA-Z0-9_]','') as creative
  from 
    dfa_meta_creative 
  where 
    creative_id is not null
    and split(regexp_replace(lower(creative),'[^a-zA-Z0-9_]',''),'_')[2] <> 'flite') c
on (
  c.creative_id = i.creative_id
  )

left outer join (
  select 
    ad_id
    , regexp_replace(lower(ad_name),'[^a-zA-Z0-9_]','') as ad_name 
  from 
    ingestion_flite_lookup
  )  f
on (
  f.ad_id = substr(i.site_data, 3, length(i.site_data))
  )

insert overwrite table prepost_dlx_user_imps_matched PARTITION(event_dt, pfcampaign) 
select /*+ MAPJOIN(pfc, p, s, c, f) */
i.dfa_user_id
, i.buy_id
, i.site_id
, s.site
, i.page_id
, p.site_placement
, i.creative_id
, c.creative as creative_name
, case
    when substr(p.site_placement,0,3) = 'pff' 
      then f.ad_name       
    else split(c.creative,'_')[2] 
  end as creative
, c.ui_creative_id
, case when p.site_placement like 'pff_%' then split(lower(i.site_data), 'u=')[1] else null end as site_data
, case
    when substr(p.site_placement,0,2) = 'pf' 
      then concat_ws('_',s.site,split(p.site_placement,'_')[1],split(p.site_placement,'_')[4])
    else concat_ws('_',s.site,split(p.site_placement,'_')[0],split(p.site_placement,'_')[3])
  end as sitetactic
, case
    when substr(p.site_placement,0,2) = 'pf'
      then concat_ws('_',s.site,split(p.site_placement,'_')[6])
    else concat_ws('_',s.site,split(p.site_placement,'_')[5])
  end as siteadsize
, case
    when substr(p.site_placement,0,3) = 'pff' 
      then concat_ws('_',s.site,f.ad_name)
    else concat_ws('_',s.site,split(c.creative,'_')[2])
  end as sitecreative
, i.event_dt
, regexp_replace(lower(pfc.pfcampaign), '[^a-zA-Z0-9]', '') as pfcampaign
;