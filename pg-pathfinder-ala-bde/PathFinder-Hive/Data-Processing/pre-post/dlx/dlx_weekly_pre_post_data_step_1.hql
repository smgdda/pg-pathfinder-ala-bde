SET hive.exec.compress.output=true;
SET hive.exec.compress.intermediate=true;
SET mapred.output.compression.type=BLOCK;
SET mapred.output.compression.codec=org.apache.hadoop.io.compress.SnappyCodec;

drop table if exists prepost_dlx_weekly_data_step_1;

create table prepost_dlx_weekly_data_step_1
as
select distinct
  ingestion_dlx_weekly_sales_signal.dfa_id
  , ingestion_dlx_weekly_sales_signal.pf_campaign

from
  ingestion_dlx_weekly_sales_signal

where
  ingestion_dlx_weekly_sales_signal.retailer_tracked = 1
  and lower(ingestion_dlx_weekly_sales_signal.level) = 'product'
;
