SET hive.auto.convert.join=true;
SET hive.exec.compress.output=true;
SET hive.exec.compress.intermediate=true;
SET mapred.output.compression.type=BLOCK;
SET mapred.output.compression.codec=org.apache.hadoop.io.compress.SnappyCodec;

drop table if exists prepost_dlx_weekly_data_step_2c;
create table
prepost_dlx_weekly_data_step_2c
as

select 
prepost_dlx_weekly_data_step_2b.dfa_id
, prepost_dlx_weekly_data_step_2b.pf_campaign

, prepost_dlx_weekly_data_step_2b.jul_wk_2_ibtr
, prepost_dlx_weekly_data_step_2b.jul_wk_2_ss
, prepost_dlx_weekly_data_step_2b.jul_wk_2_time_of_purchase

, prepost_dlx_weekly_data_step_2b.jul_wk_3_ibtr
, prepost_dlx_weekly_data_step_2b.jul_wk_3_ss
, prepost_dlx_weekly_data_step_2b.jul_wk_3_time_of_purchase

, prepost_dlx_weekly_data_step_2b.jul_wk_4_ibtr
, prepost_dlx_weekly_data_step_2b.jul_wk_4_ss
, prepost_dlx_weekly_data_step_2b.jul_wk_4_time_of_purchase

, prepost_dlx_weekly_data_step_2b.aug_wk_1_ibtr
, prepost_dlx_weekly_data_step_2b.aug_wk_1_ss
, prepost_dlx_weekly_data_step_2b.aug_wk_1_time_of_purchase

, prepost_dlx_weekly_data_step_2b.aug_wk_2_ibtr
, prepost_dlx_weekly_data_step_2b.aug_wk_2_ss
, prepost_dlx_weekly_data_step_2b.aug_wk_2_time_of_purchase

, prepost_dlx_weekly_data_step_2b.aug_wk_3_ibtr
, prepost_dlx_weekly_data_step_2b.aug_wk_3_ss
, prepost_dlx_weekly_data_step_2b.aug_wk_3_time_of_purchase

, wss_aug_wk_4.ibtr as aug_wk_4_ibtr
, wss_aug_wk_4.sales_signal_projected as aug_wk_4_ss
, wss_aug_wk_4.time_of_purchase as aug_wk_4_time_of_purchase

, wss_aug_wk_5.ibtr as aug_wk_5_ibtr
, wss_aug_wk_5.sales_signal_projected as aug_wk_5_ss
, wss_aug_wk_5.time_of_purchase as aug_wk_5_time_of_purchase

, wss_sep_wk_1.ibtr as sep_wk_1_ibtr
, wss_sep_wk_1.sales_signal_projected as sep_wk_1_ss
, wss_sep_wk_1.time_of_purchase as sep_wk_1_time_of_purchase

from
prepost_dlx_weekly_data_step_2b

left outer join (
  select distinct
  dfa_id
  , pf_campaign
  , ibtr
  , sales_signal_projected
  , time_of_purchase
  
  from
  ingestion_dlx_weekly_sales_signal
  
  where
  ingestion_dt = '2015-08-22'
  and retailer_tracked = 1
  and lower(level) = 'product'
  ) wss_aug_wk_4 
on (
  wss_aug_wk_4.dfa_id = prepost_dlx_weekly_data_step_2b.dfa_id 
  and wss_aug_wk_4.pf_campaign = prepost_dlx_weekly_data_step_2b.pf_campaign
)

left outer join (
  select distinct
  dfa_id
  , pf_campaign
  , ibtr
  , sales_signal_projected
  , time_of_purchase
  
  from
  ingestion_dlx_weekly_sales_signal
  
  where
  ingestion_dt = '2015-08-29'
  and retailer_tracked = 1
  and lower(level) = 'product'
  ) wss_aug_wk_5 
on (
  wss_aug_wk_5.dfa_id = prepost_dlx_weekly_data_step_2b.dfa_id 
  and wss_aug_wk_5.pf_campaign = prepost_dlx_weekly_data_step_2b.pf_campaign
)

left outer join (
  select distinct
  dfa_id
  , pf_campaign
  , ibtr
  , sales_signal_projected
  , time_of_purchase
  
  from
  ingestion_dlx_weekly_sales_signal
  
  where
  ingestion_dt = '2015-09-05'
  and retailer_tracked = 1
  and lower(level) = 'product'
  ) wss_sep_wk_1
on (
  wss_sep_wk_1.dfa_id = prepost_dlx_weekly_data_step_2b.dfa_id 
  and wss_sep_wk_1.pf_campaign = prepost_dlx_weekly_data_step_2b.pf_campaign
)
;
