SET hive.auto.convert.join=true;
SET hive.exec.compress.output=true;
SET hive.exec.compress.intermediate=true;
SET mapred.output.compression.type=BLOCK;
SET mapred.output.compression.codec=org.apache.hadoop.io.compress.SnappyCodec;

drop table if exists prepost_dlx_weekly_data_step_2d;
create table
prepost_dlx_weekly_data_step_2d
as
select 
prepost_dlx_weekly_data_step_2c.dfa_id
, prepost_dlx_weekly_data_step_2c.pf_campaign

, prepost_dlx_weekly_data_step_2c.jul_wk_2_ibtr
, prepost_dlx_weekly_data_step_2c.jul_wk_2_ss
, prepost_dlx_weekly_data_step_2c.jul_wk_2_time_of_purchase

, prepost_dlx_weekly_data_step_2c.jul_wk_3_ibtr
, prepost_dlx_weekly_data_step_2c.jul_wk_3_ss
, prepost_dlx_weekly_data_step_2c.jul_wk_3_time_of_purchase

, prepost_dlx_weekly_data_step_2c.jul_wk_4_ibtr
, prepost_dlx_weekly_data_step_2c.jul_wk_4_ss
, prepost_dlx_weekly_data_step_2c.jul_wk_4_time_of_purchase

, prepost_dlx_weekly_data_step_2c.aug_wk_1_ibtr
, prepost_dlx_weekly_data_step_2c.aug_wk_1_ss
, prepost_dlx_weekly_data_step_2c.aug_wk_1_time_of_purchase

, prepost_dlx_weekly_data_step_2c.aug_wk_2_ibtr
, prepost_dlx_weekly_data_step_2c.aug_wk_2_ss
, prepost_dlx_weekly_data_step_2c.aug_wk_2_time_of_purchase

, prepost_dlx_weekly_data_step_2c.aug_wk_3_ibtr
, prepost_dlx_weekly_data_step_2c.aug_wk_3_ss
, prepost_dlx_weekly_data_step_2c.aug_wk_3_time_of_purchase

, prepost_dlx_weekly_data_step_2c.aug_wk_4_ibtr
, prepost_dlx_weekly_data_step_2c.aug_wk_4_ss
, prepost_dlx_weekly_data_step_2c.aug_wk_4_time_of_purchase

, prepost_dlx_weekly_data_step_2c.aug_wk_5_ibtr
, prepost_dlx_weekly_data_step_2c.aug_wk_5_ss
, prepost_dlx_weekly_data_step_2c.aug_wk_5_time_of_purchase

, prepost_dlx_weekly_data_step_2c.sep_wk_1_ibtr
, prepost_dlx_weekly_data_step_2c.sep_wk_1_ss
, prepost_dlx_weekly_data_step_2c.sep_wk_1_time_of_purchase

, wss_sep_wk_2.ibtr as sep_wk_2_ibtr
, wss_sep_wk_2.sales_signal_projected as sep_wk_2_ss
, wss_sep_wk_2.time_of_purchase as sep_wk_2_time_of_purchase

, wss_sep_wk_3.ibtr as sep_wk_3_ibtr
, wss_sep_wk_3.sales_signal_projected as sep_wk_3_ss
, wss_sep_wk_3.time_of_purchase as sep_wk_3_time_of_purchase

, wss_sep_wk_4.ibtr as sep_wk_4_ibtr
, wss_sep_wk_4.sales_signal_projected as sep_wk_4_ss
, wss_sep_wk_4.time_of_purchase as sep_wk_4_time_of_purchase

from
prepost_dlx_weekly_data_step_2c

left outer join (
  select distinct
  dfa_id
  , pf_campaign
  , ibtr
  , sales_signal_projected
  , time_of_purchase
  
  from
  ingestion_dlx_weekly_sales_signal
  
  where
  ingestion_dt = '2015-09-12'
  and retailer_tracked = 1
  and lower(level) = 'product'
  ) wss_sep_wk_2
on (
  wss_sep_wk_2.dfa_id = prepost_dlx_weekly_data_step_2c.dfa_id 
  and wss_sep_wk_2.pf_campaign = prepost_dlx_weekly_data_step_2c.pf_campaign
)

left outer join (
  select distinct
  dfa_id
  , pf_campaign
  , ibtr
  , sales_signal_projected
  , time_of_purchase
  
  from
  ingestion_dlx_weekly_sales_signal
  
  where
  ingestion_dt = '2015-09-19'
  and retailer_tracked = 1
  and lower(level) = 'product'
  ) wss_sep_wk_3
on (
  wss_sep_wk_3.dfa_id = prepost_dlx_weekly_data_step_2c.dfa_id 
  and wss_sep_wk_3.pf_campaign = prepost_dlx_weekly_data_step_2c.pf_campaign
)

left outer join (
  select distinct
  dfa_id
  , pf_campaign
  , ibtr
  , sales_signal_projected
  , time_of_purchase
  
  from
  ingestion_dlx_weekly_sales_signal
  
  where
  ingestion_dt = '2015-09-26'
  and retailer_tracked = 1
  and lower(level) = 'product'
  ) wss_sep_wk_4
on (
  wss_sep_wk_4.dfa_id = prepost_dlx_weekly_data_step_2c.dfa_id 
  and wss_sep_wk_4.pf_campaign = prepost_dlx_weekly_data_step_2c.pf_campaign
)
;
