SET hive.auto.convert.join=true;
SET hive.exec.compress.output=true;
SET hive.exec.compress.intermediate=true;
SET mapred.output.compression.type=BLOCK;
SET mapred.output.compression.codec=org.apache.hadoop.io.compress.SnappyCodec;

drop table if exists prepost_dlx_weekly_data_step_2e;
create table
prepost_dlx_weekly_data_step_2e
as
select 
prepost_dlx_weekly_data_step_2d.dfa_id
, prepost_dlx_weekly_data_step_2d.pf_campaign

, prepost_dlx_weekly_data_step_2d.jul_wk_2_ibtr
, prepost_dlx_weekly_data_step_2d.jul_wk_2_ss
, prepost_dlx_weekly_data_step_2d.jul_wk_2_time_of_purchase

, prepost_dlx_weekly_data_step_2d.jul_wk_3_ibtr
, prepost_dlx_weekly_data_step_2d.jul_wk_3_ss
, prepost_dlx_weekly_data_step_2d.jul_wk_3_time_of_purchase

, prepost_dlx_weekly_data_step_2d.jul_wk_4_ibtr
, prepost_dlx_weekly_data_step_2d.jul_wk_4_ss
, prepost_dlx_weekly_data_step_2d.jul_wk_4_time_of_purchase

, prepost_dlx_weekly_data_step_2d.aug_wk_1_ibtr
, prepost_dlx_weekly_data_step_2d.aug_wk_1_ss
, prepost_dlx_weekly_data_step_2d.aug_wk_1_time_of_purchase

, prepost_dlx_weekly_data_step_2d.aug_wk_2_ibtr
, prepost_dlx_weekly_data_step_2d.aug_wk_2_ss
, prepost_dlx_weekly_data_step_2d.aug_wk_2_time_of_purchase

, prepost_dlx_weekly_data_step_2d.aug_wk_3_ibtr
, prepost_dlx_weekly_data_step_2d.aug_wk_3_ss
, prepost_dlx_weekly_data_step_2d.aug_wk_3_time_of_purchase

, prepost_dlx_weekly_data_step_2d.aug_wk_4_ibtr
, prepost_dlx_weekly_data_step_2d.aug_wk_4_ss
, prepost_dlx_weekly_data_step_2d.aug_wk_4_time_of_purchase

, prepost_dlx_weekly_data_step_2d.aug_wk_5_ibtr
, prepost_dlx_weekly_data_step_2d.aug_wk_5_ss
, prepost_dlx_weekly_data_step_2d.aug_wk_5_time_of_purchase

, prepost_dlx_weekly_data_step_2d.sep_wk_1_ibtr
, prepost_dlx_weekly_data_step_2d.sep_wk_1_ss
, prepost_dlx_weekly_data_step_2d.sep_wk_1_time_of_purchase

, prepost_dlx_weekly_data_step_2d.sep_wk_2_ibtr
, prepost_dlx_weekly_data_step_2d.sep_wk_2_ss
, prepost_dlx_weekly_data_step_2d.sep_wk_2_time_of_purchase

, prepost_dlx_weekly_data_step_2d.sep_wk_3_ibtr
, prepost_dlx_weekly_data_step_2d.sep_wk_3_ss
, prepost_dlx_weekly_data_step_2d.sep_wk_3_time_of_purchase

, prepost_dlx_weekly_data_step_2d.sep_wk_4_ibtr
, prepost_dlx_weekly_data_step_2d.sep_wk_4_ss
, prepost_dlx_weekly_data_step_2d.sep_wk_4_time_of_purchase

, wss_oct_wk_1.ibtr as oct_wk_1_ibtr
, wss_oct_wk_1.sales_signal_projected as oct_wk_1_ss
, wss_oct_wk_1.time_of_purchase as oct_wk_1_time_of_purchase

, wss_oct_wk_2.ibtr as oct_wk_2_ibtr
, wss_oct_wk_2.sales_signal_projected as oct_wk_2_ss
, wss_oct_wk_2.time_of_purchase as oct_wk_2_time_of_purchase

, wss_oct_wk_3.ibtr as oct_wk_3_ibtr
, wss_oct_wk_3.sales_signal_projected as oct_wk_3_ss
, wss_oct_wk_3.time_of_purchase as oct_wk_3_time_of_purchase

, wss_oct_wk_4.ibtr as oct_wk_4_ibtr
, wss_oct_wk_4.sales_signal_projected as oct_wk_4_ss
, wss_oct_wk_4.time_of_purchase as oct_wk_4_time_of_purchase

, wss_oct_wk_5.ibtr as oct_wk_5_ibtr
, wss_oct_wk_5.sales_signal_projected as oct_wk_5_ss
, wss_oct_wk_5.time_of_purchase as oct_wk_5_time_of_purchase

from
prepost_dlx_weekly_data_step_2d

left outer join (
  select distinct
  dfa_id
  , pf_campaign
  , ibtr
  , sales_signal_projected
  , time_of_purchase
  
  from
  ingestion_dlx_weekly_sales_signal
  
  where
  ingestion_dt = '2015-10-03'
  and retailer_tracked = 1
  and lower(level) = 'product'
  ) wss_oct_wk_1
on (
  wss_oct_wk_1.dfa_id = prepost_dlx_weekly_data_step_2d.dfa_id 
  and wss_oct_wk_1.pf_campaign = prepost_dlx_weekly_data_step_2d.pf_campaign
)

left outer join (
  select distinct
  dfa_id
  , pf_campaign
  , ibtr
  , sales_signal_projected
  , time_of_purchase
  
  from
  ingestion_dlx_weekly_sales_signal
  
  where
  ingestion_dt = '2015-10-10'
  and retailer_tracked = 1
  and lower(level) = 'product'
  ) wss_oct_wk_2
on (
  wss_oct_wk_2.dfa_id = prepost_dlx_weekly_data_step_2d.dfa_id 
  and wss_oct_wk_2.pf_campaign = prepost_dlx_weekly_data_step_2d.pf_campaign
)

left outer join (
  select distinct
  dfa_id
  , pf_campaign
  , ibtr
  , sales_signal_projected
  , time_of_purchase
  
  from
  ingestion_dlx_weekly_sales_signal
  
  where
  ingestion_dt = '2015-10-17'
  and retailer_tracked = 1
  and lower(level) = 'product'
  ) wss_oct_wk_3
on (
  wss_oct_wk_3.dfa_id = prepost_dlx_weekly_data_step_2d.dfa_id 
  and wss_oct_wk_3.pf_campaign = prepost_dlx_weekly_data_step_2d.pf_campaign
)

left outer join (
  select distinct
  dfa_id
  , pf_campaign
  , ibtr
  , sales_signal_projected
  , time_of_purchase
  
  from
  ingestion_dlx_weekly_sales_signal
  
  where
  ingestion_dt = '2015-10-24'
  and retailer_tracked = 1
  and lower(level) = 'product'
  ) wss_oct_wk_4
on (
  wss_oct_wk_4.dfa_id = prepost_dlx_weekly_data_step_2d.dfa_id 
  and wss_oct_wk_4.pf_campaign = prepost_dlx_weekly_data_step_2d.pf_campaign
)

left outer join (
  select distinct
  dfa_id
  , pf_campaign
  , ibtr
  , sales_signal_projected
  , time_of_purchase
  
  from
  ingestion_dlx_weekly_sales_signal
  
  where
  ingestion_dt = '2015-10-31'
  and retailer_tracked = 1
  and lower(level) = 'product'
  ) wss_oct_wk_5
on (
  wss_oct_wk_5.dfa_id = prepost_dlx_weekly_data_step_2d.dfa_id 
  and wss_oct_wk_5.pf_campaign = prepost_dlx_weekly_data_step_2d.pf_campaign
)
;
