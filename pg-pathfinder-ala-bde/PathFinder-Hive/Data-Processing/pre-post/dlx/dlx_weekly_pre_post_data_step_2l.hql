﻿SET hive.auto.convert.join=true;
SET hive.exec.compress.output=true;
SET hive.exec.compress.intermediate=true;
SET mapred.output.compression.type=BLOCK;
SET mapred.output.compression.codec=org.apache.hadoop.io.compress.SnappyCodec;

drop table if exists prepost_dlx_weekly_data_step_2l;
create table
prepost_dlx_weekly_data_step_2l
as
select 
prepost_dlx_weekly_data_step_2k.dfa_id
, prepost_dlx_weekly_data_step_2k.pf_campaign

, prepost_dlx_weekly_data_step_2k.jul_wk_2_ibtr
, prepost_dlx_weekly_data_step_2k.jul_wk_2_ss
, prepost_dlx_weekly_data_step_2k.jul_wk_2_time_of_purchase

, prepost_dlx_weekly_data_step_2k.jul_wk_3_ibtr
, prepost_dlx_weekly_data_step_2k.jul_wk_3_ss
, prepost_dlx_weekly_data_step_2k.jul_wk_3_time_of_purchase

, prepost_dlx_weekly_data_step_2k.jul_wk_4_ibtr
, prepost_dlx_weekly_data_step_2k.jul_wk_4_ss
, prepost_dlx_weekly_data_step_2k.jul_wk_4_time_of_purchase

, prepost_dlx_weekly_data_step_2k.aug_wk_1_ibtr
, prepost_dlx_weekly_data_step_2k.aug_wk_1_ss
, prepost_dlx_weekly_data_step_2k.aug_wk_1_time_of_purchase

, prepost_dlx_weekly_data_step_2k.aug_wk_2_ibtr
, prepost_dlx_weekly_data_step_2k.aug_wk_2_ss
, prepost_dlx_weekly_data_step_2k.aug_wk_2_time_of_purchase

, prepost_dlx_weekly_data_step_2k.aug_wk_3_ibtr
, prepost_dlx_weekly_data_step_2k.aug_wk_3_ss
, prepost_dlx_weekly_data_step_2k.aug_wk_3_time_of_purchase

, prepost_dlx_weekly_data_step_2k.aug_wk_4_ibtr
, prepost_dlx_weekly_data_step_2k.aug_wk_4_ss
, prepost_dlx_weekly_data_step_2k.aug_wk_4_time_of_purchase

, prepost_dlx_weekly_data_step_2k.aug_wk_5_ibtr
, prepost_dlx_weekly_data_step_2k.aug_wk_5_ss
, prepost_dlx_weekly_data_step_2k.aug_wk_5_time_of_purchase

, prepost_dlx_weekly_data_step_2k.sep_wk_1_ibtr
, prepost_dlx_weekly_data_step_2k.sep_wk_1_ss
, prepost_dlx_weekly_data_step_2k.sep_wk_1_time_of_purchase

, prepost_dlx_weekly_data_step_2k.sep_wk_2_ibtr
, prepost_dlx_weekly_data_step_2k.sep_wk_2_ss
, prepost_dlx_weekly_data_step_2k.sep_wk_2_time_of_purchase

, prepost_dlx_weekly_data_step_2k.sep_wk_3_ibtr
, prepost_dlx_weekly_data_step_2k.sep_wk_3_ss
, prepost_dlx_weekly_data_step_2k.sep_wk_3_time_of_purchase

, prepost_dlx_weekly_data_step_2k.sep_wk_4_ibtr
, prepost_dlx_weekly_data_step_2k.sep_wk_4_ss
, prepost_dlx_weekly_data_step_2k.sep_wk_4_time_of_purchase

, prepost_dlx_weekly_data_step_2k.oct_wk_1_ibtr
, prepost_dlx_weekly_data_step_2k.oct_wk_1_ss
, prepost_dlx_weekly_data_step_2k.oct_wk_1_time_of_purchase

, prepost_dlx_weekly_data_step_2k.oct_wk_2_ibtr
, prepost_dlx_weekly_data_step_2k.oct_wk_2_ss
, prepost_dlx_weekly_data_step_2k.oct_wk_2_time_of_purchase

, prepost_dlx_weekly_data_step_2k.oct_wk_3_ibtr
, prepost_dlx_weekly_data_step_2k.oct_wk_3_ss
, prepost_dlx_weekly_data_step_2k.oct_wk_3_time_of_purchase

, prepost_dlx_weekly_data_step_2k.oct_wk_4_ibtr
, prepost_dlx_weekly_data_step_2k.oct_wk_4_ss
, prepost_dlx_weekly_data_step_2k.oct_wk_4_time_of_purchase

, prepost_dlx_weekly_data_step_2k.oct_wk_5_ibtr
, prepost_dlx_weekly_data_step_2k.oct_wk_5_ss
, prepost_dlx_weekly_data_step_2k.oct_wk_5_time_of_purchase

, prepost_dlx_weekly_data_step_2k.nov_wk_1_ibtr
, prepost_dlx_weekly_data_step_2k.nov_wk_1_ss
, prepost_dlx_weekly_data_step_2k.nov_wk_1_time_of_purchase

, prepost_dlx_weekly_data_step_2k.nov_wk_2_ibtr
, prepost_dlx_weekly_data_step_2k.nov_wk_2_ss
, prepost_dlx_weekly_data_step_2k.nov_wk_2_time_of_purchase

, prepost_dlx_weekly_data_step_2k.nov_wk_3_ibtr
, prepost_dlx_weekly_data_step_2k.nov_wk_3_ss
, prepost_dlx_weekly_data_step_2k.nov_wk_3_time_of_purchase

, prepost_dlx_weekly_data_step_2k.nov_wk_4_ibtr
, prepost_dlx_weekly_data_step_2k.nov_wk_4_ss
, prepost_dlx_weekly_data_step_2k.nov_wk_4_time_of_purchase

, prepost_dlx_weekly_data_step_2k.dec_wk_1_ibtr
, prepost_dlx_weekly_data_step_2k.dec_wk_1_ss
, prepost_dlx_weekly_data_step_2k.dec_wk_1_time_of_purchase

, prepost_dlx_weekly_data_step_2k.dec_wk_2_ibtr
, prepost_dlx_weekly_data_step_2k.dec_wk_2_ss
, prepost_dlx_weekly_data_step_2k.dec_wk_2_time_of_purchase

, prepost_dlx_weekly_data_step_2k.dec_wk_3_ibtr
, prepost_dlx_weekly_data_step_2k.dec_wk_3_ss
, prepost_dlx_weekly_data_step_2k.dec_wk_3_time_of_purchase

, prepost_dlx_weekly_data_step_2k.jan_wk_1_ibtr
, prepost_dlx_weekly_data_step_2k.jan_wk_1_ss
, prepost_dlx_weekly_data_step_2k.jan_wk_1_time_of_purchase

, prepost_dlx_weekly_data_step_2k.jan_wk_2_ibtr
, prepost_dlx_weekly_data_step_2k.jan_wk_2_ss
, prepost_dlx_weekly_data_step_2k.jan_wk_2_time_of_purchase

, prepost_dlx_weekly_data_step_2k.jan_wk_3_ibtr
, prepost_dlx_weekly_data_step_2k.jan_wk_3_ss
, prepost_dlx_weekly_data_step_2k.jan_wk_3_time_of_purchase

, prepost_dlx_weekly_data_step_2k.jan_wk_4_ibtr
, prepost_dlx_weekly_data_step_2k.jan_wk_4_ss
, prepost_dlx_weekly_data_step_2k.jan_wk_4_time_of_purchase

, prepost_dlx_weekly_data_step_2k.jan_wk_5_ibtr
, prepost_dlx_weekly_data_step_2k.jan_wk_5_ss
, prepost_dlx_weekly_data_step_2k.jan_wk_5_time_of_purchase

, prepost_dlx_weekly_data_step_2k.feb_wk_1_ibtr
, prepost_dlx_weekly_data_step_2k.feb_wk_1_ss
, prepost_dlx_weekly_data_step_2k.feb_wk_1_time_of_purchase

, prepost_dlx_weekly_data_step_2k.feb_wk_2_ibtr
, prepost_dlx_weekly_data_step_2k.feb_wk_2_ss
, prepost_dlx_weekly_data_step_2k.feb_wk_2_time_of_purchase

, prepost_dlx_weekly_data_step_2k.feb_wk_3_ibtr
, prepost_dlx_weekly_data_step_2k.feb_wk_3_ss
, prepost_dlx_weekly_data_step_2k.feb_wk_3_time_of_purchase

, prepost_dlx_weekly_data_step_2k.feb_wk_4_ibtr
, prepost_dlx_weekly_data_step_2k.feb_wk_4_ss
, prepost_dlx_weekly_data_step_2k.feb_wk_4_time_of_purchase

, prepost_dlx_weekly_data_step_2k.mar_wk_1_ibtr
, prepost_dlx_weekly_data_step_2k.mar_wk_1_ss
, prepost_dlx_weekly_data_step_2k.mar_wk_1_time_of_purchase

, prepost_dlx_weekly_data_step_2k.mar_wk_2_ibtr
, prepost_dlx_weekly_data_step_2k.mar_wk_2_ss
, prepost_dlx_weekly_data_step_2k.mar_wk_2_time_of_purchase

, prepost_dlx_weekly_data_step_2k.mar_wk_3_ibtr
, prepost_dlx_weekly_data_step_2k.mar_wk_3_ss
, prepost_dlx_weekly_data_step_2k.mar_wk_3_time_of_purchase

, prepost_dlx_weekly_data_step_2k.mar_wk_4_ibtr
, prepost_dlx_weekly_data_step_2k.mar_wk_4_ss
, prepost_dlx_weekly_data_step_2k.mar_wk_4_time_of_purchase

, prepost_dlx_weekly_data_step_2k.apr_wk_1_ibtr
, prepost_dlx_weekly_data_step_2k.apr_wk_1_ss
, prepost_dlx_weekly_data_step_2k.apr_wk_1_time_of_purchase

, prepost_dlx_weekly_data_step_2k.apr_wk_2_ibtr
, prepost_dlx_weekly_data_step_2k.apr_wk_2_ss
, prepost_dlx_weekly_data_step_2k.apr_wk_2_time_of_purchase

, prepost_dlx_weekly_data_step_2k.apr_wk_3_ibtr
, prepost_dlx_weekly_data_step_2k.apr_wk_3_ss
, prepost_dlx_weekly_data_step_2k.apr_wk_3_time_of_purchase

, prepost_dlx_weekly_data_step_2k.apr_wk_4_ibtr
, prepost_dlx_weekly_data_step_2k.apr_wk_4_ss
, prepost_dlx_weekly_data_step_2k.apr_wk_4_time_of_purchase

, prepost_dlx_weekly_data_step_2k.apr_wk_5_ibtr
, prepost_dlx_weekly_data_step_2k.apr_wk_45ss
, prepost_dlx_weekly_data_step_2k.apr_wk_5_time_of_purchase

, wss_may_wk_1.ibtr as may_wk_1_ibtr
, wss_may_wk_1.sales_signal_projected as may_wk_1_ss
, wss_may_wk_1.time_of_purchase as may_wk_1_time_of_purchase

, wss_may_wk_2.ibtr as may_wk_2_ibtr
, wss_may_wk_2.sales_signal_projected as may_wk_2_ss
, wss_may_wk_2.time_of_purchase as may_wk_2_time_of_purchase

from
prepost_dlx_weekly_data_step_2k

left outer join (
  select distinct
  dfa_id
  , pf_campaign
  , ibtr
  , sales_signal_projected
  , time_of_purchase
  
  from
  ingestion_dlx_weekly_sales_signal
  
  where
  ingestion_dt = '2016-05-07'
  and retailer_tracked = 1
  and lower(level) = 'product'
  ) wss_may_wk_1
on (
  wss_may_wk_1.dfa_id = prepost_dlx_weekly_data_step_2k.dfa_id 
  and wss_may_wk_1.pf_campaign = prepost_dlx_weekly_data_step_2k.pf_campaign
)

left outer join (
  select distinct
  dfa_id
  , pf_campaign
  , ibtr
  , sales_signal_projected
  , time_of_purchase
  
  from
  ingestion_dlx_weekly_sales_signal
  
  where
  ingestion_dt = '2016-05-14'
  and retailer_tracked = 1
  and lower(level) = 'product'
  ) wss_may_wk_2
on (
  wss_may_wk_2.dfa_id = prepost_dlx_weekly_data_step_2k.dfa_id 
  and wss_may_wk_2.pf_campaign = prepost_dlx_weekly_data_step_2k.pf_campaign
)
;
