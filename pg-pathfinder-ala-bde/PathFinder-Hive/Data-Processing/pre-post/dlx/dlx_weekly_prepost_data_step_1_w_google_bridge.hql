﻿SET hive.exec.compress.intermediate=true;
SET hive.exec.compress.output=true;
SET mapred.output.compression.type=BLOCK;
SET mapred.output.compression.codec=org.apache.hadoop.io.compress.SnappyCodec;

drop table if exists prepost_dlx_weekly_data_step_1_w_google_bridge;

create table prepost_dlx_weekly_data_step_1_w_google_bridge 
ROW FORMAT SERDE 
  'org.apache.hadoop.hive.serde2.columnar.ColumnarSerDe' 
STORED AS INPUTFORMAT 
  'org.apache.hadoop.hive.ql.io.RCFileInputFormat' 
OUTPUTFORMAT 
  'org.apache.hadoop.hive.ql.io.RCFileOutputFormat'
as
select /*+ mapjoin(dlx_campaign_advertiser_id_bridge) */
distinct
prepost_dlx_weekly_data_step_1.pf_campaign
, case
  when dlx_recrypted_cookies.newly_encrypted is null then
    prepost_dlx_weekly_data_step_1.dfa_id
  else
    dlx_recrypted_cookies.newly_encrypted
  end as dfa_id

from
prepost_dlx_weekly_data_step_1

join (
  select distinct
  pfcampaign_list.dlx_campaign_name
  , b.advertiser_id as adv_id

  from
  pfcampaign_list

  join dfa_meta_campaign b
  on
  pfcampaign_list.date_dropped = ''
  and pfcampaign_list.buy_id is not null
  and pfcampaign_list.dlx_campaign_name is not null
  and pfcampaign_list.dlx_campaign_name != ''
  and b.buy_id = pfcampaign_list.buy_id
) dlx_campaign_advertiser_id_bridge
on
dlx_campaign_advertiser_id_bridge.dlx_campaign_name = prepost_dlx_weekly_data_step_1.pf_campaign

left outer join dlx_recrypted_cookies
on
dlx_recrypted_cookies.encrypted = prepost_dlx_weekly_data_step_1.dfa_id
and dlx_recrypted_cookies.advertiser_id = dlx_campaign_advertiser_id_bridge.adv_id
;
