SET mapred.output.compression.type=BLOCK;
SET mapred.output.compression.codec=org.apache.hadoop.io.compress.SnappyCodec;
SET hive.exec.compress.output=true;
SET hive.exec.compress.intermediate=true;
SET hive.exec.dynamic.partition=true;
SET hive.exec.dynamic.partition.mode=nonstrict;
SET hive.auto.convert.join=true;

INSERT INTO TABLE prepost_dlx_dfa_user_impressions_v2 PARTITION(event_dt)
SELECT
  ingestion_dfa_impressions.user_id as dfa_user_id,
  FROM_UNIXTIME(UNIX_TIMESTAMP(ingestion_dfa_impressions.time, 'MM-dd-yyyy-HH:mm:ss')) as event_dts,
  ingestion_dfa_impressions.buy_id,
  ingestion_dfa_impressions.creative_id,
  ingestion_dfa_impressions.site_id,
  ingestion_dfa_impressions.page_id,
  ingestion_dfa_impressions.site_data,
  ingestion_dfa_impressions.dlx_encrypted_dfa_user_id,
  ingestion_dfa_impressions.acxiom_encrypted_dfa_user_id,
  to_date(FROM_UNIXTIME(UNIX_TIMESTAMP(ingestion_dfa_impressions.time, 'MM-dd-yyyy-HH:mm:ss'))) as event_dt

FROM
(
  SELECT * 
  FROM pfcampaign_list 
  WHERE 
  dlx_campaign_name != '' 
  and buy_id is not NULL 
  and date_dropped = ''
) pfcampaign_list

JOIN
  ingestion_dfa_impressions
  on
    ingestion_dfa_impressions.ingestion_dt >= concat_ws('-', substring('${hiveconf:imp_begin_dt}', 1, 4), substring('${hiveconf:imp_begin_dt}', 5, 2), substring('${hiveconf:imp_begin_dt}', 7, 2))
    and ingestion_dfa_impressions.ingestion_dt < concat_ws('-', substring('${hiveconf:imp_end_dt}', 1, 4), substring('${hiveconf:imp_end_dt}', 5, 2), substring('${hiveconf:imp_end_dt}', 7, 2))
    and ingestion_dfa_impressions.buy_id = pfcampaign_list.buy_id

JOIN
  (
    select distinct
    encrypted, newly_encrypted, advertiser_id
    from dlx_recrypted_cookies
  ) dlx_recrypted_cookies
  ON
    dlx_recrypted_cookies.advertiser_id = ingestion_dfa_impressions.advertiser_id
    and dlx_recrypted_cookies.newly_encrypted = ingestion_dfa_impressions.user_id

JOIN
  (
    select distinct
    dfa_id as dfa_user_id
    , pf_campaign

    from
    prepost_dlx_weekly_data_step_1_w_google_bridge
  ) prepost_dlx_dfa_users
  on 
    prepost_dlx_dfa_users.dfa_user_id = dlx_recrypted_cookies.encrypted
    and prepost_dlx_dfa_users.pf_campaign = pfcampaign_list.dlx_campaign_name
;
