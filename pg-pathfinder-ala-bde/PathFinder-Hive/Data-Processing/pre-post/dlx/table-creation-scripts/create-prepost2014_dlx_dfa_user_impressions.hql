﻿DROP TABLE IF EXISTS prepost_dlx_dfa_user_impressions;

CREATE  TABLE prepost_dlx_dfa_user_impressions
(
  dfa_user_id string, 
  event_dts timestamp, 
  buy_id bigint, 
  creative_id bigint, 
  site_id bigint, 
  page_id bigint, 
  site_data string, 
  dlx_encrypted_dfa_user_id string, 
  acxiom_encrypted_dfa_user_id string
)
PARTITIONED BY ( 
  event_dt string)
CLUSTERED BY ( 
  dfa_user_id) 
INTO 20 BUCKETS
ROW FORMAT SERDE 
  'org.apache.hadoop.hive.serde2.columnar.ColumnarSerDe' 
STORED AS INPUTFORMAT 
  'org.apache.hadoop.hive.ql.io.RCFileInputFormat' 
OUTPUTFORMAT 
  'org.apache.hadoop.hive.ql.io.RCFileOutputFormat'
;