﻿DROP TABLE IF EXISTS prepost_dlx_recco_populations;

CREATE  TABLE prepost_dlx_recco_populations
(
  pfcampaign string, 
  recco_num string, 
  dfa_user_id string, 
  in_pre int, 
  in_post int
)
ROW FORMAT SERDE 
  'org.apache.hadoop.hive.serde2.lazy.LazySimpleSerDe' 
STORED AS INPUTFORMAT 
  'org.apache.hadoop.mapred.TextInputFormat' 
OUTPUTFORMAT 
  'org.apache.hadoop.hive.ql.io.HiveIgnoreKeyTextOutputFormat'
;
