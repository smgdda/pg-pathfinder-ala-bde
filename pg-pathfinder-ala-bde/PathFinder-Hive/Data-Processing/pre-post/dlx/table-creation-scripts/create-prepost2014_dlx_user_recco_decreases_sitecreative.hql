﻿DROP TABLE IF EXISTS prepost_dlx_user_recco_decreases_sitecreative;

CREATE  TABLE prepost_dlx_user_recco_decreases_sitecreative
(
  dfa_user_id string, 
  recco_num string, 
  decrease string, 
  decrease_pre_start string, 
  decrease_pre_end string, 
  decrease_post_start string, 
  decrease_post_end string, 
  in_decrease_pre int, 
  in_decrease_post int
)
PARTITIONED BY ( 
  pfcampaign string)
CLUSTERED BY ( 
  dfa_user_id) 
INTO 20 BUCKETS
ROW FORMAT SERDE 
  'org.apache.hadoop.hive.serde2.columnar.ColumnarSerDe' 
STORED AS INPUTFORMAT 
  'org.apache.hadoop.hive.ql.io.RCFileInputFormat' 
OUTPUTFORMAT 
  'org.apache.hadoop.hive.ql.io.RCFileOutputFormat'
;
