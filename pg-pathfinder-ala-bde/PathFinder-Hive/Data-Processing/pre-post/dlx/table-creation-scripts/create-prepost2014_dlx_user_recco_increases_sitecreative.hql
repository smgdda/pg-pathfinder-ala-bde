﻿DROP TABLE IF EXISTS prepost_dlx_user_recco_increases_sitecreative;

CREATE  TABLE prepost_dlx_user_recco_increases_sitecreative
(
  dfa_user_id string, 
  recco_num string, 
  increase string, 
  increase_pre_start string, 
  increase_pre_end string, 
  increase_post_start string, 
  increase_post_end string, 
  in_increase_pre int, 
  in_increase_post int
)
PARTITIONED BY ( 
  pfcampaign string)
CLUSTERED BY ( 
  dfa_user_id) 
INTO 20 BUCKETS
ROW FORMAT SERDE 
  'org.apache.hadoop.hive.serde2.columnar.ColumnarSerDe' 
STORED AS INPUTFORMAT 
  'org.apache.hadoop.hive.ql.io.RCFileInputFormat' 
OUTPUTFORMAT 
  'org.apache.hadoop.hive.ql.io.RCFileOutputFormat'
;
