DROP TABLE IF EXISTS prepost_mbd_user_imps_matched;

CREATE  TABLE prepost_mbd_user_imps_matched (
  dfa_user_id string,
  buy_id bigint, 
  site_id bigint, 
  site string, 
  page_id bigint, 
  site_placement string, 
  creative_id bigint, 
  creative_name string, 
  creative string, 
  ui_creative_id bigint, 
  site_data string, 
  sitetactic string, 
  siteadsize string, 
  sitecreative string
)
PARTITIONED BY ( 
  event_dt string
  , pfcampaign string)
CLUSTERED BY ( 
  dfa_user_id) 
INTO 20 BUCKETS
ROW FORMAT SERDE 
  'org.apache.hadoop.hive.serde2.columnar.ColumnarSerDe' 
STORED AS INPUTFORMAT 
  'org.apache.hadoop.hive.ql.io.RCFileInputFormat' 
OUTPUTFORMAT 
  'org.apache.hadoop.hive.ql.io.RCFileOutputFormat'
;
