SET hive.auto.convert.join=true;
SET hive.exec.compress.output=true;
SET hive.exec.compress.intermediate=true;
SET mapred.output.compression.type=BLOCK;
SET mapred.output.compression.codec=org.apache.hadoop.io.compress.SnappyCodec;

DROP TABLE IF EXISTS prepost_mbd_dfa_user_activities;

CREATE TABLE prepost_mbd_dfa_user_activities AS
SELECT DISTINCT
split(str_to_map(other_data, '\\;', '=')['u3'], '-')[0] as dl_member_id
, split(str_to_map(other_data, '\\;', '=')['u3'], '-')[1] as mbd_survey_id
, dfa_user_id
, buy_id

FROM
dfa_activity

WHERE
lower(activity_type) == 'vindid' 
and lower(activity_sub_type) == 'us_13324'
and event_dt >= '2015-07-01'
and event_dt <= '2015-11-04'
;
