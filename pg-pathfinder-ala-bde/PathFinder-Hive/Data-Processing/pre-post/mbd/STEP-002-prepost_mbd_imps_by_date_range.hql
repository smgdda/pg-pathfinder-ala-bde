SET mapred.output.compression.type=BLOCK;
SET mapred.output.compression.codec=org.apache.hadoop.io.compress.SnappyCodec;
SET hive.exec.compress.output=true;
SET hive.exec.compress.intermediate=true;
SET hive.exec.dynamic.partition=true;
SET hive.exec.dynamic.partition.mode=nonstrict;
SET hive.auto.convert.join=true;

INSERT INTO TABLE prepost_mbd_dfa_user_imps PARTITION(event_dt, pfcampaign)
SELECT
dfa_impressions.*
, regexp_replace(lower(pfcampaign_list.pfcampaign), '[^a-zA-Z0-9]', '') as pfcampaign

FROM
(
  SELECT
    mbd_survey_id
    , regexp_replace(lower(pfcampaign), '[^a-zA-Z0-9]', '') as mbd_survey_pfcampaign
  FROM
    prepost_mbd_pfcampaign_survey_id
) prepost_mbd_pfcampaign_survey_id

JOIN pfcampaign_list
ON regexp_replace(lower(pfcampaign_list.pfcampaign), '[^a-zA-Z0-9]', '') = prepost_mbd_pfcampaign_survey_id.mbd_survey_pfcampaign

JOIN (
  SELECT
    user_id as dfa_user_id
    , FROM_UNIXTIME(UNIX_TIMESTAMP(time, 'MM-dd-yyyy-HH:mm:ss')) as event_dts
    , buy_id
    , creative_id
    , site_id
    , page_id
    , site_data
    , dlx_encrypted_dfa_user_id
    , acxiom_encrypted_dfa_user_id
    , to_date(FROM_UNIXTIME(UNIX_TIMESTAMP(time, 'MM-dd-yyyy-HH:mm:ss'))) as event_dt
  FROM
    ingestion_dfa_impressions
  WHERE
    ingestion_dfa_impressions.ingestion_dt >= concat_ws('-', substring('${hiveconf:imp_begin_dt}', 1, 4), substring('${hiveconf:imp_begin_dt}', 5, 2), substring('${hiveconf:imp_begin_dt}', 7, 2))
    and ingestion_dfa_impressions.ingestion_dt < concat_ws('-', substring('${hiveconf:imp_end_dt}', 1, 4), substring('${hiveconf:imp_end_dt}', 5, 2), substring('${hiveconf:imp_end_dt}', 7, 2))
    and ingestion_dfa_impressions.user_id != '0'
) dfa_impressions
ON (
  dfa_impressions.buy_id = pfcampaign_list.buy_id
)

JOIN (
  SELECT DISTINCT
    dfa_user_id
    , mbd_survey_id
  FROM
    prepost_mbd_dfa_user_activities

  WHERE
    dfa_user_id != '0'
) prepost_mbd_dfa_users
ON (
  prepost_mbd_dfa_users.dfa_user_id = dfa_impressions.dfa_user_id
  and prepost_mbd_dfa_users.mbd_survey_id = prepost_mbd_pfcampaign_survey_id.mbd_survey_id
)
;

INSERT INTO TABLE prepost_mbd_test
SELECT
  from_unixtime(unix_timestamp())
  , concat_ws('-', substring('${hiveconf:imp_begin_dt}', 1, 4), substring('${hiveconf:imp_begin_dt}', 5, 2), substring('${hiveconf:imp_begin_dt}', 7, 2))
  , concat_ws('-', substring('${hiveconf:imp_end_dt}', 1, 4), substring('${hiveconf:imp_end_dt}', 5, 2), substring('${hiveconf:imp_end_dt}', 7, 2))
FROM pfcampaign_list limit 1
;
