SET mapred.output.compression.type=BLOCK;
SET mapred.output.compression.codec=org.apache.hadoop.io.compress.SnappyCodec;
SET hive.exec.compress.output=true;
SET hive.exec.compress.intermediate=true;
SET hive.auto.convert.join=true;
SET hive.exec.dynamic.partition=true;
SET hive.exec.dynamic.partition.mode=nonstrict;

insert overwrite table prepost_mbd_user_recco_decreases_sitetactic partition(pfcampaign)
select /*+ MAPJOIN(prepost2014_optimizations) */
prepost_mbd_user_imps_matched.dfa_user_id
, prepost2014_optimizations.recco_num
, prepost2014_optimizations.decrease
, prepost2014_optimizations.pre_start_date_final as decrease_pre_start
, prepost2014_optimizations.pre_end_date_final as decrease_pre_end
, prepost2014_optimizations.post_start_date_final as decrease_post_start
, prepost2014_optimizations.post_end_date_final as decrease_post_end
, case
  when
    prepost_mbd_user_imps_matched.event_dt >= prepost2014_optimizations.pre_start_date_final
    and prepost_mbd_user_imps_matched.event_dt <= prepost2014_optimizations.pre_end_date_final 
  then 1
  else 0 end as in_decrease_pre
, case 
  when  
    prepost_mbd_user_imps_matched.event_dt >= prepost2014_optimizations.post_start_date_final
    and prepost_mbd_user_imps_matched.event_dt <= prepost2014_optimizations.post_end_date_final 
  then 1
  else 0 end as in_decrease_post
, prepost2014_optimizations.pfcampaign

from
(
  select
  *
  
  from
  prepost2014_optimizations
  
  where
  prepost2014_optimizations.dimtype in ('site', 'sitetactic')
  and prepost2014_optimizations.analysis_completed != 1
) prepost2014_optimizations

join prepost_mbd_user_imps_matched 
on (
  prepost_mbd_user_imps_matched.sitetactic = prepost2014_optimizations.decrease
  and prepost_mbd_user_imps_matched.pfcampaign = prepost2014_optimizations.pfcampaign
  )
;
