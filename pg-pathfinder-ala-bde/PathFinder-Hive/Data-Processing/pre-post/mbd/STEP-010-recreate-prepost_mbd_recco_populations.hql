SET mapred.output.compression.type=BLOCK;
SET mapred.output.compression.codec=org.apache.hadoop.io.compress.SnappyCodec;
SET hive.exec.compress.output=true;
SET hive.exec.compress.intermediate=true;

drop table if exists prepost_mbd_recco_populations;

create table prepost_mbd_recco_populations as
select 
  pfcampaign
  , recco_num
  , dfa_user_id
  , max(in_pre) as in_pre
  , max(in_post) as in_post

from
(
  select
  pfcampaign
  , recco_num
  , dfa_user_id
  , in_decrease_pre as in_pre
  , in_decrease_post as in_post

  from
  prepost_mbd_user_recco_decreases_sitetactic

  where
  (
  in_decrease_pre = 1
  or in_decrease_post = 1
  )

  union all

  select
  pfcampaign
  , recco_num
  , dfa_user_id
  , in_increase_pre as in_pre
  , in_increase_post as in_post

  from
  prepost_mbd_user_recco_increases_sitetactic

  where
  (
  in_increase_pre = 1
  or in_increase_post = 1
  )
  
  union all
  
  select
  pfcampaign
  , recco_num
  , dfa_user_id
  , in_decrease_pre as in_pre
  , in_decrease_post as in_post

  from
  prepost_mbd_user_recco_decreases_sitecreative

  where
  (
  in_decrease_pre = 1
  or in_decrease_post = 1
  )

  union all

  select
  pfcampaign
  , recco_num
  , dfa_user_id
  , in_increase_pre as in_pre
  , in_increase_post as in_post

  from
  prepost_mbd_user_recco_increases_sitecreative

  where
  (
  in_increase_pre = 1
  or in_increase_post = 1
  )
  
  union all
  
  select
  pfcampaign
  , recco_num
  , dfa_user_id
  , in_decrease_pre as in_pre
  , in_decrease_post as in_post

  from
  prepost_mbd_user_recco_decreases_siteadsize

  where
  (
  in_decrease_pre = 1
  or in_decrease_post = 1
  )

  union all

  select
  pfcampaign
  , recco_num
  , dfa_user_id
  , in_increase_pre as in_pre
  , in_increase_post as in_post

  from
  prepost_mbd_user_recco_increases_siteadsize

  where
  (
  in_increase_pre = 1
  or in_increase_post = 1
  )
) unioned_mbd_recco_user_dimtypes
group by
  pfcampaign
  , recco_num
  , dfa_user_id
;
