﻿drop table if exists prepost_mbd_recco_populations_w_att_responses;
create table prepost_mbd_recco_populations_w_att_responses as
select
*
, case
    when in_pre = 1 and in_post = 1 then -1 -- these are being ignored since we don't know which exposure period relates to the survey results
    when in_pre = 0 and in_post = 0 then -1 -- these are being ignored since there is no exposure to relate to the survey results
    when in_pre = 0 and in_post = 1 and survey_in_pre = 0 and survey_in_post = 0 then 0
    when in_pre = 0 and in_post = 1 and survey_in_pre = 0 and survey_in_post = 1 then 0
    when in_pre = 0 and in_post = 1 and survey_in_pre = 1 and survey_in_post = 0 then 0
    when in_pre = 0 and in_post = 1 and survey_in_pre = 1 and survey_in_post = 1 then 0
    when in_pre = 1 and in_post = 0 and survey_in_pre = 0 and survey_in_post = 0 then 0
    when in_pre = 1 and in_post = 0 and survey_in_pre = 0 and survey_in_post = 1 then 0
    when in_pre = 1 and in_post = 0 and survey_in_pre = 1 and survey_in_post = 0 then att_response
    when in_pre = 1 and in_post = 0 and survey_in_pre = 1 and survey_in_post = 1 then att_response
  end as att_response_final_in_pre
, case
    when in_pre = 1 and in_post = 1 then -1 -- these are being ignored since we don't know which exposure period relates to the survey results
    when in_pre = 0 and in_post = 0 then -1 -- these are being ignored since there is no exposure to relate to the survey results
    when in_pre = 0 and in_post = 1 and survey_in_pre = 0 and survey_in_post = 0 then 0
    when in_pre = 0 and in_post = 1 and survey_in_pre = 0 and survey_in_post = 1 then att_response
    when in_pre = 0 and in_post = 1 and survey_in_pre = 1 and survey_in_post = 0 then 0
    when in_pre = 0 and in_post = 1 and survey_in_pre = 1 and survey_in_post = 1 then att_response
    when in_pre = 1 and in_post = 0 and survey_in_pre = 0 and survey_in_post = 0 then 0
    when in_pre = 1 and in_post = 0 and survey_in_pre = 0 and survey_in_post = 1 then 0
    when in_pre = 1 and in_post = 0 and survey_in_pre = 1 and survey_in_post = 0 then 0
    when in_pre = 1 and in_post = 0 and survey_in_pre = 1 and survey_in_post = 1 then 0
  end as att_response_final_in_post
from
(
select
  prepost_mbd_recco_populations.*
  , att_responses_all.exposed_control
  , att_responses_all.survey_dt
  , reccos.pre_start_date_final
  , reccos.pre_end_date_final
  , reccos.post_start_date_final
  , reccos.post_end_date_final
  , case 
    when (
      reccos.pre_start_date_final < att_responses_all.survey_dt
      and att_responses_all.survey_dt <= reccos.pre_end_date_final
    )
      then 1 
    else 0 
    end as survey_in_pre
  , case 
    when (
      reccos.post_start_date_final < att_responses_all.survey_dt
      and att_responses_all.survey_dt <= reccos.post_end_date_final
    )
      then 1 
    else 
      0 
    end as survey_in_post
  , att_responses_all.att_response

from
  (
    select
      *
    from
      prepost_mbd_recco_populations
    where
      (
        (in_pre = 1 and in_post = 0)
        or (in_pre = 0 and in_post = 1)
      )
  ) prepost_mbd_recco_populations

join
  att_responses_all
  on
    att_responses_all.pfcampaign = prepost_mbd_recco_populations.pfcampaign
    and att_responses_all.dfa_user_id = prepost_mbd_recco_populations.dfa_user_id

join
  (
    select
      prepost2014_optimizations.pfcampaign
      , prepost2014_optimizations.recco_num
      , prepost2014_optimizations.pre_start_date_final
      , prepost2014_optimizations.pre_end_date_final
      , prepost2014_optimizations.post_start_date_final
      , prepost2014_optimizations.post_end_date_final
    
    from
      prepost2014_optimizations
  ) reccos
  on reccos.recco_num = prepost_mbd_recco_populations.recco_num
) recco_pop_w_att_response_data
;