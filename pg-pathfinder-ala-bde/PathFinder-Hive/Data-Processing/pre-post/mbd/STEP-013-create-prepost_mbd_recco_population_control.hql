﻿drop table if exists prepost_mbd_recco_population_control;
create table prepost_mbd_recco_population_control as
select
reccos.pfcampaign
, reccos.recco_num
, att_responses_all.dfa_user_id
, case 
    when (
      reccos.pre_start_date_final < att_responses_all.survey_dt
      and att_responses_all.survey_dt <= reccos.pre_end_date_final
    )
      then 1 
    else 0 
  end as in_pre_survey
, case 
    when (
      reccos.post_start_date_final < att_responses_all.survey_dt
      and att_responses_all.survey_dt <= reccos.post_end_date_final
    )
      then 1 
    else 
      0 
  end as in_post_survey
, att_responses_all.att_response
, att_responses_all.exposed_control

from
(
  select
    prepost2014_optimizations.pfcampaign
    , prepost2014_optimizations.recco_num
    , prepost2014_optimizations.pre_start_date_final
    , prepost2014_optimizations.pre_end_date_final
    , prepost2014_optimizations.post_start_date_final
    , prepost2014_optimizations.post_end_date_final
  
  from
    prepost2014_optimizations
) reccos

join
att_responses_all
on
  att_responses_all.exposed_control = 'control'
  and att_responses_all.pfcampaign = reccos.pfcampaign
;
