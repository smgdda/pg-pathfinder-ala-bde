DROP TABLE IF EXISTS prepost2014_mbd_campaign_stats;

CREATE  TABLE prepost2014_mbd_campaign_stats(
  pfcampaign string, 
  in_post int, 
  in_pre int, 
  dfa_user_id string, 
  event_dts timestamp, 
  advertiser_id bigint, 
  buy_id bigint, 
  ad_id bigint, 
  creative_id bigint, 
  creative_size_id string, 
  site_id bigint, 
  page_id bigint, 
  site_data string
  )
PARTITIONED BY ( 
  event_dt string)
CLUSTERED BY ( 
  dfa_user_id) 
INTO 20 BUCKETS
ROW FORMAT SERDE 
  'org.apache.hadoop.hive.serde2.columnar.ColumnarSerDe' 
STORED AS INPUTFORMAT 
  'org.apache.hadoop.hive.ql.io.RCFileInputFormat' 
OUTPUTFORMAT 
  'org.apache.hadoop.hive.ql.io.RCFileOutputFormat'
;
