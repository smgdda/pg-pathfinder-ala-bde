SET mapred.output.compression.type-BLOCK;
SET mapred.output.compression.codec=org.apache.hadoop.io.compress.SnappyCodec;
SET hive.exec.compress.output=true;
SET hive.exec.compress.intermediate=true;
SET hive.exec.dynamic.partition=true;
SET hive.exec.dynamic.partition.mode=nonstrict;

insert into table prepost_mbd_test
select
  from_unixtime(unix_timestamp())
  , concat_ws('-', substring('${hiveconf:imp_begin_dt}', 1, 4), substring('${hiveconf:imp_begin_dt}', 5, 2), substring('${hiveconf:imp_begin_dt}', 7, 2))
  , concat_ws('-', substring('${hiveconf:imp_end_dt}', 1, 4), substring('${hiveconf:imp_end_dt}', 5, 2), substring('${hiveconf:imp_end_dt}', 7, 2))
from pfcampaign_list limit 1;

insert overwrite table prepost2014_mbd_dfa_user_impressions
select /*+ MAPJOIN(pfcampaign_list) */
dfa_impressions.*

from
pfcampaign_list

join dfa_impressions
on (
  pfcampaign_list.pfcampaign = 'variant'
  and dfa_impressions.event_dt >= concat_ws('-', substring('${hiveconf:imp_begin_dt}', 1, 4), substring('${hiveconf:imp_begin_dt}', 5, 2), substring('${hiveconf:imp_begin_dt}', 7, 2))
  and dfa_impressions.event_dt < concat_ws('-', substring('${hiveconf:imp_end_dt}', 1, 4), substring('${hiveconf:imp_end_dt}', 5, 2), substring('${hiveconf:imp_end_dt}', 7, 2))
  and dfa_impressions.buy_id = pfcampaign_list.buy_id
)

join (
  select distinct
  dfa_user_id

  from
  prepost2014_mbd_dfa_user_activities

  where
  mbd_survey_id = 1223572
) prepost2014_mbd_variant_dfa_users
on (
  prepost2014_mbd_variant_dfa_users.dfa_user_id = dfa_impressions.dfa_user_id
)
;