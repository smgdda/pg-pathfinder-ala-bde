import sys, os, subprocess
import datetime
import time

def main():
  mbd_date_range_process = date_range_process()
  mbd_date_range_process.run()
  

class date_range_process:

  def __init__(self):
    print("date_range_process class init...")

  def run(self):
    print("starting date_range_process.run()...")

    self.run_date_range_process('20140901', '20141003', '20141130', 'instaglam', '1257228', 5)
    self.run_date_range_process('20140728', '20140908', '20141227', 'shizen', '1226469', 5)
    self.run_date_range_process('20140731', '20141008', '20141208', 'body', '1224252', 5)
    self.run_date_range_process('20140804', '20140924', '20141230', 'variant', '1223572', 5)

    print('completed date_range_process.run()...')

  def run_date_range_process(self, input_start_date, input_activation_date, input_end_date, input_pfcampaign, input_mbd_survey_id, interval):
    print("starting date_range_process.run_date_range_process(" + input_start_date + ", " + input_activation_date + ", " + input_end_date + ", " + input_pfcampaign + ", " + input_mbd_survey_id + ", " + str(interval) + ")...\n\n")

    startDate = datetime.date(int(input_start_date[0:4]), int(input_start_date[4:6]), int(input_start_date[6:8]))
    activationDate = datetime.date(int(input_activation_date[0:4]), int(input_activation_date[4:6]), int(input_activation_date[6:8]))
    currentEndDate = startDate + datetime.timedelta(days=int(interval))

    while startDate < datetime.date(int(input_end_date[0:4]), int(input_end_date[4:6]), int(input_end_date[6:8])):
      print(datetime.date.isoformat(startDate) + "\t" + datetime.date.isoformat(currentEndDate))

      print ('spawning subprocess==========>\n\n')
      print(["hive", "-f", "process_mbd_campaign_imps.sql", "-hiveconf", "imp_begin_dt=" + datetime.date.isoformat(startDate).replace("-",""), "-hiveconf", "imp_act_dt=" + datetime.date.isoformat(activationDate).replace("-",""), "-hiveconf", "imp_end_dt=" + datetime.date.isoformat(currentEndDate).replace("-",""), "-hiveconf", "pfcampaign=" + input_pfcampaign, "-hiveconf", "mbd_survey_id=" + input_mbd_survey_id])
      
      #uncomment these lines for production use
      #process = subprocess.Popen(["hive", "-f", "process_mbd_campaign_imps.sql", "-hiveconf", "imp_begin_dt=" + datetime.date.isoformat(startDate).replace("-",""), "-hiveconf", "imp_act_dt=" + datetime.date.isoformat(activationDate).replace("-",""), "-hiveconf", "imp_end_dt=" + datetime.date.isoformat(currentEndDate).replace("-",""), "-hiveconf", "pfcampaign=" + input_pfcampaign, "-hiveconf", "mbd_survey_id=" + input_mbd_survey_id], stdout=subprocess.PIPE)
      #data = process.communicate()[0]
      
      print ('ending subprocess subprocess==========>\n\n')

      #reset start and end dates for next iteration of processing loop
      startDate = currentEndDate
      currentEndDate = startDate + datetime.timedelta(days=int(interval))

    print("completed date_range_process.run_date_range_process(" + input_start_date + ", " + input_activation_date + ", " + input_end_date + ", " + input_pfcampaign + ", " + input_mbd_survey_id + ", " + str(interval) + ")...\n\n")

if __name__ == "__main__":
  main()