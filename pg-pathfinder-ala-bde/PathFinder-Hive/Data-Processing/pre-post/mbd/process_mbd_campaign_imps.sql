SET mapred.output.compression.type-BLOCK;
SET mapred.output.compression.codec=org.apache.hadoop.io.compress.SnappyCodec;
SET hive.exec.compress.output=true;
SET hive.exec.compress.intermediate=true;
SET hive.exec.dynamic.partition=true;
SET hive.exec.dynamic.partition.mode=nonstrict;
SET hive.auto.convert.join=true;

insert into table prepost_mbd_imp_load_log
select
  from_unixtime(unix_timestamp())
  , concat_ws('-', substring('${hiveconf:imp_begin_dt}', 1, 4), substring('${hiveconf:imp_begin_dt}', 5, 2), substring('${hiveconf:imp_begin_dt}', 7, 2))
  , concat_ws('-', substring('${hiveconf:imp_act_dt}', 1, 4), substring('${hiveconf:imp_act_dt}', 5, 2), substring('${hiveconf:imp_act_dt}', 7, 2))
  , concat_ws('-', substring('${hiveconf:imp_end_dt}', 1, 4), substring('${hiveconf:imp_end_dt}', 5, 2), substring('${hiveconf:imp_end_dt}', 7, 2))
  , '${hiveconf:pfcampaign}'
  , '${hiveconf:mbd_survey_id}'
from pfcampaign_list limit 1;

insert into table prepost2014_mbd_campaign_stats PARTITION(event_dt)
select
pfcampaign_list.pfcampaign
, case 
  when dfa_impressions.event_dt < concat_ws('-', substring('${hiveconf:imp_act_dt}', 1, 4), substring('${hiveconf:imp_act_dt}', 5, 2), substring('${hiveconf:imp_act_dt}', 7, 2)) then 1 
  else 0 end as in_pre
, case 
  when dfa_impressions.event_dt >= concat_ws('-', substring('${hiveconf:imp_act_dt}', 1, 4), substring('${hiveconf:imp_act_dt}', 5, 2), substring('${hiveconf:imp_act_dt}', 7, 2)) then 1 
  else 0 end as in_post
, case 
  when prepost2014_mbd_dfa_campaign_users.dfa_user_id is not null then 1 
  else 0 end as is_mbd_user
, dfa_impressions.dfa_user_id
, dfa_impressions.advertiser_id
, dfa_impressions.buy_id
, dfa_impressions.ad_id
, dfa_impressions.creative_id
, dfa_impressions.creative_size_id
, dfa_impressions.site_id
, dfa_impressions.page_id
, dfa_impressions.site_data
, dfa_impressions.event_dt

from
pfcampaign_list

join (
  select
  dfa_impressions.dfa_user_id
  , dfa_impressions.advertiser_id
  , dfa_impressions.buy_id
  , dfa_impressions.ad_id
  , dfa_impressions.creative_id
  , dfa_impressions.creative_size_id
  , dfa_impressions.site_id
  , dfa_impressions.page_id
  , dfa_impressions.site_data
  , dfa_impressions.event_dt
  from
  dfa_impressions
  where
  dfa_impressions.event_dt >= concat_ws('-', substring('${hiveconf:imp_begin_dt}', 1, 4), substring('${hiveconf:imp_begin_dt}', 5, 2), substring('${hiveconf:imp_begin_dt}', 7, 2))
  and dfa_impressions.event_dt < concat_ws('-', substring('${hiveconf:imp_end_dt}', 1, 4), substring('${hiveconf:imp_end_dt}', 5, 2), substring('${hiveconf:imp_end_dt}', 7, 2))
  and dfa_impressions.dfa_user_id != 0
) dfa_impressions
on (
  pfcampaign_list.pfcampaign = '${hiveconf:pfcampaign}'
  and dfa_impressions.buy_id = pfcampaign_list.buy_id
)

left outer join (
  select distinct
  dfa_user_id

  from
  prepost2014_mbd_dfa_user_activities

  where
  mbd_survey_id = '${hiveconf:mbd_survey_id}'
) prepost2014_mbd_dfa_campaign_users
on (
  prepost2014_mbd_dfa_campaign_users.dfa_user_id = dfa_impressions.dfa_user_id
)
;