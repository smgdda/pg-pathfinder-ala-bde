﻿-- insert into table prepost_traffic_data
select
  prepost2014_bhv_plcmt_level_cpm.recco_num
  , pf_daily_impressions_per_placement.event_dt
  , sum(case 
        when prepost2014_optimizations.dimtype in ('site', 'sitetactic') and pf_daily_impressions_per_placement.sitetactic = prepost2014_optimizations.increase then pf_daily_impressions_per_placement.total_imps 
        when prepost2014_optimizations.dimtype = 'sitecreative' and pf_daily_impressions_per_placement.sitecreative = prepost2014_optimizations.increase then pf_daily_impressions_per_placement.total_imps 
        when prepost2014_optimizations.dimtype = 'siteadsize' and pf_daily_impressions_per_placement.siteadsize = prepost2014_optimizations.increase then pf_daily_impressions_per_placement.total_imps 
        else cast(0 as bigint) end) as imp_sum_inc
  , sum(case
        when prepost2014_optimizations.dimtype in ('site', 'sitetactic') and pf_daily_impressions_per_placement.sitetactic = prepost2014_optimizations.decrease then pf_daily_impressions_per_placement.total_imps 
        when prepost2014_optimizations.dimtype = 'sitecreative' and pf_daily_impressions_per_placement.sitecreative = prepost2014_optimizations.decrease then pf_daily_impressions_per_placement.total_imps 
        when prepost2014_optimizations.dimtype = 'siteadsize' and pf_daily_impressions_per_placement.siteadsize = prepost2014_optimizations.decrease then pf_daily_impressions_per_placement.total_imps 
        else cast(0 as bigint) end) as imp_sum_dec
from
  pf_daily_impressions_per_placement
join
  prepost2014_bhv_plcmt_level_cpm_backhalf prepost2014_bhv_plcmt_level_cpm
  on (
    prepost2014_bhv_plcmt_level_cpm.recco_num = ${hiveconf:recco_num}
    and prepost2014_bhv_plcmt_level_cpm.buy_id = pf_daily_impressions_per_placement.buy_id
    and prepost2014_bhv_plcmt_level_cpm.site_placement = pf_daily_impressions_per_placement.site_placement
    and prepost2014_bhv_plcmt_level_cpm.page_id = pf_daily_impressions_per_placement.page_id
    )
join
  (
    select
      recco_num
      , dimtype
      , increase
      , decrease
      , max(pre_start_date_final) as pre_start_date_final
      , max(pre_end_date_final) as pre_end_date_final
      , max(post_start_date_final) as post_start_date_final
      , max(post_end_date_final) as post_end_date_final
    from
      prepost2014_optimizations
    group by
      recco_num
      , dimtype
      , increase
      , decrease
  ) prepost2014_optimizations
  on (
    prepost2014_optimizations.recco_num = ${hiveconf:recco_num}
    and prepost2014_optimizations.recco_num = prepost2014_bhv_plcmt_level_cpm.recco_num
  )
where
    (
      (prepost2014_optimizations.dimtype in ('site', 'sitetactic') and pf_daily_impressions_per_placement.sitetactic in (prepost2014_optimizations.increase, prepost2014_optimizations.decrease))
      or (prepost2014_optimizations.dimtype = 'sitecreative' and pf_daily_impressions_per_placement.sitecreative in (prepost2014_optimizations.increase, prepost2014_optimizations.decrease))
      or (prepost2014_optimizations.dimtype = 'siteadsize' and pf_daily_impressions_per_placement.siteadsize in (prepost2014_optimizations.increase, prepost2014_optimizations.decrease))
    )
    and pf_daily_impressions_per_placement.event_dt >= prepost2014_optimizations.pre_start_date_final
    and pf_daily_impressions_per_placement.event_dt <= prepost2014_optimizations.post_end_date_final
group by
  prepost2014_bhv_plcmt_level_cpm.recco_num
  , pf_daily_impressions_per_placement.event_dt
order by
  recco_num
  , event_dt
