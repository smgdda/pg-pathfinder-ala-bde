#creates log level ordinal table

import sys, os, subprocess

def main():
  new_data = optimizations()
  new_data.run()

class optimizations:

  def __init__(self):
    print ('optimizations class init')

  def run(self):
    print('in optimizations.run...')
    optimization_dict = self.get_optimization_list_from_db()
    for optimization in optimization_dict['records']:
        print('In process loop.  Processing optimization: ' + optimization[0] + '...')
        self.process_optimization_data(optimization[0])
    print('exiting optimizations.run...')

  def get_optimization_list_from_db(self):
    hql = "select * from prepost2014_optimizations;"
    optimization_dict_list_dict = self.getHiveQueryResults(hql)
    return optimization_dict_list_dict

  def process_optimization_data(self, recco_num):
    print ('starting process_optimization_data: ' + recco_num)

    print ('processing optimization: ', recco_num)
    print ('executing logs_ordinal hql==========>')
    #run the query
    process = subprocess.Popen(["hive", "-f", "STEP_005a_build-logs_ordinal-v2-3-2-3.sql", "-hiveconf", "recco_num=" + recco_num], stdout=subprocess.PIPE)
    data = process.communicate()[0]
    print ('completed running logs_ordinal sql==========>')

  # runs hive query specified and returns rows (as array of arrays) as well as status
  #thanks Ed
  def getHiveQueryResults(self, hql):
    return_data = {}
    return_data['rc'] = ""
    return_data['records'] = []
    
    #run the query
    print ("RUNNING HIVE QUERY, hql=" + hql)
    #process = subprocess.Popen(["hive", "-e", '"' + hql + '"'], stdout=subprocess.PIPE)
    cmd = '%HIVE_HOME%\\bin\\hive -e "' + hql + '"'
    process = subprocess.Popen(cmd, stdin=None, stdout=subprocess.PIPE, shell=True)
    data = process.communicate()[0]
    return_data['rc'] = process.returncode
    
    # return if the query ended with an error
    if process.returncode != 0:
      return return_data
    
    # retrieve the data, if any exists
    if data:
      #for row in data.split('\n'):
      for row in data.decode('utf-8').split('\n'):
        if row.strip():
          return_data['records'].append(row.split('\t'))

    return return_data

if __name__ == "__main__":
  main()
