﻿select
  pfcampaign
  , dimension_type
  , frequency
  , conv_likelihood
  , sequence_pf_score
  , (100 * sequence_pf_score / total_pf_score) as indexed_pf_score
  , conversions
  , sitecreative_1
  , sitecreative_2
  , sitecreative_3
  , sitecreative_4
  , sitecreative_5


from
(
  select 
  sum(sequence_pf_score) as total_pf_score
  from
  (
    select
    sum(
      case when pf_score_1 is not null then pf_score_1 else cast(0 as double) end +
      case when pf_score_2 is not null then pf_score_2 else cast(0 as double) end +
      case when pf_score_3 is not null then pf_score_3 else cast(0 as double) end +
      case when pf_score_4 is not null then pf_score_4 else cast(0 as double) end +
      case when pf_score_5 is not null then pf_score_5 else cast(0 as double) end
      ) / count(*) as sequence_pf_score

    from
    (
      select
      pfcampaign
      , logs_product.dfa_user_id as uid_1
      , logs_product.conv_user as conv_user
      , logs_product.sitecreative as sitecreative_1
      , case when pfscore_log is not null then pfscore_log else cast(0 as double) end as pf_score_1

      from
      logs_product
      where
      logs_product.pfcampaign = 'andros'
      and logs_product.freq = 1
    ) freq_1

    left outer join
    (
      select
      logs_product.dfa_user_id as uid_2
      , logs_product.sitecreative as sitecreative_2
      , case when pfscore_log is not null then pfscore_log else cast(0 as double) end as pf_score_2

      from
      logs_product
      where
      logs_product.pfcampaign = 'andros'
      and logs_product.freq = 2
    ) freq_2
    on
    freq_2.uid_2 = freq_1.uid_1

    left outer join
    (
      select
      logs_product.dfa_user_id as uid_3
      , logs_product.sitecreative as sitecreative_3
      , case when pfscore_log is not null then pfscore_log else cast(0 as double) end as pf_score_3

      from
      logs_product
      where
      logs_product.pfcampaign = 'andros'
      and logs_product.freq = 3
    ) freq_3
    on
    freq_3.uid_3 = freq_1.uid_1

    left outer join
    (
      select
      logs_product.dfa_user_id as uid_4
      , logs_product.sitecreative as sitecreative_4
      , case when pfscore_log is not null then pfscore_log else cast(0 as double) end as pf_score_4

      from
      logs_product
      where
      logs_product.pfcampaign = 'andros'
      and logs_product.freq = 4
    ) freq_4
    on
    freq_4.uid_4 = freq_1.uid_1

    left outer join
    (
      select
      logs_product.dfa_user_id as uid_5
      , logs_product.sitecreative as sitecreative_5
      , case when pfscore_log is not null then pfscore_log else cast(0 as double) end as pf_score_5

      from
      logs_product
      where
      logs_product.pfcampaign = 'andros'
      and logs_product.freq = 5
    ) freq_5
    on
    freq_5.uid_5 = freq_1.uid_1

    group by
    pfcampaign
    , sitecreative_1
    , sitecreative_2
    , sitecreative_3
    , sitecreative_4
    , sitecreative_5
  ) sequence_pf_scores
) total_sequence_pf_score

cross join
(
  select
  pfcampaign
  , 'sitecreative' as dimension_type
  , count(*) as frequency
  , sum(conv_user) / count(*) as conv_likelihood
  , sum(
    case when pf_score_1 is not null then pf_score_1 else cast(0 as double) end +
    case when pf_score_2 is not null then pf_score_2 else cast(0 as double) end +
    case when pf_score_3 is not null then pf_score_3 else cast(0 as double) end +
    case when pf_score_4 is not null then pf_score_4 else cast(0 as double) end +
    case when pf_score_5 is not null then pf_score_5 else cast(0 as double) end
    ) / count(*) as sequence_pf_score
  , sum(conv_user) as conversions
  , sitecreative_1
  , sitecreative_2
  , sitecreative_3
  , sitecreative_4
  , sitecreative_5

  from
  (
    select
    pfcampaign
    , logs_product.dfa_user_id as uid_1
    , logs_product.conv_user as conv_user
    , logs_product.sitecreative as sitecreative_1
    , case when pfscore_log is not null then pfscore_log else cast(0 as double) end as pf_score_1

    from
    logs_product
    where
    logs_product.pfcampaign = 'andros'
    and logs_product.freq = 1
  ) freq_1

  left outer join
  (
    select
    logs_product.dfa_user_id as uid_2
    , logs_product.sitecreative as sitecreative_2
    , case when pfscore_log is not null then pfscore_log else cast(0 as double) end as pf_score_2

    from
    logs_product
    where
    logs_product.pfcampaign = 'andros'
    and logs_product.freq = 2
  ) freq_2
  on
  freq_2.uid_2 = freq_1.uid_1

  left outer join
  (
    select
    logs_product.dfa_user_id as uid_3
    , logs_product.sitecreative as sitecreative_3
    , case when pfscore_log is not null then pfscore_log else cast(0 as double) end as pf_score_3

    from
    logs_product
    where
    logs_product.pfcampaign = 'andros'
    and logs_product.freq = 3
  ) freq_3
  on
  freq_3.uid_3 = freq_1.uid_1

  left outer join
  (
    select
    logs_product.dfa_user_id as uid_4
    , logs_product.sitecreative as sitecreative_4
    , case when pfscore_log is not null then pfscore_log else cast(0 as double) end as pf_score_4

    from
    logs_product
    where
    logs_product.pfcampaign = 'andros'
    and logs_product.freq = 4
  ) freq_4
  on
  freq_4.uid_4 = freq_1.uid_1

  left outer join
  (
    select
    logs_product.dfa_user_id as uid_5
    , logs_product.sitecreative as sitecreative_5
    , case when pfscore_log is not null then pfscore_log else cast(0 as double) end as pf_score_5

    from
    logs_product
    where
    logs_product.pfcampaign = 'andros'
    and logs_product.freq = 5
  ) freq_5
  on
  freq_5.uid_5 = freq_1.uid_1

  group by
  pfcampaign
  , sitecreative_1
  , sitecreative_2
  , sitecreative_3
  , sitecreative_4
  , sitecreative_5
) sequence_list

order by
frequency DESC
;
