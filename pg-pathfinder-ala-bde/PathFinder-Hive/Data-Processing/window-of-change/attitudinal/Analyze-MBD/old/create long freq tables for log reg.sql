-- ----- old ----- ;

create table lr_mbd_sitecreative_freq_woc201503 as
  select pfcampaign, dfa_user_id, sitecreative, count(*) as freq_count
  from (select pfcampaign, dfa_user_id, sitecreative from logs_product_mbd) imp
  group by pfcampaign, dfa_user_id, sitecreative ;

create table lr_mbd_sitetactic_freq_woc201503 as
  select pfcampaign, dfa_user_id, sitetactic, count(*) as freq_count
  from (select pfcampaign, dfa_user_id, sitetactic from logs_product_mbd) imp
  group by pfcampaign, dfa_user_id, sitetactic ;

create table lr_mbd_siteadsize_freq_woc201503 as
  select pfcampaign, dfa_user_id, siteadsize, count(*) as freq_count
  from (select pfcampaign, dfa_user_id, siteadsize from logs_product_mbd) imp
  group by pfcampaign, dfa_user_id, siteadsize ;


-- ----- new ----- ;

create table lr_mbd_sitecreative_freq_woc201504 as
  select pfcampaign, dfa_user_id, sitecreative, sum(freq_count) as freq_count
  from mbd_lr_sitecreative_freq
  group by pfcampaign, dfa_user_id, sitecreative ;

create table lr_mbd_sitetactic_freq_woc201504 as
  select pfcampaign, dfa_user_id, sitetactic, sum(freq_count) as freq_count
  from mbd_lr_sitetactic_freq
  group by pfcampaign, dfa_user_id, sitetactic ;

create table lr_mbd_siteadsize_freq_woc201504 as
  select pfcampaign, dfa_user_id, siteadsize, sum(freq_count) as freq_count
  from mbd_lr_siteadsize_freq
  group by pfcampaign, dfa_user_id, siteadsize ;
