rm(list=ls())
# ---------- load_mbd_modeldata.R ----------
# Tomonori Ishikawa
# tomonori.ishikawa@smvgroup.com
#
# 2015-03-31, v6.2: Apr WOC
# 2015-03-06, v6.1: Mar WOC
#   & replaced with and in brand
#   analysis window start date parameterized
# 2015-01-23, v6: 90-day window filtering
# 2015-01-22, v5: Feb WOC
# 2014-12-17, v4: Validates model dims against PF_AGG
# 2014-12-16, v3: Loads DFA user ID as string
# 2014-11-21, v2: Loads long data




library(reshape2)

# ----- options -----
# this preserves dfa user id in integer notation instead of scientific notation
#options('scipen'=100)


# ----- strings -----
# usually saturdays
analysis_start_dt = '2015-01-16'
analysis_end_dt =   '2015-03-20'

woc = '2015-04'


# --- adjust strings ---
#cutoff = '20150320'
cutoff = gsub('-','',analysis_end_dt)
wocfile = sub('-','',woc)
wocfolder = paste('WOC',woc)
freqfilesuffix <- paste('woc', wocfile, sep='')


# ----- paths -----
mbd_root = 'D:\\Users\\tishikaw\\Documents\\PGPF 2.5 Acxiom BDE\\Analyze MBD'
local_root = 'D:\\Users\\tishikaw\\Documents\\PGPF 2.5 Acxiom BDE\\Process MBD\\MBD Local'
data_dir = paste(mbd_root, wocfolder, sep='\\')


#brand = 'tide'
#camp = 'variant'
#mbd_survey_id = '1223572'
#mbd_survey_ids <- c(1223572,1224252,1226469,1230813,1248111,1248221,1265294,1269714)


dimtypes = c('sitetactic','sitecreative','siteadsize','site','creative','adsize','tactic')
#nondims = c('att_response','bhv_response','exposed_control','dfa_user_id','dl_member_id')




# ----- load mbd list -----
# use for brand and campaign labels
mbdlist_fpath = paste(local_root,'MBD Survey List for BH1415.tsv',sep='\\')
mbdlist_df = read.delim(mbdlist_fpath,na.strings='NULL',stringsAsFactors=F)
mbdlist_df = subset(mbdlist_df, substr(surveyid,1,1)!='#')
rownames(mbdlist_df) <- mbdlist_df$surveyid
#mbdlist_df$campaign = tolower(gsub("[ /]",'-',mbdlist_df$campaign))
mbdlist_df$campaign = tolower(gsub("[ /-]",'',mbdlist_df$campaign))
mbdlist_df$brand = tolower(gsub("&",'and',mbdlist_df$brand))

# create list of mbd survey ids
mbd_survey_ids <- mbdlist_df$surveyid




# ----- load pf_agg -----
#pfagg_csvfile = 'pf_agg_final_no_exe_type_2015_01_woc_w_campaign.csv'
pfagg_csvfile = paste('pf_agg_final_no_exe_type_', sub('-','_',woc),
  '_woc_w_campaign.csv', sep='')
pfagg_fpath = paste(data_dir, pfagg_csvfile, sep='\\')
pf_agg_all <- read.csv(pfagg_fpath, stringsAsFactors=F)

# clean dims
pf_agg_ratetype <- within(pf_agg_all, { dim <- gsub("[$/,. +-]",'',dim) 
  dim <- gsub("^([0-9])", "s\\1", dim) })
#print('pf_agg_ratetype')
#print(pf_agg_ratetype[1,])
#print(names(pf_agg_ratetype))

# collapse/aggregate over rate types
pfagg_byvarlist = c('brand','pfcampaign','dimtype','dim')
pfagg_sumvarlist = c('imps','cost','pfscore',
  'scaled_imps','scaled_cost','scaled_pfscore',
  'impact','efficiency')
pf_agg = aggregate(pf_agg_ratetype[,pfagg_sumvarlist],
  by=pf_agg_ratetype[,pfagg_byvarlist],
  'sum', na.rm=TRUE)
#print('pf_agg')
#print(pf_agg[1,])
#print(names(pf_agg))



# ---------- make modeling dataset ----------
make_modeldata <- function (brand, camp, mbd_survey_id, dimtype) {
  header_msg <- sprintf("\n----- %s %s %s %s -----", mbd_survey_id, brand, camp, dimtype)
  message(header_msg)
  print(header_msg)

  # ----- load data -----
  
  # --- pf_agg list of dimensions for validating ---
  pfagg_dimlist <- pf_agg[(pf_agg$pfcampaign==camp & pf_agg$dimtype==dimtype),
    'dim']
  nvar_pfaggdims <- length(pfagg_dimlist)
  if (nvar_pfaggdims==0) {
    print('ERROR: No dimensions found in PF_AGG.')
  }

  # --- dep vars ---
  # like 1223572_all_woc201412.tsv
  tsvfile = paste(mbd_survey_id, '_all_woc', wocfile, '.tsv', sep='')
  depvars_fpath = paste(local_root,tsvfile,sep='\\')
  depvars_in_df = read.delim(depvars_fpath,na.strings='NULL',stringsAsFactors=F,
    colClasses=c(dfa_user_id='character'))

  depvars_df <- within(subset(depvars_in_df,
    depvars_in_df$survey_dt >= analysis_start_dt),
    { } )
  n_depvars = dim(depvars_df)[1]
  
  # --- long rhs vars ---
  # like lr_mbd_sitetactic_freq_all20.csv
  csvfile = paste('lr_mbd_',dimtype,'_freq_',freqfilesuffix,'.csv',sep='')
  longrhs_fpath = paste(mbd_root,csvfile,sep='\\')
  longrhs_df <- read.csv(longrhs_fpath,na.strings='NULL',stringsAsFactors=F,
    colClasses=c(dfa_user_id='character'))

  longrhs_df$freq_count <- as.numeric(longrhs_df$freq_count)
  longrhs_dimlist <- unique(longrhs_df[,dimtype])
    
  # ----- process long rhs vars to wide rhs vars -----

  # --- clean dims ---
  # remove commas, periods, spaces and dashes
  # also remove pluses and slashes
  names(longrhs_df) <- sub(dimtype,'dims', names(longrhs_df))
 
  pretrans <- within(subset(longrhs_df, pfcampaign == camp),
    { dims <- gsub("[$/,. +-]", '', dims)
      dims <- gsub("^([0-9])", "s\\1", dims) })
  pretrans_dimlist <- unique(pretrans[,'dims'])

  # --- validated dims for modeling ---
  # pretrans may have dims not appearing in pf_agg
  # converse is also possible but rarer--arises from small samples
  model_dimlist <- intersect(pretrans_dimlist, pfagg_dimlist)

  # --- print status of dims ---
#  print("All dims appearing in long RHS freq file:")
#  print(longrhs_dimlist)

  print("All dims appearing in DFA impressions:")
  print(pretrans_dimlist)
#  print("Valid dims that DO appear in pf_agg:")
#  print(pfagg_dimlist)
  print("Valid dims that DO appear in both impressions and pf_agg:")
  print(model_dimlist)
#  print("Dropping dims that DON'T appear in pf_agg:")
#  print(setdiff(pretrans_dimlist,pfagg_dimlist))
   
  # --- cast long to wide ---
  # dcast to return dataframes
  posttrans <- dcast(pretrans, dfa_user_id ~ dims, fun.aggregate=sum, value.var = 'freq_count')
  n_post = dim(posttrans)[1]

  # downselect to only pf_agg validated vars
  posttrans <- posttrans[,c('dfa_user_id',model_dimlist)]

  # ----- merge dep vars with wide rhs vars -----
  mergedata_df <- merge(depvars_df, posttrans, by='dfa_user_id', all=T)
  n_merge = dim(mergedata_df)[1]

  # --- create list of dimensions ---
  dims = setdiff(colnames(posttrans),'dfa_user_id')
  
  # ----- print warning when only one dim -----
  if (length(dims)==1) {
    onedim_msg <- paste('WARNING! NO OPTIMIZATION OPPORTUNITIES--ONLY ONE ',
      toupper(dimtype), ':\n  ', dims, sep='')
    message(onedim_msg)
  }
  
  # --- control ---
  # valid controls have exposed_control == 2 and non-null (dl) member_id
  valid_control = subset(mergedata_df,exposed_control==2 & !is.na(member_id))
  # set dimension values to zero for controls (they are now missing)
  valid_control[,dims] = 0
  n_ctrl = dim(valid_control)[1]
  
  # --- exposed ---
  # valid exposed have exposed_control == 1 and non-null dfa_user_id
  valid_exposed = subset(mergedata_df,exposed_control==1 & !is.na(dfa_user_id))
  n_expo_gross = dim(valid_exposed)[1]
  
  # calculate sum of NAs per obs
  # apply without as.matrix below fails when dims has
  #   only one column because in that case the slice
  #   becomes a list, doesn't stay as a dataframe or matrix
  expo_nasum <- apply(is.na(as.matrix(valid_exposed[,dims])),1,sum)
  expo_allna <- subset(valid_exposed, (expo_nasum==length(dims)))
  n_expo_allna = dim(expo_allna)[1]
  
  # remove exposed obs with all NA exposures -- means no DFA user ID match
  model_exposed = subset(valid_exposed, (expo_nasum < length(dims)))
  
  # --- stack model exposed and valid control to create model data ---
  valid_modeldata = rbind(model_exposed,valid_control)
  n_modeldata = dim(valid_modeldata)[1]
    
  # --- write valid modeling data to appropriately named dataframe --- 
  modeldata_str = paste('mbd',brand,camp,dimtype,cutoff,sep='_')
  assign(modeldata_str,valid_modeldata,envir=.GlobalEnv)
  
  # ----- cleanup -----
  rm(depvars_df,depvars_fpath,longrhs_df,longrhs_fpath,
    mergedata_df,pretrans,posttrans,
    valid_control,valid_exposed,valid_modeldata)

  # ----- save dataframe to Rdata file -----
  rdatafile = paste(modeldata_str,'.Rdata',sep='')
  rdata_fpath = paste(mbd_root,wocfolder,rdatafile,sep='\\')
  message(modeldata_str)
  save(list=c(modeldata_str),file=rdata_fpath)
  
  # ----- print counts -----
  pct_allna = 100*(n_expo_allna/n_expo_gross)
  n_expo_notna = n_expo_gross - n_expo_allna
  n_total = n_ctrl + n_expo_notna
  message(sprintf("%d dep var rows
%d wide rhs var rows
%d merged data rows
%d modeldata rows, of which
  %d are valid controls
    %d are gross exposeds
      %d (%4.1f%%) of these have all NA values, leaving
  %d net exposeds
%d total rows available for modeling", n_depvars, n_post, n_merge,
    n_modeldata, n_ctrl, n_expo_gross, n_expo_allna, pct_allna,
    n_expo_notna, n_total))  
}  




# ----- logging functions -----
open_logfile <- function () {
  now <- Sys.time()
  now_str <- as.POSIXlt(now)
  nowfile_str <- strftime(now, format="%Y%m%d-%I%M%p")
  today_str <- strftime(now, format="%Y%m%d")
#  sinkfile_str <- paste('load_mbd_modeldata_', nowfile_str, '.log', sep='')
  sinkfile_str <- paste('load_mbd_modeldata_', today_str, '.log', sep='')
  sinkfile_fpath = paste(mbd_root, wocfolder, sinkfile_str,sep='\\')
  sinkfile = file(sinkfile_fpath, open='wt')
  sink(sinkfile)
  sink(type='output')
  sink(sinkfile, type='message')
  start_msg <- paste('---------- ', sinkfile_str, ' ----------\n',
    'Running on ', now_str, sep='')
  message(start_msg)
}

close_logfile <- function () {
  sink(type='message')
  closeAllConnections()
#sink(file=NULL, type='output')
}




# ----- create datasets for all campaigns in a grand loop -----

# --- adjust campaign list ---
# skip megabrand -- started recruiting end of feb
#skipsurveys <- c('1383397')
#mbd_survey_ids = setdiff(mbd_survey_ids, skipsurveys)

# --- adjust dimtypes ---
#skiptypes <- c('creative')
#dimtypes = setdiff(dimtypes,skiptypes)



open_logfile()

for (mbd_survey_id in mbd_survey_ids) {
for (dimtype in dimtypes) {
  msi = as.character(mbd_survey_id)
  br = tolower(mbdlist_df[msi,'brand'])
  camp = tolower(mbdlist_df[msi,'campaign'])
#  make_modeldata (br,camp,msi,dimtype)
}
}

make_modeldata ('clearblue','weeks estimator','1229341',dimtype)

close_logfile()


message('Done.')
