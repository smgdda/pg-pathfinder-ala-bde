#!python



# ---------- reprocess_freqs ----------
# Tomonori Ishikawa
# tomonori.ishikawa@smvgroup.com
#
# Reprocesses impressions frequency files.
#   siteadsize -> adsize
#   sitecreative -> creative
#   sitetactic -> site
#   sitetactic -> tactic
#
# 2015-03-31, v4: updated for Apr WOC. Orrin added DLX ID field
# 2015-03-10, v3.2: updated for Mar WOC 
# 2015-03-06, v3.1: updated for Feb WOC 
# 2015-01-27, v3: fix for null creative bug
# 2015-01-23, v2.1: updated for Feb WOC 
# 2014-12-16, v2: 
# 2014-11-22, v1: 

import os, shutil, datetime

# ----- window of change parameters -----
woc = 'WOC201504'
# cutoff date
cutoff_dt = '2015-03-20'
# freq file suffix
freqfile_suffix = 'woc201504'

# ----- old 1314 mbd survey ids -----
old1314_surveyids = ['1049159','1049165','1049842','1049843','1049844','1103473','1127179','1138706']

# ----- create datetime stamp -----
# need to import datetime
# current datetime stamp like "Sun 2002-02-02 06:30PM"
now_str = datetime.datetime.now().strftime('%a %Y-%m-%d %I:%M%p %Z')
# current datetime stamp like "20020202_0630PM", useful for filenames
nowfile_str = datetime.datetime.now().strftime('%Y%m%d_%I%M%p')
# current date stamp like "20020202"
today_str = datetime.datetime.now().strftime('%Y%m%d')

# ----- paths -----
mbd_root = r'D:\Users\tishikaw\Documents\PGPF 2.5 Acxiom BDE\Analyze MBD'
oldfreq_dir = mbd_root
newfreq_dir = mbd_root


newfreq = dict(siteadsize='adsize',
               sitecreative='creative',
               sitetactic='site')


def write_newfreq (oldfreq_str, newfreq_dict):
  print ('Reprocessing %s freqs into %s freqs' % (oldfreq_str, newfreq_dict[oldfreq_str]))
  
  # ----- open both freq files -----
  newfreq_fpath = newfreq_dir + r'\lr_mbd_' + newfreq_dict[oldfreq_str] + '_freq_' + freqfile_suffix + '.csv'
  newfreq = open(newfreq_fpath, 'w')
  
  oldfreq_fpath = oldfreq_dir + r'\lr_mbd_' + oldfreq_str + '_freq_' + freqfile_suffix + '.csv'
  oldfreq = open(oldfreq_fpath, 'r')
  
  # ----- process old freq file -----
  for of in oldfreq:
    of = of.replace(', ','')    ## fixes f-ing msft
    
    # --- split old on comma ---
    of_line = of.split(',')
    if len(of_line) == 4:
      # parse each line
      pfcampaign = of_line[0]
      dfa_user_id = of_line[1]
      old_sitedim = of_line[2]
      freq = of_line[3]
      
      # write new header from old header
      if pfcampaign.startswith('"pfcamp'):
        newfreq_out = '%s,%s,"%s",%s' %(pfcampaign,dfa_user_id,newfreq_dict[oldfreq_str],freq)
        newfreq.write(newfreq_out)
        continue

      # process old data lines -- splitting old_sitedim on underscore
      else:  
        od_tok = old_sitedim.split('_')
        num_tok = len(od_tok)

        # site is first token
        # even if malformed with no underscores, first token will always exist
        site = '%s"' % od_tok[0]
        
        # tactic has two tokens: execution type and targeting type
        # creative and adsize have one token each
        if newfreq_dict[oldfreq_str] == 'tactic':
          tactic = '"%s_%s' % (od_tok[1],od_tok[2])
        else:
          # shizen has a null creative that's causing problems
          try:
            newdim = '"%s' % od_tok[1]
          except:
            print (len(od_tok))
            print (of)
  
        # write
        if newfreq_dict[oldfreq_str] == 'site':
          newfreq_out = ','.join([pfcampaign,dfa_user_id,site,freq])
        elif newfreq_dict[oldfreq_str] == 'tactic':
          newfreq_out = ','.join([pfcampaign,dfa_user_id,tactic,freq])
        else:
          newfreq_out = ','.join([pfcampaign,dfa_user_id,newdim,freq])
        
        newfreq.write(newfreq_out)
      # f-ing msft
    else:
      print (of, 'has more than 3 commas')
  oldfreq.close()
  newfreq.close()




# ----- execute reprocessing -----
for oldfreq in newfreq.keys():
  write_newfreq (oldfreq, newfreq)
# to avoid duplicate keys in newfreq, must handle sitetactic to tactic separately
write_newfreq ('sitetactic',dict(sitetactic='tactic'))




print ('Done.')
