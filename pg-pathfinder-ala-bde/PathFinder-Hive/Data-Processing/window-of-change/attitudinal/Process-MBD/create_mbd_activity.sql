-- create_mbd_activity in Beeswax -- ;

drop table if exists mbd_activity_20150331;

create table mbd_activity_20150331 as select
  dfa_user_id, other_data, event_dts
  from dfa_activity
  where lower(activity_type) == 'vindid' and lower(activity_sub_type) == 'us_13324';

drop table if exists mbd_dfa_matchtable_20150331;

create table mbd_dfa_matchtable_20150331 as select
  split(split(other_data,';')[0], '=')[1] as dl_member_id,
  split(split(other_data,';')[1], '/')[4] as mbd_survey_id,
  dfa_user_id,
  split(event_dts,' ')[0] as event_dt
  from dfa_activity
  where lower(activity_type) == 'vindid' and lower(activity_sub_type) == 'us_13324';
