#creates log level ordinal table for mbd users

import sys, os, subprocess

def main():
  new_data = mbd_imp_builder()
  new_data.run()

class mbd_imp_builder:

  def __init__(self):
    print ('mbd_imp_builder class init')

  def run(self):
    print('in mbd_imp_builder.run...')
    campaign_dict = self.get_campaign_list_from_db()
    for campaign in campaign_dict['records']:
        print('In process loop.  Processing campaign: ' + campaign[0] + '...')
        self.insert_campaign_data(campaign[0])
    print('exiting mbd_imp_builder.run...')

  def get_campaign_list_from_db(self):
    hql = "select distinct lower(regexp_replace(pfcampaign, '[^a-zA-Z0-9\\_]', '')) as pfcampaign from pfcampaign_list where date_dropped = '';"
    campaign_list_dict = self.getHiveQueryResults(hql)
    return campaign_list_dict

  def insert_campaign_data(self, pfcampaign_name):
    print ('starting insert_campaign_data: ' + pfcampaign_name)

    print ('inserting campaign: ', pfcampaign_name)
    print ('executing mbd_matched_by_pfcampaign.sql ==========>')
    #run the query
    process = subprocess.Popen(["hive", "-f", "mbd_matched_by_pfcampaign.sql", "-hiveconf", "pf_campaign_name=" + pfcampaign_name], stdout=subprocess.PIPE)
    data = process.communicate()[0]
    print ('completed running mbd_matched_by_pfcampaign.sql ==========>')

    return

  # runs hive query specified and returns rows (as array of arrays) as well as status
  def getHiveQueryResults(self, hql):
    return_data = {}
    return_data['rc'] = ""
    return_data['records'] = []
    
    #run the query
    print ("RUNNING HIVE QUERY, hql=" + hql)
    process = subprocess.Popen(["hive", "-e", '"' + hql + '"'], stdout=subprocess.PIPE)
    data = process.communicate()[0]
    return_data['rc'] = process.returncode
    
    # return if the query ended with an error
    if process.returncode != 0:
      return return_data
    
    # retrieve the data, if any exists
    if data:
      for row in data.split('\n'):
        if row.strip():
          return_data['records'].append(row.split('\t'))

    return return_data

if __name__ == "__main__":
  main()
