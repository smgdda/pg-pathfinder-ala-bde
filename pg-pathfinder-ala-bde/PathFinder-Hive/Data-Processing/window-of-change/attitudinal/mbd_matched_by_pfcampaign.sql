SET hive.exec.compress.intermediate=true;
SET hive.exec.compress.output=true;
SET mapred.output.compression.type=BLOCK;
SET mapred.output.compression.codec=org.apache.hadoop.io.compress.SnappyCodec;
SET hive.optimize.bucketmapjoin=true;
SET hive.auto.convert.join=true;
SET hive.exec.dynamic.partition=true; 
SET hive.exec.dynamic.partition.mode=nonstrict; 

INSERT INTO TABLE logs_product_mbd PARTITION (pfcampaign)
select i.*
from
(
  select distinct trim(dfa_user_id) as dfa_user_id
  from pf_mbd_survey_users
) mbd_dfa

join (select * from logs_product where pfcampaign = '${hiveconf:pf_campaign_name}') i
on (i.dfa_user_id = mbd_dfa.dfa_user_id)
;
