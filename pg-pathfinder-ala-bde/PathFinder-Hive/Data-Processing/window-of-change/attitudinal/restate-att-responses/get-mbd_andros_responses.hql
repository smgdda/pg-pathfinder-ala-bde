﻿SET hive.exec.compress.intermediate=TRUE;
SET hive.exec.compress.output=TRUE;
SET mapred.output.compression.type=BLOCK;
SET mapred.output.compression.codec=org.apache.hadoop.io.compress.SnappyCodec;

DROP TABLE IF EXISTS att_responses_andros;

CREATE TABLE att_responses_andros
ROW FORMAT SERDE 
  'org.apache.hadoop.hive.serde2.columnar.ColumnarSerDe' 
STORED AS INPUTFORMAT 
  'org.apache.hadoop.hive.ql.io.RCFileInputFormat' 
OUTPUTFORMAT 
  'org.apache.hadoop.hive.ql.io.RCFileOutputFormat'
as
select
survey_users.respondent_id
, pf_mbd_survey_users.dfa_user_id
, to_date(age_q.starttime) as survey_dt
, age_q.user_response_text as age
, exposed_control.user_response_text as exposed_control
, dseen.user_response_text as dseen
, case when dseen.user_response_text is null then 0 else 1 end as derived_dseen
, dheard.user_response_text as dheard
, case when dheard.user_response_text is null then 0 else 1 end as derived_dheard
, dpi.user_response_text as dpi
, case when dpi.user_response_text is null then 0 else 1 end as derived_dpi
, case when dheard.user_response_text is null then 0 else 1 end as att_response

from
(select distinct respondent_id from attitudinal_survey_responses where question_id in (9380380,9380426,9380429,9380430,9380432)) survey_users

join
(select distinct dl_member_id, dfa_user_id, mbd_survey_id from pf_mbd_survey_users) pf_mbd_survey_users
on 
  pf_mbd_survey_users.mbd_survey_id = 263047
  and
  pf_mbd_survey_users.dl_member_id = survey_users.respondent_id

left outer join
(
  select *
  from attitudinal_survey_responses
  where attitudinal_survey_responses.question_id = 9380380
) exposed_control
on exposed_control.respondent_id = survey_users.respondent_id

left outer join
(
  select *
  from attitudinal_survey_responses
  where attitudinal_survey_responses.question_id = 9380426
) age_q
on age_q.respondent_id = survey_users.respondent_id

left outer join
(
  select *
  from attitudinal_survey_responses
  where attitudinal_survey_responses.question_id = 9380430
  and trim(user_response_text) = 'I Have Seen Head  Shoulders Instant Relief'
) dseen
on dseen.respondent_id = survey_users.respondent_id

left outer join
(
  select *
  from attitudinal_survey_responses
  where attitudinal_survey_responses.question_id = 9380429
  and trim(user_response_text) = 'I Have Heard Of Head  Shoulders Instant Relief'
) dheard
on dheard.respondent_id = survey_users.respondent_id

left outer join
(
  select *
  from attitudinal_survey_responses
  where attitudinal_survey_responses.question_id = 9380432
  and trim(user_response_text) in ('Very Likely Head  Shoulders Instant Relief','Somewhat Likely Head  Shoulders Instant Relief')
) dpi
on dpi.respondent_id = survey_users.respondent_id
;
