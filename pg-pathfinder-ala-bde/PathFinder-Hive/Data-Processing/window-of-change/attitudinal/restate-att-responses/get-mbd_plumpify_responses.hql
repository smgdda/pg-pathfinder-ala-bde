﻿SET hive.exec.compress.intermediate=TRUE;
SET hive.exec.compress.output=TRUE;
SET mapred.output.compression.type=BLOCK;
SET mapred.output.compression.codec=org.apache.hadoop.io.compress.SnappyCodec;

DROP TABLE IF EXISTS att_responses_plumpify;

CREATE TABLE att_responses_plumpify
ROW FORMAT SERDE 
  'org.apache.hadoop.hive.serde2.columnar.ColumnarSerDe' 
STORED AS INPUTFORMAT 
  'org.apache.hadoop.hive.ql.io.RCFileInputFormat' 
OUTPUTFORMAT 
  'org.apache.hadoop.hive.ql.io.RCFileOutputFormat'
as
select
s_plumpfiy_users.respondent_id
, pf_mbd_survey_users.dfa_user_id
, to_date(plumpify_age_q.starttime) as survey_dt
, plumpify_age_q.user_response_text as age
, plumpify_exposed_control.user_response_text as exposed_control
, plumpify_dheard.user_response_text as dheard
, case when plumpify_dheard.user_response_text is null then 0 else 1 end as derived_dheard
, plumpify_dpi.user_response_text as dpi
, case when plumpify_dpi.user_response_text is null then 0 else 1 end as derived_dpi
, case when plumpify_dheard.user_response_text is null then 0 else 1 end as att_response

from
(select distinct respondent_id from attitudinal_survey_responses where question_id in (10550425,10549760,10550438,10550434)) s_plumpfiy_users

join
(select distinct dl_member_id, dfa_user_id, mbd_survey_id from pf_mbd_survey_users) pf_mbd_survey_users
on 
  pf_mbd_survey_users.mbd_survey_id = 270820
  and
  pf_mbd_survey_users.dl_member_id = s_plumpfiy_users.respondent_id

left outer join
(
  select *
  from attitudinal_survey_responses
  where attitudinal_survey_responses.question_id = 10550425
) plumpify_age_q
on plumpify_age_q.respondent_id = s_plumpfiy_users.respondent_id

left outer join
(
  select *
  from attitudinal_survey_responses
  where attitudinal_survey_responses.question_id = 10549760
) plumpify_exposed_control
on plumpify_exposed_control.respondent_id = s_plumpfiy_users.respondent_id

left outer join
(
  select *
  from attitudinal_survey_responses
  where attitudinal_survey_responses.question_id = 10550438
  and trim(user_response_text) in ('I Have Heard Of COVERGIRL Plumpify Mascara')
) plumpify_dheard
on plumpify_dheard.respondent_id = s_plumpfiy_users.respondent_id

left outer join
(
  select *
  from attitudinal_survey_responses
  where attitudinal_survey_responses.question_id = 10550434
  and trim(user_response_text) in ('Very Likely COVERGIRL Plumpify Mascara','Somewhat Likely COVERGIRL Plumpify Mascara')
) plumpify_dpi
on plumpify_dpi.respondent_id = s_plumpfiy_users.respondent_id
;
