﻿SET hive.exec.compress.intermediate=TRUE;
SET hive.exec.compress.output=TRUE;
SET mapred.output.compression.type=BLOCK;
SET mapred.output.compression.codec=org.apache.hadoop.io.compress.SnappyCodec;

DROP TABLE IF EXISTS att_responses_secret;

CREATE TABLE att_responses_secret
ROW FORMAT SERDE 
  'org.apache.hadoop.hive.serde2.columnar.ColumnarSerDe' 
STORED AS INPUTFORMAT 
  'org.apache.hadoop.hive.ql.io.RCFileInputFormat' 
OUTPUTFORMAT 
  'org.apache.hadoop.hive.ql.io.RCFileOutputFormat'
as
select
s_secret_users.respondent_id
, pf_mbd_survey_users.dfa_user_id
, to_date(secret_age_q.starttime) as survey_dt
, secret_age_q.user_response_text as age
, secret_exposed_control.user_response_text as exposed_control
, secret_dseen.user_response_text as dseen
, case when secret_dseen.user_response_text is null then 0 else 1 end as derived_dseen
, secret_dheard.user_response_text as dheard
, case when secret_dheard.user_response_text is null then 0 else 1 end as derived_dheard
, secret_dcons.user_response_text as dcons
, case when secret_dcons.user_response_text is null then 0 else 1 end as derived_dcons
, secret_dpi.user_response_text as dpi
, case when secret_dpi.user_response_text is null then 0 else 1 end as derived_dpi
, case when secret_dcons.user_response_text is null then 0 else 1 end as att_response

from
(select distinct respondent_id from attitudinal_survey_responses where question_id in (8812076,8811928,8812080,8812079,8812082,9240649)) s_secret_users

join
(select distinct dl_member_id, dfa_user_id, mbd_survey_id from pf_mbd_survey_users) pf_mbd_survey_users
on 
  pf_mbd_survey_users.mbd_survey_id = 261279
  and
  pf_mbd_survey_users.dl_member_id = s_secret_users.respondent_id

left outer join
(
  select *
  from attitudinal_survey_responses
  where attitudinal_survey_responses.question_id = 8812076
) secret_age_q
on secret_age_q.respondent_id = s_secret_users.respondent_id

left outer join
(
  select *
  from attitudinal_survey_responses
  where attitudinal_survey_responses.question_id = 8811928
) secret_exposed_control
on secret_exposed_control.respondent_id = s_secret_users.respondent_id

left outer join
(
  select *
  from attitudinal_survey_responses
  where attitudinal_survey_responses.question_id = 8812080
  and trim(user_response_text) in ('I Have Seen Secret Clinical Strength')
) secret_dseen
on secret_dseen.respondent_id = s_secret_users.respondent_id

left outer join
(
  select *
  from attitudinal_survey_responses
  where attitudinal_survey_responses.question_id = 8812079
  and trim(user_response_text) in ('I Have Heard Of Secret Clinical Strength')
) secret_dheard
on secret_dheard.respondent_id = s_secret_users.respondent_id

left outer join
(
  select *
  from attitudinal_survey_responses
  where attitudinal_survey_responses.question_id = 8812082
  and trim(user_response_text) in ('Very Likely Secret Clinical Strength','Somewhat Likely Secret Clinical Strength')
) secret_dpi
on secret_dpi.respondent_id = s_secret_users.respondent_id

left outer join
(
  select *
  from attitudinal_survey_responses
  where attitudinal_survey_responses.question_id = 9240649
  and trim(user_response_text) in ('Very Likely Secret Clinical Strength','Somewhat Likely Secret Clinical Strength')
) secret_dcons
on secret_dcons.respondent_id = s_secret_users.respondent_id
;
