﻿SET hive.exec.compress.intermediate=TRUE;
SET hive.exec.compress.output=TRUE;
SET mapred.output.compression.type=BLOCK;
SET mapred.output.compression.codec=org.apache.hadoop.io.compress.SnappyCodec;

DROP TABLE IF EXISTS att_responses_supersizer;

CREATE TABLE att_responses_supersizer
ROW FORMAT SERDE 
  'org.apache.hadoop.hive.serde2.columnar.ColumnarSerDe' 
STORED AS INPUTFORMAT 
  'org.apache.hadoop.hive.ql.io.RCFileInputFormat' 
OUTPUTFORMAT 
  'org.apache.hadoop.hive.ql.io.RCFileOutputFormat'
as
select
survey_users.respondent_id
, pf_mbd_survey_users.dfa_user_id
, to_date(age_q.starttime) as survey_dt
, age_q.user_response_text as age
, exposed_control.user_response_text as exposed_control
, dseen.user_response_text as dseen
, case when dseen.user_response_text is null then 0 else 1 end as derived_dseen
, dheard.user_response_text as dheard
, case when dheard.user_response_text is null then 0 else 1 end as derived_dheard
, dcons.user_response_text as dcons
, case when dcons.user_response_text is null then 0 else 1 end as derived_dcons
, case when dheard.user_response_text is null then 0 else 1 end as att_response

from
(select distinct respondent_id from attitudinal_survey_responses where question_id in (9303085,9303305,9303308,9303310,9303311)) survey_users

join
(select distinct dl_member_id, dfa_user_id, mbd_survey_id from pf_mbd_survey_users) pf_mbd_survey_users
on 
  pf_mbd_survey_users.mbd_survey_id = 262432
  and
  pf_mbd_survey_users.dl_member_id = survey_users.respondent_id

left outer join
(
  select *
  from attitudinal_survey_responses
  where attitudinal_survey_responses.question_id = 9303085
) exposed_control
on exposed_control.respondent_id = survey_users.respondent_id

left outer join
(
  select *
  from attitudinal_survey_responses
  where attitudinal_survey_responses.question_id = 9303305
) age_q
on age_q.respondent_id = survey_users.respondent_id

left outer join
(
  select *
  from attitudinal_survey_responses
  where attitudinal_survey_responses.question_id = 9303308
  and trim(user_response_text) = 'I Have Seen COVERGIRL'
) dseen
on dseen.respondent_id = survey_users.respondent_id

left outer join
(
  select *
  from attitudinal_survey_responses
  where attitudinal_survey_responses.question_id = 9303310
  and trim(user_response_text) = 'I Have Heard Of COVERGIRL Super Sizer Mascara'
) dheard
on dheard.respondent_id = survey_users.respondent_id

left outer join
(
  select *
  from attitudinal_survey_responses
  where attitudinal_survey_responses.question_id = 9303311
  and trim(user_response_text) in ('Very Likely COVERGIRL Super Sizer Mascara','Somewhat Likely COVERGIRL Super Sizer Mascara')
) dcons
on dcons.respondent_id = survey_users.respondent_id
;
