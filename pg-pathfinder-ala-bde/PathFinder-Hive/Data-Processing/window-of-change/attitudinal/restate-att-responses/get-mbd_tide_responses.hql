﻿SET hive.exec.compress.intermediate=TRUE;
SET hive.exec.compress.output=TRUE;
SET mapred.output.compression.type=BLOCK;
SET mapred.output.compression.codec=org.apache.hadoop.io.compress.SnappyCodec;

DROP TABLE IF EXISTS att_responses_tide;

CREATE TABLE att_responses_tide
ROW FORMAT SERDE 
  'org.apache.hadoop.hive.serde2.columnar.ColumnarSerDe' 
STORED AS INPUTFORMAT 
  'org.apache.hadoop.hive.ql.io.RCFileInputFormat' 
OUTPUTFORMAT 
  'org.apache.hadoop.hive.ql.io.RCFileOutputFormat'
as
select
s_tide_users.respondent_id
, pf_mbd_survey_users.dfa_user_id
, to_date(tide_age_q.starttime) as survey_dt
, tide_age_q.user_response_text as age
, tide_exposed_control.user_response_text as exposed_control
, tide_dseen.user_response_text as dseen
, case when tide_dseen.user_response_text is null then 0 else 1 end as derived_dseen
, tide_dheard.user_response_text as dheard
, case when tide_dheard.user_response_text is null then 0 else 1 end as derived_dheard
, tide_dpi.user_response_text as dpi
, case when tide_dpi.user_response_text is null then 0 else 1 end as derived_dpi
, case when tide_dpi.user_response_text is null then 0 else 1 end as att_response

from
(select distinct respondent_id from attitudinal_survey_responses where question_id in (8860303,8860786,8860789,8860795,8861001)) s_tide_users

join
(select distinct dl_member_id, dfa_user_id, mbd_survey_id from pf_mbd_survey_users) pf_mbd_survey_users
on 
  pf_mbd_survey_users.mbd_survey_id = 261643
  and
  pf_mbd_survey_users.dl_member_id = s_tide_users.respondent_id

left outer join
(
  select *
  from attitudinal_survey_responses
  where attitudinal_survey_responses.question_id = 8860786
) tide_age_q
on tide_age_q.respondent_id = s_tide_users.respondent_id

left outer join
(
  select *
  from attitudinal_survey_responses
  where attitudinal_survey_responses.question_id = 8860303
) tide_exposed_control
on tide_exposed_control.respondent_id = s_tide_users.respondent_id

left outer join
(
  select *
  from attitudinal_survey_responses
  where attitudinal_survey_responses.question_id = 8860789
  and trim(user_response_text) = 'I Have Seen Tide'
) tide_dseen
on tide_dseen.respondent_id = s_tide_users.respondent_id

left outer join
(
  select *
  from attitudinal_survey_responses
  where attitudinal_survey_responses.question_id = 8860795
  and trim(user_response_text) = 'I Have Heard Of Tide Pods'
) tide_dheard
on tide_dheard.respondent_id = s_tide_users.respondent_id

left outer join
(
  select *
  from attitudinal_survey_responses
  where attitudinal_survey_responses.question_id = 8861001
  and trim(user_response_text) in ('Very Likely Tide Pods','Somewhat Likely Tide Pods')
) tide_dpi
on tide_dpi.respondent_id = s_tide_users.respondent_id
;
