﻿SET hive.exec.compress.intermediate=TRUE;
SET hive.exec.compress.output=TRUE;
SET mapred.output.compression.type=BLOCK;
SET mapred.output.compression.codec=org.apache.hadoop.io.compress.SnappyCodec;

DROP TABLE IF EXISTS att_responses_zzzquil;

CREATE TABLE att_responses_zzzquil
ROW FORMAT SERDE 
  'org.apache.hadoop.hive.serde2.columnar.ColumnarSerDe' 
STORED AS INPUTFORMAT 
  'org.apache.hadoop.hive.ql.io.RCFileInputFormat' 
OUTPUTFORMAT 
  'org.apache.hadoop.hive.ql.io.RCFileOutputFormat'
as
select
survey_users.respondent_id
, pf_mbd_survey_users.dfa_user_id
, to_date(age_q.starttime) as survey_dt
, age_q.user_response_text as age
, exposed_control.user_response_text as exposed_control
, dseen.user_response_text as dseen
, case when dseen.user_response_text is null then 0 else 1 end as derived_dseen
, dheard.user_response_text as dheard
, case when dheard.user_response_text is null then 0 else 1 end as derived_dheard
, dpi.user_response_text as dpi
, case when dpi.user_response_text is null then 0 else 1 end as derived_dpi
, case when dheard.user_response_text is null then 0 else 1 end as att_response

from
(select distinct respondent_id from attitudinal_survey_responses where question_id in (9309331,9309376,9309384,9309385,9309388)) survey_users

join
(select distinct dl_member_id, dfa_user_id, mbd_survey_id from pf_mbd_survey_users) pf_mbd_survey_users
on 
  pf_mbd_survey_users.mbd_survey_id = 262518
  and
  pf_mbd_survey_users.dl_member_id = survey_users.respondent_id

left outer join
(
  select *
  from attitudinal_survey_responses
  where attitudinal_survey_responses.question_id = 9309376
) age_q
on age_q.respondent_id = survey_users.respondent_id

left outer join
(
  select *
  from attitudinal_survey_responses
  where attitudinal_survey_responses.question_id = 9309331
) exposed_control
on exposed_control.respondent_id = survey_users.respondent_id

left outer join
(
  select *
  from attitudinal_survey_responses
  where attitudinal_survey_responses.question_id = 9309385
  and trim(user_response_text) = 'I Have Seen ZzzQuil'
) dseen
on dseen.respondent_id = survey_users.respondent_id

left outer join
(
  select *
  from attitudinal_survey_responses
  where attitudinal_survey_responses.question_id = 9309384
  and trim(user_response_text) = 'I Have Heard Of ZzzQuil'
) dheard
on dheard.respondent_id = survey_users.respondent_id

left outer join
(
  select *
  from attitudinal_survey_responses
  where attitudinal_survey_responses.question_id = 9309388
  and trim(user_response_text) in ('Very Likely ZzzQuil','Somewhat Likely ZzzQuil')
) dpi
on dpi.respondent_id = survey_users.respondent_id
;
