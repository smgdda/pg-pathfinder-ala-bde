SET hive.exec.compress.intermediate=TRUE;
SET hive.exec.compress.output=TRUE;
SET mapred.output.compression.type=BLOCK;
SET mapred.output.compression.codec=org.apache.hadoop.io.compress.SnappyCodec;
SET hive.exec.dynamic.partition=TRUE;
SET hive.exec.dynamic.partition.mode=NONSTRICT;
SET hive.optimize.bucketmapjoin=TRUE;

INSERT INTO TABLE logs_pfcamp_matched PARTITION (event_dt,pfcampaign)
SELECT /*+ MAPJOIN(s), MAPJOIN(p), MAPJOIN(c), MAPJOIN(f), MAPJOIN(pfc)  */
  FROM_UNIXTIME(UNIX_TIMESTAMP(i.time, 'MM-dd-yyyy-HH:mm:ss')) as event_dts,
  i.user_id as dfa_user_id,
  i.dlx_encrypted_dfa_user_id,
  lower(pfc.brand) as brand,
  i.site_id,
  s.site,
  i.page_id,
  p.site_placement as placement,
  c.creative_id,
  c.ui_creative_id,
  c.creative as creative_name,
  case 
    when substr(p.site_placement,0,2) = 'pf'
      then concat_ws('_',split(p.site_placement,'_')[1],split(p.site_placement,'_')[4])
    else concat_ws('_',split(p.site_placement,'_')[0],split(p.site_placement,'_')[3])
  end as tactic,  
  case
    when substr(p.site_placement,0,2) = 'pf' 
      then concat_ws('_',s.site,split(p.site_placement,'_')[1],split(p.site_placement,'_')[4])
    else concat_ws('_',s.site,split(p.site_placement,'_')[0],split(p.site_placement,'_')[3])
  end as sitetactic,
  case
    when substr(p.site_placement,0,2) = 'pf'
      then concat_ws('_',s.site,split(p.site_placement,'_')[6])
    else concat_ws('_',s.site,split(p.site_placement,'_')[5])
  end as siteadsize,
  case
    when substr(p.site_placement,0,3) = 'pff' 
      then concat_ws('_',s.site,f.ad_name)
    else concat_ws('_',s.site,split(c.creative,'_')[2])
  end as sitecreative,  
  case
    when pfc.channel like '%ssm%' then 
      'ssm'
    when substr(p.site_placement,0,2) = 'pf' then 
      split(p.site_placement,'_')[1]
    else 
      split(p.site_placement,'_')[0] 
  end as exe_type,
  case
    when substr(p.site_placement,0,2) = 'pf' 
      then split(p.site_placement,'_')[2]
    else split(p.site_placement,'_')[1] 
  end as vendor_code,
  case
    when substr(p.site_placement,0,2) = 'pf' 
      then split(p.site_placement,'_')[3]
    else split(p.site_placement,'_')[2] 
  end as sfg_type,
  case
    when substr(p.site_placement,0,2) = 'pf' 
      then split(p.site_placement,'_')[4]
    else split(p.site_placement,'_')[3] 
  end as targeting_type,
  case
    when substr(p.site_placement,0,2) = 'pf' 
      then split(p.site_placement,'_')[5]
    else split(p.site_placement,'_')[4] 
  end as content_genre,
  case
    when substr(p.site_placement,0,2) = 'pf' 
      then split(p.site_placement,'_')[6] 
    else split(p.site_placement,'_')[5] 
  end as ad_size,
  case
    when substr(p.site_placement,0,3) = 'pff' 
      then f.ad_name       
    else split(c.creative,'_')[2] 
  end as creative,
  to_date(from_unixtime(unix_timestamp())) as current_date,
  date_sub(to_date(from_unixtime(unix_timestamp())), 60) as sixty_days_ago,
  p.dcm_rate_type,
  p.dcm_rate,
  p.dcm_qty,
  to_date(FROM_UNIXTIME(UNIX_TIMESTAMP(i.time, 'MM-dd-yyyy-HH:mm:ss'))) as event_dt,
  pfc.pfcampaign

FROM
(select 
    regexp_replace(lower(brand), '[^a-zA-Z0-9]', '') as brand
    , regexp_replace(lower(pfcampaign), '[^a-zA-Z0-9]', '') as pfcampaign
    , regexp_replace(lower(channel), '[^a-zA-Z0-9]', '') as channel 
    , buy_id 
  from 
    pfcampaign_list
  where
    pfcampaign_list.buy_id IS NOT NULL
    and pfcampaign_list.date_dropped = '') pfc 

JOIN (
  select * 
  from ingestion_dfa_impressions 
  where ingestion_dt = '${event_dt}' 
  and user_id <> '0') i
on (i.buy_id = pfc.buy_id)

JOIN (
  select 
    page_id
    , split(regexp_replace(lower(site_placement), '[^a-zA-Z0-9_\\|]', ''), '\\|')[0] as site_placement 
    , regexp_replace(lower(pricing_type), '[^a-zA-Z0-9\\_]', '') as dcm_rate_type
    , purchase_cost as dcm_rate
    , purchase_quantity as dcm_qty
  from 
    dfa_meta_page 
  where 
    page_id is not null
    and split(regexp_replace(lower(site_placement), '[^a-zA-Z0-9_\\|]', ''), '\\|')[0] not like '%cancel%'
  ) p 
on (p.page_id = i.page_id)

JOIN (select 
    site_id
    , regexp_replace(lower(site), '[^a-zA-Z0-9_]', '') as site 
  from 
    dfa_meta_site) s
on (s.site_id = i.site_id)

JOIN (select 
    creative_id
    , ui_creative_id
    , regexp_replace(lower(creative),'[^a-zA-Z0-9_]','') as creative
  from 
    dfa_meta_creative 
  where 
    creative_id is not null) c
on (c.creative_id = i.creative_id)

LEFT OUTER JOIN (
  select 
    ad_id
    , regexp_replace(lower(ad_name),'[^a-zA-Z0-9_]','') as ad_name 
  from 
    ingestion_flite_lookup
  )  f
on ( substr(i.site_data, 3, length(i.site_data))  = f.ad_id )

WHERE
  split(c.creative,'_')[2] <> 'flite'
;

ALTER TABLE logs_pfcamp_matched DROP IF EXISTS PARTITION (event_dt < '${new_start_partition_date}')
;
