﻿SET hive.exec.compress.intermediate=TRUE;
SET hive.exec.compress.output=TRUE;
SET mapred.output.compression.type=BLOCK;
SET mapred.output.compression.codec=org.apache.hadoop.io.compress.SnappyCodec;

DROP TABLE IF EXISTS cpa_plcmt_activities;

CREATE TABLE cpa_plcmt_activities
ROW FORMAT SERDE 
  'org.apache.hadoop.hive.serde2.columnar.ColumnarSerDe' 
STORED AS INPUTFORMAT 
  'org.apache.hadoop.hive.ql.io.RCFileInputFormat' 
OUTPUTFORMAT 
  'org.apache.hadoop.hive.ql.io.RCFileOutputFormat'
AS 
SELECT
  plcmt_delivery.creative_id
  , dfa_meta_creative.ui_creative_id
  , plcmt_delivery.impressions
  , plcmt_delivery.creative
  , plcmt_delivery.creative_name
  , plcmt_delivery.page_id
  , plcmt_delivery.site_id
  , dfa_activities.buy_id as dfa_activities_buy_id
  , dfa_activities.site_id as dfa_activities_site_id
  , dfa_activities.page_id as dfa_activities_page_id
  , dfa_activities.creative_id as dfa_activities_creative_id
  , dfa_activities.activity_ct

FROM
  (select * from plcmt_delivery where dcm_rate_type = 'cpa') plcmt_delivery

LEFT OUTER JOIN
  (
    select 
      buy_id
      , site_id
      , page_id
      , creative_id
      , count(*) as activity_ct
    from
      dfa_activity
    where
      event_dt >= '${woc_start_dt}'
      and event_dt <= '${woc_end_dt}'
      and dfa_user_id != '0'
    group by
      buy_id
      , site_id
      , page_id
      , creative_id
  ) dfa_activities
on (dfa_activities.page_id = plcmt_delivery.page_id and dfa_activities.creative_id = plcmt_delivery.creative_id)

LEFT OUTER JOIN 
  dfa_meta_creative 
on (dfa_meta_creative.creative_id = plcmt_delivery.creative_id)
;
