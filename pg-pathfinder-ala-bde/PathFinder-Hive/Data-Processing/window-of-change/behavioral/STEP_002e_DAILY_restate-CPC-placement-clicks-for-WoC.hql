﻿SET hive.exec.compress.intermediate=TRUE;
SET hive.exec.compress.output=TRUE;
SET mapred.output.compression.type=BLOCK;
SET mapred.output.compression.codec=org.apache.hadoop.io.compress.SnappyCodec;

DROP TABLE IF EXISTS cpc_plcmt_clicks;

CREATE TABLE cpc_plcmt_clicks
ROW FORMAT SERDE 
  'org.apache.hadoop.hive.serde2.columnar.ColumnarSerDe' 
STORED AS INPUTFORMAT 
  'org.apache.hadoop.hive.ql.io.RCFileInputFormat' 
OUTPUTFORMAT 
  'org.apache.hadoop.hive.ql.io.RCFileOutputFormat'
as
SELECT
  plcmt_delivery.creative_id
  , plcmt_delivery.creative
  , plcmt_delivery.creative_name
  , plcmt_delivery.page_id
  , plcmt_delivery.site_id
  , dfa_clicks.buy_id
  , dfa_clicks.site_id as dfa_click_site_id
  , dfa_clicks.page_id as dfa_click_page_id
  , dfa_clicks.creative_id as dfa_click_creative_id
  , dfa_clicks.click_ct

from
  (select * from plcmt_delivery where dcm_rate_type = 'cpc') plcmt_delivery

left outer join
  (
    select 
      buy_id
      , site_id
      , page_id
      , creative_id
      , count(*) as click_ct
    from
      dfa_clicks
    where
      event_dt >= '${woc_start_dt}'
      and event_dt <= '${woc_end_dt}'
      and dfa_user_id != '0'
    group by
      buy_id
      , site_id
      , page_id
      , creative_id
  ) dfa_clicks 
on (dfa_clicks.page_id = plcmt_delivery.page_id and dfa_clicks.creative_id = plcmt_delivery.creative_id)

;
