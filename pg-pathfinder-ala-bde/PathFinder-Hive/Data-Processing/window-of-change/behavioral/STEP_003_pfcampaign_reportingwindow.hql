SET hive.exec.compress.intermediate=TRUE;
SET hive.exec.compress.output=TRUE;
SET mapred.output.compression.type=BLOCK;
SET mapred.output.compression.codec=org.apache.hadoop.io.compress.SnappyCodec;

DROP TABLE IF EXISTS pfcampaign_reportingwindow;

CREATE TABLE pfcampaign_reportingwindow
ROW FORMAT SERDE 
  'org.apache.hadoop.hive.serde2.columnar.ColumnarSerDe' 
STORED AS INPUTFORMAT 
  'org.apache.hadoop.hive.ql.io.RCFileInputFormat' 
OUTPUTFORMAT 
  'org.apache.hadoop.hive.ql.io.RCFileOutputFormat'
as
SELECT
  pfcampaign
  , to_date(min(event_dts)) as startdate
  , to_date(max(event_dts)) as enddate
  , count(*) as pfc_matched_ct
FROM
  logs_pfcamp_matched
WHERE
  event_dt >= ${hiveconf:woc_start_dt}
  and event_dt <= ${hiveconf:woc_end_dt}
GROUP BY
  pfcampaign
;