SET hive.exec.compress.intermediate=TRUE;
SET hive.exec.compress.output=TRUE;
SET mapred.output.compression.type=BLOCK;
SET mapred.output.compression.codec=org.apache.hadoop.io.compress.SnappyCodec;
SET hive.auto.convert.join=TRUE;

DROP TABLE IF EXISTS dfa_user_buy_activity
;

CREATE TABLE dfa_user_buy_activity as
SELECT
  pfcampaign_list.pfcampaign as pfcampaign
  , user_id as dfa_user_id
  , case when dlx_encrypted_dfa_user_id is NULL then user_id else dlx_encrypted_dfa_user_id end as dlx_encrypted_dfa_user_id
  , max(FROM_UNIXTIME(UNIX_TIMESTAMP(time, 'MM-dd-yyyy-HH:mm:ss'))) as event_dts
  , count(*) as instance_ct
FROM (
  SELECT
  ingestion_dfa_activity.* 

  FROM
  ingestion_dfa_activity

  FULL OUTER JOIN (
    SELECT
    * 
    FROM
    pf_analysis_window 
    ORDER BY
    woc_year DESC
    , woc_month DESC
    LIMIT 1
  ) pf_analysis_window

  WHERE
  to_date(FROM_UNIXTIME(UNIX_TIMESTAMP(time, 'MM-dd-yyyy-HH:mm:ss'))) >= pf_analysis_window.analysis_start_dt
  and to_date(FROM_UNIXTIME(UNIX_TIMESTAMP(time, 'MM-dd-yyyy-HH:mm:ss'))) <= pf_analysis_window.analysis_end_dt
  and activity_type = 'pfinder'
  and user_id != '0'
  ) dfa_activity
JOIN (
  SELECT
  *
  FROM
  pfcampaign_list
  WHERE
  pfcampaign_list.date_dropped = ''
  ) pfcampaign_list
  on (pfcampaign_list.buy_id = dfa_activity.buy_id)

GROUP BY
pfcampaign_list.pfcampaign
, user_id
, case when dlx_encrypted_dfa_user_id is NULL then user_id else dlx_encrypted_dfa_user_id end
;
