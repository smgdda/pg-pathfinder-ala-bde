SET hive.exec.compress.intermediate=TRUE;
SET hive.exec.compress.output=TRUE;
SET mapred.output.compression.type=BLOCK;
SET mapred.output.compression.codec=org.apache.hadoop.io.compress.SnappyCodec;

DROP TABLE IF EXISTS pf_mbd_survey_users;

CREATE TABLE pf_mbd_survey_users
ROW FORMAT SERDE 
  'org.apache.hadoop.hive.serde2.columnar.ColumnarSerDe' 
STORED AS INPUTFORMAT 
  'org.apache.hadoop.hive.ql.io.RCFileInputFormat' 
OUTPUTFORMAT 
  'org.apache.hadoop.hive.ql.io.RCFileOutputFormat'
as
SELECT DISTINCT
  user_id
  , case 
    when dlx_encrypted_dfa_user_id is null or dlx_encrypted_dfa_user_id = '' then user_id 
    else dlx_encrypted_dfa_user_id 
    end as dlx_encrypted_dfa_user_id
  , split(split(other_data,';')[0], '=')[1] as dl_member_id
  , split(split(other_data,';')[1], '/')[4] as mbd_survey_id
  , ingestion_dfa_activity.buy_id

FROM
  pf_advertisers

JOIN
  dfa_meta_campaign
  on (
    pf_advertisers.in_pathfinder = 1
    and dfa_meta_campaign.advertiser_id = pf_advertisers.advertiser_id
    )

JOIN
  (
    select
      *
    from
      ingestion_dfa_activity
    where
--      ingestion_dt >= '2015-05-01'
--      and 
      lower(activity_type) == 'vindid' 
      and lower(activity_sub_type) == 'us_13324'
      and user_id != '0'
      and cast(split(split(other_data,';')[1], '/')[4] as double) is not null
  ) ingestion_dfa_activity
;
