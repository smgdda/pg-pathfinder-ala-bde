set hive.exec.dynamic.partition=true;
set hive.exec.dynamic.partition.mode=nonstrict;
set hive.exec.compress.output=true;
set hive.exec.compress.intermediate=true;
set mapred.compress.map.output=true;
set mapred.output.compression.type=BLOCK;
set mapred.output.compression.codec=org.apache.hadoop.io.compress.SnappyCodec;
set hive.exec.parallel=true;

ADD JAR rank.jar;
CREATE TEMPORARY FUNCTION p_rank AS 'com.m6d.hiveudf.Rank';

ALTER TABLE logs_ordinal DROP IF EXISTS PARTITION (pfcampaign='${hiveconf:pf_campaign_name}');

INSERT INTO TABLE logs_ordinal PARTITION (pfcampaign='${hiveconf:pf_campaign_name}')
SELECT
  sitetactic
  ,dfa_user_id
  ,dlx_encrypted_dfa_user_id
  ,event_dts
  ,brand
  ,site
  ,placement
  ,page_id
  ,creative_name
  ,creative_id
  ,ui_creative_id
  ,tactic
  ,siteadsize
  ,sitecreative
  ,exe_type
  ,vendor_code
  ,sfg_type
  ,targeting_type
  ,content_genre
  ,ad_size
  ,creative
  ,current_date
  ,sixty_days_ago
  ,site_id
  ,dcm_rate_type
  ,dcm_rate
  ,dcm_qty
  ,p_rank(sitetactic, dfa_user_id) as p_rank
  ,user_seq_num

FROM
(
  SELECT
    sitetactic
    ,dfa_user_id
    ,dlx_encrypted_dfa_user_id
    ,event_dts
    ,brand
    ,site
    ,placement
    ,page_id
    ,creative_name
    ,creative_id
    ,ui_creative_id
    ,tactic
    ,siteadsize
    ,sitecreative
    ,exe_type
    ,vendor_code
    ,sfg_type
    ,targeting_type
    ,content_genre
    ,ad_size
    ,creative
    ,current_date
    ,sixty_days_ago
    ,site_id
    ,dcm_rate_type
    ,dcm_rate
    ,dcm_qty
    ,user_seq_num

  FROM
    (
      SELECT
        a.sitetactic
        ,a.dfa_user_id
        ,a.dlx_encrypted_dfa_user_id
        ,a.event_dts
        ,a.brand
        ,a.site
        ,a.placement
        ,a.page_id
        ,a.creative_name
        ,a.creative_id
        ,a.ui_creative_id
        ,a.tactic
        ,a.siteadsize
        ,a.sitecreative
        ,a.exe_type
        ,a.vendor_code
        ,a.sfg_type
        ,a.targeting_type
        ,a.content_genre
        ,a.ad_size
        ,a.creative
        ,a.current_date
        ,a.sixty_days_ago
        ,a.site_id
        ,a.dcm_rate_type
        ,a.dcm_rate
        ,a.dcm_qty
        ,p_rank(a.dfa_user_id) as user_seq_num

      FROM
        (
          SELECT
            case when site = 'audiencescience' then regexp_replace(sitetactic, site, concat_ws('', site, sfg_type)) else sitetactic end as sitetactic
            ,dfa_user_id
            ,dlx_encrypted_dfa_user_id
            ,event_dts
            ,brand
            ,site
            ,placement
            ,page_id
            ,creative_name
            ,creative_id
            ,ui_creative_id
            ,tactic
            ,case when site = 'audiencescience' then regexp_replace(siteadsize, site, concat_ws('', site, sfg_type)) else siteadsize end as siteadsize
            ,case when site = 'audiencescience' then regexp_replace(sitecreative, site, concat_ws('', site, sfg_type)) else sitecreative end as sitecreative
            ,exe_type
            ,vendor_code
            ,sfg_type
            ,targeting_type
            ,content_genre
            ,ad_size
            ,creative
            ,current_date
            ,sixty_days_ago
            ,site_id
            ,dcm_rate_type
            ,dcm_rate
            ,dcm_qty
          FROM
            logs_pfcamp_matched

          WHERE
            pfcampaign = '${hiveconf:pf_campaign_name}'
            AND to_date(event_dts) >= '${hiveconf:woc_start_dt}'
            AND to_date(event_dts) <= '${hiveconf:woc_end_dt}'
          DISTRIBUTE BY
            dfa_user_id
          SORT BY
            dfa_user_id, event_dts
        ) a
      ) sequenced
  DISTRIBUTE BY
    sitetactic, dfa_user_id
  SORT BY
    sitetactic, dfa_user_id, event_dts
) re_sorted;
