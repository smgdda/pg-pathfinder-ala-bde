SET hive.exec.dynamic.partition=true; 
SET hive.exec.dynamic.partition.mode=nonstrict; 
SET hive.exec.compress.output=true; 
SET hive.exec.compress.intermediate=true; 
SET hive.optimize.bucketmapjoin=true; 
SET hive.enforce.bucketing=true; 
SET mapred.output.compression.type=BLOCK; 
SET mapred.output.compression.codec=org.apache.hadoop.io.compress.SnappyCodec; 
SET hive.exec.parallel=true; 
SET hive.auto.convert.join=true; 

ALTER TABLE log_conv DROP IF EXISTS PARTITION (pfcampaign='${hiveconf:pf_campaign_name}');

FROM (
  select
    * 
  from
    pfcampaign_reportingwindow
  where
    pfcampaign = '${hiveconf:pf_campaign_name}'
  ) campaign

JOIN (
  select
    * 
  from
    logs_ordinal 
  where
    pfcampaign = '${hiveconf:pf_campaign_name}'
  ) user_imp 
ON (user_imp.pfcampaign = campaign.pfcampaign)

LEFT OUTER JOIN (
  SELECT
    dfa_user_buy_activity.dfa_user_id
    , regexp_replace(lower(pfcampaign_list.pfcampaign), '[^a-zA-Z0-9]', '') as activity_campaign
    , max(dfa_user_buy_activity.max_activity_dts) as event_dts 

  FROM
    pfcampaign_list
 
  JOIN
    dfa_user_buy_activity
    ON (
      pfcampaign_list.date_dropped = ''
      and pfcampaign_list.buy_id is not null
      and regexp_replace(lower(pfcampaign_list.pfcampaign), '[^a-zA-Z0-9]', '') = '${hiveconf:pf_campaign_name}'
      and dfa_user_buy_activity.buy_id = pfcampaign_list.buy_id
    )

  GROUP BY
    dfa_user_buy_activity.dfa_user_id
    , regexp_replace(lower(pfcampaign_list.pfcampaign), '[^a-zA-Z0-9]', '')
  ) user_activity 
ON ( user_activity.dfa_user_id = user_imp.dfa_user_id and user_activity.activity_campaign = user_imp.pfcampaign ) 

INSERT INTO TABLE log_conv PARTITION(pfcampaign='${hiveconf:pf_campaign_name}') 
SELECT 
  user_imp.event_dts
  , user_imp.dfa_user_id
  , user_imp.dlx_encrypted_dfa_user_id
  , user_imp.brand
  , user_imp.site
  , user_imp.placement
  , user_imp.page_id
  , user_imp.creative_name
  , user_imp.creative_id
  , user_imp.ui_creative_id
  , user_imp.tactic
  , user_imp.sitetactic
  , user_imp.siteadsize
  , user_imp.sitecreative
  , user_imp.exe_type
  , user_imp.vendor_code
  , user_imp.sfg_type
  , user_imp.targeting_type
  , user_imp.content_genre
  , user_imp.ad_size
  , user_imp.creative
  , user_imp.current_date
  , user_imp.sixty_days_ago
  , user_imp.site_id
  , user_imp.dcm_rate_type
  , user_imp.dcm_rate
  , user_imp.dcm_qty
  , user_imp.p_rank as freq
  , case when user_activity.dfa_user_id is not null then 1 else 0 end as conv_user
  , case 
    when user_activity.dfa_user_id is not null and user_activity.event_dts > user_imp.event_dts then 
      ( unix_timestamp(user_activity.event_dts) - unix_timestamp(user_imp.event_dts)  ) / 3600 
    else
      ( unix_timestamp(campaign.enddate,  'yyyy-MM-dd') - unix_timestamp(user_imp.event_dts) )/3600 
    end as t2c
  , user_imp.user_seq_num
WHERE
  user_imp.pfcampaign = '${hiveconf:pf_campaign_name}'
;
