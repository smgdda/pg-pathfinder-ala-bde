SET hive.exec.compress.output=true;
SET hive.exec.compress.intermediate=true;
SET mapred.compress.map.output=true;
SET mapred.output.compression.type=BLOCK;
SET mapred.output.compression.codec=org.apache.hadoop.io.compress.SnappyCodec;
SET hive.exec.parallel=true;

DROP TABLE IF EXISTS plcmt_delivery;

CREATE TABLE plcmt_delivery
ROW FORMAT SERDE 
  'org.apache.hadoop.hive.serde2.columnar.ColumnarSerDe' 
STORED AS INPUTFORMAT 
  'org.apache.hadoop.hive.ql.io.RCFileInputFormat' 
OUTPUTFORMAT 
  'org.apache.hadoop.hive.ql.io.RCFileOutputFormat'
AS
select 
  brand,
  pfcampaign,
  site,
  sitetactic,
  sitecreative,
  siteadsize,
  tactic,
  creative,
  ad_size,
  placement,
  creative_name,
  page_id,
  site_id,
  dcm_rate_type,
  dcm_rate,
  dcm_qty,
  count(*) as impressions,
  case when substr(placement,0,2) = 'pf' then substr(regexp_replace(placement,'\\.|,|-|!| |-','-'),5,100)
    else regexp_replace(placement,'\\.|,|-|!| |-','-')
    end as spectra_matchkey
  
from logs_conv

group by   
  brand,
  pfcampaign,
  site,
  sitetactic,
  sitecreative,
  siteadsize,
  tactic,
  creative,
  ad_size,
  placement,
  creative_name,
  page_id,
  site_id,
  dcm_rate_type,
  dcm_rate,
  dcm_qty

;