set hive.optimize.bucketmapjoin = true;
set hive.exec.compress.intermediate = true;
set hive.exec.compress.output = true;
set mapred.output.compression.type = BLOCK;
set mapred.output.compression.codec = org.apache.hadoop.io.compress.SnappyCodec;

DROP TABLE IF EXISTS freq_table;

CREATE TABLE freq_table
ROW FORMAT SERDE 
  'org.apache.hadoop.hive.serde2.columnar.ColumnarSerDe' 
STORED AS INPUTFORMAT 
  'org.apache.hadoop.hive.ql.io.RCFileInputFormat' 
OUTPUTFORMAT 
  'org.apache.hadoop.hive.ql.io.RCFileOutputFormat'
AS
SELECT
  pfcampaign
  , sitetactic
  , concat_ws('_', pfcampaign, sitetactic) as camp_tactic
  , freq
  , count(*) as total_users
  , sum(conv_user) as conversions
  , sum(user_imps) as total_user_imps
  , sum(conv_user) / count(*) as conv_rt
  , TO_DATE( FROM_UNIXTIME( UNIX_TIMESTAMP() )) as processing_date

FROM
  (
    SELECT
      pfcampaign
      , sitetactic
      , dfa_user_id
      , conv_user
      , max(freq) as freq
      , count(*) as user_imps
  
    FROM
      log_conv
  
    GROUP BY
      pfcampaign
      , sitetactic
      , dfa_user_id
      , conv_user
  ) log_conv 

group by 
  pfcampaign
  , sitetactic
  , freq
;

