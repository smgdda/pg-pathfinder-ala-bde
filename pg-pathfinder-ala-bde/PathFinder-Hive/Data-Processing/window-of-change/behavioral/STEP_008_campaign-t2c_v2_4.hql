set hive.optimize.bucketmapjoin = true;
set hive.exec.compress.intermediate = true;
set hive.exec.compress.output = true;
set mapred.output.compression.type = BLOCK;
set mapred.output.compression.codec = org.apache.hadoop.io.compress.SnappyCodec;

DROP TABLE IF EXISTS campaign_t2c;

CREATE TABLE campaign_t2c
ROW FORMAT SERDE 
  'org.apache.hadoop.hive.serde2.columnar.ColumnarSerDe' 
STORED AS INPUTFORMAT 
  'org.apache.hadoop.hive.ql.io.RCFileInputFormat' 
OUTPUTFORMAT 
  'org.apache.hadoop.hive.ql.io.RCFileOutputFormat'
AS
SELECT
  brand
  , pfcampaign
  , avg(t2c) as lambda
FROM
  log_conv 
GROUP BY
  brand
  , pfcampaign
;
