library(plyr)
library(dplyr)
library(reshape)

# can be either "debug" or "live".  "debug" uses files, instead of connecting to Hive.
local_debug_mode = "live"

# filters the freq_data dataframe during analysis, to protect against bias from larger frequencies
max_freq_to_consider = 30

print('setting at-risk threshold to 30...')
at_risk_threshold = 30

print('setting up functions...')
get.table <- function(table.name){
  print('getting table...')
  tablename <- table.name
  
  switch(local_debug_mode, 
         "debug" = {
           table <- read.csv(paste(tablename,".csv",sep=""), stringsAsFactors=F)
         },
         "live" = {
           table = rhive.load.table2(tableName=tablename,limit=-1,remote=TRUE)
         })
  
  print('returning table...')
  return(table)
}

write.hive.table <- function(data.frame,tablename) {
  print('writing  table...')
  
  switch(local_debug_mode,
         "debug" = {
           write.csv(data.frame, paste(tablename,".csv",sep=""), quote=FALSE)
         },
         "live" = {
           rhive.execute(paste("drop table if exists ",tablename))
           rhive.write.table(data.frame, tablename)
         })
  
}

clean.freq_data <- function(freq_data, at_risk_threshold){
  print('cleaning freq data...')
  freq_data <- within(freq_data, camp_tactic <- paste(pfcampaign,sitetactic,sep="_"))
  freq_data <- within(freq_data, conv_rt <- conversions / total_users)
  freq_data <- freq_data %>% group_by(pfcampaign, sitetactic) %>% arrange(desc(freq)) %>% mutate(at_risk = cumsum(total_users))
  freq_data <- subset(freq_data, at_risk >= at_risk_threshold)
  print('returning clean freq data...')
  return(freq_data)
}

calc.pdf <- function(freq_data){
  print('calc-ing pdf...')
  freq_data <- within(freq_data, term <- (at_risk - conversions) / at_risk)
  freq_data <- freq_data %>% arrange(pfcampaign, sitetactic, freq) %>% group_by(pfcampaign, sitetactic) %>% dplyr::mutate(S.t = cumprod(term)) 
  freq_data <- within(freq_data, F.t <- 1 - S.t)
  freq_data[is.na(freq_data$F.t)] <- 0
  freq_data$F.t <- as.numeric(freq_data$F.t)
  freq_data <- freq_data %>% group_by(pfcampaign, sitetactic) %>% dplyr::arrange(freq) %>% dplyr::mutate(group_index = row_number()) %>% dplyr::mutate(f.t = ifelse(group_index == 1, F.t, F.t - lag(F.t)))
  freq_data <- freq_data[, -which(names(freq_data) %in% "group_index")]
  freq_data <- freq_data %>% group_by(pfcampaign, sitetactic) %>% dplyr::mutate(f.t_total = sum(f.t, na.rm=TRUE )) 
  freq_data <- within(freq_data, scaled_f.t <- f.t/f.t_total)
  print('returning calc pdf...')
  return(freq_data)
}

loess.fit <- function(freq_data){
  print('fitting to loess...')
  freq_data <- freq_data %>% group_by(camp_tactic) %>% dplyr::mutate(total_obs = n())
  freq_data <- subset(freq_data, total_obs > 3)
  
  # added subset by freq to avoid bias from higher frequencies
  freq_data <- subset(freq_data, freq <= max_freq_to_consider)
  
  # method call un-abbreviated to allow for finer-grained control over loess fit
  models <- list()
  for(i in unique(freq_data$camp_tactic)){
    currentDF <- as.data.frame(subset(freq_data, camp_tactic == i))
    models[[i]] <- loess(f.t ~ freq
                         , data = currentDF
                         , span=0.5
                         , degree=0
                         , method="loess"
                         , normalize = TRUE
                         , control=loess.control(
                           surface="interpolate"
                           , statistics="approximate"
                           , cell=0.05
                           , iterations=4
                         )
    )
  }
  pt.est <- lapply(models, function (x) predict(x, newdata = seq(1,max_freq_to_consider,1)))
  pt.est <- data.frame(do.call(rbind,pt.est))
  print('returning loess fit...')
  
  switch(local_debug_mode, 
         "debug" = {
           x_axis = c(1:max_freq_to_consider)
           pt_est_rownames <- rownames(pt.est)
           
           pdf(file = paste("maxFreq_", max_freq_to_consider, "_atRiskThresh_", at_risk_threshold, "_f_t-vs_loess.pdf", sep=""))
           par(mfrow = c(2,2))
           for (i in 1:length(pt.est[,1])) {
             pt_est_rowname <- pt_est_rownames[i]
             print(paste(i, "/", length(pt.est), ": ", pt_est_rowname))
             
             loess_value = pt.est[pt_est_rowname,1:max_freq_to_consider]
             print(paste(length(loess_value), " loess values for ", pt_est_rowname))
             
             subset_freq <- subset(freq_data, camp_tactic == pt_est_rowname)
             print(paste(length(subset_freq), " subset_freq values for ", pt_est_rowname))
             
             freq = subset_freq$freq
             f.t = subset_freq$f.t
             conv_rt = subset_freq$conv_rt
             
             plot(freq,f.t,col=1, sub = paste(pt_est_rowname), ylab = "f.t & loess", )
             # black plot is actual freq distribution
             points(x_axis, loess_value, col=3)
             # green plot is pt.est
             plot(freq,conv_rt,col=6, sub = paste(pt_est_rowname), ylab = "conversion rate")
           }
           dev.off()
         },
         "live" = {
           x_axis = c(1:max_freq_to_consider)
           pt_est_rownames <- rownames(pt.est)
           
           pdf(file = paste("maxFreq_", max_freq_to_consider, "_atRisk_", at_risk_threshold, "_f_t-vs-loess.pdf", sep=""))
           par(mfrow = c(2,2))
           for (i in 1:length(pt.est[,1])) {
             pt_est_rowname <- pt_est_rownames[i]
             print(paste(i, "/", length(pt.est), ": ", pt_est_rowname))
             
             loess_value = pt.est[pt_est_rowname,1:max_freq_to_consider]
             print(paste(length(loess_value), " loess values for ", pt_est_rowname))
             
             subset_freq <- subset(freq_data, camp_tactic == pt_est_rowname)
             print(paste(length(subset_freq), " subset_freq values for ", pt_est_rowname))
             
             freq = subset_freq$freq
             f.t = subset_freq$f.t
             conv_rt = subset_freq$conv_rt
             
             plot(freq,f.t,col=1, sub = paste(pt_est_rowname), ylab = "f.t & loess", )
             # black plot is actual freq distribution
             points(x_axis, loess_value, col=3)
             # green plot is pt.est
             plot(freq,conv_rt,col=6, sub = paste(pt_est_rowname), ylab = "conversion rate")
           }
           dev.off()
           outputFilePath = paste("/user/owatson/ft-vs-loess/f_t-vs-loess", "_", format(Sys.Date(), "%Y-%m-%d"), ".pdf", sep="")
           rhive.hdfs.put(src=paste("maxFreq_", max_freq_to_consider, "_atRisk_", at_risk_threshold, "_f_t-vs-loess.pdf", sep=""), dst=outputFilePath, srcDel=TRUE, overwrite=TRUE)
         })
  
  return(pt.est)
}

munge.output <- function(pt.est){
  print('munging...')
  pt.est$sitetactic <- row.names(pt.est)
  pt.est <- melt(pt.est, id=c("sitetactic"))
  
  #clean data
  names(pt.est) <- c("camp_tactic", "freq", "value")
  pt.est$freq <- as.numeric(sub("X","",pt.est$freq))
  
  #delete rows w NA
  pt.est <- pt.est[complete.cases(pt.est),]
  pt.est <- within(pt.est, freq_matchkey <- paste(camp_tactic,freq,sep="."))
  
  print('returning munge...')
  return(pt.est)
}

correct.freq <- function(freq_data, pt.est){
  print('correcting freq data...')
  
  # identify if all conversions happen at the first 3 frequencies
  # these will lead to pfscore = 0 if not corrected, despite all conversions occurring in the first 3 frequencies
  
  freq_data <- freq_data %>% group_by(pfcampaign, sitetactic) %>% mutate(count_dummy = 1, obs_count = sum(count_dummy))
  
  campaigns_to_correct <- subset(freq_data, obs_count < 4 )
  campaigns_to_correct <- as.data.frame(campaigns_to_correct)[c('camp_tactic','freq','f.t')]
  names(campaigns_to_correct) <- c('camp_tactic','freq','value')
  campaigns_to_correct = within(campaigns_to_correct, {
    freq_matchkey <- paste(camp_tactic,freq,sep=".")
  })
  
  drop_col = c('obs_count', 'count_dummy')
  freq_data <- freq_data[, !names(freq_data) %in% drop_col]
  
  pt.est <- rbind(pt.est, campaigns_to_correct)
  pt.est <- pt.est %>% arrange(camp_tactic, freq)
  
  print('returning corrected freq data...')
  return(pt.est)
}

print('connecting...')
switch(
  local_debug_mode,
  "debug" = {
    print("skipping Hive connection, using local files")
  },
  "live" = {
    connect()
  }
)

print('getting freq data...')
freq_data <- get.table('freq_table')

print('freq data received, cleaning freq data...')
freq_data <- clean.freq_data(freq_data, at_risk_threshold)

print('freq data cleaned, calculating pdf...')
freq_data <- calc.pdf(freq_data)
#write.hive.table(freq_data,"freq_data_w_pd")

print('freq data pdfs calculated, fitting to loess...')
pt.est <- loess.fit(freq_data)

print('freq data fit to loess, munging output...')
pt.est <- munge.output(pt.est)

print('correct freq data for anything with less than 3 observations...')
pt.est <- correct.freq(freq_data, pt.est)

print('writing back to db...')
write.hive.table(pt.est,"freq_modelled")
#rename to freq_modelled for final production use

print('closing connection...')
switch(
  local_debug_mode,
  "debug" = {
    print("skipping Hive connection close, since using local files")
  },
  "live" = {
    rhive.close()
  }
)


print('clearing used variables...')
remove(freq_data, pt.est, at_risk_threshold, local_debug_mode, max_freq_to_consider)

print('clearing functions...')
remove(calc.pdf,clean.freq_data,get.table,loess.fit,munge.output,correct.freq,write.hive.table)

print(format(Sys.time(), "%a %b %d %X %Y"))
