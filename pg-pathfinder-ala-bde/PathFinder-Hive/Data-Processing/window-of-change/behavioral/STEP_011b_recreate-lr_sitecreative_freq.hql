set hive.exec.compress.intermediate=true;
set hive.exec.compress.output=true;
set mapred.output.compression.type=BLOCK;
set mapred.output.compression.codec=org.apache.hadoop.io.compress.SnappyCodec;
set mapred.compress.map.output=true;
set hive.exec.dynamic.partition=true;
set hive.exec.dynamic.partition.mode=nonstrict;

ALTER TABLE lr_sitecreative_freq DROP IF EXISTS PARTITION (pfcampaign='${hiveconf:pf_campaign_name}');

INSERT INTO TABLE lr_sitecreative_freq PARTITION(pfcampaign='${hiveconf:pf_campaign_name}')
SELECT
dfa_user_id
, case when dlx_encrypted_dfa_user_id is not NULL then dlx_encrypted_dfa_user_id else dfa_user_id end as dlx_encrypted_dfa_user_id
, sitecreative
, count(*) as freq_count
, max(conv_user) as conv_user
, exe_type

from
logs_product imp

WHERE
pfcampaign = '${hiveconf:pf_campaign_name}'

group by
dfa_user_id
, case when dlx_encrypted_dfa_user_id is not NULL then dlx_encrypted_dfa_user_id else dfa_user_id end
, sitecreative
, exe_type

;
