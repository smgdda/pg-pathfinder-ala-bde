SET hive.exec.compress.intermediate=TRUE;
SET hive.exec.compress.output=TRUE;
SET mapred.output.compression.type=BLOCK;
SET mapred.output.compression.codec=org.apache.hadoop.io.compress.SnappyCodec;

DROP TABLE IF EXISTS pf_mbd_survey_users;

CREATE TABLE pf_mbd_survey_users 
ROW FORMAT SERDE 
  'org.apache.hadoop.hive.serde2.columnar.ColumnarSerDe' 
STORED AS INPUTFORMAT 
  'org.apache.hadoop.hive.ql.io.RCFileInputFormat' 
OUTPUTFORMAT 
  'org.apache.hadoop.hive.ql.io.RCFileOutputFormat'
AS
SELECT DISTINCT
split(str_to_map(dfa_activity.other_data, '\\;', '=')['u3'], '-')[0] as dl_member_id
, split(str_to_map(dfa_activity.other_data, '\\;', '=')['u3'], '-')[1] as mbd_survey_id
, dfa_user_id
, dlx_encrypted_dfa_user_id
, buy_id
, mbd_campaign_name

FROM
dfa_activity

JOIN
attitudinal_surveys
on attitudinal_surveys.survey_id = split(str_to_map(dfa_activity.other_data, '\\;', '=')['u3'], '-')[1]

WHERE
event_dt >= '2015-07-01'
and lower(activity_type) = 'vindid'
and lower(activity_sub_type) = 'us_13324'
and dfa_activity.other_data like '%http://www.insightexpress.com/ix/Survey.aspx%'
and dfa_user_id != '0'
;
