SET hive.exec.compress.intermediate=TRUE;
SET hive.exec.compress.output=TRUE;
SET mapred.output.compression.type=BLOCK;
SET mapred.output.compression.codec=org.apache.hadoop.io.compress.SnappyCodec;
SET hive.exec.dynamic.partition=TRUE;
SET hive.exec.dynamic.partition.mode=NONSTRICT;
SET hive.auto.convert.join=TRUE;

INSERT INTO TABLE mbd_lr_sitetactic_freq PARTITION (pfcampaign='${hiveconf:pf_campaign_name}')
select
  lr.dfa_user_id
  , lr.dlx_encrypted_dfa_user_id
  , lr.sitetactic
  , lr.freq_count
  , lr.conv_user
  , lr.exe_type

from
  (
  SELECT
    regexp_replace(lower(mbd_survey_list.campaign), '[^a-z0-9]', '') as pfcampaign
    , pf_mbd_survey_users.dfa_user_id
    , pf_mbd_survey_users.dl_member_id as mbd_respondent_id
  FROM
    mbd_survey_list
  JOIN
    pf_mbd_survey_users
    on regexp_replace(lower(mbd_survey_list.campaign), '[^a-z0-9]', '') = '${hiveconf:pf_campaign_name}'
    and pf_mbd_survey_users.mbd_survey_id = mbd_survey_list.surveyid
  ) mbd_respondents

join
  lr_sitetactic_freq lr
  on
    lr.pfcampaign = '${hiveconf:pf_campaign_name}'
    and lr.dfa_user_id = mbd_respondents.dfa_user_id
    and lr.pfcampaign = mbd_respondents.pfcampaign
;