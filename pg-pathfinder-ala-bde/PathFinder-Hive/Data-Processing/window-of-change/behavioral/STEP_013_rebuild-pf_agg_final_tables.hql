set hive.exec.dynamic.partition=true;
set hive.exec.dynamic.partition.mode=nonstrict;
set hive.exec.compress.output=true;
set hive.exec.compress.intermediate=true;
set mapred.compress.map.output=true;
set mapred.output.compression.type=BLOCK;
set mapred.output.compression.codec=org.apache.hadoop.io.compress.SnappyCodec;

ALTER TABLE pf_agg_final_no_exe_type DROP IF EXISTS PARTITION (woc_spec='${hiveconf:woc_spec}');
INSERT INTO TABLE pf_agg_final_no_exe_type PARTITION (woc_spec='${hiveconf:woc_spec}')
SELECT
  woc_pf_agg.*

FROM
  (
    SELECT
    *
    
    FROM
    vw_pf_agg_final_no_exe_type
    
    WHERE
    pfscore is not null
  ) woc_pf_agg
;

ALTER TABLE pf_agg_final DROP IF EXISTS PARTITION (woc_spec='${hiveconf:woc_spec}');
INSERT INTO TABLE pf_agg_final PARTITION (woc_spec='${hiveconf:woc_spec}')
SELECT
  woc_pf_agg.*

FROM
  (
    SELECT
    *
    
    FROM
    vw_pf_agg_final
    
    WHERE
    pfscore is not null
  ) woc_pf_agg
;
