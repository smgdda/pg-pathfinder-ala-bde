﻿DROP TABLE IF EXISTS pf_camp_totals_manual_update;
CREATE TABLE pf_camp_totals_manual_update as
SELECT 
  lower(dcm_spectra_merge.brand) as brand, 
  lower(dcm_spectra_merge.pfcampaign) as pfcampaign,
  case when spectra_rate_type is not null and spectra_rate_type != '' then spectra_rate_type else dcm_rate_type end as rate_type,
  lower(exe_type) as exe_type,
  sum(impressions) as imps_tot,
  sum(calculated_actual_cost) as cost_tot,
  sum(ss_cost_sum) as ss_cost_tot,
  ((1000 * sum(calculated_actual_cost)) / sum(impressions)) as calc_cpm,
  ((1000 * sum(ss_cost_sum)) / sum(impressions)) as calc_ss_cpm,
  sum(pfscore_plcmt) as pfscore_tot
FROM
  (select distinct lower(brand) as brand, lower(pfcampaign) as pfcampaign from pfcampaign_list where date_dropped = '' and buy_id is not null) pfcampaign_list
join
  dcm_spectra_merge_agg_w_innovid_manual_update dcm_spectra_merge
  on dcm_spectra_merge.brand = pfcampaign_list.brand
  and dcm_spectra_merge.pfcampaign = pfcampaign_list.pfcampaign
GROUP BY
  lower(dcm_spectra_merge.brand), 
  lower(dcm_spectra_merge.pfcampaign),
  case when spectra_rate_type is not null and spectra_rate_type != '' then spectra_rate_type else dcm_rate_type end,
  lower(exe_type)
;

DROP TABLE IF EXISTS pf_camp_totals_no_exe_type_manual_update;
CREATE TABLE pf_camp_totals_no_exe_type_manual_update as
SELECT 
  lower(dcm_spectra_merge.brand) as brand, 
  lower(dcm_spectra_merge.pfcampaign) as pfcampaign,
  case when spectra_rate_type is not null and spectra_rate_type != '' then spectra_rate_type else dcm_rate_type end as rate_type,
  sum(impressions) as imps_tot,
  sum(calculated_actual_cost) as cost_tot,
  sum(ss_cost_sum) as ss_cost_tot,
  ((1000 * sum(calculated_actual_cost)) / sum(impressions)) as calc_cpm,
  ((1000 * sum(ss_cost_sum)) / sum(impressions)) as calc_ss_cpm,
  sum(pfscore_plcmt) as pfscore_tot
FROM
  (select distinct lower(brand) as brand, lower(pfcampaign) as pfcampaign from pfcampaign_list where date_dropped = '' and buy_id is not null) pfcampaign_list
join
  dcm_spectra_merge_agg_w_innovid_manual_update dcm_spectra_merge
  on dcm_spectra_merge.brand = pfcampaign_list.brand
  and dcm_spectra_merge.pfcampaign = pfcampaign_list.pfcampaign
GROUP BY
  lower(dcm_spectra_merge.brand), 
  lower(dcm_spectra_merge.pfcampaign),
  case when spectra_rate_type is not null and spectra_rate_type != '' then spectra_rate_type else dcm_rate_type end
;

DROP TABLE IF EXISTS pf_dimtotals_manual_update;
CREATE TABLE pf_dimtotals_manual_update AS 
SELECT
  lower(dims.brand) as brand,
  dims.pfcampaign,
  dims.exe_type,
  dims.rate_type,
  dims.dimtype,
  dims.dim,
  dims.imps,
  dims.cost,
  dims.ss_cost,
  dims.calc_cpm,
  dims.calc_ss_cpm,
  dims.pfscore
FROM
  (
    SELECT 
      brand,
      pfcampaign,
      exe_type,
      case when spectra_rate_type is not null and spectra_rate_type != '' then spectra_rate_type else dcm_rate_type end as rate_type,
      'site' as dimtype,
      lower(site) as dim,
      sum(impressions) as imps,
      sum(calculated_actual_cost) as cost,
      sum(ss_cost_sum) as ss_cost,
      ((1000 * sum(calculated_actual_cost)) / sum(impressions)) as calc_cpm,
      ((1000 * sum(ss_cost_sum)) / sum(impressions)) as calc_ss_cpm,
      sum(pfscore_plcmt) as pfscore
    FROM
      dcm_spectra_merge_agg_w_innovid_manual_update dcm_spectra_merge
    GROUP BY 
      brand,
      pfcampaign,
      exe_type,
      case when spectra_rate_type is not null and spectra_rate_type != '' then spectra_rate_type else dcm_rate_type end,
      lower(site)

    union all

    SELECT 
      brand,
      pfcampaign,
      exe_type,
      case when spectra_rate_type is not null and spectra_rate_type != '' then spectra_rate_type else dcm_rate_type end as rate_type,
      'tactic' as dimtype,
      lower(tactic) as dim,
      sum(impressions) as imps,
      sum(calculated_actual_cost) as cost,
      sum(ss_cost_sum) as ss_cost,
      ((1000 * sum(calculated_actual_cost)) / sum(impressions)) as calc_cpm,
      ((1000 * sum(ss_cost_sum)) / sum(impressions)) as calc_ss_cpm,
      sum(pfscore_plcmt) as pfscore
    FROM
      dcm_spectra_merge_agg_w_innovid_manual_update dcm_spectra_merge
    GROUP BY 
      brand,
      pfcampaign,
      exe_type,
      case when spectra_rate_type is not null and spectra_rate_type != '' then spectra_rate_type else dcm_rate_type end,
      lower(tactic)

    union all

    SELECT 
      brand,
      pfcampaign,
      exe_type,
      case when spectra_rate_type is not null and spectra_rate_type != '' then spectra_rate_type else dcm_rate_type end as rate_type,
      'adsize' as dimtype,
      lower(ad_size) as dim,
      sum(impressions) as imps,
      sum(calculated_actual_cost) as cost,
      sum(ss_cost_sum) as ss_cost,
      ((1000 * sum(calculated_actual_cost)) / sum(impressions)) as calc_cpm,
      ((1000 * sum(ss_cost_sum)) / sum(impressions)) as calc_ss_cpm,
      sum(pfscore_plcmt) as pfscore
    FROM
      dcm_spectra_merge_agg_w_innovid_manual_update dcm_spectra_merge
    GROUP BY 
      brand,
      pfcampaign,
      exe_type,
      case when spectra_rate_type is not null and spectra_rate_type != '' then spectra_rate_type else dcm_rate_type end,
      lower(ad_size)

    union all

    SELECT 
      brand,
      pfcampaign,
      exe_type,
      case when spectra_rate_type is not null and spectra_rate_type != '' then spectra_rate_type else dcm_rate_type end as rate_type,
      'creative' as dimtype,
      lower(creative) as dim,
      sum(impressions) as imps,
      sum(calculated_actual_cost) as cost,
      sum(ss_cost_sum) as ss_cost,
      ((1000 * sum(calculated_actual_cost)) / sum(impressions)) as calc_cpm,
      ((1000 * sum(ss_cost_sum)) / sum(impressions)) as calc_ss_cpm,
      sum(pfscore_plcmt) as pfscore
    FROM
      dcm_spectra_merge_agg_w_innovid_manual_update dcm_spectra_merge
    GROUP BY 
      brand,
      pfcampaign,
      exe_type,
      case when spectra_rate_type is not null and spectra_rate_type != '' then spectra_rate_type else dcm_rate_type end,
      lower(creative)

    union all

    SELECT 
      brand,
      pfcampaign,
      exe_type,
      case when spectra_rate_type is not null and spectra_rate_type != '' then spectra_rate_type else dcm_rate_type end as rate_type,
      'sitetactic' as dimtype,
      lower(sitetactic) as dim,
      sum(impressions) as imps,
      sum(calculated_actual_cost) as cost,
      sum(ss_cost_sum) as ss_cost,
      ((1000 * sum(calculated_actual_cost)) / sum(impressions)) as calc_cpm,
      ((1000 * sum(ss_cost_sum)) / sum(impressions)) as calc_ss_cpm,
      sum(pfscore_plcmt) as pfscore
    FROM
      dcm_spectra_merge_agg_w_innovid_manual_update dcm_spectra_merge
    GROUP BY 
      brand,
      pfcampaign,
      exe_type,
      case when spectra_rate_type is not null and spectra_rate_type != '' then spectra_rate_type else dcm_rate_type end,
      lower(sitetactic)

    union all

    SELECT 
      brand,
      pfcampaign,
      exe_type,
      case when spectra_rate_type is not null and spectra_rate_type != '' then spectra_rate_type else dcm_rate_type end as rate_type,
      'siteadsize' as dimtype,
      lower(siteadsize) as dim,
      sum(impressions) as imps,
      sum(calculated_actual_cost) as cost,
      sum(ss_cost_sum) as ss_cost,
      ((1000 * sum(calculated_actual_cost)) / sum(impressions)) as calc_cpm,
      ((1000 * sum(ss_cost_sum)) / sum(impressions)) as calc_ss_cpm,
      sum(pfscore_plcmt) as pfscore
    FROM
      dcm_spectra_merge_agg_w_innovid_manual_update dcm_spectra_merge
    GROUP BY 
      brand,
      pfcampaign,
      exe_type,
      case when spectra_rate_type is not null and spectra_rate_type != '' then spectra_rate_type else dcm_rate_type end,
      lower(siteadsize)

    union all

    SELECT 
      brand,
      pfcampaign,
      exe_type,
      case when spectra_rate_type is not null and spectra_rate_type != '' then spectra_rate_type else dcm_rate_type end as rate_type,
      'sitecreative' as dimtype,
      lower(sitecreative) as dim,
      sum(impressions) as imps,
      sum(calculated_actual_cost) as cost,
      sum(ss_cost_sum) as ss_cost,
      ((1000 * sum(calculated_actual_cost)) / sum(impressions)) as calc_cpm,
      ((1000 * sum(ss_cost_sum)) / sum(impressions)) as calc_ss_cpm,
      sum(pfscore_plcmt) as pfscore
    FROM
      dcm_spectra_merge_agg_w_innovid_manual_update dcm_spectra_merge
    GROUP BY 
      brand,
      pfcampaign,
      exe_type,
      case when spectra_rate_type is not null and spectra_rate_type != '' then spectra_rate_type else dcm_rate_type end,
      lower(sitecreative)
  ) dims
JOIN
  (
    select distinct 
      lower(brand) as brand
      , lower(pfcampaign) as pfcampaign 
    from 
      pfcampaign_list 
    where 
      date_dropped = '' 
      and buy_id is not null
  ) pfcampaign_list
on (
  pfcampaign_list.brand = dims.brand
  and pfcampaign_list.pfcampaign = dims.pfcampaign
)
;

DROP TABLE IF EXISTS pf_dimtotals_no_exe_type_manual_update;
CREATE TABLE pf_dimtotals_no_exe_type_manual_update AS 
SELECT
  lower(dims.brand) as brand,
  dims.pfcampaign,
  dims.rate_type,
  dims.dimtype,
  dims.dim,
  dims.imps,
  dims.cost,
  dims.ss_cost,
  dims.calc_cpm,
  dims.calc_ss_cpm,
  dims.pfscore
FROM
  (
    SELECT 
      brand,
      pfcampaign,
      case when spectra_rate_type is not null and spectra_rate_type != '' then spectra_rate_type else dcm_rate_type end as rate_type,
      'site' as dimtype,
      lower(site) as dim,
      sum(impressions) as imps,
      sum(calculated_actual_cost) as cost,
      sum(ss_cost_sum) as ss_cost,
      ((1000 * sum(calculated_actual_cost)) / sum(impressions)) as calc_cpm,
      ((1000 * sum(ss_cost_sum)) / sum(impressions)) as calc_ss_cpm,
      sum(pfscore_plcmt) as pfscore
    FROM
      dcm_spectra_merge_agg_w_innovid_manual_update dcm_spectra_merge
    GROUP BY 
      brand,
      pfcampaign,
      case when spectra_rate_type is not null and spectra_rate_type != '' then spectra_rate_type else dcm_rate_type end,
      lower(site)

    union all

    SELECT 
      brand,
      pfcampaign,
      case when spectra_rate_type is not null and spectra_rate_type != '' then spectra_rate_type else dcm_rate_type end as rate_type,
      'tactic' as dimtype,
      lower(tactic) as dim,
      sum(impressions) as imps,
      sum(calculated_actual_cost) as cost,
      sum(ss_cost_sum) as ss_cost,
      ((1000 * sum(calculated_actual_cost)) / sum(impressions)) as calc_cpm,
      ((1000 * sum(ss_cost_sum)) / sum(impressions)) as calc_ss_cpm,
      sum(pfscore_plcmt) as pfscore
    FROM
      dcm_spectra_merge_agg_w_innovid_manual_update dcm_spectra_merge
    GROUP BY 
      brand,
      pfcampaign,
      case when spectra_rate_type is not null and spectra_rate_type != '' then spectra_rate_type else dcm_rate_type end,
      lower(tactic)

    union all

    SELECT 
      brand,
      pfcampaign,
      case when spectra_rate_type is not null and spectra_rate_type != '' then spectra_rate_type else dcm_rate_type end as rate_type,
      'adsize' as dimtype,
      lower(ad_size) as dim,
      sum(impressions) as imps,
      sum(calculated_actual_cost) as cost,
      sum(ss_cost_sum) as ss_cost,
      ((1000 * sum(calculated_actual_cost)) / sum(impressions)) as calc_cpm,
      ((1000 * sum(ss_cost_sum)) / sum(impressions)) as calc_ss_cpm,
      sum(pfscore_plcmt) as pfscore
    FROM
      dcm_spectra_merge_agg_w_innovid_manual_update dcm_spectra_merge
    GROUP BY 
      brand,
      pfcampaign,
      case when spectra_rate_type is not null and spectra_rate_type != '' then spectra_rate_type else dcm_rate_type end,
      lower(ad_size)

    union all

    SELECT 
      brand,
      pfcampaign,
      case when spectra_rate_type is not null and spectra_rate_type != '' then spectra_rate_type else dcm_rate_type end as rate_type,
      'creative' as dimtype,
      lower(creative) as dim,
      sum(impressions) as imps,
      sum(calculated_actual_cost) as cost,
      sum(ss_cost_sum) as ss_cost,
      ((1000 * sum(calculated_actual_cost)) / sum(impressions)) as calc_cpm,
      ((1000 * sum(ss_cost_sum)) / sum(impressions)) as calc_ss_cpm,
      sum(pfscore_plcmt) as pfscore
    FROM
      dcm_spectra_merge_agg_w_innovid_manual_update dcm_spectra_merge
    GROUP BY 
      brand,
      pfcampaign,
      case when spectra_rate_type is not null and spectra_rate_type != '' then spectra_rate_type else dcm_rate_type end,
      lower(creative)

    union all

    SELECT 
      brand,
      pfcampaign,
      case when spectra_rate_type is not null and spectra_rate_type != '' then spectra_rate_type else dcm_rate_type end as rate_type,
      'sitetactic' as dimtype,
      lower(sitetactic) as dim,
      sum(impressions) as imps,
      sum(calculated_actual_cost) as cost,
      sum(ss_cost_sum) as ss_cost,
      ((1000 * sum(calculated_actual_cost)) / sum(impressions)) as calc_cpm,
      ((1000 * sum(ss_cost_sum)) / sum(impressions)) as calc_ss_cpm,
      sum(pfscore_plcmt) as pfscore
    FROM
      dcm_spectra_merge_agg_w_innovid_manual_update dcm_spectra_merge
    GROUP BY 
      brand,
      pfcampaign,
      case when spectra_rate_type is not null and spectra_rate_type != '' then spectra_rate_type else dcm_rate_type end,
      lower(sitetactic)

    union all

    SELECT 
      brand,
      pfcampaign,
      case when spectra_rate_type is not null and spectra_rate_type != '' then spectra_rate_type else dcm_rate_type end as rate_type,
      'siteadsize' as dimtype,
      lower(siteadsize) as dim,
      sum(impressions) as imps,
      sum(calculated_actual_cost) as cost,
      sum(ss_cost_sum) as ss_cost,
      ((1000 * sum(calculated_actual_cost)) / sum(impressions)) as calc_cpm,
      ((1000 * sum(ss_cost_sum)) / sum(impressions)) as calc_ss_cpm,
      sum(pfscore_plcmt) as pfscore
    FROM
      dcm_spectra_merge_agg_w_innovid_manual_update dcm_spectra_merge
    GROUP BY 
      brand,
      pfcampaign,
      case when spectra_rate_type is not null and spectra_rate_type != '' then spectra_rate_type else dcm_rate_type end,
      lower(siteadsize)

    union all

    SELECT 
      brand,
      pfcampaign,
      case when spectra_rate_type is not null and spectra_rate_type != '' then spectra_rate_type else dcm_rate_type end as rate_type,
      'sitecreative' as dimtype,
      lower(sitecreative) as dim,
      sum(impressions) as imps,
      sum(calculated_actual_cost) as cost,
      sum(ss_cost_sum) as ss_cost,
      ((1000 * sum(calculated_actual_cost)) / sum(impressions)) as calc_cpm,
      ((1000 * sum(ss_cost_sum)) / sum(impressions)) as calc_ss_cpm,
      sum(pfscore_plcmt) as pfscore
    FROM
      dcm_spectra_merge_agg_w_innovid_manual_update dcm_spectra_merge
    GROUP BY 
      brand,
      pfcampaign,
      case when spectra_rate_type is not null and spectra_rate_type != '' then spectra_rate_type else dcm_rate_type end,
      lower(sitecreative)
  ) dims
JOIN
  (
    select distinct 
      lower(brand) as brand
      , lower(pfcampaign) as pfcampaign 
    from 
      pfcampaign_list 
    where 
      date_dropped = '' 
      and buy_id is not null
  ) pfcampaign_list
on (
  pfcampaign_list.brand = dims.brand
  and pfcampaign_list.pfcampaign = dims.pfcampaign
)
;

DROP TABLE IF EXISTS pf_scaled_manual_update;
CREATE TABLE pf_scaled_manual_update AS 
SELECT
  a.brand
  ,a.pfcampaign
  ,a.rate_type
  ,lower(a.exe_type) as exe_type
  ,a.dimtype
  ,a.dim
  ,a.imps
  ,a.cost
  ,a.ss_cost
  ,a.pfscore
  ,a.imps/b.imps_tot as scaled_imps
  ,a.cost/b.cost_tot as scaled_cost
  ,a.ss_cost/b.ss_cost_tot as scaled_ss_cost
  ,a.pfscore/b.pfscore_tot as scaled_pfscore
FROM
  pf_dimtotals_manual_update a 

LEFT OUTER JOIN (
  SELECT
  brand
  ,pfcampaign
  ,rate_type
  ,lower(exe_type) as exe_type
  ,imps_tot
  ,cost_tot
  ,ss_cost_tot
  ,pfscore_tot
  FROM
  pf_camp_totals_manual_update
  ) b 
ON (
  a.pfcampaign = b.pfcampaign 
  and a.brand = b.brand
  and lower(a.exe_type) = lower(b.exe_type)
  and a.rate_type = b.rate_type
  )
;

DROP TABLE IF EXISTS pf_scaled_no_exe_type_manual_update;
CREATE TABLE pf_scaled_no_exe_type_manual_update AS 
SELECT
  a.brand
  ,a.pfcampaign
  ,a.rate_type
  ,a.dimtype
  ,a.dim
  ,a.imps
  ,a.cost
  ,a.ss_cost
  ,a.pfscore
  ,a.imps/b.imps_tot as scaled_imps
  ,a.cost/b.cost_tot as scaled_cost
  ,a.ss_cost/b.ss_cost_tot as scaled_ss_cost
  ,a.pfscore/b.pfscore_tot as scaled_pfscore
FROM
  pf_dimtotals_no_exe_type_manual_update a 

LEFT OUTER JOIN (
  SELECT
  brand
  ,pfcampaign
  ,rate_type
  ,imps_tot
  ,cost_tot
  ,ss_cost_tot
  ,pfscore_tot
  FROM
  pf_camp_totals_no_exe_type_manual_update
  ) b 
ON (
  a.pfcampaign = b.pfcampaign 
  and a.brand = b.brand
  and a.rate_type = b.rate_type
  )
;

DROP VIEW IF EXISTS pf_agg_final_manual_update;
CREATE VIEW pf_agg_final_manual_update AS 
SELECT
  brand
  , pfcampaign
  , rate_type
  , exe_type
  , dimtype
  , dim
  , imps
  , cost
  , ss_cost
  , pfscore
  , scaled_imps
  , scaled_cost
  , scaled_ss_cost
  , scaled_pfscore
  , scaled_pfscore/scaled_imps as impact
  , scaled_pfscore/scaled_cost as efficiency
  , scaled_pfscore/scaled_ss_cost as actual_efficiency
from
  pf_scaled_manual_update
;

DROP TABLE IF EXISTS pf_agg_final_no_exe_type_manual_update;
CREATE TABLE pf_agg_final_no_exe_type_manual_update AS 
SELECT
  brand
  , pfcampaign
  , rate_type
  , dimtype
  , dim
  , imps
  , cost
  , ss_cost
  , pfscore
  , scaled_imps
  , scaled_cost
  , scaled_ss_cost
  , scaled_pfscore
  , scaled_pfscore/scaled_imps as impact
  , scaled_pfscore/scaled_cost as efficiency
  , scaled_pfscore/scaled_ss_cost as actual_efficiency
from
  pf_scaled_no_exe_type_manual_update
;


ALTER TABLE pf_agg_final DROP IF EXISTS PARTITION (woc_spec='2016_5');
INSERT INTO TABLE pf_agg_final PARTITION (woc_spec='2016_5')
SELECT
brand
, pfcampaign
, rate_type
, exe_type
, dimtype
, dim
, imps
, cost
, NULL as ss_cost
, pfscore
, scaled_imps
, scaled_cost
, NULL as_ss_cost
, scaled_pfscore
, impact
, efficiency
, NULL as actual_efficiency

FROM
pf_agg_final_manual_update
;


ALTER TABLE pf_agg_final_no_exe_type DROP IF EXISTS PARTITION (woc_spec='2016_5');
INSERT INTO TABLE pf_agg_final_no_exe_type PARTITION (woc_spec='2016_5')
SELECT
brand
, pfcampaign
, rate_type
, dimtype
, dim
, imps
, cost
, NULL as ss_cost
, pfscore
, scaled_imps
, scaled_cost
, NULL as_ss_cost
, scaled_pfscore
, impact
, efficiency
, NULL as actual_efficiency

FROM
pf_agg_final_no_exe_type_manual_update
;

