﻿-- DO NOT USE THIS SCRIPT.  WILL BE REMOVED IN FAVOR OF ALT, WHICH DELIVERS ACCURATE STATS.  THIS QUERY WAS CREATING DUPLICATES.
DROP TABLE IF EXISTS pf_recco_placements;

CREATE TABLE pf_recco_placements
AS
SELECT DISTINCT
  campaign_recco_dims.recco_pfcampaign
  , campaign_recco_dims.recnum
  , campaign_recco_dims.dimtype
  , campaign_recco_dims.dim_value
  , campaign_recco_dims.inc_or_dec
  , woc_recco_plcmts.pfcampaign
  , woc_recco_plcmts.site_id
  , woc_recco_plcmts.page_id
  , woc_recco_plcmts.creative_id
  , woc_recco_plcmts.siteadsize
  , woc_recco_plcmts.sitecreative
  , woc_recco_plcmts.sitetactic

FROM
  (
    SELECT
      *
    FROM
      (
        SELECT
          lower(pf_reccos.pfcampaign) as recco_pfcampaign
          , pf_reccos.recnum
          , case 
            when pf_reccos.dimtype in ('site','sitecustom') then 'sitetactic' 
            else pf_reccos.dimtype 
            end as dimtype
          , pf_reccos.increase_dim as dim_value
          , 'increase' as inc_or_dec
        FROM
          pf_reccos

        UNION ALL

        SELECT
          lower(pf_reccos.pfcampaign) as recco_pfcampaign
          , pf_reccos.recnum
          , case 
            when pf_reccos.dimtype in ('site','sitecustom') then 'sitetactic' 
            else pf_reccos.dimtype
            end as dimtype
          , pf_reccos.reduce_dim as dim_value
          , 'decrease' as inc_or_dec
        FROM
          pf_reccos
      ) campaign_recco_incs_and_decs
  ) campaign_recco_dims
JOIN
  (
    SELECT
      plcmt_delivery.pfcampaign
      ,plcmt_delivery.site_id
      ,plcmt_delivery.page_id
      ,plcmt_delivery.creative_id
      ,plcmt_delivery.siteadsize
      ,plcmt_delivery.sitecreative
      ,plcmt_delivery.sitetactic

    FROM
      plcmt_delivery
  ) woc_recco_plcmts
  on (
    woc_recco_plcmts.pfcampaign = campaign_recco_dims.recco_pfcampaign
  )
WHERE
  (
    (
      campaign_recco_dims.dimtype = 'sitetactic'
      and woc_recco_plcmts.sitetactic = campaign_recco_dims.dim_value
    )
    or
    (
      campaign_recco_dims.dimtype = 'siteadsize'
      and woc_recco_plcmts.siteadsize = campaign_recco_dims.dim_value
    )
    or
    (
      campaign_recco_dims.dimtype = 'sitecreative'
      and woc_recco_plcmts.sitecreative = campaign_recco_dims.dim_value
    )
  )
;
