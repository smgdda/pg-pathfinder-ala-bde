﻿DROP TABLE IF EXISTS pf_recco_placement_daily_stats;

CREATE TABLE pf_recco_placement_daily_stats
AS
SELECT
  campaign_recco_dims.recco_pfcampaign
  , campaign_recco_dims.recnum
  , daily_stats.event_dt
  , sum(case when campaign_recco_dims.inc_or_dec = 'increase' then daily_stats.imp_sum else cast(0 as bigint) end) as imp_sum_inc
  , sum(case when campaign_recco_dims.inc_or_dec = 'decrease' then daily_stats.imp_sum else cast(0 as bigint) end) as imp_sum_dec
  , sum(case when campaign_recco_dims.inc_or_dec = 'increase' then daily_stats.act_sum else cast(0 as bigint) end) as conv_sum_inc
  , sum(case when campaign_recco_dims.inc_or_dec = 'decrease' then daily_stats.act_sum else cast(0 as bigint) end) as conv_sum_dec

FROM
  (
    SELECT
      *
    FROM
      (
        SELECT
          lower(pf_reccos.pfcampaign) as recco_pfcampaign
          , pf_reccos.recnum
          , case 
            when pf_reccos.dimtype in ('site','sitecustom','sitecustompremium') then 'sitetactic' 
            else pf_reccos.dimtype 
            end as dimtype
          , pf_reccos.increase_dim as dim_value
          , 'increase' as inc_or_dec
        FROM
          pf_reccos

        UNION ALL

        SELECT
          lower(pf_reccos.pfcampaign) as recco_pfcampaign
          , pf_reccos.recnum
          , case 
            when pf_reccos.dimtype in ('site','sitecustom','sitecustompremium') then 'sitetactic' 
            else pf_reccos.dimtype
            end as dimtype
          , pf_reccos.reduce_dim as dim_value
          , 'decrease' as inc_or_dec
        FROM
          pf_reccos
      ) campaign_recco_incs_and_decs
  ) campaign_recco_dims

JOIN
  pf_daily_dfa_plcmt_stats_w_meta daily_stats
  on
  daily_stats.pfcampaign = campaign_recco_dims.recco_pfcampaign

WHERE
  (
    (
      campaign_recco_dims.dimtype = 'sitetactic'
      and daily_stats.site_tactic = campaign_recco_dims.dim_value
    )
    or
    (
      campaign_recco_dims.dimtype = 'siteadsize'
      and daily_stats.site_ad_size = campaign_recco_dims.dim_value
    )
    or
    (
      campaign_recco_dims.dimtype = 'sitecreative'
      and daily_stats.site_creative = campaign_recco_dims.dim_value
    )
  )

GROUP BY
  campaign_recco_dims.recco_pfcampaign
  , campaign_recco_dims.recnum
  , daily_stats.event_dt

ORDER BY
  recco_pfcampaign
  , recnum
  , event_dt
;
