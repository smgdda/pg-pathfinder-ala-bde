brand	pfcampaign	channel	campaign_name	buy_id	dlx_campaign_name	date_dropped	date_added	status_notes
Always	Discreet	eComm	PG1_YZ_124153_GMDemiEcomm_US_15_W55P_YZ_3	8758818			2015-06-29	
Always	Discreet	iMedia	PG1_YZ_124154_GMDemiIMedia_US_15_W55P_YZ_1	8767377			2015-06-29	
Bounty	Mach 7	eComm						Not yet bought in
Bounty	Mach 7	iMedia	PG1_BN_125360_GMIMEBNGMiMediaA18+FHFY1516_US_15_A18P_BNZ_1	8787173			2015-06-29	
Bounty	Mach 7	iMedia						Not yet bought in
Bounty	Mach 7	OLV	PG1_BN_125086_BNGMTrueViewSpecBuy_US_15_W2554_BNZ_1					
Bounty	Mach 7	OLV	PG1_BN_125362_GMOLVBNGMOLVA18+FHFY1516_US_15_BNZ_1	8834696			2015-06-29	
Bounty	Mach 7	OLV						Not yet bought in
Bounty	Mach 7	OLV USH	PG1_BN_125361_USHOLVBNUSHOLVHA1849FHFY1516_US_16_BNZ_HA1849_1	8834769			2015-06-29	
Bounty	Mach 7	OLV USH	pg1_bn_126247_BNUSHTrueViewSpecBuyFY1516_US_15_W2554_bnze_1	8838437			2015-06-29	
Bounty	Mach 7	OLV USH						Not yet bought in
Bounty	Mach 7							
Charmin	CoCo	OLV						Launches in AMJ
Charmin	CoCo	OLV						likely 8/1 launch will provide once created
Charmin	CoCo	OLV						Launches in JFM
Charmin	eCommerce	eComm	PG1_CN_123966_GMECOMCharmineCommGMUSH_US_15_W2554_CNQG_3	8685965			2015-06-29	
Charmin	Mega Roll	iMedia	PG1_CN_125084_CNNonUseriMediaW25-54FY1516FH_US_15/16_WM2554_CNQF_1 	8799155			2015-06-29	
Charmin	Mega Roll	iMedia						Launches in AMJ
Charmin	Mega Roll	iMedia						Launches in JFM
Charmin	Mega Roll	iMedia USH	PG1_CN_125180_CNNonUserUSHiMediaHW25-54FY1516FH_US_15_HW2554_CNQF_1 	8783754			2015-06-29	
Charmin	Mega Roll	iMedia USH						Launches in January
Charmin	Mega Roll	Mobile						Mobile
Charmin	Mega Roll	OLV	PG1_CN_125115_CNNonUserOnlineVideoTW18-49FY1516FH_US_15/16_WM1825_CNQF_1 	8799232			2015-06-29	
Charmin	Mega Roll	OLV						Launches in AMJ
Charmin	Mega Roll	OLV						Launches in JFM
Charmin	Mega Roll	OLV USH						Launches in January
Charmin	Ultra Soft	iMedia	PG1_CN_125109_CNUltraSoftiMediaW25-54FY1516FH_US_1516_WM2554_CNK_1	8787379			2015-06-29	
Charmin	Ultra Soft	iMedia						Launches in AMJ
Charmin	Ultra Soft	iMedia						Launches in JFM
Charmin	Ultra Soft	iMedia USH	PG1_CN_125177_CNUltraSoftUSHiMediaHW25-54FY1516FH_US_15_HW2554_CNK_1	8787284			2015-06-29	
Charmin	Ultra Soft	iMedia USH						Launches in January
Charmin	Ultra Soft	OLV	PG1_CN_125130_CNUltraSoftOnlineVideoW25-54FY1516_US_15/16_WM2554_CNK_1	8799240			2015-06-29	
Charmin	Ultra Strong	iMedia	PG1_CN_125671_GMIMEUltraStongiMediaW25541516FH_US_15_W2554_CNJ_1	8806755			2015-06-29	
Charmin	Ultra Strong	iMedia						Launches in AMJ
Charmin	Ultra Strong	iMedia						Launches in JFM
Charmin	Ultra Strong	iMedia USH	PG1_CN_125176_CNUltraStrongUSHiMediaHW25-54FY1516FH_US_15_HW2554_CNJ_1	8787283			2015-06-29	
Charmin	Ultra Strong	iMedia USH						Launches in AMJ
Charmin	Ultra Strong	iMedia USH						Launches in JFM
Charmin	Ultra Strong	OLV	PG1_CN_125134_TMOLVCNUltraStrongOnlineVideoTW1849FY1516FH_US_15_TW1849_CNJ_1	8787030			2015-06-29	
Charmin	Ultra Strong	OLV						Launches in AMJ
Charmin	Ultra Strong	OLV						Launches in JFM
Charmin	Ultra Strong	OLV USH	PG1_CN_125174_CNUltraStrongUSHOLVHW25-54FY1516_US_15_HW2554_CNJ_1 	8795623			2015-06-29	
Charmin	Ultra Strong							Launches in September
Covergirl	Plumpify							BH
Covergirl	Supersizer							not planned yet
Downy	Calvin							Launches in Sept - 100% TrueView
Gain	Ambrosia	eComm	PG1_GN_124175_TMPECOMecommGainAmbrosiaFY1516_US_15_W2554_GNP_1	8734981			2015-06-29	
Gain	Ambrosia	iMedia						Partners and Ios not bought in yet
Gain	Ambrosia	iMedia						Partners and Ios not bought in yet
Gain	Ambrosia	Mobile						Mobile
Gain	Ambrosia	Mobile						Mobile
Gain	Ambrosia	OLV	pg1_gn_124189_TMPOLVGainAmbrosiaTMPOLVFY1516_US_15_W2554_GNP_1	8759167			2015-06-29	
Gain	Ambrosia	OLV						Partners and Ios not bought in yet
Gain	Ambrosia	OLV USH	PG1_GN_124171_UHOLVGainAmbrosiaFY1516_US_15_W2554_GNC5_1	8758750			2015-06-29	
Gain	Ambrosia							Partners and Ios not bought in yet
Head & Shoulders	Andros	OLV	PG1_HS_125319_GMOLVHSANDROSOLV_US_15_A2554_HSBJ_1	8810740			2015-06-29	
Luvs	McLuvin	OLV	PG1_LU_125210_GMOLVLuvs_US_15_W1834_LU_1	8783524			2015-06-29	
Metamucil	Meta Base - Heart Health	iMedia	pg1_MU_124294_GMIMEMetaBaseiMedia_US_15_A2554_MU_2					
Metamucil	Meta Base - Heart Health	iMedia	pg1_MU_124296_GMGSBMetaBaseSBiMedia_US_15_A2554_MU_2	8795458			2015-06-29	
Metamucil	Meta Health Bar Yr 2	iMedia	pg1_mu_126125_gmimeMetaHealthBarGMiMediaAwareness_US_15_a2554_mua_1					
Olay	Yoga Body							
Olay	Yoga Skin							
Pampers	Runway (BabyCenter SB)	iMedia	pg1_pm_125215_TMGSBPampSwadGMBCSpecialBuyTW18341516_US_15_TW1834_PMQ6_1	8783315			2015-06-29	
Pampers	Runway (BabyCenter SB)	iMedia	pg1_pm_125246_TMGSBPampEasyUPGMBCSpecialBuyTW18341516_US_15_TW1834_pm7_1 	8786127			2015-06-29	
Pampers	Runway (BabyCenter SB)	iMedia	pg1_pm_125247_PampMegaGMBCSpecialBuyTW18341516_US_15_TW1834_pmq7_1 	8783550			2015-06-29	
Pampers	Runway (BabyCenter SB)	iMedia	pg1_pm_125248_TMGSBPampCruisGMBCSpecialBuyTW18341516_US_15_TW1834_pmq8_1 	8783148			2015-06-29	
Secret	Darla	iMedia	PG1_SC_121640_GMMMPSecretDarlaOWN_US_14_W2554_SCP1_1	8532024			2015-06-29	
Secret	Darla	iMedia	PG1_SC_124802_GMIMESecretDarlaHawkeye_US_15_W2554_SCB2_1	8767250			2015-06-29	
Secret	Darla	iMedia	PG1_SC_124818_GMGSBSecretDarlaAgile_US_15_W2554_SCB2_1	8768308			2015-06-29	
Secret	Darla	OLV	PG1_SC_124808_GMOLVSecretDarlaOLV_US_15_W2554_SCB2_1	8767271			2015-06-29	
Tide	He-Man	iMedia	PG1_TB_124936_GMIMEHeManFHiMediaPAH_US_15_W2554_TBM1_3	8767103			2015-06-29	
Tide	He-Man	OLV	PG1_TB_124937_GMOLVHeManPAH_US_15_W2554_TBM1_3	8766914			2015-06-29	
Tide	Jupiler	iMedia	PG1_TB_125842_GMIMETideUSRTargetSequentialiMedia_US_15_W2554_TBBN_3	8821527			2015-06-29	
Tide	Jupiler	iMedia						Not yet bought in � launching in August
Tide	Jupiler	Mobile	PG1_TB_124938_GMMOBTIDEJUPILER_US_15_W2554_TBBN_3					
Tide	Jupiler	Mobile	PG1_TB_124939_UHMOBTIDEJUPILER_US_15_HW2554_TBBN_3					
Tide	Jupiler	OLV						Not yet bought in � launching in August
Tide	Jupiler	OLV USH	PG1_TB_124935_UHOLVTIDEJUPILER_US_15_W2554_TBBN_3	8768290			2015-06-29	
Tide	Springsteen/Diamond +	eComm	PG1_TB_124878_ECOMSPRINGSTEEN_US_15_W2554_TBM2_3	8767334			2015-06-29	
Tide	Springsteen/Diamond +	eComm	PG1_TB_124928_GMECOMDiamond+ecomm_US_15_W2554_TBM4_3	8766903			2015-06-29	
Tide	Springsteen/Diamond +	eComm						Not yet bought in � launching later in the year
Tide	Springsteen/Diamond +	iMedia	PG1_TB_124843_GMIMEDiamond+iMedia_US_15_W2554_TBM4_3	8766852			2015-06-29	
Tide	Springsteen/Diamond +	iMedia	PG1_TB_124868_GMIMESPRINGSTEEN_US_15_W2554_TBM2_3	8766900			2015-06-29	
Tide	Springsteen/Diamond +	Mobile	PG1_TB_124164_GMMOBCatSpringsteen_US_15_W2554_TBM2_3					
Tide	Springsteen/Diamond +	Mobile	PG1_TB_124881_UHMOBSPRINGSTEEN_US_15_W2554_TBM2_3					
Tide	Springsteen/Diamond +	Mobile						Not yet bought in
Tide	Springsteen/Diamond +	OLV	PG1_TB_124876_GMOLVSPRINGSTEEN_US_15_W2554_TBM2_3	8768277			2015-06-29	
Tide	Springsteen/Diamond +	OLV	PG1_TB_124877_GMOLVDiamond+FH1516_US_15_W2554_TBM4_3	8768260			2015-06-29	
Tide	Springsteen/Diamond +	OLV						Not yet bought in
Tide	Springsteen/Diamond +	OLV USH	PG1_TB_124880_UHOLVSPRINGSTEEN_US_15_W2554_TBM2_3	8767284			2015-06-29	
Tide	Springsteen/Diamond +	OLV USH	PG1_TB_124909_UHOLVDiamond+FHUSH_US_15_W2554_TBM4_3	8767076			2015-06-29	
Tide	Springsteen/Diamond+	iMedia						Not yet bought in � launching later in the year
Tide	Springsteen/Diamond+	Mobile	PG1_TB_124163_GMMOBCatalinaD+_US_15_W2554_TBM4_3					
Tide	Springsteen/Diamond+	Mobile	PG1_TB_124926_UHMOBDiamond+FHUSHMobile_US_15_W2554_TBM4_3					
Vicks	ZzzQuil	eComm	PG1_NC_124054_GMZzzQuilGMEcommA2554FY1415_US_14_A2554_NC12_1	8701960			2015-06-29	
Vicks	ZzzQuil	iMedia	pg1_nc_124906_GMIMEZzzQuilGMAwareiMedia_US_15_A1849_nc12_1 	8767186			2015-06-29	
Vicks	ZzzQuil	iMedia						
Vicks	ZzzQuil	iMedia USH	PG1_NC_125041_UHIMEZzzQuilUSHAwareiMediaHA1849FY1516_US_15_A1849_NC12_1 	8768392			2015-06-29	
Vicks	ZzzQuil	Mobile						
Vicks	ZzzQuil	Mobile						
Vicks	ZzzQuil	OLV	PG1_NC_125025_TMPOLVZzzQuilTMPOLVTA1854FY1516_US_15_TA1854_NC12_1	8771800			2015-06-29	
								
