﻿DROP TABLE IF EXISTS woc_process_log;
CREATE TABLE woc_process_log
AS

SELECT * FROM
(
  SELECT
    'logs_pfcamp_matched' as source_table
    , pfcampaign
    , count(*) as imp_ct
    , count(distinct dfa_user_id) as user_ct
    , NULL as conv_imp_ct
    , NULL as conv_user_ct

  FROM
    logs_pfcamp_matched

  WHERE
    event_dt >= '2015-07-07'
    and event_dt <= '2015-09-05'

  GROUP BY
    pfcampaign

UNION ALL

  SELECT
    'logs_ordinal' as source_table
    , pfcampaign
    , count(*) as imp_ct
    , count(distinct dfa_user_id) as user_ct
    , NULL as conv_imp_ct
    , NULL as conv_user_ct

  FROM
    logs_ordinal

  GROUP BY
    pfcampaign

UNION ALL

  SELECT
    'log_conv' as source_table
    , log_conv.pfcampaign
    , count(*) as imp_ct
    , count(distinct dfa_user_id) as user_ct
    , sum(conv_user) as conv_imp_ct
    , conv_user_stats.conv_user_ct

  FROM
    log_conv

  LEFT OUTER JOIN
    (
      SELECT
        pfcampaign as conv_campaign
        , count(distinct dfa_user_id) as conv_user_ct
      FROM
        log_conv
      WHERE
        conv_user = 1
      GROUP BY
        pfcampaign
    ) conv_user_stats
  on conv_user_stats.conv_campaign = log_conv.pfcampaign

  GROUP BY
    log_conv.pfcampaign
    , conv_user_stats.conv_user_ct
) all_tables
;
