﻿set hive.exec.dynamic.partition=true;
set hive.exec.dynamic.partition.mode=nonstrict;
set hive.exec.compress.output=true;
set hive.exec.compress.intermediate=true;

insert into table dlx_oss_impressions partition (pfcampaign)
select
  logs_product.*

from
  (
    select distinct
    regexp_replace(lower(pfcampaign), '[^a-z0-9]', '') as pf_campaign_name
    , lower(dlx_campaign_name) as dlx_campaign_name
    
    from
    pfcampaign_list 
    
    where
    date_dropped = '' 
    and buy_id is not null
    and lower(dlx_campaign_name) = '${hiveconf:dlx_campaign_name}'
  ) pfcampaign_list

join
  (
    select distinct
    dlx_oss_unique_2016_3_woc_users.dfa_id
    , lower(dlx_oss_unique_2016_3_woc_users.pf_campaign) as dlx_pf_campaign_name
    from
    dlx_oss_unique_2016_3_woc_users
    where
    lower(dlx_oss_unique_2016_3_woc_users.pf_campaign) = '${hiveconf:dlx_campaign_name}'
    
  ) dlx_oss_unique_2016_3_woc_users
  on
  dlx_oss_unique_2016_3_woc_users.dlx_pf_campaign_name = pfcampaign_list.dlx_campaign_name

join
  logs_product
  on
  logs_product.pfcampaign = pfcampaign_list.pf_campaign_name
  and logs_product.dfa_user_id = dlx_oss_unique_2016_3_woc_users.dfa_id
;
