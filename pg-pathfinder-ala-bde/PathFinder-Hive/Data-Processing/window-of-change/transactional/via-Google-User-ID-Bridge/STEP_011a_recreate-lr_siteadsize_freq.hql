﻿set hive.exec.compress.intermediate=true;
set hive.exec.compress.output=true;
set mapred.output.compression.type=BLOCK;
set mapred.output.compression.codec=org.apache.hadoop.io.compress.SnappyCodec;
set mapred.compress.map.output=true;
set hive.exec.dynamic.partition=true;
set hive.exec.dynamic.partition.mode=nonstrict;

ALTER TABLE dlx_lr_siteadsize_freq DROP IF EXISTS PARTITION (pfcampaign='${hiveconf:pf_campaign_name}');

INSERT INTO TABLE dlx_lr_siteadsize_freq PARTITION(pfcampaign='${hiveconf:pf_campaign_name}')
SELECT
CASE
  WHEN dlx_recrypted_cookies.encrypted is null THEN lr_siteadsize_freq.dfa_user_id
  ELSE dlx_recrypted_cookies.encrypted
  END AS dfa_user_id
, lr_siteadsize_freq.dlx_encrypted_dfa_user_id
, lr_siteadsize_freq.siteadsize
, lr_siteadsize_freq.freq_count
, lr_siteadsize_freq.conv_user
, lr_siteadsize_freq.exe_type

FROM
lr_siteadsize_freq

LEFT OUTER JOIN
(
  select distinct
  encrypted, newly_encrypted 
  from dlx_recrypted_cookies 
  where advertiser_id = ${hiveconf:advertiser_id}
) dlx_recrypted_cookies
ON dlx_recrypted_cookies.newly_encrypted= lr_siteadsize_freq.dfa_user_id

WHERE
pfcampaign = '${hiveconf:pf_campaign_name}'
;