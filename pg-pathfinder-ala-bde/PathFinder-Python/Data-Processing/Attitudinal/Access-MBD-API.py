﻿#MBD api calls to generate consistent data inputs for BDE
#writes to a csv to hard disk
#copies file to axiom's sftp server

'''
notes:
'''

#import libs
import requests
import time
from datetime import date, timedelta
import datetime
import csv
import paramiko
import os
import re
import boto
from filechunkio import FileChunkIO

#dictionary of relevant credentials for accessing Flite API.  Loop thru this when creating reference table.
#format is "brand":["publicKey","privateKey"]
authCredentialDict = {
  #"API_Key":["D79E1770-F5E6-4C00-B36E-EDEBD23C929D"] # THIS IS THE DEMO API KEY
  "API_Key":["846F045C-0643-4627-8BB7-0EC98287660C"] # THIS IS THE PRODUCTION API KEY
  }
survey_list_url = "http://api.insightexpress.com/surveys/shared"
question_types_url = "http://api.insightexpress.com/questions/types"
survey_details_url = "http://api.insightexpress.com/surveys/SURVEY_ID"
survey_answers_url = "http://api.insightexpress.com/surveys/SURVEY_ID/responses"

#define a local path for file mgt prior to sftp upload
#localpath = os.path.dirname(__file__) + '\\'
#localpath += '\\input-data\\mbd-survey-results\\'
localpath = "Y:\\MBD-API-Data-Xfer\\"

survey_question_types_filename = "final_table_question_types_" + time.strftime("%Y%m%d") + ".tsv"
survey_results_filename = 'mbd_survey_results_' + time.strftime("%Y%m%d") + '.tsv'
survey_questions_filename = 'final_table_questions_' + time.strftime("%Y%m%d") + '.tsv'
surveys_filename = 'final_table_surveys_' + time.strftime("%Y%m%d") + '.tsv'

def get_surveys():
  print('getting survey list',end='...')
  request_session = requests.Session()
  request_session.headers.update({"X-ApiKey": authCredentialDict["API_Key"][0]})
  survey_data = request_session.get(survey_list_url)
  survey_json = survey_data.json()
  return survey_json

def get_question_types():
  print('\rgetting survey metadata...')
  request_session = requests.Session()
  request_session.headers.update({"X-ApiKey": authCredentialDict["API_Key"][0]})
  q_types_data = request_session.get(question_types_url)
  q_types_json = q_types_data.json()
  return q_types_json

def get_survey_details(survey_id):
  print('\rgetting survey details for', str(survey_id), sep=' ',end='...')
  request_session = requests.Session()
  request_session.headers.update({"X-ApiKey": authCredentialDict["API_Key"][0]})
  request_url = survey_details_url.replace("SURVEY_ID", str(survey_id))
  survey_details_data = request_session.get(request_url)
  survey_details_json = survey_details_data.json()
  return survey_details_json

def get_survey_results(survey_id):
  print('\rgetting survey results for', str(survey_id), sep=' ',end='...\n')
  request_session = requests.Session()
  request_session.headers.update({"X-ApiKey": authCredentialDict["API_Key"][0]})
  request_url = survey_answers_url.replace("SURVEY_ID", str(survey_id))
  survey_results_data = request_session.post(request_url)
  survey_results_json = survey_results_data.json()
  return survey_results_json

def compile_survey_data():
  print('\rcompiling survey data...')

  final_table_question_types = []
  final_table_question_types.append(["type_desc","type_id","type_name"])
  
  final_table_surveys = []
  final_table_surveys.append(["survey_id","survey_name","survey_access_code","survey_status","start_date","complete_date","survey_preview_url", "campaign_id", "campaign_name"])
  
  final_table_questions = []
  final_table_questions.append(["survey_id", "survey_name", "question_id", "question_type", "ordinal_position", "is_required", "skips", "skip_unless", "question_text"])

  final_table_responses = []
  final_table_responses.append(["question_id", "question_number", "respondent_id", "user_response_col_idx", "user_response_col_text", "user_response_row_idx", "user_response_row_text", "user_response_idx", "user_response_text", "status", "startTime", "completeTime"])
  
  final_table_response_options = []

  final_table_question_types = []
  question_types = get_question_types()
  for qType in question_types:
    final_table_question_types.append([qType['id'], qType['description'], qType['name']])
  write_table(final_table_question_types, survey_question_types_filename)
  print('\rquestion types retrieved...')
  
  surveys = get_surveys()
  print('\rcompile_survey_data() - surveys retrieved',end='...')
  iSurvey = 0
  for surveyInfo in surveys:
    iSurvey += 1
    print(str(iSurvey),"of",str(len(surveys)), "surveys processed", sep=" ", end="...\n\n")

    if surveyInfo["status"] >= 2:
      # status codes are below.  We are only acting on surveys that are running or completed:
      # 0 - Not yet launched
      # 1 - Pending client approval
      # 2 - Running (collecting completes)
      # 10 - Completed

      #get survey details
      print('compile_survey_data() - getting survey details:', surveyInfo["name"], sep=' ',end='...')
      survey_details = get_survey_details(surveyInfo["id"])
      final_table_surveys.append([survey_details["id"], survey_details["name"], survey_details["accessCode"], survey_details["status"], survey_details["startDate"], survey_details["completeDate"], survey_details["previewUrl"], survey_details["campaignId"], survey_details["campaignName"]])
      if "exceptionMessage" not in survey_details:
        for question in survey_details["questions"]:
          if question["type"] == 14:
            questionText = re.sub('<[^>]*>', '', question["responses"][0]["text"])
            questionText = re.sub(r'[^ \,\.\'a-zA-Z0-9\;]', '', questionText)
            questionText = re.sub('\n', ' ', questionText)
            if not (question["skipUnless"] is None):
              skipUnless = re.sub('\n', ' ', re.sub(r'[^ \,\.a-zA-Z0-9\;]', '', question["skipUnless"]))
            else:
              skipUnless = None
            final_table_questions.append([survey_details['id'], survey_details['name'], question["id"], question["type"], question["position"], question["required"], question["skips"], skipUnless, questionText])
          else:
            questionText = re.sub('<[^>]*>', '', question["label"])
            questionText = re.sub(r'[^ \,\.\'a-zA-Z0-9\;]', '', questionText)
            questionText = re.sub('\n', ' ', questionText)
            if not (question["skipUnless"] is None):
              skipUnless = re.sub('\n', ' ', re.sub(r'[^ \,\.a-zA-Z0-9\;]', '', question["skipUnless"]))
            else:
              skipUnless = None
            final_table_questions.append([survey_details['id'], survey_details['name'], question["id"], question["type"], question["position"], question["required"], question["skips"], skipUnless, questionText])
        print('\r..survey questions compiled for:', surveyInfo["name"], sep=' ', end='...')

      #get survey responses (set to True for production use)
      get_survey_responses = True
      if get_survey_responses:
        print('\rcompile_survey_data() - getting survey responses:', surveyInfo["id"], surveyInfo["name"], sep=' ',end='...\n')
        survey_responses = get_survey_results(surveyInfo["id"])
        if "exceptionMessage" not in survey_responses:
          iSurveyResponse = 0
          for response in survey_responses:
            iSurveyResponse += 1
            print("\r",str(iSurveyResponse),"of",str(len(survey_responses)), "survey_responses processed", sep=" ", end="...")
            #iQuestions = 0
            for question_response in response["Responses"]:
              #iQuestions += 1
              #print(str(iQuestions),"of",str(len(response["responses"])), "question_responses processed", sep=" ", end="\n")
              startTime = datetime.datetime.fromtimestamp(int(re.sub(r'[^0-9]', '', response['startTime']))/1000).strftime('%Y-%m-%d %H:%M:%S')
              completeTime = datetime.datetime.fromtimestamp(int(re.sub(r'[^0-9]', '', response['completeTime']))/1000).strftime('%Y-%m-%d %H:%M:%S')

              if question_response['question']["typeId"] == 1:
                responseText = re.sub('\[\%[^\]]*\%\]', '', str(question_response["response"]))
                responseText = re.sub(r'[^ \,\.\'a-zA-Z0-9\;]', '', responseText)
                responseText = re.sub('\n\r', ' ', responseText)
                final_table_responses.append([question_response['question']['id'], question_response['question']['number'], response['id'], None, None, None, None, None, responseText, response["status"], startTime, completeTime])

              elif question_response['question']["typeId"] == 3:
                responseText = re.sub('\[\%[^\]]*\%\]', '', str(question_response["text"]))
                responseText = re.sub(r'[^ \,\.\'a-zA-Z0-9\;]', '', responseText)
                responseText = re.sub('\n\r', ' ', responseText)
                final_table_responses.append([question_response['question']['id'], question_response['question']['number'], response['id'], None, None, None, None, str(question_response['response']), responseText, response["status"], startTime, completeTime])

              elif question_response['question']["typeId"] == 4:
                responseText = re.sub('\[\%[^\]]*\%\]', '', str(question_response["text"]))
                responseText = re.sub(r'[^ \,\.\'a-zA-Z0-9\;]', '', responseText)
                responseText = re.sub('\n\r', ' ', responseText)
                final_table_responses.append([question_response['question']['id'], question_response['question']['number'], response['id'], None, None, None, None, str(question_response['response']), responseText, response["status"], startTime, completeTime])

              elif question_response['question']["typeId"] == 7:
                responseText = re.sub('\[\%[^\]]*\%\]', '', str(question_response["response"]))
                responseText = re.sub(r'[^ \,\.\'a-zA-Z0-9\;]', '', responseText)
                responseText = re.sub('\n\r', ' ', responseText)
                final_table_responses.append([question_response['question']['id'], question_response['question']['number'], response['id'], None, None, None, None, None, responseText, response["status"], startTime, completeTime])

              elif question_response['question']["typeId"] == 9:
                final_table_responses.append([question_response['question']['id'], question_response['question']['number'], response['id'], None, None, None, None, None, str(question_response['response']), response["status"], startTime, completeTime])

              elif question_response['question']["typeId"] == 12:
                responseText = re.sub('\[\%[^\]]*\%\]', '', question_response["response"])
                responseText = re.sub(r'[^ \,\.\'a-zA-Z0-9\;]', '', responseText)
                responseText = re.sub('\n\r', ' ', responseText)
                final_table_responses.append([question_response['question']['id'], question_response['question']['number'], response['id'], None, None, None, None, None, responseText, response["status"], startTime, completeTime])

              elif question_response['question']["typeId"] == 2:
                responseText = re.sub('\[\%[^\]]*\%\]', '', question_response["response"])
                responseText = re.sub(r'[^ \,\.\'a-zA-Z0-9\;]', '', responseText)
                responseText = re.sub('\n\r', ' ', responseText)
                final_table_responses.append([question_response['question']['id'], question_response['question']['number'], response['id'], None, None, None, None, None, responseText, response["status"], startTime, completeTime])

              elif question_response['question']["typeId"] == 14:
                responseText = re.sub('\[\%[^\]]*\%\]', '', question_response["response"])
                responseText = re.sub(r'[^ \,\.\'a-zA-Z0-9\;]', '', responseText)
                responseText = re.sub('\n\r', ' ', responseText)
                final_table_responses.append([question_response['question']['id'], question_response['question']['number'], response['id'], None, None, None, None, None, responseText, response["status"], startTime, completeTime])

              elif question_response['question']["typeId"] == 15:
                print("question ", question_response['question']["id"], " for user ", response['id'], " type is image\n")
                # add handling for this question type : IN PROCESS
                for sub_response in question_response["responses"]:
                  final_table_responses.append([question_response['question']['id'], question_response['question']['number'], response['id'], str(sub_response["columnIndex"]), None, str(sub_response["rowIndex"]), None, None, None, response["status"], startTime, completeTime])
              
              elif question_response['question']["typeId"] == 17:
                for sub_response in question_response["responses"]:
                  columnText = re.sub('\[\%[^\]]*\%\]', '', sub_response["columnText"])
                  columnText = re.sub(r'[^ \,\.\'a-zA-Z0-9\;]', '', columnText)
                  columnText = re.sub('\n', ' ', columnText)
                  rowText = re.sub('\[\%[^\]]*\%\]', '', sub_response["rowText"])
                  rowText = re.sub(r'[^ \,\.\'a-zA-Z0-9\;]', '', rowText)
                  rowText = re.sub('\n\r', ' ', rowText)
                  final_table_responses.append([question_response['question']['id'], question_response['question']['number'], response['id'], str(sub_response["columnIndex"]), columnText, str(sub_response["rowIndex"]), rowText, None, columnText + ' ' + rowText, response["status"], startTime, completeTime])

              elif question_response['question']["typeId"] == 18:
                for sub_response in question_response["responses"]:
                  rowText = re.sub('\[\%[^\]]*\%\]', '', sub_response["rowText"])
                  rowText = re.sub(r'[^ \,\.\'a-zA-Z0-9\;]', '', rowText)
                  rowText = re.sub('\n\r', ' ', rowText)
                  final_table_responses.append([question_response['question']['id'], question_response['question']['number'], response['id'], None, None, str(sub_response["rowIndex"]), rowText, None, str(sub_response["response"]), response["status"], startTime, completeTime])

              elif question_response['question']["typeId"] == 19:
                for sub_response in question_response["responses"]:
                  responseText = re.sub('\[\%[^\]]*\%\]', '', sub_response["text"])
                  responseText = re.sub(r'[^ \,\.\'a-zA-Z0-9\;]', '', responseText)
                  responseText = re.sub('\n\r', ' ', responseText)
                  final_table_responses.append([question_response['question']['id'], question_response['question']['number'], response['id'], None, None, None, None, str(sub_response["response"]), responseText, response["status"], startTime, completeTime])

              elif question_response['question']["typeId"] == 20:
                print("question ", question_response['question']["id"], " for user ", response['id'], " type is multi-select grid\n")
                # add handling for this question type

              elif question_response['question']["typeId"] == 21:
                print("question ", question_response['question']["id"], " for user ", response['id'], " type is file upload\n")
                # add handling for this question type

              elif question_response['question']["typeId"] == 22:
                # print("question ", question_response['question']["id"], " for user ", response['id'], " type is textbox grid\n")
                # add handling for this question type
                for sub_response in question_response["responses"]:
                  responseText = re.sub('\[\%[^\]]*\%\]', '', sub_response["response"])
                  responseText = re.sub(r'[^ \,\.\'a-zA-Z0-9\;]', '', responseText)
                  responseText = re.sub('\n\r', ' ', responseText)
                  final_table_responses.append([question_response['question']['id'], question_response['question']['number'], response['id'], None, None, None, None, str(sub_response["response"]), responseText, response["status"], startTime, completeTime])


              else:
                print('hit the else statement')

          print('..survey results compiled for:', surveyInfo["name"], sep=' ', end='...\n')
        else:
          print('..error getting survey results for:', surveyInfo["name"], survey_responses["exceptionMessage"], survey_responses["exceptionType"], sep=' ', end='...\n\n')

    else:
        print('..survey not yet launched or pending client approval - skipping:', surveyInfo["name"], sep=' ', end='\n\n')

    print('.survey data compiled\n\n')
    
  write_table(final_table_questions, survey_questions_filename)
  write_table(final_table_surveys, surveys_filename)
  # write_table(final_table_question_types, "final_table_question_types_" + time.strftime("%Y%m%d") + ".tsv")

  return final_table_responses

def name_file():
	print('naming file',sep=' ',end='...\n')
	filename = survey_results_filename
	return filename

#writes data to csv with date at the end of the file
def write_table(final_table,filename):
	print('writing tsv',filename,sep=' ',end='...\n')
	with open(localpath + filename,'w',newline='') as f:
		writer = csv.writer(f, dialect='excel-tab', delimiter='\t', quoting=csv.QUOTE_NONE, quotechar='')
		writer.writerows(final_table)


def open_connection():
	print('connecting to acxiom sftp for upload...\n')
	#make connection

	transport = paramiko.Transport(('esftpi.acxiom.com'))
	transport.connect(username='eaf736', password='$054vaS$')
	sftp = paramiko.SFTPClient.from_transport(transport)

	return sftp

#execute everything

final_table = compile_survey_data()
filename = name_file()
write_table(final_table,filename)

##copy from hard drive to axiom's sftp

sftp = open_connection()

sftp.put(localpath + surveys_filename, surveys_filename)
print(localpath + surveys_filename + ' uploaded.')

sftp.put(localpath + survey_questions_filename, survey_questions_filename)
print(localpath + survey_questions_filename + ' uploaded.')

sftp.put(localpath + survey_results_filename, survey_results_filename)
print(localpath + survey_results_filename + ' uploaded.')

print ('All MBD files copied to sftp.')
#print (sftp.listdir())
