#! python
# -*- coding: cp1252 -*-


# ---------- concat_mbd_datafiles_local ---------
# Tomonori Ishikawa
# tomonori.ishikawa@smvgroup.com
#
# Concatenate MBD data files.
#
# 2015-03-02, v6: Count disquals.
# 2015-02-26, v5.2: Updated for Mar WOC.
#   12/10 - 2/8
# 2015-01-21, v5.1: Updated for Feb WOC.
# 2014-12-11, v5: Updated for Jan WOC.
# 2014-11-21, v4: Add more campaigns, create att_response.
# 2014-11-13, v3: Check against MBD-DFA matchtable based on DFA activity.
#   Concatenation-only routines removed.
# 2014-11-12, v2: Dec WOC. Pass multiple KPIs to final table.
#   Basic logging features.
# 2014-11-03, v1: Local version on laptop because BDE sucks.


import os, shutil, datetime

# ----- window of change parameters -----
woc = 'WOC201504'
# cutoff date -- usually the first sunday
cutoff_dt = '2015-03-20'
# softlayer datafile pull datetime - goes into raw root path name
slpull_dts = '2015-04-20-1621'.upper()

# mbd dfa matchtable datafile pull datetime or label
#mtpull_dts = '20141113_340am'.lower()
#mtpull_dts = 'woc201502'
mtpull_dts = '20150331'

# ----- old 1314 mbd survey ids -----
old1314_surveyids = ['1049159','1049165','1049842','1049843','1049844','1103473','1127179','1138706']

# ----- create datetime stamp -----
# need to import datetime
# current datetime stamp like "Sun 2002-02-02 06:30PM"
now_str = datetime.datetime.now().strftime('%a %Y-%m-%d %I:%M%p %Z')
# current datetime stamp like "20020202_0630PM", useful for filenames
nowfile_str = datetime.datetime.now().strftime('%Y%m%d_%I%M%p')
# current date stamp like "20020202"
today_str = datetime.datetime.now().strftime('%Y%m%d')

# ----- paths -----
mbd_root = r"C:\Users\orrwatso\Source\Repos\pg-pathfinder-ala-bde\pg-pathfinder-ala-bde\PathFinder-Python\Data-Processing\Attitudinal\Process-MBD"
raw_root = mbd_root + r"\MillwardBrownDigital_" + slpull_dts
#bde_root = mbd_root + "\\MBD Acxiom BDE"
local_root = mbd_root + "\MBD-Local"
match_root = mbd_root + r"\DFA-User-IDs"

oldcamp_dir = local_root + r"\FY1314-Campaigns"
newcamp_dir = local_root + r"\FY1415-Campaigns"
input_dir = local_root + r"\FY1415-Campaigns"
output_dir = local_root




# ----- load manually maintained mbd survey list -----
# includes regimen and clearblue as of woc201501
print ('----- load mbd survey list -----')
brand = {}
campaign = {}
mbdlist_fpath = local_root + "\MBD Survey List for BH1415.tsv"
mbdlist = open(mbdlist_fpath, 'r')

for ll in mbdlist:
  # skip header and any comment lines starting with a hash
  if ll.startswith('surveyid') or ll.startswith('#'):
    continue
  else:
    mbdlist_line = ll.split('\t')
    if len(mbdlist_line) == 3:
      # parse
      survid = mbdlist_line[0]
      br = mbdlist_line[1]
      camp = mbdlist_line[2].rstrip()
      
      # load into dicts with survid as key
      brand[survid] = br
      campaign[survid] = camp
mbdlist.close()
print(campaign.values())




# ----- load mbd dfa matchtable -----
# writes to dict and list
# updated for 20150115 which includes event_dt
def load_matchtable ():
  match_fpath = match_root + r'\mbd_dfa_matchtable_' + mtpull_dts + '.csv'
  matchfile = open(match_fpath, 'r')
  for ll in matchfile:
    # skip header and any comment lines starting with a hash
    if ll.startswith('"dl_member_id"') or ll.startswith('#'):
      continue
    else:
      mbdmatch_line = ll.split(',')
      if len(mbdmatch_line) == 4:
        # parse
        dl_member_id = mbdmatch_line[0].strip('"')
        mbd_survey_id = mbdmatch_line[1].strip('"')
        dfa_user_id = mbdmatch_line[2].strip('"')
        event_dt = mbdmatch_line[3].strip('"\n')
      
        # load into dicts with survid as key
        if mbd_survey_id not in old1314_surveyids and mbd_survey_id.isdigit():
          if mbd_survey_id not in survmatch_list:
            mbd_dfa_matchtable[mbd_survey_id] = {}
            survmatch_list.append(mbd_survey_id)
          else:
            mbd_dfa_matchtable[mbd_survey_id][dl_member_id] = dfa_user_id
        else:
          pass
  matchfile.close()


# --- initialize data structures and load matchtable ---
mbd_dfa_matchtable = {}
survmatch_list = []
load_matchtable()




# ----- cwd to local root -----
os.chdir(local_root)




# ----- process all files in raw root -----
print ('----- processing files in raw root -----')

def organize_raw_files ():
  print ('Moving files from:\n  %s' % raw_root)
  print ('into folders organized by MBD Survey ID. (This may take a while.)')
  all_surveyids = []
  rawdata_files = os.listdir(raw_root)
  for rd in rawdata_files:  
    # -- 7-digit mbd survey id is the first token in the filename --
    surveyid = rd.split('_')[0]

    if surveyid in old1314_surveyids:
      input_dir = oldcamp_dir
    else:
      input_dir = newcamp_dir
      all_surveyids.append(surveyid)

    # -- check if survey id subdir already exists --
    surveyid_dpath = '%s\\%s' % (input_dir, surveyid)
    if os.path.exists(surveyid_dpath):
      pass
    # -- if not then mkdir it --
    else:
      try:
        os.makedirs(surveyid_dpath)
      except:
        print ('Could not create %s subdirectory' % surveyid)
    
    # ----- move file from raw root to surveyid dpath -----
    src_fpath = raw_root + r'\\' + rd
    saveto_fpath = surveyid_dpath + r'\\' + rd
    try:
      shutil.move(src_fpath, saveto_fpath)
    except:
      print ('Failed to move %s' % rd)
  print ('File moves done.')
  return all_surveyids
# --- end organize_raw_files ---


all_surveyids = organize_raw_files()




# ---------- concatenate files for 1415 camapigns ----------

# ----- create a dictionary to hold all the survey data -----
print ('----- loading data files into surveydata data structure -----')

# --- initialize dict to hold all survey data ---
surveydata = {}

# --- for all 1415, but not 1314 camapigns ---
unique_surveyids = [u for u in campaign.keys() if u not in old1314_surveyids]
for u in sorted(unique_surveyids):
  print ('-- %s %s %s --' % (u, brand[u], campaign[u]))

  # --- warn if no survey subdirectory exists - means no data files for this survey ---
  surveyid_root = '%s\\%s' % (input_dir, u)
  if not os.path.exists(surveyid_root):
    print ('  No survey data files found for %s %s %s!' % (u, brand[u], campaign[u]))
    continue

  # --- initialize dict for each survey ---
  surveydata[u] = {}
  
  # --- initialize list to hold all dl member ids within each survey ---
  surveydata[u]['all_dlmemberids'] = []

  # --- for all files in survey subdir ---
  surveydata_files = os.listdir(surveyid_root)
  for sf in surveydata_files:

    # skip column listing or survey data legend files
    if sf.endswith('_cols.lst') or sf.endswith('_legend.tsv'):
      continue

    # --- parse data files ---
    else:
      # dl member id is part of the second token in the filename
      dlmemberid = sf.split('_')[1].rstrip('.tsv')
      surveydata[u]['all_dlmemberids'].append(dlmemberid)

      # initialize a dict for each dl member id within each survey
      surveydata[u][dlmemberid] = {}

      # --- process each survey data file ---
      # one survey data file per dl member id
      surveydata_fpath = '%s\\%s' % (surveyid_root, sf)
      surveydatafile = open(surveydata_fpath, 'r')

      # count lines in each survey data file with sdi
      sdi = 1
      for sdli in surveydatafile:
        # ignore first line, but init header and data lists
        if sdi == 1:
          sdli_header = []
          sdli_data = []
          pass
        # load header from second line
        if sdi == 2:
#          sdli_header = sdli.split('\t')
          sdli_header = sdli.replace('\n','').split('\t')
        # load data from third line
        if sdi == 3:
#          sdli_data = sdli.split('\t')
          sdli_data = sdli.replace('\n','').split('\t')
        sdi += 1

      # close survey data file
      surveydatafile.close()

      # --- load data dict --- 

      # check number of header fields versus number of data files
      headerlen = len(sdli_header)
      datalen = len(sdli_data)
      if headerlen > datalen:
        print ('  Header (%d) has more fields than data (%d) in %s' % (headerlen, datalen, sf))
      elif headerlen < datalen:
        print ('  Header (%d) has fewer fields than data (%d) in %s' % (headerlen, datalen, sf))
      
      # load header = data key-value pair into dict
      else:
        for hj in range(headerlen):
#          sdli_header[hj] = sdli_header[hj].replace('\n','')
#          sdli_data[hj] = sdli_data[hj].replace('\n','')
          surveydata[u][dlmemberid][sdli_header[hj]] = sdli_data[hj]
print ('----- done loading surveydata dict -----\n')




# ---- write a kpi-coded concatenated datafile -----
def write_kpicoded_concat_datafile (mbdsurveyid_str, kpivarvalues, attrespvar):
  # Write out multiple encoded KPIs.
  concat_start_msg = '----- %s %s %s -----' % (mbdsurveyid_str,
    brand[mbdsurveyid_str], campaign[mbdsurveyid_str])
  logfile.write(concat_start_msg + '\n')
  print (concat_start_msg)

  # --- make a list of dynamic logic variable names - original to survey data file ---
  # initialize with respondent-characteristic variables
  # SURVEY_DT has been recoded from DATE1 to make it sortable and comparable
  dlvarnames = ['AGE','MEMBER_ID','SURVEY_DT','EXPOSED_CONTROL']  
  # append original kpi items
  orig_kpis = [kk for k in kpivarvalues.keys() for kk in kpivarvalues[k].keys()]
  dlvarnames += orig_kpis
  # append dichotomized/encoded kpi items
  enc_kpis = kpivarvalues.keys()
  dlvarnames += enc_kpis
  dlvarnames += ['dfa_user_id']
  dlvarnames += ['att_response']

#  print '\n'.join(dlvarnames)

  # --- open output file ---
  concat_fpath = '%s\\%s_all_%s.tsv' % (output_dir, mbdsurveyid_str, woc.lower().strip(' '))
  concat_datafile = open(concat_fpath, 'w')

  # --- write header row ---
  header_out = '\t'.join(dlvarnames)
#  print header_out
  concat_datafile.write(header_out.lower() + '\n')

  # --- initialize expo-ctrl counts ---
  nexpo_nodfa = 0
  nexpo = 0
  nctrl = 0
  ndq = 0

  # --- write data rows ---
  for dlmi in surveydata[mbdsurveyid_str]['all_dlmemberids']:

    # --- skip if no exposed_control field ---
    # needed for febreze wygs
    if 'EXPOSED_CONTROL' not in surveydata[mbdsurveyid_str][dlmi].keys():
      continue
    # --- skip if not exposed or control ---
    # 1=exposed, 2=control
    if surveydata[mbdsurveyid_str][dlmi]['EXPOSED_CONTROL'] not in ['1','2']:
      ndq += 1
      continue

    # --- create survey_dt in sortable, comparable YYYY-MM-DD format ---
    survey_date = surveydata[mbdsurveyid_str][dlmi]['DATE1'].split('/')
    survey_dt = '%s-%s-%s' % (survey_date[2],survey_date[0],survey_date[1])
    surveydata[mbdsurveyid_str][dlmi]['SURVEY_DT'] = survey_dt
    
    # --- skip if after cutoff date ---
    if survey_dt > cutoff_dt:
      continue

    # --- dichotomize orig kpis to create enc kpis ---
    for enc_kpi in kpivarvalues.keys():
      for oname,ovalues in kpivarvalues[enc_kpi].iteritems():
        if surveydata[mbdsurveyid_str][dlmi][oname] == '':
          surveydata[mbdsurveyid_str][dlmi][enc_kpi] = ''
        elif surveydata[mbdsurveyid_str][dlmi][oname] in ovalues:
          surveydata[mbdsurveyid_str][dlmi][enc_kpi] = '1'
        else:
          surveydata[mbdsurveyid_str][dlmi][enc_kpi] = '0'
#      print oname, surveydata[mbdsurveyid_str][dlmi][oname], surveydata[mbdsurveyid_str][dlmi][enc_kpi], enc_kpi
  
    # --- assign att_response ---
    surveydata[mbdsurveyid_str][dlmi]['att_response'] = surveydata[mbdsurveyid_str][dlmi][attrespvar]

    # --- match dl member id to dfa user id ---
    # match assigns the dfa_user_id, no match leaves an empty string for dfa user id
    matched_dlmemberids = mbd_dfa_matchtable[mbdsurveyid_str].keys()
#    print matched_dlmemberids
    if dlmi in matched_dlmemberids:
#      surveydata[mbdsurveyid_str][dlmi]['dfa_user_id'] = '"%s"' % mbd_dfa_matchtable[mbdsurveyid_str][dlmi]
      surveydata[mbdsurveyid_str][dlmi]['dfa_user_id'] = mbd_dfa_matchtable[mbdsurveyid_str][dlmi]
    else:
      surveydata[mbdsurveyid_str][dlmi]['dfa_user_id'] = ''

    # --- count expo ctrl ---
    if surveydata[mbdsurveyid_str][dlmi]['EXPOSED_CONTROL'] == '1':
      nexpo += 1
      if surveydata[mbdsurveyid_str][dlmi]['dfa_user_id'] < '1':
        nexpo_nodfa += 1
        continue
    elif surveydata[mbdsurveyid_str][dlmi]['EXPOSED_CONTROL'] == '2':
      nctrl += 1
    else:
      pass
    
    # --- join and write data row ---
    data_out = '\t'.join([surveydata[mbdsurveyid_str][dlmi][dok] for dok in dlvarnames])
#    print data_out
    concat_datafile.write(data_out + '\n')

  # -- tidy up and finish --
  concat_datafile.flush()
  concat_datafile.close()
  concat_done_msg = 'Concatenating data files ... done.'
  logfile.write(concat_done_msg + '\n')
  print (concat_done_msg)

  # --- print count summary ---
  ntotal_valid = nctrl + nexpo - nexpo_nodfa
  logout_str = '''control = %d, exposed = %d (%d exposed have no matching dfa_user_id--not useful for log reg)
  disqual = %d
  => %d obs available for modeling\n\n''' % (nctrl, nexpo, ndq, nexpo_nodfa, ntotal_valid)
  logfile.write(logout_str)
  print (logout_str)
# ---- end of function write_kpicoded_concat_datafile -----




# ---------- new syntax as of 11-12 ----------

# ----- create concatenation log -----
logfile_fpath = '%s\\MBD Concat KPI Creation - %s - %s.log' % (local_root, woc, today_str)
logfile = open(logfile_fpath, 'w')
log_topmsg = '''---------- concat_mbd_datafiles_local.py ----------
Version 3, running on %s
Processing files pulled from SoftLayer on %s\n\n\n''' % (now_str, slpull_dts)
logfile.write(log_topmsg)




# ---------- FH1415 ----------
# campaign codings removed




# ---------- BH1415 ----------
'''
#----- 1229926 Febreze WYGS -----
# embedded newlines problem
# _1 = cvc = car vent clips
# _2 = fr = fabric refresher
# _3 = ae = air effects
# _4 = sar = set and refresh
# _5 = noti = noticeables
write_kpicoded_concat_datafile ('1229926', dict(dheard={'AWARE_4': ['2']},
                                                dseen={'AIDEDADVAWARE_4': ['3']},
                                                dpi={'INTENT_4': ['3','4']},
                                                dheard_cvc={'AIDEDPRODAWARE_1': ['2']},
                                                dseen_cvc={'ONLINEAWAREPROD_1': ['3']},
                                                dpi_cvc={'PRODPURCHINTENT_1': ['3','4']},
                                                dheard_fr={'AIDEDPRODAWARE_2': ['2']},
                                                dseen_fr={'ONLINEAWAREPROD_2': ['3']},
                                                dpi_fr={'PRODPURCHINTENT_2': ['3','4']}),
                                'dheard_fr')


# ----- 1232696 Crest 3DW White Strips -----
write_kpicoded_concat_datafile ('1232696', dict(dheard={'AWARE_4': ['2']},
                                                dpi={'INTENT_4': ['3','4']},
                                                dfav={'OPINION_4': ['3','4']}),
                                'dpi')


# ----- 1265294 Always Demi -----
write_kpicoded_concat_datafile ('1265294', dict(dheard={'AWARE_4': ['2']},
                                                dseen={'AIDEDADVAWARE_4': ['3']},
                                                dcons={'CONSIDERINTENT_4': ['3','4']},
                                                dpi={'INTENT_4': ['3','4']},
                                                dswitch={'SWITCHBEHAVIOR_4': ['1']}),
                                'dheard')


# ----- 1269714 Metamucil Wellness Crackle -----
# there are also uncoded product heard, seen and pi items 
write_kpicoded_concat_datafile ('1269714', dict(dheard_meta={'AWARE_4': ['2']},
                                                dheard_well={'AWARE_1': ['2']},
                                                dseen_meta={'AIDEDADVAWARE_4': ['3']},
                                                dseen_well={'AIDEDADVAWARE_1': ['3']},
                                                dfav_meta={'OPINION_4': ['3','4']},
                                                dfav_well={'OPINION_1': ['3','4']},
                                                dpi_meta={'INTENT_4': ['3','4']},
                                                dpi_well={'INTENT_1': ['3','4']}),
                                'dpi_meta')
'''

# ----- 1229341 Clearblue Weeks Estimator -----
# _1 = weeks = Clearblue Digital Pregnancy Test with Weeks Estimator
# _2 = fert = Clearblue Fertility Monitor
# _3 = ovul = Clearblue Digital Ovulation Test
# _4 = preg = Clearblue Digital Pregnancy Test
write_kpicoded_concat_datafile ('1229341',
                                dict(dheard={'AWARE_4': ['2']},
                                     dseen={'AIDEDADVAWARE_4': ['3']},
                                     dcons={'INTENT_4': ['3','4']},
                                     dheard_weeks={'PRODUCTAWARE_1': ['1']},
                                     dheard_fert={'PRODUCTAWARE_2': ['2']},
                                     dheard_ovul={'PRODUCTAWARE_3': ['3']},
                                     dheard_preg={'PRODUCTAWARE_4': ['4']}),
                                'dheard')
'''

# ----- 1225765 Crest Regimen -----
# _1 = 3dwt = 3D White Toothpaste
# _2 = bril = 3D White Brilliance Toothpaste
# _3 = proh = Pro-Health Toothpaste
# _4 = compl = Complete + Scope Toothpaste
# _5 = bbpt = 3D White Brilliance Boost Polishing Treatment
# _6 = be = Be Toothpaste
# _7 = sensi = Sensi-Relief Toothpaste
write_kpicoded_concat_datafile ('1225765', 
                                dict(dheard={'AWARE_4': ['2']},
                                     dseen={'AIDEDADVAWARE_4': ['3']},
                                     dfav={'OPINION_4': ['3','4']},
                                     dpi={'INTENT_4': ['3','4']}),
                                'dheard')


# ----- 1370055 Head & Shoulders Andros -----
# olv, survey res obj is awareness
# surveyed hero product is instant relief
# AWARE1_4 (Head & Shoulders Instant Relief)	4	I Have Heard Of
# AWARE1_4 (Head & Shoulders Instant Relief)	3	I Have Not Heard Of
# AWARE1_4 (Head & Shoulders Instant Relief)	2	Not Sure
# AWARE1_4 (Head & Shoulders Instant Relief)	1	N/A – I have never heard of this brand
# AIDEDADVAWARE_4 (Head & Shoulders Instant Relief)	3	I Have Seen
# AIDEDADVAWARE_4 (Head & Shoulders Instant Relief)	1	I Have Not Seen
# AIDEDADVAWARE_4 (Head & Shoulders Instant Relief)	2	Not Sure
# OPINION_4 (Head & Shoulders Instant Relief)	4	Very Favorable
# OPINION_4 (Head & Shoulders Instant Relief)	3	Somewhat Favorable
# OPINION_4 (Head & Shoulders Instant Relief)	2	Neutral
# OPINION_4 (Head & Shoulders Instant Relief)	1	Somewhat Unfavorable
# OPINION_4 (Head & Shoulders Instant Relief)	0	Very Unfavorable
# INTENT_4 (Head & Shoulders Instant Relief)	4	Very Likely
# INTENT_4 (Head & Shoulders Instant Relief)	3	Somewhat Likely
# INTENT_4 (Head & Shoulders Instant Relief)	2	Neutral
# INTENT_4 (Head & Shoulders Instant Relief)	1	Somewhat Unlikely
# INTENT_4 (Head & Shoulders Instant Relief)	0	Very Unlikely
write_kpicoded_concat_datafile ('1370055', 
                                dict(dheard={'AWARE1_4': ['4']},
                                     dseen={'AIDEDADVAWARE_4': ['3']},
                                     dfav={'OPINION_4': ['3','4']},
                                     dpi={'INTENT_4': ['3','4']}),
                                'dheard')


# ----- 1367828 Secret Evita -----
# olv, survey res obj is purchase intent
# surveyed hero product (they call it brand) is clear gel
# AWARE_4 (Secret Clear Gel)	2	I Have Heard Of
# AWARE_4 (Secret Clear Gel)	1	I Have Not Heard Of
# AWARE_4 (Secret Clear Gel)	0	Not Sure
# AIDEDADVAWARE_4 (Secret Clear Gel)	3	I Have Seen
# AIDEDADVAWARE_4 (Secret Clear Gel)	1	I Have Not Seen
# AIDEDADVAWARE_4 (Secret Clear Gel)	2	Not Sure
# OPINION_4 (Secret Clear Gel)	4	Very Favorable
# OPINION_4 (Secret Clear Gel)	3	Somewhat Favorable
# OPINION_4 (Secret Clear Gel)	2	Neutral
# OPINION_4 (Secret Clear Gel)	1	Somewhat Unfavorable
# OPINION_4 (Secret Clear Gel)	0	Very Unfavorable
# INTENT_4 (Secret Clear Gel)	4	Very Likely
# INTENT_4 (Secret Clear Gel)	3	Somewhat Likely
# INTENT_4 (Secret Clear Gel)	2	Neutral
# INTENT_4 (Secret Clear Gel)	1	Somewhat Unlikely
# INTENT_4 (Secret Clear Gel)	0	Very Unlikely
write_kpicoded_concat_datafile ('1367828', 
                                dict(dheard={'AWARE_4': ['2']},
                                     dseen={'AIDEDADVAWARE_4': ['3']},
                                     dfav={'OPINION_4': ['3','4']},
                                     dpi={'INTENT_4': ['3','4']}),
                                'dpi')


# ----- 1383397 Swiffer Megabrand -----
# GG 2015-02-27 856am email says use question 6 consideration
# this is brand, not product level consideration
# _1 = sweep = Sweeper
# _2 = wetj = WetJet
# _4 = dust = Dusters
# AIDEDADVAWARE1_4 (Swiffer)	3	I Have Seen
# AIDEDADVAWARE1_4 (Swiffer)	1	I Have Not Seen
# AIDEDADVAWARE1_4 (Swiffer)	2	Not Sure
# INTENT1_4 (Swiffer)	4	Very Likely
# INTENT1_4 (Swiffer)	3	Somewhat Likely
# INTENT1_4 (Swiffer)	2	Neutral
# INTENT1_4 (Swiffer)	1	Somewhat Unlikely
# INTENT1_4 (Swiffer)	0	Very Unlikely
# AWARE_1 (Swiffer Sweeper)	2	I Have Heard Of
# AWARE_1 (Swiffer Sweeper)	1	I Have Not Heard Of
# AWARE_1 (Swiffer Sweeper)	0	Not Sure
# AWARE_2 (Swiffer WetJet)	2	I Have Heard Of
# AWARE_2 (Swiffer WetJet)	1	I Have Not Heard Of
# AWARE_2 (Swiffer WetJet)	0	Not Sure
# AWARE_4 (Swiffer Dusters)	2	I Have Heard Of
# AWARE_4 (Swiffer Dusters)	1	I Have Not Heard Of
# AWARE_4 (Swiffer Dusters)	0	Not Sure
# AIDEDADVAWARE_1 (Swiffer Sweeper)	3	I Have Seen
# AIDEDADVAWARE_1 (Swiffer Sweeper)	1	I Have Not Seen
# AIDEDADVAWARE_1 (Swiffer Sweeper)	2	Not Sure
# AIDEDADVAWARE_2 (Swiffer WetJet)	3	I Have Seen
# AIDEDADVAWARE_2 (Swiffer WetJet)	1	I Have Not Seen
# AIDEDADVAWARE_2 (Swiffer WetJet)	2	Not Sure
# AIDEDADVAWARE_4 (Swiffer Dusters)	3	I Have Seen
# AIDEDADVAWARE_4 (Swiffer Dusters)	1	I Have Not Seen
# AIDEDADVAWARE_4 (Swiffer Dusters)	2	Not Sure
# OPINION_1 (Swiffer Sweeper)	4	Very Favorable
# OPINION_1 (Swiffer Sweeper)	3	Somewhat Favorable
# OPINION_1 (Swiffer Sweeper)	2	Neutral
# OPINION_1 (Swiffer Sweeper)	1	Somewhat Unfavorable
# OPINION_1 (Swiffer Sweeper)	0	Very Unfavorable
# OPINION_2 (Swiffer WetJet)	4	Very Favorable
# OPINION_2 (Swiffer WetJet)	3	Somewhat Favorable
# OPINION_2 (Swiffer WetJet)	2	Neutral
# OPINION_2 (Swiffer WetJet)	1	Somewhat Unfavorable
# OPINION_2 (Swiffer WetJet)	0	Very Unfavorable
# OPINION_4 (Swiffer Dusters)	4	Very Favorable
# OPINION_4 (Swiffer Dusters)	3	Somewhat Favorable
# OPINION_4 (Swiffer Dusters)	2	Neutral
# OPINION_4 (Swiffer Dusters)	1	Somewhat Unfavorable
# OPINION_4 (Swiffer Dusters)	0	Very Unfavorable
# INTENT_1 (Swiffer Sweeper)	4	Very Likely
# INTENT_1 (Swiffer Sweeper)	3	Somewhat Likely
# INTENT_1 (Swiffer Sweeper)	2	Neutral
# INTENT_1 (Swiffer Sweeper)	1	Somewhat Unlikely
# INTENT_1 (Swiffer Sweeper)	0	Very Unlikely
# INTENT_2 (Swiffer WetJet)	4	Very Likely
# INTENT_2 (Swiffer WetJet)	3	Somewhat Likely
# INTENT_2 (Swiffer WetJet)	2	Neutral
# INTENT_2 (Swiffer WetJet)	1	Somewhat Unlikely
# INTENT_2 (Swiffer WetJet)	0	Very Unlikely
# INTENT_4 (Swiffer Dusters)	4	Very Likely
# INTENT_4 (Swiffer Dusters)	3	Somewhat Likely
# INTENT_4 (Swiffer Dusters)	2	Neutral
# INTENT_4 (Swiffer Dusters)	1	Somewhat Unlikely
# INTENT_4 (Swiffer Dusters)	0	Very Unlikely
write_kpicoded_concat_datafile ('1383397', 
                                dict(dseen={'AIDEDADVAWARE1_4': ['3']},
                                     dcons={'INTENT1_4': ['3','4']}),
                                'dcons')


# ----- 1267885 Crest Oral-B Power Tooth Brush -----
# purchase intent objective, code brand not product
# AWARE_4 (Oral-B electric toothbrush)	2	I Have Heard Of
# AWARE_4 (Oral-B electric toothbrush)	1	I Have Not Heard Of
# AWARE_4 (Oral-B electric toothbrush)	0	Not Sure
# INTENT_4 (Oral-B electric toothbrush)	4	Very Likely
# INTENT_4 (Oral-B electric toothbrush)	3	Somewhat Likely
# INTENT_4 (Oral-B electric toothbrush)	2	Neutral
# INTENT_4 (Oral-B electric toothbrush)	1	Somewhat Unlikely
# INTENT_4 (Oral-B electric toothbrush)	0	Very Unlikely
# PRODUCTAWARE_4 (Oral-B Electric Toothbrush with Bluetooth Connectivity)	2	I Have Heard Of
# PRODUCTAWARE_4 (Oral-B Electric Toothbrush with Bluetooth Connectivity)	1	I Have Not Heard Of
# PRODUCTAWARE_4 (Oral-B Electric Toothbrush with Bluetooth Connectivity)	0	Not Sure
# AIDEDADVAWARE_4 (Oral-B Electric Toothbrush with Bluetooth Connectivity)	3	I Have Seen
# AIDEDADVAWARE_4 (Oral-B Electric Toothbrush with Bluetooth Connectivity)	1	I Have Not Seen
# AIDEDADVAWARE_4 (Oral-B Electric Toothbrush with Bluetooth Connectivity)	2	Not Sure
# PRODUCTINTENT_4 (Oral-B Electric Toothbrush with Bluetooth Connectivity)	4	Very Likely
# PRODUCTINTENT_4 (Oral-B Electric Toothbrush with Bluetooth Connectivity)	3	Somewhat Likely
# PRODUCTINTENT_4 (Oral-B Electric Toothbrush with Bluetooth Connectivity)	2	Neutral
# PRODUCTINTENT_4 (Oral-B Electric Toothbrush with Bluetooth Connectivity)	1	Somewhat Unlikely
# PRODUCTINTENT_4 (Oral-B Electric Toothbrush with Bluetooth Connectivity)	0	Very Unlikely
write_kpicoded_concat_datafile ('1267885', 
                                dict(dheard={'AWARE_4': ['2']},
                                     dpi={'INTENT_4': ['3','4']}),
                                'dpi')
'''

# ----- clean-up and finish logging -----
logfile.flush()
logfile.close()




print ('Done.')
