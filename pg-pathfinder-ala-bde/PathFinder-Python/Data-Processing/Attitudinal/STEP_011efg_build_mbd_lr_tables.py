#creates mbd_lr_site dimension tables for

import sys, os, subprocess

def main():
  new_data = mbd_lr_sitedim_freq()
  new_data.run()

class mbd_lr_sitedim_freq:

  def __init__(self):
    print ('mbd_lr_sitedim_freq init...')

  def run(self):
    print('mbd_lr_sitedim_freq.run...\n\n')

    clear_old_results = self.clear_old_mbd_lr_partitions()

    campaign_dict = self.get_campaign_list_from_db()
    for campaign in campaign_dict['records']:
      print('In process loop.  Processing: ' + campaign[0] + '...')
      self.insert_campaign_data(campaign[0])

    print('exiting mbd_lr_sitedim_freq.run')

  def clear_old_mbd_lr_partitions(self):
    hql = "alter table mbd_lr_siteadsize_freq drop partition (pfcampaign > '');"
    clear_result = self.getHiveQueryResults(hql)

    hql = "alter table mbd_lr_sitecreative_freq drop partition (pfcampaign > '');"
    clear_result = self.getHiveQueryResults(hql)

    hql = "alter table mbd_lr_sitetactic_freq drop partition (pfcampaign > '');"
    clear_result = self.getHiveQueryResults(hql)

    return clear_result

  def get_campaign_list_from_db(self):
    hql = """
      select distinct
        lower(regexp_replace(campaign, '[^a-zA-Z0-9\_]', '')) as pfcampaign
      from
      mbd_survey_list;
    """
    campaign_list_dict = self.getHiveQueryResults(hql)
    return campaign_list_dict

  def insert_campaign_data(self, pfcampaign_name):
    print ('start insert_campaign_data() for: ' + pfcampaign_name + '...\n\n')

    print ('start STEP_011e_regenerate-mbd_lr_siteadsize_freq hql...')
    process = subprocess.Popen(["hive", "-f", "STEP_011e_regenerate-mbd_lr_siteadsize_freq.hql", "-hiveconf", "pf_campaign_name=" + pfcampaign_name], stdout=subprocess.PIPE)
    data = process.communicate()[0]
    print ('end STEP_011e_regenerate-mbd_lr_siteadsize_freq sql...')

    print ('start STEP_011f_regenerate-mbd_lr_sitecreative_freq hql...')
    process = subprocess.Popen(["hive", "-f", "STEP_011f_regenerate-mbd_lr_sitecreative_freq.hql", "-hiveconf", "pf_campaign_name=" + pfcampaign_name], stdout=subprocess.PIPE)
    data = process.communicate()[0]
    print ('end STEP_011f_regenerate-mbd_lr_sitecreative_freq hql...')

    print ('start STEP_011g_regenerate-mbd_lr_sitetactic_freq hql...')
    process = subprocess.Popen(["hive", "-f", "STEP_011g_regenerate-mbd_lr_sitetactic_freq.hql", "-hiveconf", "pf_campaign_name=" + pfcampaign_name], stdout=subprocess.PIPE)
    data = process.communicate()[0]
    print ('end STEP_011g_regenerate-mbd_lr_sitetactic_freq ql...')

    print ('end insert_campaign_data() for: ' + pfcampaign_name + '...\n\n')
    return

  # runs hive query specified and returns rows (as array of arrays) as well as status
  def getHiveQueryResults(self, hql):
    return_data = {}
    return_data['rc'] = ""
    return_data['records'] = []
    
    #run the query
    print ("RUNNING HIVE QUERY, hql=" + hql)
    process = subprocess.Popen(["hive", "-e", '"' + hql + '"'], stdout=subprocess.PIPE)
    data = process.communicate()[0]
    return_data['rc'] = process.returncode
    
    # return if the query ended with an error
    if process.returncode != 0:
      return return_data
    
    # retrieve the data, if any exists
    if data:
      for row in data.split('\n'):
        if row.strip():
          return_data['records'].append(row.split('\t'))

    return return_data

if __name__ == "__main__":
  main()
