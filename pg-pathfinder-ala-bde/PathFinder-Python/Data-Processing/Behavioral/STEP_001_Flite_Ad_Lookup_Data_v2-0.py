#flite api call to generate creative id lookup table for specified campaigns
#writes to a csv to hard disk
#copies file to axiom's sftp server
'''
notes:
-each set of oauth credentials are associated with each brand in Flite's system
-flite reps send oauth creds to SMG DD&A team (C.Wallace/O.Watson)
-Shawn DeLosReyes <shawn.delosreyes@flite.com>
-Lev Pevzner <lev.pevzner@flite.com>
'''

#import libs
import requests
from requests_oauthlib import OAuth1
import time
from datetime import date, timedelta
import csv
import paramiko
import os, sys

#dictionary of relevant credentials for accessing Flite API.  Loop thru this when creating reference table.
#format is "brand":["publicKey","privateKey"]
authCredentialDict = {
  "PG_Braun":["CN66nI3jgtxr5Z4TRH1fme1tznS3EFkFHEIvevUaPRgfKBMClxVRnCkeIwKUCkMNsaxSXzcX6O9t0PdL9sawogZZYfAkRwnD","RJ1Obxusbu5Zt3uzYtCqU0ZEUKnkirLgwnmiC7JEVPXOXuqmuDqqEjOmyJokyuPO"]
  ,"PG_Clearblue":["fP0jPHyoEvZS1bw7OxWRNu5UaPooAaAK7yqhxPhSeZupL3ZXectZamGkYriW6LuLkW5x5RQvYLlDcDycsnWnTGJd0iabQocS","3ZFEkc7Lbvsm60GgE24daWLh2nyY3GETJZdfqhSckv3GciD4JLEkK0rYOU5C3oBw"]
  ,"PG_CoverGirl":["rDZmy5ebHOpJxqh8EHYuRZ3pp5rOrwsY1quSTYcyPOl2Al7JNyd0INp3vtSWC4p1sxdh0YE8EryuLX1UEPvvk0n15h6zBUuZ","z6CglZ9L45FyQGuJFPoAsczY7HiVhnc4PzlH2rwNR5VcerEBgUKkXjDskOequwCw"]
  ,"PG_CoverGirl_CAD":["WZnixPaIbhoMCVACDqV3ynQpoSNeNYjtGNL82qqwKw2rdIJ127IkhoVPd8etLjdZsLN6Dk0xSoyE9Z4qn8MpRuQpySqgkpPH","hEQQ0rPKFbuQLCg9LjrYyAWB8UHT9KuT2oCaGko3EAn4HAp5G91hgWLDtY1yB1Qx"]
  ,"PG_Metamucil":["AQDeNJcehj4fsf80B6iyD7rKMtBru7L8YZCXLcMel7Hj3gNXHDWp1h9H16bB3nUjnLAzGX331t8WilzGvcb6rhiimoVGchTC","oHvnTJFVW4qjPGekjl2rf1KTll4c00Zph7fQ0cRRekT4W2mZiV1NugIBfNA0vbUg"]
  ,"PG_OralCare":["cvskjnirf8dta1aqngklfd2tkuqjksxrs9immxundtnq4rgux91ihykmuvsmel6ceavwfh33f3x9a44bz2riuo20y6zylnbx","sa0vgN2QmNIfSDfBCTrRiKjLMeF7cugbNlL9cN9C5gUE9CBeoyZ393kUrXgNnD5v"]
  ,"PG_Pantene":["CNjE3JxfsilKEa0mLALnyff4k7bhyh7ao4gsNH8Z2G1oI76FsnIzCgUDV2bA5V6yGd0AB0N9UAL7X9SgSE3ED63X60P9mnHH","Ae8WIrpKkyxsM7ONDizuF21XcjB1ZOz0KfvQ7H0gvwTJTQ0GtyLzo2JjXbjNKUOq"]
  ,"PG_Prilosec":["DOIh0IwYsbeMiKYpOnhEnBTvEt5YMcmgABvF2XCplHHkV20aAWGma07LZsVlzpfiGO3UeEPVKNsJJvqMi79BoWQKWj8Sw4Bu","xlfUnQn5iDVdiXDKONQiPXMAjelDi3pnbXBV5uLXq89kRlQ3t4diizIGXbskxhAo"]
  ,"PG_Secret":["CgPZ4LdwjIagCckCNYPkaH9onzi6YK3nWoIAGl6ClUqMlmSnTAXtZ9gMKjhwdibPEpmtMXnHQI6uS8qWvRo7H9aYh9yhoZIE","18M5gvqGD51yz3ozF4WNWh2riJC1x7E9lVo50UIHv3m9kwbzOXzuwlsJiwwVq9TP"]
  ,"PG_StarcomMG":["oyn3xhresbffqqwfbk9zxtx6ibj8l93taniqgkatd4abqw2knftmhzgahgnapxevzsa87dyjxbcwxyfjgstr6xgrnvvf5vqf","kHPJw5NNKSwKrdGNACqgYKF4Gy6yJCZ4QXW0njFlRCQiWEhBg6ixcDv2WrtnMbP9"]
  ,"PG_Vicks":["eN1gp0CCJ9no2qmUcUmpA5pqHVLD0qKywNsGABTzN4gcdbrHKCWAYGtP0dbtjz1HkMFR0ssRVxNntKjjZzW40cXfLSL9fcsD","hVeT1nOO46u0yYSwmhkTjE5fIhUilLlJa7CpWG6N3bZAdr2NfM4WtvW4CuG50PiT"]
  ,"SMG_Bounce":["m5CivywWoU07LumLZmMN7AGTiX3gy69dZ4LAvVRlywPx0hmghRr3xpu1LgB7XnJxgACxcwTv19J6SNmoJzYxuCiFWdN3snb8","NHE99Y0ZpoqrlIIEo6qu13aglLPaNetwAEuClGbZRRf5BGwxR6CyGSLu6pmIVrKj"]
  ,"SMG_Crackle":["I9TRZpVa8WomxhDl2yl86YTdjnCUkKVQx44I43dJJp2KZAODe4qKXTch82mjP0tVkkqEVGCROSW8DvS70fFitKPwdJI2Yql3","oCe7fq0etwx9jNl3ZohsLVQ7dqclX1wzQpilIUmBt0t4KJ1VvDELcXD8HKZZxKSD"]
  ,"SMG_HeadandShoulders_DXB":["K3KGe45iK8WsRdqknYm1kN79YHiYMYLYYfM6jCirfzBsK6Y836egnZzXIjfcqtA45VCK8KcDyzegbVVXxaOyr30rNxNDhgUa","1j4PIKWyC3aSB7EfUX8SprsQhCSB41q1tBwcw9b4YvUi8JeIO0YwC7C83FK69X7A"]
  ,"SMG_Pampers":["1QuD3EacegCN9LCZZwevLjo5vbyJQfEhc6VHx1dAEYRbhoNbSmIQAYR4V1CICdFwxvd4GrJXc0nu1zEeFioW5wcqmBRuCkNt","sFqmsebgaa3tFYEo111mlIRl1D51ITm8EPVl3cieljrRRMTSucXMbsfR4CEaI2Ao"]
  ,"SMG_PG":["ZXm1plPpm2Sy1BG302NZ45HpALaztONC2JDKU8efe6irK4RPshW9yC4swgSZl6kvNR6sjVL6VYTft65BaRwn6aUkRacWnZP1","VOpp9xs0d4Rdhshu17HhWBGUvfxBmGoXw5uqU0DzSohSSKfKwXqNazcM8YZfkwwH"]
  ,"SMG_PG_Beauty":["JXukUBVNdiJEtQ812C8iiCm9POw4ZN7VoGKqQJIdb1wGHnicbCn4OCNrmWg2FNPBnU51l12RRvziDLgKYSF2zwkjPKuxVsWf","aBkqB2mfdmNvVxE8tG3fmoLQAfEFzxmOzeQizFCchbwD3PajluUICSqQJvDntFvs"]
  }

#define a local path for file mgt prior to sftp upload
localpath = os.path.dirname(__file__)
localpath = localpath + '\\input-data\\flite-ad-lookup\\'

def main():
  new_data = flite_api_call()
  new_data.run()
  
class flite_api_call:

  def __init__(self):
    print ('log_ordinals_and_conv class init')

  def run(self):
    #execute everything
    final_table = self.compile_flite_ad_table()
    filename = self.name_file()
    self.write_table(final_table,filename)

    #copy from hard drive to axiom's sftp
    sftp = self.open_connection()
    sftp.put(localpath + filename,filename)
    print ('File transferred to Acxiom SFTP:')

  def get_ad_ids(self, auth):
    print('getting ad Info',auth.client.client_secret,sep=' ',end='...\n')
    url = "http://api.flite.com/rr/v1.0/data/fetch/ad/"
    data_url = url 
    ad_data = requests.get(data_url, auth=auth).json()
    
    return ad_data

  #iterate over creative_lookup()
  def compile_flite_ad_table(self):
    print('compiling flite ads lookup...\n\n')
    final_table = []
  
    for credInfo in authCredentialDict:
      print('retrieving flite ads for',credInfo,sep=' ',end='...\n')
      current_auth = OAuth1(
        authCredentialDict[credInfo][0]
        ,authCredentialDict[credInfo][1]
        ,authCredentialDict[credInfo][0]
        ,authCredentialDict[credInfo][1]
        )
      ads = self.get_ad_ids(current_auth)
      print('flite ads retrieved for',credInfo,sep=' ',end='...\n')
      for adInfo in ads.get('resources'):
        final_table.append([adInfo['name'],adInfo['guid']])
      print('flite ads lookup compiled for',credInfo,sep=' ',end='...\n\n')
    
    print('flite ads lookup compiled\n\n')
    return final_table
  
  def name_file(self):
    print('naming file',sep=' ',end='...\n')
    filename = 'flite_lookup_' + time.strftime("%Y%m%d") + '.csv'
    return filename
  
  #writes data to csv with date at the end of the file
  def write_table(self,final_table,filename):
    print('writing csv',filename,sep=' ',end='...\n')
    with open(localpath + filename,'w',newline='') as f:
      writer = csv.writer(f)
      writer.writerows(final_table) 
  
  def open_connection(self):
    print('connecting to acxiom sftp for upload...\n')
    #make connection
    transport = paramiko.Transport(('esftpi.acxiom.com'))
    transport.connect(username='eaf736', password='$054vaS$')
    sftp = paramiko.SFTPClient.from_transport(transport)
    return sftp

if __name__ == "__main__":
  main()
