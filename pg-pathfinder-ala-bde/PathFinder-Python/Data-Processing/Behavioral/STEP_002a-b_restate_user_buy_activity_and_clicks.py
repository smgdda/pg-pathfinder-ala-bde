#creates log level ordinal table

import sys, os, subprocess

def main():
  new_data = daily_restate()
  new_data.run()

class daily_restate:

  def __init__(self):
    print ('daily_restate class init')

  def run(self):
    print('in daily_restate.run()...')

    window_specs_dict = self.get_woc_specs()
    woc_start_dt = window_specs_dict['records'][0][0]
    woc_end_dt = window_specs_dict['records'][0][1]
    
    print ('restating user_buy_activity...\n\n')
    process = subprocess.Popen(["hive", "-f", "STEP_002a_DAILY_restate_user_buy_activity.hql", "-hiveconf", "woc_start_dt=" + woc_start_dt, "-hiveconf", "woc_end_dt=" + woc_end_dt], stdout=subprocess.PIPE)
    data = process.communicate()[0]
    print ('user_buy_activity restated...\n\n')

    print ('restating user_buy_click...\n\n')
    process = subprocess.Popen(["hive", "-f", "STEP_002b_DAILY_restate_user_buy_click.hql", "-hiveconf", "woc_start_dt=" + woc_start_dt, "-hiveconf", "woc_end_dt=" + woc_end_dt], stdout=subprocess.PIPE)
    data = process.communicate()[0]
    print ('user_buy_click restated...\n\n')

    print ('restating CPA-plcmt_activities...\n\n')
    process = subprocess.Popen(["hive", "-f", "STEP_002d_DAILY_restate-CPA-plcmt_activities-for-WoC.hql", "-hiveconf", "woc_start_dt=" + woc_start_dt, "-hiveconf", "woc_end_dt=" + woc_end_dt], stdout=subprocess.PIPE)
    data = process.communicate()[0]
    print ('user_buy_activity restated...\n\n')

    print ('restating CPC-placement-clicks...\n\n')
    process = subprocess.Popen(["hive", "-f", "STEP_002e_DAILY_restate-CPC-placement-clicks-for-WoC.hql", "-hiveconf", "woc_start_dt=" + woc_start_dt, "-hiveconf", "woc_end_dt=" + woc_end_dt], stdout=subprocess.PIPE)
    data = process.communicate()[0]
    print ('user_buy_click restated...\n\n')

    print ('restating pfcampaign_reportingwindow...\n\n')
    process = subprocess.Popen(["hive", "-f", "STEP_003_pfcampaign_reportingwindow.hql", "-hiveconf", "woc_start_dt=" + woc_start_dt, "-hiveconf", "woc_end_dt=" + woc_end_dt], stdout=subprocess.PIPE)
    data = process.communicate()[0]
    print ('pfcampaign_reportingwindow restated...\n\n')

    print('exiting daily_restate.run()...')

  def get_woc_specs(self):
    # the following query should return the list of partitions to be processed
    hql = "select analysis_start_dt, analysis_end_dt from pf_analysis_window where analysis_end_dt <= TO_DATE( FROM_UNIXTIME( UNIX_TIMESTAMP() )) order by analysis_end_dt desc limit 1;"
    output_dict = self.getHiveQueryResults(hql)
    return output_dict

  # runs hive query specified and returns rows (as array of arrays) as well as status
  def getHiveQueryResults(self, hql):
    return_data = {}
    return_data['rc'] = ""
    return_data['records'] = []
    
    #run the query
    print ("RUNNING HIVE QUERY, hql=" + hql)
    process = subprocess.Popen(["hive", "-e", '"' + hql + '"'], stdout=subprocess.PIPE)
    data = process.communicate()[0]
    return_data['rc'] = process.returncode
    
    # return if the query ended with an error
    if process.returncode != 0:
      return return_data
    
    # retrieve the data, if any exists
    if data:
      for row in data.split('\n'):
        if row.strip():
          return_data['records'].append(row.split('\t'))

    return return_data

if __name__ == "__main__":
  main()
