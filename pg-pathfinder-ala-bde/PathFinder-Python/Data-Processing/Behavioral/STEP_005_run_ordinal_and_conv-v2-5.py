#creates log level ordinal table

import sys, os, subprocess

def main():
  new_data = log_ordinals()
  new_data.run()

class log_ordinals:

  def __init__(self):
    print ('log_ordinals_and_conv init...')

  def run(self):
    print('log_ordinals_and_conv.run...\n\n')

    window_specs_dict = self.get_woc_specs()
    woc_start_dt = window_specs_dict['records'][0][0]
    woc_end_dt = window_specs_dict['records'][0][1]
    
    campaign_dict = self.get_campaign_list_from_db()
    for campaign in campaign_dict['records']:
      print('In process loop.  Processing: ' + campaign[0] + '...')
      self.insert_campaign_data(campaign[0], woc_start_dt, woc_end_dt)

    print('exiting log_ordinals_and_conv.run')

  def get_campaign_list_from_db(self):
    hql = "select distinct lower(regexp_replace(pfcampaign, '[^a-zA-Z0-9\\_]', '')) as pfcampaign from pfcampaign_list where date_dropped = '' and buy_id is not null;"
    campaign_list_dict = self.getHiveQueryResults(hql)
    return campaign_list_dict

  def get_woc_specs(self):
    # the following query should return the list of partitions to be processed
    hql = "select analysis_start_dt, analysis_end_dt from pf_analysis_window where analysis_end_dt <= TO_DATE( FROM_UNIXTIME( UNIX_TIMESTAMP() )) order by analysis_end_dt desc limit 1;"
    output_dict = self.getHiveQueryResults(hql)
    return output_dict

  def insert_campaign_data(self, pfcampaign_name, woc_start_dt, woc_end_dt):
    print ('start insert_campaign_data() for: ' + pfcampaign_name + '...\n\n')

    print ('start logs_ordinal hql...')
    process = subprocess.Popen(["hive", "-f", "STEP_005a_build-logs_ordinal-v2-5.hql", "-hiveconf", "pf_campaign_name=" + pfcampaign_name, "-hiveconf", "woc_start_dt=" + woc_start_dt, "-hiveconf", "woc_end_dt=" + woc_end_dt], stdout=subprocess.PIPE)
    data = process.communicate()[0]
    print ('end logs_ordinal sql...')

    print ('start log_conv hql...')
    process = subprocess.Popen(["hive", "-f", "STEP_005b_build-log_conv-v2-5.hql", "-hiveconf", "pf_campaign_name=" + pfcampaign_name], stdout=subprocess.PIPE)
    data = process.communicate()[0]
    print ('end log_conv ql...')

    print ('end insert_campaign_data() for: ' + pfcampaign_name + '...\n\n')
    return

  # runs hive query specified and returns rows (as array of arrays) as well as status
  def getHiveQueryResults(self, hql):
    return_data = {}
    return_data['rc'] = ""
    return_data['records'] = []
    
    #run the query
    print ("RUNNING HIVE QUERY, hql=" + hql)
    process = subprocess.Popen(["hive", "-e", '"' + hql + '"'], stdout=subprocess.PIPE)
    data = process.communicate()[0]
    return_data['rc'] = process.returncode
    
    # return if the query ended with an error
    if process.returncode != 0:
      return return_data
    
    # retrieve the data, if any exists
    if data:
      for row in data.split('\n'):
        if row.strip():
          return_data['records'].append(row.split('\t'))

    return return_data

if __name__ == "__main__":
  main()
