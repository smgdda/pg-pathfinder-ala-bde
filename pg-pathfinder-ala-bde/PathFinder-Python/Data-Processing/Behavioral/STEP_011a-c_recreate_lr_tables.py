#creates log level ordinal table

import sys, os, subprocess

def main():
  new_data = lr_table_generator()
  new_data.run()

class lr_table_generator:

  def __init__(self):
    print ('lr_table_generator class init')

  def run(self):
    print('in lr_table_generator.run...')
    campaign_dict = self.get_campaign_list_from_db()
    for campaign in campaign_dict['records']:
        print('\n\nIn process loop.  Processing campaign: ' + campaign[0] + '...')
        self.insert_campaign_data(campaign[0])
    print('exiting lr_table_generator.run...')

  def get_campaign_list_from_db(self):
    hql = "select distinct lower(regexp_replace(pfcampaign, '[^a-zA-Z0-9\\_]', '')) as pfcampaign from pfcampaign_list where date_dropped = '';"
    campaign_list_dict = self.getHiveQueryResults(hql)
    return campaign_list_dict

  def insert_campaign_data(self, pfcampaign_name):
    print ('\n\nstarting insert_campaign_data: ' + pfcampaign_name)
    print ('inserting campaign: ', pfcampaign_name)

    print ('executing lr_siteadsize_freq hql==========>')
    #run the query
    process = subprocess.Popen(["hive", "-f", "STEP_011a_recreate-lr_siteadsize_freq.hql", "-hiveconf", "pf_campaign_name=" + pfcampaign_name], stdout=subprocess.PIPE)
    data = process.communicate()[0]
    print ('completed running lr_siteadsize_freq sql==========>\n\n')

    print ('executing lr_sitecreative_freq hql==========>')
    #run the query
    process = subprocess.Popen(["hive", "-f", "STEP_011b_recreate-lr_sitecreative_freq.hql", "-hiveconf", "pf_campaign_name=" + pfcampaign_name], stdout=subprocess.PIPE)
    data = process.communicate()[0]
    print ('completed running lr_sitecreative_freq sql==========>\n\n')

    print ('executing lr_sitetactic_freq hql==========>')
    #run the query
    process = subprocess.Popen(["hive", "-f", "STEP_011c_recreate-lr_sitetactic_freq.hql", "-hiveconf", "pf_campaign_name=" + pfcampaign_name], stdout=subprocess.PIPE)
    data = process.communicate()[0]
    print ('completed running lr_sitetactic_freq sql==========>\n\n')

    return

  # runs hive query specified and returns rows (as array of arrays) as well as status
  def getHiveQueryResults(self, hql):
    return_data = {}
    return_data['rc'] = ""
    return_data['records'] = []
    
    #run the query
    print ("RUNNING HIVE QUERY, hql=" + hql)
    process = subprocess.Popen(["hive", "-e", '"' + hql + '"'], stdout=subprocess.PIPE)
    data = process.communicate()[0]
    return_data['rc'] = process.returncode
    
    # return if the query ended with an error
    if process.returncode != 0:
      return return_data
    
    # retrieve the data, if any exists
    if data:
      for row in data.split('\n'):
        if row.strip():
          return_data['records'].append(row.split('\t'))

    return return_data

if __name__ == "__main__":
  main()
