﻿import pyodbc
import sys
import csv
import time
from datetime import date, timedelta
import datetime
import boto
from filechunkio import FileChunkIO
import re

class RedshiftConnector:

    def __init__(self, **kwargs):

      return super().__init__(**kwargs)

    def getWoCPlacementStatsFromSkySkraper(self):
      wocPlcmtStatsQry = """
        select
	        campaignid
	        , siteid
	        , placementid
	        , creativeid
	        , sum(impressions) as imp_sum
	        , sum(clicks) as click_sum
	        , sum(mediacost) as cost_sum

        from
	        agency_views_dcm.factimpressionsclickscost

        join
	        agency_views_dcm.dimadvertiser 
	        on (
		        agency_views_dcm.dimadvertiser.networkid = 5767
		        and agency_views_dcm.dimadvertiser.advertiserid = agency_views_dcm.factimpressionsclickscost.advertiserid
	        )

        where
	        agency_views_dcm.factimpressionsclickscost.dayseqnum >= '20160316'
        --	and agency_views_dcm.factimpressionsclickscost.dayseqnum <= TO_CHAR(date(current_date - cast('1 days' as interval)), 'YYYYMMDD')
	        and agency_views_dcm.factimpressionsclickscost.dayseqnum <= '20160531'

        group by
	        campaignid
	        , siteid
	        , placementid
	        , creativeid
      """
      wocPlcmtStats = self.conn.cursor()
      wocPlcmtStats.execute(wocPlcmtStatsQry)
      wocPlcmtStats_results = wocPlcmtStats.fetchall()
        
      file_name = "skyskraper-woc-plcmt-stats-" + time.strftime("%Y_%m_%d") + ".tsv"
      file_path = 'c:\\WoC_inputs\\'

      with open (file_path + file_name, 'w', newline='') as csvoutput:
        tsv_writer = csv.writer(csvoutput, dialect='excel-tab', delimiter='\t', quoting=csv.QUOTE_NONE, quotechar='')
        tsv_writer.writerows(wocPlcmtStats_results)

    def getWoCPlacementSpectraDataFromSkySkraper(self):
      wocPlcmtQry = """
        select distinct
          adserver_name
          , adserver_network_id
          , adserver_campaign_id
          , adserver_placement_id
          , adserver_site_id
          , campaign_id
          , campaign_name
          , campaign_reporting_type
          , digital_type
          , client_id
          , placement_id
          , placement_name
          , placement_start_date
          , placement_end_date
          , placement_rate_type
          , cast(NULL as varchar(50)) as grp_rate_type
          , rate
          , cast(NULL as FLOAT) as grp_rate
          , placement_reporting_type
          , placement_type
          , product_id
          , product_name
          , group_name
          , vendor_id
          , ordered_net
          , cast(NULL as decimal(12,2)) as grp_ordered_net
          , units
          , cast(NULL as BIGINT) as grp_units
          , actual_impressions
          , cast(NULL as BIGINT) as grp_actual_impressions

        from 
          agency_views_mbox.digital_placement

        where 
          CLIENT_ID = 'PG1'
          and lower(placement_name) not like '%cancel%'
          and placement_start_date >= '2015-05-01'
          and (group_name is NULL or group_name = '')

        union all

        select distinct
          agency_views_mbox.digital_placement.adserver_name
          , agency_views_mbox.digital_placement.adserver_network_id
          , agency_views_mbox.digital_placement.adserver_campaign_id
          , agency_views_mbox.digital_placement.adserver_placement_id
          , agency_views_mbox.digital_placement.adserver_site_id
          , agency_views_mbox.digital_placement.campaign_id
          , agency_views_mbox.digital_placement.campaign_name
          , agency_views_mbox.digital_placement.campaign_reporting_type
          , agency_views_mbox.digital_placement.digital_type
          , agency_views_mbox.digital_placement.client_id
          , agency_views_mbox.digital_placement.placement_id
          , agency_views_mbox.digital_placement.placement_name
          , agency_views_mbox.digital_placement.placement_start_date
          , agency_views_mbox.digital_placement.placement_end_date
          , agency_views_mbox.digital_placement.placement_rate_type
          , plcmt_group.placement_rate_type as grp_rate_type
          , agency_views_mbox.digital_placement.rate
          , plcmt_group.rate as grp_rate
          , agency_views_mbox.digital_placement.placement_reporting_type
          , agency_views_mbox.digital_placement.placement_type
          , agency_views_mbox.digital_placement.product_id
          , agency_views_mbox.digital_placement.product_name
          , agency_views_mbox.digital_placement.group_name
          , agency_views_mbox.digital_placement.vendor_id
          , agency_views_mbox.digital_placement.ordered_net
          , plcmt_group.ordered_net as grp_ordered_net
          , agency_views_mbox.digital_placement.units
          , plcmt_group.units as grp_units
          , agency_views_mbox.digital_placement.actual_impressions
          , plcmt_group.actual_impressions as grp_actual_impressions

        from
          agency_views_mbox.digital_placement

        left outer join
          (select * from agency_views_mbox.digital_placement where CLIENT_ID = 'PG1' and lower(placement_type) = 'placement group') plcmt_group
        on
          (
          plcmt_group.group_name = agency_views_mbox.digital_placement.group_name
          and plcmt_group.campaign_id = agency_views_mbox.digital_placement.campaign_id
          )

        where 
          agency_views_mbox.digital_placement.CLIENT_ID = 'PG1'
          and lower(agency_views_mbox.digital_placement.placement_name) not like '%cancel%'
          and agency_views_mbox.digital_placement.placement_start_date >= '2015-05-01'
          and (
            agency_views_mbox.digital_placement.group_name is not NULL 
            and agency_views_mbox.digital_placement.group_name != '' 
            and lower(agency_views_mbox.digital_placement.placement_type) != 'placement group'
          )
        ;
      """
      wocPlcmtData = self.conn.cursor()
      wocPlcmtData.execute(wocPlcmtQry)
      wocPlcmtData_results = wocPlcmtData.fetchall()
        
      file_name = "pf_spectra_placements-" + time.strftime("%Y_%m_%d") + ".tsv"
      file_path = 'c:\\WoC_inputs\\'

      with open (file_path + file_name, 'w', newline='') as csvoutput:
        tsv_writer = csv.writer(csvoutput, dialect='excel-tab', delimiter='\t', quoting=csv.QUOTE_NONE, quotechar='')
        tsv_writer.writerows(wocPlcmtData_results)

    def openSkySkraperConnection(self):

      self.conn = pyodbc.connect("Driver={Amazon Redshift (x64)}; Server=redshift.cloud.vivaki.com; Port=5439; Database=skyskraper; UID=orrin_watson; PWD=Change.me1", timeout=9600)
      self.conn.autocommit = True

    def closeConnection(self, **kwargs):

      self.conn.close()
    
def main(argv):

  # INSTANTIATE CLASS AND RUN

  rsConn = RedshiftConnector()

  # THIS CODE NEEDS TO BE KEPT TO GATHER SKYSKRAPER DATA FOR EACH WoC
  #
  print("opening SkySkraper connection...")
  rsConn.openSkySkraperConnection()
  print("getting spectra data from skyskraper...")
  rsConn.getWoCPlacementSpectraDataFromSkySkraper()
  print("getting plcmt stats from skyskraper...")
  rsConn.getWoCPlacementStatsFromSkySkraper()
  print("closing SkySkraper connection...")
  rsConn.closeConnection()
  
if __name__ == "__main__":

  main(sys.argv)
