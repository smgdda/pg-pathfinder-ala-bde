#creates log level ordinal table

import sys, os, subprocess

def main():
  new_data = pf_agg_restates()
  new_data.run()

class pf_agg_restates:

  def __init__(self):
    print ('class init')

  def run(self):
    print('in .run()...')

    window_specs_dict = self.get_woc_specs()
    woc_spec = window_specs_dict['records'][0][0]
    
    print ('restating pf_agg_final...\n\n')
    process = subprocess.Popen(["hive", "-f", "STEP_013_rebuild-pf_agg_final_tables.hql", "-hiveconf", "woc_spec=" + woc_spec], stdout=subprocess.PIPE)
    data = process.communicate()[0]
    return_data['rc'] = process.returncode
    
    # return if the query ended with an error
    if process.returncode != 0:
      print('ERROR OCCURRED.  Error Code returned: ' + str(process.returncode))
    
    print ('pf_agg_final restated...\n\n')

    print('exiting .run()...')

  def get_woc_specs(self):
    # the following query should return the list of partitions to be processed
    hql = "select concat_ws('_', cast(woc_year as string), cast(woc_month as string)) as woc_spec, analysis_end_dt from pf_analysis_window where analysis_end_dt <= TO_DATE( FROM_UNIXTIME( UNIX_TIMESTAMP() )) order by analysis_end_dt desc limit 1;"
    output_dict = self.getHiveQueryResults(hql)
    return output_dict

  # runs hive query specified and returns rows (as array of arrays) as well as status
  def getHiveQueryResults(self, hql):
    return_data = {}
    return_data['rc'] = ""
    return_data['records'] = []
    
    #run the query
    print ("RUNNING HIVE QUERY, hql=" + hql)
    process = subprocess.Popen(["hive", "-e", '"' + hql + '"'], stdout=subprocess.PIPE)
    data = process.communicate()[0]
    return_data['rc'] = process.returncode
    
    # return if the query ended with an error
    if process.returncode != 0:
      return return_data
    
    # retrieve the data, if any exists
    if data:
      for row in data.split('\n'):
        if row.strip():
          return_data['records'].append(row.split('\t'))

    return return_data

if __name__ == "__main__":
  main()
