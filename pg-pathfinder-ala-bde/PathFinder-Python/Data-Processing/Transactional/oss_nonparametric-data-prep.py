#creates mbd_lr_site dimension tables for

import sys, os, subprocess

def main():
  new_data = oss_nonparametric_data_prep()
  new_data.run()

class oss_nonparametric_data_prep:

  def __init__(self):
    print ('oss_nonparametric_data_prep init...')

  def run(self):
    print('oss_nonparametric_data_prep.run...\n\n')

    clear_old_results = self.clear_old_partitions()

    campaign_dict = self.get_campaign_list_from_db()
    for campaign in campaign_dict['records']:
      print('In process loop.  Processing: ' + campaign[0] + '...')
      self.insert_campaign_data(campaign[0])

    print('exiting oss_nonparametric_data_prep.run')

  def clear_old_partitions(self):
    hql = "alter table dlx_oss_impressions drop partition (pfcampaign > '');"
    clear_result = self.getHiveQueryResults(hql)

    return clear_result

  def get_campaign_list_from_db(self):
    hql = """
      select distinct
        lower(dlx_oss_unique_2016_3_woc_users.pf_campaign) as pf_campaign

      from
        dlx_oss_unique_2016_3_woc_users

      order by
        pf_campaign
      ;
    """
    campaign_list_dict = self.getHiveQueryResults(hql)
    return campaign_list_dict

  def insert_campaign_data(self, dlx_campaign_name):
    print ('start insert_campaign_data() for: ' + dlx_campaign_name + '...\n\n')

    print ('start oss_nonparametric-data-prep hql...')
    process = subprocess.Popen(["hive", "-f", "oss_nonparametric-data-prep.hql", "-hiveconf", "dlx_campaign_name=" + dlx_campaign_name], stdout=subprocess.PIPE)
    data = process.communicate()[0]
    print ('end oss_nonparametric-data-prep sql...')

    print ('end insert_campaign_data() for: ' + dlx_campaign_name + '...\n\n')
    return

  # runs hive query specified and returns rows (as array of arrays) as well as status
  def getHiveQueryResults(self, hql):
    return_data = {}
    return_data['rc'] = ""
    return_data['records'] = []
    
    #run the query
    print ("RUNNING HIVE QUERY, hql=" + hql)
    process = subprocess.Popen(["hive", "-e", '"' + hql + '"'], stdout=subprocess.PIPE)
    data = process.communicate()[0]
    return_data['rc'] = process.returncode
    
    # return if the query ended with an error
    if process.returncode != 0:
      return return_data
    
    # retrieve the data, if any exists
    if data:
      for row in data.split('\n'):
        if row.strip():
          return_data['records'].append(row.split('\t'))

    return return_data

if __name__ == "__main__":
  main()
