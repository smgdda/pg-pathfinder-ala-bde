#creates log level ordinal table

import sys, os, subprocess

def main():
  new_data = partition_process()
  new_data.run()

class partition_process:

  def __init__(self):
    print ('partition_process class init')

  def run(self):
    print('in partition_process.run...')
    partition_dict = self.get_partition_list()

    for partition in partition_dict['records']:
        print('\n\nIn process loop.  Processing partition: ' + partition[0] + '...')
        self.process_partition(partition[0])
    print('exiting partition_process.run...')

  def get_partition_list(self):
    hql = "select distinct recco_num from prepost2014_optimizations where lower(recco_num) != 'recco_num' and analysis_completed != 1;"
    output_dict = self.getHiveQueryResults(hql)
    return output_dict

  def process_partition(self, recco_num):
    print ('\n\nstarting partition_process.process_partition(): ' + recco_num)

    print ('executing recreate-prepost_bhv_recco_users_agg.hql ==========>')
    #run the query
    process = subprocess.Popen(["hive", "-f", "recreate-prepost_bhv_recco_users_agg.hql", "-hiveconf", "recco_num=" + recco_num], stdout=subprocess.PIPE)
    data = process.communicate()[0]
    print ('completed running recreate-prepost_bhv_recco_users_agg.hql ==========>\n\n')

    return

  # runs hive query specified and returns rows (as array of arrays) as well as status
  def getHiveQueryResults(self, hql):
    return_data = {}
    return_data['rc'] = ""
    return_data['records'] = []
    
    #run the query
    print ("RUNNING HIVE QUERY, hql=" + hql)
    process = subprocess.Popen(["hive", "-e", '"' + hql + '"'], stdout=subprocess.PIPE)
    data = process.communicate()[0]
    return_data['rc'] = process.returncode
    
    # return if the query ended with an error
    if process.returncode != 0:
      return return_data
    
    # retrieve the data, if any exists
    if data:
      for row in data.split('\n'):
        if row.strip():
          return_data['records'].append(row.split('\t'))

    return return_data

if __name__ == "__main__":
  main()
