﻿import sys, os, subprocess
import datetime
import time

def main():
  mbd_date_range_process = date_range_process()
  mbd_date_range_process.run()
  

class date_range_process:

  def __init__(self):
    print("date_range_process class init...")

  def run(self):
    print("starting date_range_process.run()...")

    self.run_date_range_process('20150630', '20151105', 3, 'prepost2014_dlx_imps_by_date_range.hql')

    print('completed date_range_process.run()...')

  def run_date_range_process(self, input_start_date, input_end_date, interval, sql_script_file):
    print("starting date_range_process.run_date_range_process(" + input_start_date + ", " + input_end_date + ", " + str(interval) + ")...")

    startDate = datetime.date(int(input_start_date[0:4]), int(input_start_date[4:6]), int(input_start_date[6:8]))
    currentEndDate = startDate + datetime.timedelta(days=int(interval))

    while startDate < datetime.date(int(input_end_date[0:4]), int(input_end_date[4:6]), int(input_end_date[6:8])):
      print(datetime.date.isoformat(startDate) + "\t" + datetime.date.isoformat(currentEndDate))

      print ('executing sql script ===>')
      #run the query
      
      print(["hive", "-f", sql_script_file, "-hiveconf", "imp_begin_dt=" + datetime.date.isoformat(startDate).replace("-",""), "-hiveconf", "imp_end_dt=" + datetime.date.isoformat(currentEndDate).replace("-","")])
      
      process = subprocess.Popen(["hive", "-f", sql_script_file, "-hiveconf", "imp_begin_dt=" + datetime.date.isoformat(startDate).replace("-",""), "-hiveconf", "imp_end_dt=" + datetime.date.isoformat(currentEndDate).replace("-","")], stdout=subprocess.PIPE)
      
      #cmd = 'hive -f ' + sql_script_file + ' -hiveconf imp_begin_dt=' + datetime.date.isoformat(startDate) + ' imp_end_dt=' + datetime.date.isoformat(currentEndDate)
      #print(cmd)
      #process = subprocess.Popen(cmd, stdin=None, stdout=subprocess.PIPE)
      
      data = process.communicate()[0]
      
      print ('completed running sql script ===>\n\n')

      #reset start and end dates for next iteration of processing loop
      startDate = currentEndDate
      currentEndDate = startDate + datetime.timedelta(days=int(interval))

    print("completed date_range_process.run_date_range_process(" + input_start_date + ", " + input_end_date + ", " + str(interval) + ")...\n\n")


if __name__ == "__main__":
  main()