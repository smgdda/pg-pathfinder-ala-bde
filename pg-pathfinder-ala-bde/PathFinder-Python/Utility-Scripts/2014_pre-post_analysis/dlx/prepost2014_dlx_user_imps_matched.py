#creates log level ordinal table

import sys, os, subprocess

def main():
  new_data = partition_process()
  new_data.run()

class partition_process:

  def __init__(self):
    print ('partition_process class init')

  def run(self):
    print('in partition_process.run...')
    partition_dict = self.get_partition_list()

    for partition in partition_dict['records']:
        print('\n\nIn process loop.  Processing partition: ' + partition[0] + '...')
        self.process_partition(partition[0])
    print('exiting partition_process.run...')

  def get_partition_list(self):
    hql = "select distinct pfcampaign from pfcampaign_list WHERE dlx_campaign_name != '' and buy_id is not NULL and date_dropped = '';"
    output_dict = self.getHiveQueryResults(hql)
    return output_dict

  def process_partition(self, partition_value):
    print ('\n\nstarting partition_process.process_partition(): ' + partition_value)

    print ('executing prepost2014_dlx_user_imps_matched.hql  ==========>')
    #run the query
    process = subprocess.Popen(["hive", "-f", "prepost2014_dlx_user_imps_matched.hql", "-hiveconf", "pf_campaign_name=" + partition_value], stdout=subprocess.PIPE)
    data = process.communicate()[0]
    print ('completed running prepost2014_dlx_user_imps_matched.hql ==========>\n\n')

    return

  # runs hive query specified and returns rows (as array of arrays) as well as status
  def getHiveQueryResults(self, hql):
    return_data = {}
    return_data['rc'] = ""
    return_data['records'] = []
    
    #run the query
    print ("RUNNING HIVE QUERY, hql=" + hql)
    process = subprocess.Popen(["hive", "-e", '"' + hql + '"'], stdout=subprocess.PIPE)
    data = process.communicate()[0]
    return_data['rc'] = process.returncode
    
    # return if the query ended with an error
    if process.returncode != 0:
      return return_data
    
    # retrieve the data, if any exists
    if data:
      for row in data.split('\n'):
        if row.strip():
          return_data['records'].append(row.split('\t'))

    return return_data

if __name__ == "__main__":
  main()
