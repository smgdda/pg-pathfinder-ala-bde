#flite api call to create placement daily aggregate stats for specified campaigns
#writes to a csv to hard disk
#does not copy file to axiom's sftp server

'''
notes:
-each set of oauth credentials are associated with each brand in Flite's system
-flite reps send oauth creds to SMG DD&A team (C.Wallace/O.Watson)
-Shawn DeLosReyes <shawn.delosreyes@flite.com>
-Lev Pevzner <lev.pevzner@flite.com>
'''

#import libs
import requests
from requests_oauthlib import OAuth1
import time
from datetime import date, timedelta
import csv
import paramiko

#dictionary of relevant credentials for accessing Flite API.  Loop thru this when creating reference table.
#format is "brand":["publicKey","privateKey"]
authCredentialDict = {
  "PG_StarcomMG": ["oyn3xhresbffqqwfbk9zxtx6ibj8l93taniqgkatd4abqw2knftmhzgahgnapxevzsa87dyjxbcwxyfjgstr6xgrnvvf5vqf","kHPJw5NNKSwKrdGNACqgYKF4Gy6yJCZ4QXW0njFlRCQiWEhBg6ixcDv2WrtnMbP9"]
  ,"PG_Clearblue":["fP0jPHyoEvZS1bw7OxWRNu5UaPooAaAK7yqhxPhSeZupL3ZXectZamGkYriW6LuLkW5x5RQvYLlDcDycsnWnTGJd0iabQocS","3ZFEkc7Lbvsm60GgE24daWLh2nyY3GETJZdfqhSckv3GciD4JLEkK0rYOU5C3oBw"]
  ,"PG_Pantene":["CNjE3JxfsilKEa0mLALnyff4k7bhyh7ao4gsNH8Z2G1oI76FsnIzCgUDV2bA5V6yGd0AB0N9UAL7X9SgSE3ED63X60P9mnHH","Ae8WIrpKkyxsM7ONDizuF21XcjB1ZOz0KfvQ7H0gvwTJTQ0GtyLzo2JjXbjNKUOq"]
  ,"PG_CoverGirl":["rDZmy5ebHOpJxqh8EHYuRZ3pp5rOrwsY1quSTYcyPOl2Al7JNyd0INp3vtSWC4p1sxdh0YE8EryuLX1UEPvvk0n15h6zBUuZ","z6CglZ9L45FyQGuJFPoAsczY7HiVhnc4PzlH2rwNR5VcerEBgUKkXjDskOequwCw"]
  ,"SMG_HeadandShoulders_DXB":["K3KGe45iK8WsRdqknYm1kN79YHiYMYLYYfM6jCirfzBsK6Y836egnZzXIjfcqtA45VCK8KcDyzegbVVXxaOyr30rNxNDhgUa","1j4PIKWyC3aSB7EfUX8SprsQhCSB41q1tBwcw9b4YvUi8JeIO0YwC7C83FK69X7A"]
  ,"PG_OralCare":["cvskjnirf8dta1aqngklfd2tkuqjksxrs9immxundtnq4rgux91ihykmuvsmel6ceavwfh33f3x9a44bz2riuo20y6zylnbx","sa0vgN2QmNIfSDfBCTrRiKjLMeF7cugbNlL9cN9C5gUE9CBeoyZ393kUrXgNnD5v"]
  ,"PG_Secret":["CgPZ4LdwjIagCckCNYPkaH9onzi6YK3nWoIAGl6ClUqMlmSnTAXtZ9gMKjhwdibPEpmtMXnHQI6uS8qWvRo7H9aYh9yhoZIE","18M5gvqGD51yz3ozF4WNWh2riJC1x7E9lVo50UIHv3m9kwbzOXzuwlsJiwwVq9TP"]
  ,"PG_Metamucil":["AQDeNJcehj4fsf80B6iyD7rKMtBru7L8YZCXLcMel7Hj3gNXHDWp1h9H16bB3nUjnLAzGX331t8WilzGvcb6rhiimoVGchTC","oHvnTJFVW4qjPGekjl2rf1KTll4c00Zph7fQ0cRRekT4W2mZiV1NugIBfNA0vbUg"]
  }

#define a local path for file mgt prior to sftp upload
localpath = '.\\Data-Processing\\Behavioral\\input-data\\flite-ad-lookup\\'

#define global variables
ad_data_all = []
report_list = []

def get_ad_reports():
  
  url = "http://api.flite.com/rr/v1.0/data/fetch/ad/"
  data_url = url 

  report_request_url = "http://api.flite.com/rr/v1.0/data/ad/"
  report_params = {
    'columns': 'placementName,placementId,adName,adId,impressions,clickthroughs'
    , 'start': '4/15/2015'
    , 'end': '4/15/2015'
    #, 'name': 'reportData'
    }

  report_status_url = "http://api.flite.com/rr/v1.0/data/report/info/"
  report_download_url = "http://api.flite.com/rr/v1.0/data/report/download/"

  ad_report_data = ''

  for credInfo in authCredentialDict:
    current_auth = OAuth1(
      authCredentialDict[credInfo][0]
      ,authCredentialDict[credInfo][1]
      ,authCredentialDict[credInfo][0]
      ,authCredentialDict[credInfo][1]
      )
    print('  -> getting ads for', credInfo, sep=' ', end='...\n')
    ad_data = requests.get(data_url, auth=current_auth).json()

    returned_items = ad_data['resources']
    print(len(returned_items),'ads retrieved',sep=' ', end='...\n')

    for returnedItem in returned_items:
      ad_data_all.append(returnedItem)

      try:
        report_data = requests.get(report_request_url + returnedItem['guid'] + ".csv", params=report_params, auth=current_auth).json()
      except:
        print('waiting 3 seconds for retry', end='...\n')
        time.sleep(3)
        report_data = requests.get(report_request_url + returnedItem['guid'] + ".csv", params=report_params, auth=current_auth).json()

      report_list.append(report_data['reportId'])
      print(report_data)

    status_response = []
    status = ''
    for report in report_list:

      while (str(status) != 'COMPLETE'):
        print('waiting 5 seconds', end='...\n')
        time.sleep(5)
        try:
          print('requesting report status',report,sep=' ', end='...\n')
          status_response = requests.get(report_status_url + report, auth=current_auth).json()
          status = status_response.get('status')
          print(status, end='...\n')
        except:
          print('REPORT NOT READY', end='...\n')

      print('time to get the report', end='...\n')
      download_response = requests.get(report_download_url + report, auth=current_auth)
      print('downloaded report', report, end='...\n')
      ad_report_data += str(download_response.text)
      print('post-processed ad report data for report', report, end='...\n')
    print('processing complete for', credInfo, end='...\n')
  return ad_report_data

#iterate over reports()
def compile_report_output():
  print('compiling report_results...\n\n')
  final_table = []
  
  for credInfo in authCredentialDict:
    print('retrieving flite ads for',credInfo,sep=' ',end='...\n')
    current_auth = OAuth1(
      authCredentialDict[credInfo][0]
      ,authCredentialDict[credInfo][1]
      ,authCredentialDict[credInfo][0]
      ,authCredentialDict[credInfo][1]
      )
    ads = get_ad_ids(current_auth)
    print('flite ads retrieved for',credInfo,sep=' ',end='...\n')
    for adInfo in ads.get('resources'):
      final_table.append([adInfo['name'],adInfo['guid']])

    print('flite ads lookup compiled for',credInfo,sep=' ',end='...\n\n')
    
  print('flite ads lookup compiled\n\n')
  return final_table

def name_file():
	print('naming file',sep=' ',end='...\n')
	filename = 'flite_lookup_' + time.strftime("%Y%m%d") + '.csv'
	return filename

#writes data to csv with date at the end of the file
def write_table(final_table,filename):
	print('writing csv',filename,sep=' ',end='...\n')
	with open(localpath + filename,'w',newline='') as f:
		writer = csv.writer(f)
		writer.writerows(final_table) 


def open_connection():
	print('connecting to acxiom sftp for upload...\n')
	#make connection
	transport = paramiko.Transport(('esftpi.acxiom.com'))
	transport.connect(username='eaf736', password='D0ud8#$F')
	sftp = paramiko.SFTPClient.from_transport(transport)
	return sftp

#execute everything

ad_report_data = get_ad_reports()

for report in report_list:
  report_data = get_report(report)

final_table = compile_report_output()
filename = name_file()
write_table(final_table,filename)

##copy from hard drive to axiom's sftp
#sftp = open_connection()
#sftp.put(localpath + filename,filename)
#print ('files on sftp:')
#print (sftp.listdir())

