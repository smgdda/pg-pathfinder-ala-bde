import sys, os, subprocess, gzip, csv, string
import datetime, time, math
from datetime import timedelta
import boto
from filechunkio import FileChunkIO
import re
import argparse
#
#these assignments should be moved into the __init__ handler
#
parser = argparse.ArgumentParser(description='Transfer DFA log files from GCS to Amazon S3.')
#parser.add_argument('LOCAL_FILE_PATH', metavar='LOCAL_FILE_PATH', type=int, nargs=1, help='')
#parser.add_argument('GCS_BUCKET_NAME', metavar='GCS_BUCKET_NAME', type=int, nargs=1, help='')
#parser.add_argument('AWS_ACCESS_KEY_ID', metavar='AWS_ACCESS_KEY_ID', type=int, nargs=1, help='')
#parser.add_argument('AWS_SECRET_ACCESS_KEY', metavar='AWS_SECRET_ACCESS_KEY', type=int, nargs=1, help='')
#parser.add_argument('AWS_ROOT_FOLDER', metavar='AWS_ROOT_FOLDER', type=int, nargs=1, help='')
#parser.add_argument('AWS_BUCKET_NAME', metavar='AWS_BUCKET_NAME', type=int, nargs=1, help='')
parser.add_argument('--START_DATE', metavar='yyyymmdd', help='START_DATE is the first date from which to process.  Default value if not specified is today - 2 days.', default=datetime.date.strftime(datetime.datetime.now() - timedelta(days=2), "%Y%m%d"))
parser.add_argument('--END_DATE', metavar='yyyymmdd', help='END_DATE is the last date through which to process.  Default value if not specified is today - 2 days.', default=datetime.date.strftime(datetime.datetime.now() - timedelta(days=2), "%Y%m%d"))
#GOOGLE_STORAGE = cli_args.GOOGLE_STORAGE
#LOCAL_FILE_PATH = cli_args.LOCAL_FILE_PATH
#GCS_BUCKET_NAME = cli_args.GCS_BUCKET_NAME
#AWS_ACCESS_KEY_ID = cli_args.AWS_ACCESS_KEY_ID
#AWS_SECRET_ACCESS_KEY = cli_args.AWS_SECRET_ACCESS_KEY
#AWS_ROOT_FOLDER = cli_args.AWS_ROOT_FOLDER
#AWS_BUCKET_NAME = cli_args.AWS_BUCKET_NAME

GOOGLE_STORAGE = 'gs'
LOCAL_FILE_PATH = "Y:\\DFA-Log-File-Xfer"
#LOCAL_FILE_PATH = "C:\\Users\\orrwatso\\Documents\\Platforms\\Google-Cloud-Services\\downloaded-log-files"
#LOCAL_FILE_PATH = "E:\\Downloads\\GCS-5767"
GCS_BUCKET_NAME = 'dfa_-5597f60609a69e687e58ac0aab51f76495d8222e'

AWS_ACCESS_KEY_ID = 'AKIAIOJMVOWVYSN4PUZA'
AWS_SECRET_ACCESS_KEY = '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'
AWS_ROOT_FOLDER = "DCM-Log-Files"
AWS_BUCKET_NAME = "smg-dda-network-5767"
DCM_NETWORK_ID = "5767"

#START_DATE = '20150201'
#END_DATE = '20150202'

def main(argv):
  cli_args = parser.parse_args(argv[1:])
  START_DATE = cli_args.START_DATE
  END_DATE = cli_args.END_DATE

  dt_rng_proc = date_range_process()
  dt_rng_proc.clean_up_unfinished_downloads()
  dt_rng_proc.run(START_DATE, END_DATE)
  

class date_range_process:
  def __init__(self):
    print("date_range_process class init...")

  def run(self, input_start_date, input_end_date):
    print("starting date_range_process.run()...")
    print("input_start_date: " + input_start_date)
    print("input_end_date: " + input_end_date)
    self.run_date_range_process(input_start_date, input_end_date)
    print('completed date_range_process.run()...')
    sys.exit(0)

  def clean_up_unfinished_downloads(self):
    file_list = os.listdir(LOCAL_FILE_PATH)
    for file_to_copy in file_list:
      if file_to_copy.find("Impression") > -1:
        file_type = "Impression"
      elif file_to_copy.find("Click") > -1:
        file_type = "Click"
      elif file_to_copy.find("Activity") > -1:
        file_type = "Activity"
      elif file_to_copy.find("advertiser.log") > -1 and file_to_copy.find("NetworkMatchtablesIC_") > -1:
        file_type = "Match-Advertiser"
      elif file_to_copy.find("advertiser.log") > -1 and file_to_copy.find("NetworkMatchtablesActivity_") > -1:
        file_type = "Match-Activity-Advertiser"
      elif file_to_copy.find("campaign.log") > -1:
        file_type = "Match-Campaign"
      elif file_to_copy.find("creative.log") > -1:
        file_type = "Match-Creative"
      elif file_to_copy.find("custom_creative_fields.log") > -1:
        file_type = "Match-Custom-Creative-Data"
      elif file_to_copy.find("page.log") > -1:
        file_type = "Match-Page"
      elif file_to_copy.find("page_cost.log") > -1:
        file_type = "Match-Page-Cost"
      elif file_to_copy.find("page_flight_cost.log") > -1:
        file_type = "Match-Page-Flight-Cost"
      elif file_to_copy.find("site.log") > -1:
        file_type = "Match-Site"
      elif file_to_copy.find("user_defined_spot_vars.log") > -1:
        file_type = "Match-User-Defined-Spot-Var"
      
      # get date from file for copy_file_from_gcs_to_s3() function
      file_date = re.search('[0-9]{2}-[0-9]{2}-[0-9]{4}',file_to_copy).group()
      file_date = datetime.datetime.strptime(file_date, '%m-%d-%Y').date()
      self.copy_file_from_gcs_to_s3(file_to_copy, file_type, file_date, True)

  def copy_file_from_gcs_to_s3(self, file_name, input_data_type, process_date, delete_local_copy):
    return_data = {}
    return_data['rc'] = ""
    return_data['records'] = []

    # copy daily files from gcs to local system
    process = subprocess.Popen(["c:\\Python27\Python", "C:\\gsutil\\gsutil", "cp", GOOGLE_STORAGE + "://" + GCS_BUCKET_NAME + "/" + file_name, LOCAL_FILE_PATH], stdout=subprocess.PIPE)
    data = process.communicate()[0]
    return_data['rc'] = process.returncode

    # return if the query ended with an error
    if return_data['rc'] != 0:
      return return_data

    #pre-process file by replacing thorn delimiters with tabs
    #input_file = gzip.open(LOCAL_FILE_PATH + "\\" + file_name, 'rb')
    #input_file_contents = input_file.read(-1)
    #input_file.close()
    ##translate file contents from bytes to utf-8, to get thorns
    #input_file_contents = input_file_contents.decode('utf-8')
    #input_file_contents = input_file_contents.replace(u"\u00FE", '\t')
    #input_file_contents = re.sub(r'[^a-zA-Z0-9\n\t]', '', input_file_contents)
    #output_file = gzip.open(LOCAL_FILE_PATH + "\\" + file_name, 'w')
    #output_file.write(bytes(input_file_contents, 'utf-8'))
    #output.close()

    # copy local file to s3
    conn = boto.connect_s3(AWS_ACCESS_KEY_ID, AWS_SECRET_ACCESS_KEY)
    s3_bucket = conn.get_bucket(AWS_BUCKET_NAME)

    #source_path = LOCAL_FILE_PATH + "\\CLEAN_" + file_name
    source_path = LOCAL_FILE_PATH + "\\" + file_name
    source_size = os.stat(source_path).st_size
    dest_path = AWS_ROOT_FOLDER + "/" + input_data_type + "/" + datetime.date.strftime(process_date, "%Y/%m/%d/") + file_name

    # set chunk size for multipart upload to 16MB and determine number of chunks
    chunk_size = 16777216
    chunk_count = int(math.ceil(source_size / float(chunk_size)))
    # initiate multipart upload
    multipart_uploader = s3_bucket.initiate_multipart_upload(dest_path)
    for i in range(chunk_count):
      print("uploading", source_path, "chunk", str(i), "of", str(chunk_count), sep=" ", end="...\n")
      offset = chunk_size * i
      bytes = min(chunk_size, source_size - offset)
      with FileChunkIO(source_path, 'r', offset=offset, bytes=bytes) as fp:
        multipart_uploader.upload_part_from_file(fp, part_num=i+1)
    multipart_uploader.complete_upload()

    # delete local copy of file, if delete_local_copy = T
    if delete_local_copy == True:
      print("ready to delete local file:", source_path, sep=" ", end="...\n")
      try:
        os.remove(source_path)
      except OSError:
        pass


  def run_date_range_process(self, input_start_date, input_end_date):
    # input_start_date must be in the following format: yyyymmdd
    # input_end_date must be in the following format: yyyymmdd
    print("starting date_range_process.run_date_range_process(" + input_start_date + ", " + input_end_date + ")...")

    startDate = datetime.date(int(input_start_date[0:4]), int(input_start_date[4:6]), int(input_start_date[6:8]))
    endDate = datetime.date(int(input_end_date[0:4]), int(input_end_date[4:6]), int(input_end_date[6:8]))

    processingDate = startDate

    while processingDate <= endDate:
      print ('\nin loop', "processing", datetime.date.strftime(processingDate, "%m-%d-%Y"), sep=" ", end="...\n")
      
      # copy daily files from gcs to s3
      
      CURRENT_FILE_NAME = "NetworkClick_" + DCM_NETWORK_ID + "_" + datetime.date.strftime(processingDate, "%m-%d-%Y") + ".log.gz"
      self.copy_file_from_gcs_to_s3(CURRENT_FILE_NAME, "Click", processingDate, True)
      
      CURRENT_FILE_NAME = "NetworkImpression_" + DCM_NETWORK_ID + "_" + datetime.date.strftime(processingDate, "%m-%d-%Y") + ".log.gz"
      self.copy_file_from_gcs_to_s3(CURRENT_FILE_NAME, "Impression", processingDate, True)

      CURRENT_FILE_NAME = "NetworkMatchtablesIC_" + DCM_NETWORK_ID + "_" + datetime.date.strftime(processingDate, "%m-%d-%Y") + "_advertiser.log.gz"
      self.copy_file_from_gcs_to_s3(CURRENT_FILE_NAME, "Match-Advertiser", processingDate, True)

      CURRENT_FILE_NAME = "NetworkMatchtablesActivity_" + DCM_NETWORK_ID + "_" + datetime.date.strftime(processingDate, "%m-%d-%Y") + "_advertiser.log.gz"
      self.copy_file_from_gcs_to_s3(CURRENT_FILE_NAME, "Match-Activity-Advertiser", processingDate, True)

      bucket_file_list = {}
      bucket_file_list['rc'] = ""
      bucket_file_list['records'] = []

      process = subprocess.Popen(["c:\\Python27\Python", "C:\\gsutil\\gsutil", "ls", GOOGLE_STORAGE + "://" + GCS_BUCKET_NAME + "/NetworkActivity*"], stdout=subprocess.PIPE)
      data = process.communicate()[0]
      bucket_file_list['rc'] = process.returncode

      # if the query ended without error
      if bucket_file_list['rc'] == 0:
        # retrieve the data, if any exists
        if data:
          file_list = data.decode('utf-8')
          file_list = file_list.replace('\r', '')
          file_list = file_list.replace(GOOGLE_STORAGE + '://' + GCS_BUCKET_NAME + '/', '')
          file_list_data = file_list.split('\n')

          for row in file_list_data:
            if row.strip():
              file_item = row.split('_')
              #file_type = file_item[0]# - this part contains the data type
              #network_id = file_item[1]# - this part contains the network id
              #advertiser_id = file_item[2]# - this part contains the advertiser id
              file_date = file_item[3].split('.')[0]# - this part contains the file date
              is_log = (file_item[3].split('.')[1] == 'log')
              if file_date == datetime.date.strftime(processingDate, "%m-%d-%Y") and is_log:
                self.copy_file_from_gcs_to_s3(row, "Activity", processingDate, True)
                
          print(bucket_file_list['records'])

      CURRENT_FILE_NAME = "NetworkMatchtablesIC_" + DCM_NETWORK_ID + "_" + datetime.date.strftime(processingDate, "%m-%d-%Y") + "_campaign.log.gz"
      self.copy_file_from_gcs_to_s3(CURRENT_FILE_NAME, "Match-Campaign", processingDate, True)

      CURRENT_FILE_NAME = "NetworkMatchtablesIC_" + DCM_NETWORK_ID + "_" + datetime.date.strftime(processingDate, "%m-%d-%Y") + "_creative.log.gz"
      self.copy_file_from_gcs_to_s3(CURRENT_FILE_NAME, "Match-Creative", processingDate, True)

      CURRENT_FILE_NAME = "NetworkMatchtablesIC_" + DCM_NETWORK_ID + "_" + datetime.date.strftime(processingDate, "%m-%d-%Y") + "_custom_creative_fields.log.gz"
      self.copy_file_from_gcs_to_s3(CURRENT_FILE_NAME, "Match-Custom-Creative-Data", processingDate, True)

      CURRENT_FILE_NAME = "NetworkMatchtablesIC_" + DCM_NETWORK_ID + "_" + datetime.date.strftime(processingDate, "%m-%d-%Y") + "_page.log.gz"
      self.copy_file_from_gcs_to_s3(CURRENT_FILE_NAME, "Match-Page", processingDate, True)

      CURRENT_FILE_NAME = "NetworkMatchtablesIC_" + DCM_NETWORK_ID + "_" + datetime.date.strftime(processingDate, "%m-%d-%Y") + "_page_cost.log.gz"
      self.copy_file_from_gcs_to_s3(CURRENT_FILE_NAME, "Match-Page-Cost", processingDate, True)

      CURRENT_FILE_NAME = "NetworkMatchtablesIC_" + DCM_NETWORK_ID + "_" + datetime.date.strftime(processingDate, "%m-%d-%Y") + "_page_flight_cost.log.gz"
      self.copy_file_from_gcs_to_s3(CURRENT_FILE_NAME, "Match-Page-Flight-Cost", processingDate, True)

      CURRENT_FILE_NAME = "NetworkMatchtablesIC_" + DCM_NETWORK_ID + "_" + datetime.date.strftime(processingDate, "%m-%d-%Y") + "_site.log.gz"
      self.copy_file_from_gcs_to_s3(CURRENT_FILE_NAME, "Match-Site", processingDate, True)

      CURRENT_FILE_NAME = "NetworkMatchtablesIC_" + DCM_NETWORK_ID + "_" + datetime.date.strftime(processingDate, "%m-%d-%Y") + "_user_defined_spot_vars.log.gz"
      self.copy_file_from_gcs_to_s3(CURRENT_FILE_NAME, "Match-User-Defined-Spot-Var", processingDate, True)
      
      # update processing date for next iteration of process loop
      print("updating processingDate for next iteration...\n")
      processingDate = processingDate + datetime.timedelta(days=1)

    print("\n\ncompleted date_range_process.run_date_range_process(" + input_start_date + ", " + input_end_date + ")...\n")

if __name__ == "__main__":
  main(sys.argv)