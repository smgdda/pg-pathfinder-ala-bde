# generates concordance table

import sys, os, subprocess

def main():
  new_data = concordance_process()
  new_data.run()

class concordance_process:

  def __init__(self):
    print ('concordance_process init...')

  def run(self):
    print('concordance_process.run...\n\n')

    window_specs_dict = self.get_woc_specs()
    woc_year = window_specs_dict['records'][0][0]
    woc_month = window_specs_dict['records'][0][1]
    woc_spec = str(woc_year) + '-' + str(woc_month)
    
    campaign_dict = self.get_campaign_list_from_db()

    for campaign in campaign_dict['records']:
      print('In process loop.  Processing: ' + campaign[0] + '...')
      self.generate_campaign_concordance(campaign[0], woc_spec)

    print('exiting concordance_process.run')

  def get_campaign_list_from_db(self):
    hql = "select distinct lower(regexp_replace(pfcampaign, '[^a-zA-Z0-9\\_]', '')) as pfcampaign from pfcampaign_list where date_dropped = '' and buy_id is not null;"
    campaign_list_dict = self.getHiveQueryResults(hql)
    return campaign_list_dict

  def get_woc_specs(self):
    # the following query should return the list of partitions to be processed
    hql = "select woc_year, woc_month, analysis_end_dt from pf_analysis_window where analysis_end_dt <= TO_DATE( FROM_UNIXTIME( UNIX_TIMESTAMP() )) order by analysis_end_dt desc limit 1;"
    output_dict = self.getHiveQueryResults(hql)
    return output_dict

  def generate_campaign_concordance(self, pf_campaign_name, woc_spec):
    print ('start insert_campaign_data() for: ' + pf_campaign_name + '...\n\n')

    print ('start generate_campaign_concordance hql...')
    process = subprocess.Popen(["hive", "-f", "calculate-pfcampaign-concordance.hql", "-hiveconf", "pf_campaign_name=" + pf_campaign_name, "-hiveconf", "woc_spec=" + woc_spec], stdout=subprocess.PIPE)
    data = process.communicate()[0]
    print ('end generate_campaign_concordance sql...')

    print ('end insert_campaign_data() for: ' + pf_campaign_name + '...\n\n')
    return

  # runs hive query specified and returns rows (as array of arrays) as well as status
  def getHiveQueryResults(self, hql):
    return_data = {}
    return_data['rc'] = ""
    return_data['records'] = []
    
    #run the query
    print ("RUNNING HIVE QUERY, hql=" + hql)
    process = subprocess.Popen(["hive", "-e", '"' + hql + '"'], stdout=subprocess.PIPE)
    data = process.communicate()[0]
    return_data['rc'] = process.returncode
    
    # return if the query ended with an error
    if process.returncode != 0:
      return return_data
    
    # retrieve the data, if any exists
    if data:
      for row in data.split('\n'):
        if row.strip():
          return_data['records'].append(row.split('\t'))

    return return_data

if __name__ == "__main__":
  main()
