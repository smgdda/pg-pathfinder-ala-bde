import sys, os, subprocess
import datetime
import time

def main():
  mbd_date_range_process = date_range_process()
  mbd_date_range_process.run()
  

class date_range_process:

  def __init__(self):
    print("date_range_process class init...")

  def run(self):
    print("starting date_range_process.run()...")
    self.run_date_range_process('20150410', '20150701', 5, 'create-moat-review-dataset.hql')
    print('completed date_range_process.run()...')

  def run_date_range_process(self, input_start_date, input_end_date, interval, sql_script_file):
    # input_start_date must be in the following format: yyyymmdd
    # input_end_date must be in the following format: yyyymmdd
    # interval must be an integer value
    # sql_script_file is the name of a hive script that takes 2 parameters: ${begin_dt} and ${end_dt}
    print("starting date_range_process.run_date_range_process(" + input_start_date + ", " + input_end_date + ", " + str(interval) + ", " + sql_script_file + ")...")

    startDate = datetime.date(int(input_start_date[0:4]), int(input_start_date[4:6]), int(input_start_date[6:8]))
    currentEndDate = startDate + datetime.timedelta(days=int(interval))

    while startDate < datetime.date(int(input_end_date[0:4]), int(input_end_date[4:6]), int(input_end_date[6:8])):
      print ('executing sql script==========>')
      #run the query
      print(["hive", "-f", sql_script_file, "-hiveconf", "begin_dt=" + datetime.date.isoformat(startDate), "-hiveconf", "end_dt=" + datetime.date.isoformat(currentEndDate)])
      
      process = subprocess.Popen(["hive", "-f", sql_script_file, "-hiveconf", "begin_dt=" + datetime.date.isoformat(startDate), "-hiveconf", "end_dt=" + datetime.date.isoformat(currentEndDate)], stdout=subprocess.PIPE)
      data = process.communicate()[0]
      
      print ('completed running sql script==========>\n\n')

      #reset start and end dates for next iteration of processing loop
      startDate = currentEndDate
      currentEndDate = startDate + datetime.timedelta(days=int(interval))
      #if the current end date is past the window we want to check, set it to the end of the window we want to check
      if currentEndDate > datetime.date(int(input_end_date[0:4]), int(input_end_date[4:6]), int(input_end_date[6:8])):
        currentEndDate = datetime.date(int(input_end_date[0:4]), int(input_end_date[4:6]), int(input_end_date[6:8]))

    print("completed date_range_process.run_date_range_process(" + input_start_date + ", " + input_end_date + ", " + str(interval) + ", " + sql_script_file + ")...\n\n")


if __name__ == "__main__":
  main()