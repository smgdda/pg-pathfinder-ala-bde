﻿import sys, os, subprocess

def main():
  new_data = tsv_generator()
  new_data.run()

class tsv_generator:
  def __init__(self):
    print ('+++ tsv_generator class init')

  def run(self):
    print('+++ start .run...')

    hql = "select distinct advertiser_id, dfa_macro_user_id_from_dlx from distinct_dlx_cookies where advertiser_id = 3232230;"
    partition_dict = self.generate_tsv(hql, "/user/owatson/csv-output-files/output/dlx_cookies-advertiser_3232230.tsv")

    hql = "select distinct advertiser_id, dfa_macro_user_id_from_dlx from distinct_dlx_cookies where advertiser_id = 3232146;"
    partition_dict = self.generate_tsv(hql, "/user/owatson/csv-output-files/output/dlx_cookies-advertiser_3232146.tsv")

    hql = "select distinct advertiser_id, dfa_macro_user_id_from_dlx from distinct_dlx_cookies where advertiser_id = 3232216;"
    partition_dict = self.generate_tsv(hql, "/user/owatson/csv-output-files/output/dlx_cookies-advertiser_3232216.tsv")

    hql = "select distinct advertiser_id, dfa_macro_user_id_from_dlx from distinct_dlx_cookies where advertiser_id = 3232122;"
    partition_dict = self.generate_tsv(hql, "/user/owatson/csv-output-files/output/dlx_cookies-advertiser_3232122.tsv")

    hql = "select distinct advertiser_id, dfa_macro_user_id_from_dlx from distinct_dlx_cookies where advertiser_id = 3232132;"
    partition_dict = self.generate_tsv(hql, "/user/owatson/csv-output-files/output/dlx_cookies-advertiser_3232132.tsv")

    hql = "select distinct advertiser_id, dfa_macro_user_id_from_dlx from distinct_dlx_cookies where advertiser_id = 3232142;"
    partition_dict = self.generate_tsv(hql, "/user/owatson/csv-output-files/output/dlx_cookies-advertiser_3232142.tsv")

    hql = "select distinct advertiser_id, dfa_macro_user_id_from_dlx from distinct_dlx_cookies where advertiser_id = 3232182;"
    partition_dict = self.generate_tsv(hql, "/user/owatson/csv-output-files/output/dlx_cookies-advertiser_3232182.tsv")

    hql = "select distinct advertiser_id, dfa_macro_user_id_from_dlx from distinct_dlx_cookies where advertiser_id = 3232123;"
    partition_dict = self.generate_tsv(hql, "/user/owatson/csv-output-files/output/dlx_cookies-advertiser_3232123.tsv")

    hql = "select distinct advertiser_id, dfa_macro_user_id_from_dlx from distinct_dlx_cookies where advertiser_id = 3232143;"
    partition_dict = self.generate_tsv(hql, "/user/owatson/csv-output-files/output/dlx_cookies-advertiser_3232143.tsv")

    hql = "select distinct advertiser_id, dfa_macro_user_id_from_dlx from distinct_dlx_cookies where advertiser_id = 3232144;"
    partition_dict = self.generate_tsv(hql, "/user/owatson/csv-output-files/output/dlx_cookies-advertiser_3232144.tsv")

    hql = "select distinct advertiser_id, dfa_macro_user_id_from_dlx from distinct_dlx_cookies where advertiser_id = 3232149;"
    partition_dict = self.generate_tsv(hql, "/user/owatson/csv-output-files/output/dlx_cookies-advertiser_3232149.tsv")

    print('+++ end .run...')

  def generate_tsv(self, hql, hdfs_output_path):
    #run the query
    print ("+++ RUNNING HIVE QUERY, hql=" + hql)
    
    hive_process = subprocess.Popen(['hive','-e','"' + hql + '"'], stdout=subprocess.PIPE)
    hadoop_process = subprocess.Popen(['hadoop', 'fs', '-put', '-', hdfs_output_path], stdin=hive_process.stdout)
    data = hadoop_process.communicate()[0]

if __name__ == "__main__":
  main()