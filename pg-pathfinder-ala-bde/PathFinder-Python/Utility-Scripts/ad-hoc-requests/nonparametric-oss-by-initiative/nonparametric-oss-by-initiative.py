import sys, os, subprocess

def main():
  new_data = tsv_generator()
  new_data.run()

class tsv_generator:
  def __init__(self):
    print ('+++ tsv_generator class init')

  def run(self):
    print('+++ start .run...')

    partition_dict = self.get_partition_list()

    for partition in partition_dict['records']:
      print('\n\nIn process loop.  Processing partition: ' + partition[0] + '...')
      hql = "select * from logs_product_w_oss where lower(pfcampaign) = '" + partition[0] + "';"
      partition_dict = self.generate_tsv(hql, "/user/owatson/csv-output-files/output/logs_product_w_oss__" + partition[0] + ".tsv")

    print('+++ end .run...')

  def generate_tsv(self, hql, hdfs_output_path):
    #run the query
    print ("+++ RUNNING HIVE QUERY, hql=" + hql)
    
    print("initiate hive query process...")
    hive_process = subprocess.Popen(['hive','-e','"' + hql + '"'], stdout=subprocess.PIPE)
    print("initiate hadoop fs put process...")
    hadoop_process = subprocess.Popen(['hadoop', 'fs', '-put', '-', hdfs_output_path], stdin=hive_process.stdout)
    print("run piped processes...")
    data = hadoop_process.communicate()[0]
    print("completed piped processes.")

  def get_partition_list(self):
    # the following query should return the list of partitions to be processed

    hql = "select distinct lower(pfcampaign) as pfcampaign from logs_product_w_oss;"
    output_dict = self.getHiveQueryResults(hql)
    return output_dict
  
  # runs hive query specified and returns rows (as array of arrays) as well as status
  def getHiveQueryResults(self, hql):
    return_data = {}
    return_data['rc'] = ""
    return_data['records'] = []
    
    #run the query
    print ("RUNNING HIVE QUERY, hql=" + hql)
    process = subprocess.Popen(["hive", "-e", '"' + hql + '"'], stdout=subprocess.PIPE)
    data = process.communicate()[0]
    return_data['rc'] = process.returncode
    
    # return if the query ended with an error
    if process.returncode != 0:
      return return_data
    
    # retrieve the data, if any exists
    if data:
      for row in data.split('\n'):
        if row.strip():
          return_data['records'].append(row.split('\t'))

    return return_data

if __name__ == "__main__":
  main()