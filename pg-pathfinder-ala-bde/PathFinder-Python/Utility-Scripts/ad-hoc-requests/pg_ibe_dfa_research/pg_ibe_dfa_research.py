﻿#creates log level ordinal table

import sys, os, subprocess

def main():
  new_data = partition_process()
  new_data.run()

class partition_process:

  def __init__(self):
    print ('partition_process class init')

  def run(self):
    print('in partition_process.run...')
    partition_dict = self.get_partition_list()

    for partition in partition_dict['records']:
        print('\n\nIn process loop.  Processing partition: ' + partition[0] + '...')
        self.process_partition(partition[0], "pg_ibe_dfa_research.hql")
        #self.process_partition(partition[0], "ibe_conv_users_by_wk_of_yr.hql")
    print('exiting partition_process.run...')

  def get_week_specs(self, week_of_year):
    # the following query should return the list of partitions to be processed
    hql = "select min(event_dt) as min_dt, max(event_dt) as max_dt from pg_default.dfa_ibe_research_event_dt_week where dfa_ibe_research_event_dt_week.wk_of_yr = " + str(week_of_year) + ";"
    output_dict = self.getHiveQueryResults(hql)
    return output_dict

  def get_partition_list(self):
    # the following query should return the list of partitions to be processed
    hql = "select distinct wk_of_yr from pg_default.dfa_ibe_research_event_dt_week order by wk_of_yr;"
    output_dict = self.getHiveQueryResults(hql)
    return output_dict

  def process_partition(self, partition_value, script_to_execute):
    # partition_value is any valid hive partition name
    # script_to_execute is the name of a hive script that takes a single input parameter named ${hiveconf:partition_name}
    print ('starting partition_process.process_partition(' + partition_value + ", " + script_to_execute + '):\n\n')

    week_specs_dict = self.get_week_specs(partition_value)
    week_start_dt = week_specs_dict['records'][0][0]
    week_end_dt = week_specs_dict['records'][0][1]

    process = subprocess.Popen(["hive", "-f", script_to_execute, "-hiveconf", "week_start_dt=" + week_start_dt, "-hiveconf", "week_end_dt=" + week_end_dt], stdout=subprocess.PIPE)
    data = process.communicate()[0]
    print ('completed running' + script_to_execute + '\n\n')

    return

  # runs hive query specified and returns rows (as array of arrays) as well as status
  def getHiveQueryResults(self, hql):
    return_data = {}
    return_data['rc'] = ""
    return_data['records'] = []
    
    #run the query
    print ("RUNNING HIVE QUERY, hql=" + hql)
    process = subprocess.Popen(["hive", "-e", '"' + hql + '"'], stdout=subprocess.PIPE)
    data = process.communicate()[0]
    return_data['rc'] = process.returncode
    
    # return if the query ended with an error
    if process.returncode != 0:
      return return_data
    
    # retrieve the data, if any exists
    if data:
      for row in data.split('\n'):
        if row.strip():
          return_data['records'].append(row.split('\t'))

    return return_data

if __name__ == "__main__":
  main()
