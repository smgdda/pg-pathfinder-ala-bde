import pyodbc
import sys
import csv
import time
from datetime import date, timedelta
import datetime
import boto
from filechunkio import FileChunkIO
import re

class RedshiftConnector:

    def __init__(self, **kwargs):

      return super().__init__(**kwargs)

    def get_ea_data(self):
      data_qry = """
        SELECT

        user_id
        , buy_id
        , site_id
        , page_id
        , creative_id
        , case when activity_flag is null then 0 else 1 end as activity_flag
        , case when t2c is not null then cast(t2c as decimal(10,5)) / cast(60 as decimal(10,5)) else cast(DATEDIFF(minutes, date_time, CURRENT_DATE - 1 ) as decimal(10,5)) / cast(60 as decimal(10,5)) end as t2c

        FROM pf_gcs_4478.event
      """
      qry_data_request = self.conn.cursor()
      qry_data_request.execute(data_qry)
      qry_data_results = qry_data_request.fetchall()
        
      file_name = "ea_4478_t2c_data-" + time.strftime("%Y_%m_%d") + ".tsv"
      file_path = 'c:\\Users\\Orrin\\Desktop\\'

      with open (file_path + file_name, 'w', newline='') as csvoutput:
        tsv_writer = csv.writer(csvoutput, dialect='excel-tab', delimiter='\t', quoting=csv.QUOTE_NONE, quotechar='')
        tsv_writer.writerows(wocPlcmtStats_results)

    def openConnection(self):

      self.conn = pyodbc.connect("Driver={Amazon Redshift (x64)}; Server=172.31.29.147; Port=5439; Database=smgdda; UID=smgdda; PWD=SMGCh1ca90", timeout=9600)
      self.conn.autocommit = True

    def closeConnection(self, **kwargs):

      self.conn.close()
    
def main(argv):

  # INSTANTIATE CLASS AND RUN

  rsConn = RedshiftConnector()

  # THIS CODE NEEDS TO BE KEPT TO GATHER SKYSKRAPER DATA FOR EACH WoC
  #
  print("opening EA connection...")
  rsConn.openConnection()
  print("getting data from EA...")
  rsConn.get_ea_data()
  print("closing EA connection...")
  rsConn.closeConnection()
  
if __name__ == "__main__":

  main(sys.argv)
