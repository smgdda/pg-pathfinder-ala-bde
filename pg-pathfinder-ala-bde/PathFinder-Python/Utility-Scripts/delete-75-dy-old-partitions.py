import sys, os, subprocess
import datetime
import time

def main():
  delete_old_partitions_process = delete_old_partitions()
  delete_old_partitions_process.run()
  

class delete_old_partitions:

  def __init__(self):
    print("delete_old_partitions class init...")

  def run(self):
    startDate = datetime.date.today()
    currentEndDate = startDate - datetime.timedelta(days=75)
    
    print ('executing sql script ==>')

    #run the query
    hql = "alter table logs_pfcamp_matched drop partition (event_dt < '" + datetime.date.isoformat(currentEndDate).replace("-","") + "');"
    print(["hive", "-e", hql])
      
    #process = subprocess.Popen(["hive", "-e", hql], stdout=subprocess.PIPE)
    #data = process.communicate()[0]
      
    print ('completed running sql script ==>\n\n')
    print("completed delete_old_partitions.run()...\n\n")

if __name__ == "__main__":
  main()