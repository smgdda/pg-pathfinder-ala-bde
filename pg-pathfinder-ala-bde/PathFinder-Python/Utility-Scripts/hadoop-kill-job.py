import sys, os, subprocess
import datetime
import time

def main():
  job_killer = hadoop_job_killer()
  job_killer.run()

class hadoop_job_killer:

  def __init__(self):
    print("hadoop_job_killer class init...")

  def run(self):
    print("starting hadoop_job_killer.run()...")
    process = subprocess.Popen(["hadoop", "job", "-kill", "job_201504190506_3580"], stdout=subprocess.PIPE)
    data = process.communicate()[0]
    print('completed hadoop_job_killer.run()...')

if __name__ == "__main__":
  main()