import sys, os, subprocess

def main():
  new_data = tsv_generator()
  new_data.run()

class tsv_generator:
  def __init__(self):
    print ('+++ tsv_generator class init')

  def run(self):
    print('+++ start .run...')

    hql = "select * from logs_product_w_oss;"
    partition_dict = self.generate_tsv(hql, "/user/owatson/csv-output-files/output/logs_product_w_oss.tsv")

    print('+++ end .run...')

  def generate_tsv(self, hql, hdfs_output_path):
    #run the query
    print ("+++ RUNNING HIVE QUERY, hql=" + hql)
    
    print("initiate hive query process...")
    hive_process = subprocess.Popen(['hive','-e','"' + hql + '"'], stdout=subprocess.PIPE)
    print("initiate hadoop fs put process...")
    hadoop_process = subprocess.Popen(['hadoop', 'fs', '-put', '-', hdfs_output_path], stdin=hive_process.stdout)
    print("run piped processes...")
    data = hadoop_process.communicate()[0]
    print("completed piped processes.")

if __name__ == "__main__":
  main()