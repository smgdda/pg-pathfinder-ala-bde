import sys, os, subprocess
import datetime
import time

def main():
  parametric_survival_analysis_date_range_process = date_range_process()
  parametric_survival_analysis_date_range_process.run()
  

class date_range_process:

  def __init__(self):
    print("date_range_process class init...")

  def run(self):
    print("starting date_range_process.run()...")
    clearHql = "ALTER TABLE survival_analysis_data_setup_step_002 DROP IF EXISTS PARTITION(pfcampaign!='');"
    clear_success = self.getHiveQueryResults(clearHql)
    partition_dict = self.get_partition_list()
    for partition in partition_dict['records']:
        print('\n\nIn process loop.  Processing partition: ' + partition[0] + '...')
        self.run_date_range_process(partition[0], '20150601', '20150630', 5, 'survival_analysis_data_setup_step_002.hql')
    print('exiting date_range_process.run...')
    print('completed date_range_process.run()...')

  def get_partition_list(self):
    # the following query should return the list of partitions to be processed
    hql = "select distinct lower(pfcampaign) from pfcampaign_list where buy_id is not null and date_dropped = '';"
    output_dict = self.getHiveQueryResults(hql)
    return output_dict

  def run_date_range_process(self, pfcampaign, input_start_date, input_end_date, interval, sql_script_file):
    print("starting date_range_process.run_date_range_process(" + pfcampaign + ", " + input_start_date + ", " + input_end_date + ", " + str(interval) + ", " + sql_script_file + ")...")

    startDate = datetime.date(int(input_start_date[0:4]), int(input_start_date[4:6]), int(input_start_date[6:8]))
    currentEndDate = startDate + datetime.timedelta(days=int(interval))

    while startDate < datetime.date(int(input_end_date[0:4]), int(input_end_date[4:6]), int(input_end_date[6:8])):
      print ('executing sql script==========>')
      #run the query
      print(["hive", "-f", sql_script_file, "-hiveconf", "imp_begin_dt=" + datetime.date.isoformat(startDate).replace("-",""), "-hiveconf", "imp_end_dt=" + datetime.date.isoformat(currentEndDate).replace("-",""), "-hiveconf", "pfcampaign=" + pfcampaign])
      
      process = subprocess.Popen(["hive", "-f", sql_script_file, "-hiveconf", "imp_begin_dt=" + datetime.date.isoformat(startDate).replace("-",""), "-hiveconf", "imp_end_dt=" + datetime.date.isoformat(currentEndDate).replace("-",""), "-hiveconf", "pfcampaign=" + pfcampaign], stdout=subprocess.PIPE)
      data = process.communicate()[0]
      
      print ('completed running sql script==========>\n\n')

      #reset start and end dates for next iteration of processing loop
      startDate = currentEndDate
      currentEndDate = startDate + datetime.timedelta(days=int(interval))
      #if the current end date is past the window we want to check, set it to the end of the window we want to check
      if currentEndDate > datetime.date(int(input_end_date[0:4]), int(input_end_date[4:6]), int(input_end_date[6:8])):
        currentEndDate = datetime.date(int(input_end_date[0:4]), int(input_end_date[4:6]), int(input_end_date[6:8]))

    print("completed date_range_process.run_date_range_process(" + input_start_date + ", " + input_end_date + ", " + str(interval) + ", " + sql_script_file + ")...\n\n")

  def getHiveQueryResults(self, hql):
    return_data = {}
    return_data['rc'] = ""
    return_data['records'] = []
    
    #run the query
    print ("RUNNING HIVE QUERY, hql=" + hql)
    process = subprocess.Popen(["hive", "-e", '"' + hql + '"'], stdout=subprocess.PIPE)
    data = process.communicate()[0]
    return_data['rc'] = process.returncode
    
    # return if the query ended with an error
    if process.returncode != 0:
      return return_data
    
    # retrieve the data, if any exists
    if data:
      for row in data.split('\n'):
        if row.strip():
          return_data['records'].append(row.split('\t'))

    return return_data

if __name__ == "__main__":
  main()