import sys, os, subprocess
import datetime
import time

def main():
  parametric_survival_analysis_partition_process = partition_process()
  parametric_survival_analysis_partition_process.run()
  

class partition_process:

  def __init__(self):
    print("partition_process class init...")

  def run(self):
    print("starting partition_process.run()...")
    clearHql = "ALTER TABLE survival_analysis_data_setup_final_output DROP IF EXISTS PARTITION(pfcampaign!='');"
    clear_success = self.getHiveQueryResults(clearHql)
    partition_dict = self.get_partition_list()
    for partition in partition_dict['records']:
        print('\n\nIn process loop.  Processing partition: ' + partition[0] + '...')
        self.process_partition(partition[0], 'survival_analysis_data_setup_step_007_FINAL.hql')
    print('exiting partition_process.run...')
    print('completed partition_process.run()...')

  def get_partition_list(self):
    # the following query should return the list of partitions to be processed
    hql = "select distinct lower(pfcampaign) from pfcampaign_list where buy_id is not null and date_dropped = '';"
    output_dict = self.getHiveQueryResults(hql)
    return output_dict

  def process_partition(self, partition_value, script_to_execute):
    # partition_value is any valid hive partition name
    # script_to_execute is the name of a hive script that takes a single input parameter named ${hiveconf:partition_name}
    print ('starting partition_process.process_partition(', partition_value, script_to_execute, '):', end='\n\n', sep=' ')
    process = subprocess.Popen(["hive", "-f", script_to_execute, "-hiveconf", "pfcampaign=" + partition_value], stdout=subprocess.PIPE)
    data = process.communicate()[0]
    print ('completed running', script_to_execute, end='\n\n', sep=' ')

    return

  def getHiveQueryResults(self, hql):
    return_data = {}
    return_data['rc'] = ""
    return_data['records'] = []
    
    #run the query
    print ("RUNNING HIVE QUERY, hql=" + hql)
    process = subprocess.Popen(["hive", "-e", '"' + hql + '"'], stdout=subprocess.PIPE)
    data = process.communicate()[0]
    return_data['rc'] = process.returncode
    
    # return if the query ended with an error
    if process.returncode != 0:
      return return_data
    
    # retrieve the data, if any exists
    if data:
      for row in data.split('\n'):
        if row.strip():
          return_data['records'].append(row.split('\t'))

    return return_data

if __name__ == "__main__":
  main()