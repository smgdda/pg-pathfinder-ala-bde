﻿import pyodbc
import sys
import csv
import time
from datetime import date, timedelta
import datetime
import boto
from filechunkio import FileChunkIO
import re

class RedshiftConnector:

    def __init__(self, **kwargs):

      return super().__init__(**kwargs)

    def getSiteIdListForSql(self):

      self.openConnection()
      pageList = self.conn.cursor()
      pageList.execute("select site_id from pf_gcs_763.tmpsite;")
      output = pageList.fetchall()
      self.closeConnection()

      site_id_list = []
      site_id_list_str = ''
      for site in output:
        print(site.site_id)
        site_id_list.append(str(site.site_id))
      site_id_list_str = ','.join(site_id_list)

      print("SITE LIST RETRIEVED...\n\n")

      return site_id_list_str

    def getPageIdListForMigrationLoop(self):

      self.openConnection()
      pageList = self.conn.cursor()
      pageList.execute("select distinct page_id from pf_net_763.impression_joined;")
      output = pageList.fetchall()
      self.closeConnection()

      print("PAGE LIST RETRIEVED...\n\n")

      return output

    def migrateActivityData(self):

      self.openConnection()

      self.conn.execute("INSERT INTO pf_gcs_763.activity SELECT * FROM pf_gcs_763.activity_uncompressed;")
      print("migrateActivityData - DATA COPIED FROM ORIGINAL TO COMPRESSED TABLE...")
        
      self.closeConnection()

    def getEdwinData(self):
      edwinDataQry = """
        Select
          mbox.Vendor_Name,
          d.Site As Publication,
          mbox.Placement_Name As Placement,
          f.Creative,
          f.CreativeID,
          c.Campaign,
          a.PlacementID,
          b.DayDate,
          Case
      	    When mbox.Placement_Rate_Type='CPC' Then 
              cast ((mbox.Rate*cast( a.Clicks As Decimal (18,2)) ) as Decimal (18,2))
      	    When mbox.Placement_Rate_Type='CPM' Then 
              cast((mbox.Rate*(cast(a.Impressions As Decimal (18,4))/1000.0000)) as Decimal(18,2))
      	    When mbox.Placement_Rate_Type In ('Flat Rate Clicks','Flat Rate Impressions') Then 
              (CAST((CAST(CASE WHEN SYSDATE > e.PlacementEndDate THEN e.PlacementEndDate ELSE SYSDATE END as DATE) - e.PlacementStartDate) + 1 as Decimal(18,2))/cast(((e.PlacementEndDate - e.PlacementStartDate)+1) As Decimal(18,2)))* mbox.Planned_Cost
      	    Else 0
          End As Net_Cost,
          sum(a.MEDIACOST) As DFA_Media_Cost,
          sum(a.Impressions) As Impressions,
          sum(a.Clicks) As Clicks

        From  
          AGENCY_VIEWS_DCM.FACTIMPRESSIONSCLICKSCOST a

        Inner Join 
          AGENCY_VIEWS_DCM.DIM_DAY b
          On a.DAYSEQNUM=b.DATENUM

        Join 
          AGENCY_VIEWS_DCM.DIMCAMPAIGN c
    	    On a.CampaignID=c.CampaignID
        Join 
          AGENCY_VIEWS_DCM.DIMSITE d
    	    On a.SiteID=d.SiteID
        Join 
          AGENCY_VIEWS_DCM.DIMPLACEMENT e
    	    On a.PlacementID=e.PlacementID
        Join 
          AGENCY_VIEWS_DCM.DIMCREATIVE f
    	    On a.CreativeID=f.CreativeID
        Join 
          AGENCY_VIEWS_DCM.DIMAD g
    	    On a.ADID=g.ADID

        Join
          (
  		    select Distinct 
            a.Campaign_ID
            ,a.Placement_ID
            ,a.Placement_Name
            ,b.Placement_Reporting_Type
            ,a.Vendor_Name
            ,a.DFA_SITE_NAME
            ,b.Product_Name
            ,b.Product_ID
            ,a.Group_Name
            ,a.Package_Details
            ,b.UDF3
            ,a.Placement_Rate_Type
            ,a.Rate
            ,a.Planned_Cost
        
  		    from
            AGENCY_VIEWS_MBOX.SMG_PACING_V2 a
        
  		    Left Join
            (
  				    Select Distinct 
                cast(a.ADSERVER_PLACEMENT_ID AS BIGINT) as Placement_ID
                ,a.Placement_Name
                ,a.Placement_Reporting_Type
                ,a.Product_Name
                ,a.Product_ID
                ,a.Group_Name
                ,b.UDF3
  				    From
                AGENCY_VIEWS_MBOX.DIGITAL_PLACEMENT a
  				    Left Join
                AGENCY_VIEWS_MBOX.DIGITAL_DETAIL b
  					    On  (
                  a.CLIENT_ID = b.CLIENT_ID
  						    and a.PRODUCT_ID = b.PRODUCT_ID
  						    and a.VENDOR_ID =b.VENDOR_ID
  						    and a.loc_id = b.loc_id
  						    and a.INSERTION_ORDER_ID = b.INSERTION_ORDER_ID
                )
  				    Where
                regexp_count(a.ADSERVER_PLACEMENT_ID::text, '(^-?[0-9]+)'::character varying::text) =1
                And a.Client_ID In ('WMS','WMH','WMT','WMC','WMTC')
  				    Group By
                a.ADSERVER_PLACEMENT_ID
                ,a.Placement_Name
                ,a.Placement_Reporting_Type
                ,a.Product_Name
                ,a.Product_ID
                ,a.Group_Name
                ,b.UDF3
  			    ) b
            On a.Placement_ID=b.Placement_ID
  		    Where
            a.Client_ID In ('WMS','WMH','WMT','WMC','WMTC')
  			    And a.Package_Details In ('Non-Package')
  		    Group By
  			    a.Campaign_ID,a.Placement_ID,a.Placement_Name,b.Placement_Reporting_Type,a.Vendor_Name,a.DFA_SITE_NAME,b.Product_Name,b.Product_ID,a.Group_Name,a.Package_Details,b.UDF3,
  			    a.Placement_Rate_Type,a.Rate,a.Planned_Cost

          Union All

  		    select Distinct 
            a.Campaign_ID
            ,a.Placement_ID
            ,a.Placement_Name
            ,b.Placement_Reporting_Type
            ,a.Vendor_Name
            ,a.DFA_SITE_NAME
            ,b.Product_Name
            ,b.Product_ID
            ,a.Group_Name
            ,a.Package_Details
            ,b.UDF3
            ,c.Placement_Rate_Type
            ,c.Rate,c.Planned_Cost
  		
          from
            AGENCY_VIEWS_MBOX.SMG_PACING_V2 a
        
  		    Left Join
            (
  				    Select Distinct 
                cast(a.ADSERVER_PLACEMENT_ID AS BIGINT) as Placement_ID
                ,a.Placement_Name
                ,a.Placement_Reporting_Type
                ,a.Product_Name
                ,a.Product_ID
                ,a.Group_Name
                ,b.UDF3
            
  				    From
                AGENCY_VIEWS_MBOX.DIGITAL_PLACEMENT a
            
  				    Left Join
                AGENCY_VIEWS_MBOX.DIGITAL_DETAIL b
  					    On  (
                  a.CLIENT_ID = b.CLIENT_ID
  						    and a.PRODUCT_ID = b.PRODUCT_ID
  						    and a.VENDOR_ID =b.VENDOR_ID
  						    and a.loc_id = b.loc_id
  						    and a.INSERTION_ORDER_ID = b.INSERTION_ORDER_ID)
  				    Where
                regexp_count(a.ADSERVER_PLACEMENT_ID::text, '(^-?[0-9]+$)'::character varying::text) =1
  					    And a.Client_ID In ('WMS','WMH','WMT','WMC','WMTC')
  		        Group By
                a.ADSERVER_PLACEMENT_ID
                ,a.Placement_Reporting_Type
                ,a.Placement_Name
                ,a.Product_Name
                ,a.Product_ID
                ,a.Group_Name
                ,b.UDF3
  			    ) b
            On a.Placement_ID=b.Placement_ID

  		    Left Join
            (
  				    select Distinct
                Campaign_ID
                ,Group_Name
                ,Placement_Rate_Type
                ,Rate
                ,Planned_Cost
  				    from 
                AGENCY_VIEWS_MBOX.SMG_PACING_V2 a
  				    Where
                Client_ID In ('WMS','WMH','WMT','WMC','WMTC')
  					    And Package_Details ='Parent'
  			    ) c
            On (
              a.Campaign_ID= c.Campaign_ID
  				    And a.Group_Name = c.Group_Name
            )

  		    Where
            a.Client_ID In ('WMS','WMH','WMT','WMC','WMTC')
  			    And a.Package_Details In ('Child')
          
  		    Group By
            a.Campaign_ID
            ,a.Placement_ID
            ,a.Placement_Name
            ,b.Placement_Reporting_Type
            ,a.Vendor_Name
            ,a.DFA_SITE_NAME
            ,b.Product_Name
            ,b.Product_ID
            ,a.Group_Name
            ,a.Package_Details
            ,b.UDF3
            ,c.Placement_Rate_Type
            ,c.Rate
            ,c.Planned_Cost
  		    ) mbox
          On a.PlacementID = mbox.Placement_ID

  	    Where 
          a.dayseqnum between 20150601 and 20150831
  		    And a.AdvertiserID = 1559799
  	    Group By
  		    mbox.Vendor_Name,
  		    d.Site,
  		    mbox.Placement_Name,
  		    f.Creative,
  		    f.CreativeID,
  		    c.Campaign,
  		    a.PlacementID,
  		    b.DayDate,
  		    Case
  			     When mbox.Placement_Rate_Type='CPC' Then 
  				    cast ((mbox.Rate*cast( a.Clicks As Decimal (18,2)) ) as Decimal (18,2))
  			     When mbox.Placement_Rate_Type='CPM' Then 
  				    cast((mbox.Rate*(cast(a.Impressions As Decimal (18,4))/1000.0000)) as Decimal(18,2))
  			     When mbox.Placement_Rate_Type In ('Flat Rate Clicks','Flat Rate Impressions') Then 
             (CAST((CAST(CASE WHEN SYSDATE > e.PlacementEndDate THEN e.PlacementEndDate ELSE SYSDATE END as DATE) - e.PlacementStartDate) + 1 as Decimal(18,2))/cast(((e.PlacementEndDate - e.PlacementStartDate)+1) As Decimal(18,2)))* mbox.Planned_Cost
  			     Else 0
  		    End
      """
      self.openSkySkraperConnection()

      edwinData = self.conn.cursor()
      edwinData.execute(edwinDataQry)
      edwinData_results = edwinData.fetchall()
        
      self.closeConnection()
      
      file_name = "edwin-data-" + time.strftime("%Y_%m_%d") + ".tsv"
      file_path = 'c:\\users\\orrwatso\\Desktop\\'

      with open (file_path + file_name, 'w', newline='') as csvoutput:
        tsv_writer = csv.writer(csvoutput, dialect='excel-tab', delimiter='\t', quoting=csv.QUOTE_NONE, quotechar='')
        tsv_writer.writerows(edwinData_results)

    def getChrisData(self):
      chrisDataQry = """
        select
          dcm.dayseqnum,
          mbox.vendor_name,
          mbox.placement_reporting_type,
          mbox.adserver_placement_id,
          dcm.placementid,
          ctv.creative,
          dcm.creativeid,
          sum(dcm.impressions) as impressions,
          cast(sum(dcm.mediacost) as integer) as net_cost
   
        from 
          agency_views_dcm.factimpressionsclickscost dcm
    
        join
          agency_views_dcm.dimsite site
          on dcm.siteid = site.siteid
    
        join
          agency_views_dcm.dimcreative ctv
          on  ctv.creativeid = dcm.creativeid
    
        join
          agency_views_mbox.digital_placement mbox
          on mbox.adserver_placement_id = dcm.placementid
    
        where 
          dcm.dayseqnum between 20150601 and 20150831 
          and dcm.advertiserid =1559799
    
        group by
          dcm.dayseqnum, mbox.vendor_name, mbox.placement_reporting_type, mbox.adserver_placement_id, dcm.placementid, ctv.creative, dcm.creativeid
    
        order by
          dcm.placementid DESC
      """
      self.openSkySkraperConnection()

      chrisData = self.conn.cursor()
      chrisData.execute(chrisDataQry)
      chrisData_results = chrisData.fetchall()
        
      self.closeConnection()
      
      file_name = "chris-data-" + time.strftime("%Y_%m_%d") + ".tsv"
      file_path = 'c:\\users\\orrwatso\\Desktop\\'

      with open (file_path + file_name, 'w', newline='') as csvoutput:
        tsv_writer = csv.writer(csvoutput, dialect='excel-tab', delimiter='\t', quoting=csv.QUOTE_NONE, quotechar='')
        tsv_writer.writerows(chrisData_results)

    def getWoCPlacementStatsFromSkySkraper(self):
      wocPlcmtStatsQry = """
        select
	        campaignid
	        , siteid
	        , placementid
	        , creativeid
	        , sum(impressions) as imp_sum
	        , sum(clicks) as click_sum
	        , sum(mediacost) as cost_sum

        from
	        agency_views_dcm.factimpressionsclickscost

        join
	        agency_views_dcm.dimadvertiser 
	        on (
		        agency_views_dcm.dimadvertiser.networkid = 5767
		        and agency_views_dcm.dimadvertiser.advertiserid = agency_views_dcm.factimpressionsclickscost.advertiserid
	        )

        where
	        agency_views_dcm.factimpressionsclickscost.dayseqnum >= '20151110'
        --	and agency_views_dcm.factimpressionsclickscost.dayseqnum <= TO_CHAR(date(current_date - cast('1 days' as interval)), 'YYYYMMDD')
	        and agency_views_dcm.factimpressionsclickscost.dayseqnum <= '20160109'

        group by
	        campaignid
	        , siteid
	        , placementid
	        , creativeid
      """
      wocPlcmtStats = self.conn.cursor()
      wocPlcmtStats.execute(wocPlcmtStatsQry)
      wocPlcmtStats_results = wocPlcmtStats.fetchall()
        
      file_name = "skyskraper-woc-plcmt-stats-" + time.strftime("%Y_%m_%d") + ".tsv"
      file_path = 'c:\\users\\orrwatso\\Desktop\\'

      with open (file_path + file_name, 'w', newline='') as csvoutput:
        tsv_writer = csv.writer(csvoutput, dialect='excel-tab', delimiter='\t', quoting=csv.QUOTE_NONE, quotechar='')
        tsv_writer.writerows(wocPlcmtStats_results)

    def getWoCPlacementSpectraDataFromSkySkraper(self):
      wocPlcmtQry = """
        select distinct
          adserver_name
          , adserver_network_id
          , adserver_campaign_id
          , adserver_placement_id
          , adserver_site_id
          , campaign_id
          , campaign_name
          , campaign_reporting_type
          , digital_type
          , client_id
          , placement_id
          , placement_name
          , placement_start_date
          , placement_end_date
          , placement_rate_type
          , cast(NULL as varchar(50)) as grp_rate_type
          , rate
          , cast(NULL as FLOAT) as grp_rate
          , placement_reporting_type
          , placement_type
          , product_id
          , product_name
          , group_name
          , vendor_id
          , ordered_net
          , cast(NULL as decimal(12,2)) as grp_ordered_net
          , units
          , cast(NULL as BIGINT) as grp_units
          , actual_impressions
          , cast(NULL as BIGINT) as grp_actual_impressions

        from 
          agency_views_mbox.digital_placement

        where 
          CLIENT_ID = 'PG1'
          and lower(placement_name) not like '%cancel%'
          and placement_start_date >= '2015-05-01'
          and (group_name is NULL or group_name = '')

        union all

        select distinct
          agency_views_mbox.digital_placement.adserver_name
          , agency_views_mbox.digital_placement.adserver_network_id
          , agency_views_mbox.digital_placement.adserver_campaign_id
          , agency_views_mbox.digital_placement.adserver_placement_id
          , agency_views_mbox.digital_placement.adserver_site_id
          , agency_views_mbox.digital_placement.campaign_id
          , agency_views_mbox.digital_placement.campaign_name
          , agency_views_mbox.digital_placement.campaign_reporting_type
          , agency_views_mbox.digital_placement.digital_type
          , agency_views_mbox.digital_placement.client_id
          , agency_views_mbox.digital_placement.placement_id
          , agency_views_mbox.digital_placement.placement_name
          , agency_views_mbox.digital_placement.placement_start_date
          , agency_views_mbox.digital_placement.placement_end_date
          , agency_views_mbox.digital_placement.placement_rate_type
          , plcmt_group.placement_rate_type as grp_rate_type
          , agency_views_mbox.digital_placement.rate
          , plcmt_group.rate as grp_rate
          , agency_views_mbox.digital_placement.placement_reporting_type
          , agency_views_mbox.digital_placement.placement_type
          , agency_views_mbox.digital_placement.product_id
          , agency_views_mbox.digital_placement.product_name
          , agency_views_mbox.digital_placement.group_name
          , agency_views_mbox.digital_placement.vendor_id
          , agency_views_mbox.digital_placement.ordered_net
          , plcmt_group.ordered_net as grp_ordered_net
          , agency_views_mbox.digital_placement.units
          , plcmt_group.units as grp_units
          , agency_views_mbox.digital_placement.actual_impressions
          , plcmt_group.actual_impressions as grp_actual_impressions

        from
          agency_views_mbox.digital_placement

        left outer join
          (select * from agency_views_mbox.digital_placement where CLIENT_ID = 'PG1' and lower(placement_type) = 'placement group') plcmt_group
        on
          (
          plcmt_group.group_name = agency_views_mbox.digital_placement.group_name
          and plcmt_group.campaign_id = agency_views_mbox.digital_placement.campaign_id
          )

        where 
          agency_views_mbox.digital_placement.CLIENT_ID = 'PG1'
          and lower(agency_views_mbox.digital_placement.placement_name) not like '%cancel%'
          and agency_views_mbox.digital_placement.placement_start_date >= '2015-05-01'
          and (
            agency_views_mbox.digital_placement.group_name is not NULL 
            and agency_views_mbox.digital_placement.group_name != '' 
            and lower(agency_views_mbox.digital_placement.placement_type) != 'placement group'
          )
        ;
      """
      wocPlcmtData = self.conn.cursor()
      wocPlcmtData.execute(wocPlcmtQry)
      wocPlcmtData_results = wocPlcmtData.fetchall()
        
      file_name = "pf_spectra_placements-" + time.strftime("%Y_%m_%d") + ".tsv"
      file_path = 'c:\\users\\orrwatso\\Desktop\\'

      with open (file_path + file_name, 'w', newline='') as csvoutput:
        tsv_writer = csv.writer(csvoutput, dialect='excel-tab', delimiter='\t', quoting=csv.QUOTE_NONE, quotechar='')
        tsv_writer.writerows(wocPlcmtData_results)

    def get_samsung_activity_data(self):
      samsung_activity_data_qry = """
        select
          a.*

        from
          (
            select
              pf_gcs_4478.activitycat.activity_id
              , max(pf_gcs_4478.activitycat.gcs_file_insert_date) as max_date
            from
              pf_gcs_4478.activitycat
            where
              report_name in (
                'Landing Page Unique'
                , 'Email Form Unique'
                , 'Email Thank You Page Unique'
                , 'Mobility S7 Thank You Page Unique'
                , 'Mobility S7 Landing Page Unique'
                , 'Mobility S7 Email form Unique'
              )
            group by
              activity_id
          ) max_activity_info

        join
          pf_gcs_4478.activitycat acat
            on
            acat.activity_id = max_activity_info.activity_id
            and acat.gcs_file_insert_date = max_activity_info.max_date

        join
          pf_gcs_4478.activity a
            on
            a.activity_type = acat.activity_type
            and a.activity_sub_type = acat.activity_sub_type
            and cast(date_time as date) >= current_date - 60
            and a.user_id != '0'
        ;
      """
      samsung_activity_data_connector = self.conn.cursor()
      samsung_activity_data_connector.execute(samsung_activity_data_qry)
      samsung_activity_data = samsung_activity_data_connector.fetchall()
        
      file_name = "samsung_activity_data-" + time.strftime("%Y_%m_%d") + ".tsv"
      file_path = 'c:\\users\\orrwatso\\Desktop\\'

      with open (file_path + file_name, 'w', newline='') as csvoutput:
        tsv_writer = csv.writer(csvoutput, dialect='excel-tab', delimiter='\t', quoting=csv.QUOTE_NONE, quotechar='')
        tsv_writer.writerows(samsung_activity_data)

    def migrateDataForPageId(self, page_id):

      print (str(page_id) + " PROCESSING...\n\n")

      self.openConnection()

      self.conn.execute("INSERT INTO pf_net_763.impression_joined_compressed SELECT * FROM pf_net_763.impression_joined WHERE page_id = " + str(page_id))
      print("DATA COPIED FROM ORIGINAL TO COMPRESSED TABLE...")
        
      self.conn.execute("DELETE FROM pf_net_763.impression_joined WHERE page_id = " + str(page_id))
      print("REMOVED DATA FROM ORIGINAL TABLE...")
        
      #self.conn.execute("VACUUM DELETE ONLY pf_net_763.impression_joined")
      #print("VACUUMED ORIGINAL TABLE TO RELEASE DISK SPACE...\n\n")
        
      self.closeConnection()

    def openConnection(self):

      self.conn = pyodbc.connect("Driver={Amazon Redshift (x64)}; Server=172.31.29.147; Port=5439; Database=smgdda; UID=smgdda; PWD=SMGCh1ca90", timeout=9600)
      self.conn.autocommit = True

    def openMoatConnection(self):

      self.conn = pyodbc.connect("Driver={Amazon Redshift (x64)}; Server=172.31.29.147; Port=5439; Database=moat_data_review; UID=smgdda; PWD=SMGCh1ca90", timeout=9600)
      self.conn.autocommit = True

    def openSkySkraperConnection(self):

      self.conn = pyodbc.connect("Driver={Amazon Redshift (x64)}; Server=redshift.cloud.vivaki.com; Port=5439; Database=skyskraper; UID=orrin_watson; PWD=Change.me1", timeout=9600)
      self.conn.autocommit = True

    def closeConnection(self, **kwargs):

      self.conn.close()

    def copyGCSDataFromS3(self):
      print ("copyGCSDataFromS3() PROCESSING...\n\n")

      self.openMoatConnection()

      for day_of_month in range(2,31):
        print( "copying data file from S3 for day " + str(format(day_of_month, '02d')) + '...')
        sql = """
          copy moat_data_review.moat_review.tmp_single_column_using_acceptinvchar (ingestion_line) 
          from 's3://smg-dda-network-5767/DCM-Log-Files/Impression/2015/05/""" + str(format(day_of_month, '02d')) + """' 
          credentials 'aws_access_key_id=AKIAIOJMVOWVYSN4PUZA;aws_secret_access_key=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' 
          acceptinvchars '^' 
          delimiter '^' 
          gzip;
          """
        self.conn.execute(sql)

        print( "staging data for day " + str(format(day_of_month, '02d')) + '...')
        sql = """
          INSERT INTO moat_data_review.moat_review.ingestion_stage
          SELECT
            *

          FROM
            (
            SELECT
              ingestion_line
              , TO_DATE(SPLIT_PART(ingestion_line, '^', 1), 'MM-DD-YYYY-HH24:MI:SS') as imp_date_time
              , SPLIT_PART(ingestion_line, '^', 2) as dfa_user_id
              , CAST(SPLIT_PART(ingestion_line, '^', 3) AS BIGINT) as advertiser_id
              , CAST(SPLIT_PART(ingestion_line, '^', 4) as BIGINT) as buy_id
              , CAST(SPLIT_PART(ingestion_line, '^', 5) AS BIGINT) as ad_id
              , CAST(SPLIT_PART(ingestion_line, '^', 6) as BIGINT) as creative_id
              , SPLIT_PART(ingestion_line, '^', 8) as creative_size_id
              , CAST(SPLIT_PART(ingestion_line, '^', 9) AS BIGINT) as site_id
              , CAST(SPLIT_PART(ingestion_line, '^', 10) AS BIGINT) as page_id
              , SPLIT_PART(ingestion_line, '^', 21) as site_data
              , SPLIT_PART(ingestion_line, '^', 23) as partner_1_id
              , SPLIT_PART(ingestion_line, '^', 24) as partner_2_id

            FROM 
              moat_data_review.moat_review.tmp_single_column_using_acceptinvchar

            WHERE
              lower(SPLIT_PART(ingestion_line, '^', 1)) <> 'time'
              and SPLIT_PART(ingestion_line, '^', 2) != '0'
            ) moat_data
          ;
        """
        self.conn.execute(sql)

        print( "filtering campaign imps with moat data for day " + str(format(day_of_month, '02d')) + '...')
        sql = """
          INSERT INTO moat_data_review.moat_review.ingestion_final
          SELECT
            *

          FROM
            moat_data_review.moat_review.ingestion_stage

          WHERE
            buy_id IN (8612634, 8609548, 8612570, 8610435)
            AND (
              site_data LIKE '%iv=1%'
              or site_data LIKE '%iv=0%'
            )
          ;
        """
        self.conn.execute(sql)

        print( "clearing ingestion staging table for day " + str(format(day_of_month, '02d')) + '...')
        self.conn.execute("truncate table moat_data_review.moat_review.ingestion_stage;")
        
        print( "clearing ingestion landing table for day " + str(format(day_of_month, '02d')) + '...')
        self.conn.execute("truncate table moat_data_review.moat_review.tmp_single_column_using_acceptinvchar;")
        
        print("day " + str(format(day_of_month, '02d')) + " processed...\n\n")

      self.closeConnection()

      print("copyGCSDataFromS3() COMPLETE.\n\n")
    
def main(argv):

  # INSTANTIATE CLASS AND RUN

  rsConn = RedshiftConnector()

  # THIS CODE CAN BE KILLED AFTER THE MOAT DATA LOAD, THEN RE-ACTIVATE NEXT SECTION
  # rsConn.copyGCSDataFromS3()

  # THIS CODE NEEDS TO BE KEPT TO GATHER SKYSKRAPER DATA FOR EACH WoC
  #
  print("opening connection...")
  rsConn.openConnection()
  # print("getting spectra data from skyskraper...")
  # rsConn.getWoCPlacementSpectraDataFromSkySkraper()
  # print("getting plcmt stats from skyskraper...")
  # rsConn.getWoCPlacementStatsFromSkySkraper()
  # print("getting samsung activity data...")
  rsConn.get_samsung_activity_data()
  print("closing connection...")
  rsConn.closeConnection()
  
#  siteList = rsConn.getSiteIdListForSql()
#  print(siteList)
#    page_list = rsConn.getPageIdListForMigrationLoop()
#  rsConn.migrateActivityData()

#  rsConn.getChrisData()
#  rsConn.getEdwinData()
#    for pg in page_list:
#        print(pg.page_id)
#        rsConn.migrateDataForPageId(pg.page_id)

if __name__ == "__main__":

  main(sys.argv)
