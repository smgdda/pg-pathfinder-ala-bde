# ---------------- MBD_All_R_Script.R ---------------- 
# CREATED BY: Sabrina Yee
# MODIFIED FROM TOMONORI ISHIKAWAS CODES: LOAD_MBD_MODELDATA.R, MBD_MAIN.R, AND LOOK_MODEL_DATES.R
# LAST EDITED BY ON: 02-22-2016
# MODIFIED FOR BDE by Sabrina Yee 

# ---------------- INPUTS ---------------- 
######
# IMPORTANT: Remember to create or update table MBD_Survey_List on bde

# clear environment 
rm(list = ls())

# list campaigns we want to use
campaign_use <- c('springsteendiamond', 'springsteen', 'diamond', 'diamondush', 'ambrosia', 'zzzquil', 'zzzquilush')


# ---------------- set up environment and load libraries -------------------


# Load library
# reshape2 needed for dcast function: long table to wide table
library(reshape2)
# boot for inverse logit function
library(boot)
library(plyr) #data frame functions
library(dplyr) #data frame functions

connect()


# ---------------- WOC ------------------

woc_query <- "SELECT concat_ws('_', cast(woc_year as string),
cast(woc_month as string)) as woc_spec,
analysis_end_dt 
FROM pf_analysis_window where analysis_end_dt <= TO_DATE( FROM_UNIXTIME( UNIX_TIMESTAMP() )) 
ORDER BY analysis_end_dt desc 
LIMIT 1"
woc_result <- rhive.query(woc_query)
woc <- as.character(woc_result$woc_spec)
rm(woc_query)
rm(woc_result)
campaign.analysis.month <- as.numeric(unlist(strsplit(woc, '_'))[2])
campaign.analysis.year <- as.numeric(unlist(strsplit(woc, '_'))[1])

wocfile <- sub('_', '', woc)
wocfolder <- paste('WOC_', woc)
freqfilesuffix <- paste('woc', wocfile, sep = '')

# ------ dates ------
# Get this range from table: pf_analysis_window in BDE
window_info_query <- paste("SELECT analysis_start_dt as analysis_start_dt,
                        analysis_end_dt as analysis_end_dt FROM pf_analysis_window
                        WHERE woc_month=", campaign.analysis.month, sep = ' ')
window_info_query <- paste(window_info_query, "AND woc_year=", campaign.analysis.year, sep = ' ')
window_info <- rhive.query(window_info_query)

analysis_start_dt <- window_info$analysis_start_dt
analysis_end_dt <- window_info$analysis_end_dt

cutoff <- gsub('-', '', analysis_end_dt)


# ----- paths -----

# analysis results folder
mbd_out = '/home/rchin_bde/'

# receiving folder in HDFS for download
mbd_root = '/user/rchin'

# ----- Attitudinal KPI Variable Name -----
kpitype <- 'att_response'

# ----- dimtypes -----
dimtypes <- c('sitetactic', 'sitecreative', 'siteadsize')
#dimtypes <- c('sitetactic','sitecreative','siteadsize','site','creative','adsize','tactic')

# ------ files -----
# this needs to be downloaded into the shared folder
mbdlist <- "MBD_Survey_List"


# ---------------- LOAD DATA ---------------

# ----- load mbd list -----
# use for brand and campaign labels
mbdlist_df <- rhive.load.table2(mbdlist)
#mbdlist_df 		<- subset(mbdlist_df, substr(surveyid,1,1)!='#')
#rownames(mbdlist_df)    <- mbdlist_df$surveyid


mbdlist_df$campaign <- tolower(gsub("[ /-]", '', mbdlist_df$campaign))
mbdlist_df$brand <- tolower(gsub("&", 'and', mbdlist_df$brand))

#subsets on the campaigns we choose in the inputs section
mbdlist_df <- subset(mbdlist_df, mbdlist_df$campaign %in% campaign_use)

# create column camplabel for the function print_modeling_dates
mbdlist_df$camplabel <- mbdlist_df$campaign

# Create list of MBD Survey IDs
mbd_survey_ids <- mbdlist_df$surveyid

# ------ Load and Clean PF_AGG Table ------
# This table is created by Orrin Watson every month

query_pf_agg <- paste("select * FROM pf_agg_final WHERE woc_spec='", woc, "'", sep = '')
PF_Agg = rhive.query(query_pf_agg)
PF_Agg = subset(PF_Agg, select = c('pfcampaign', 'rate_type', 'exe_type', 'dimtype',
                                   'dim', 'imps', 'cost', 'scaled_imps', 'scaled_cost'))


# Clean Dims
PF_Agg <- within(PF_Agg, {
    dim <- gsub("[$/,. +-]", '', dim)
    dim <- gsub("^([0-9])", "s\\1", dim)
})
PF_Agg$cost = as.numeric(PF_Agg$cost)
PF_Agg$scaled_imps = as.numeric(PF_Agg$scaled_imps)
PF_Agg$scaled_cost = as.numeric(PF_Agg$scaled_cost)



# remove bad characters on match key: "!" or "-" or " " or "," or "." or "$" or "+"
PF_Agg$dim = gsub('["]', "",
                            gsub("[+]", "",
                                 gsub("[/]", "",
                                      gsub("[$]", "",
                                           gsub("\\.", "",
                                                gsub("!", "",
                                                     gsub("-", "",
                                                          gsub(" ", "",
                                                               gsub(",", "", PF_Agg$dim)))))))))
# Collapse/Aggregate Over Rate Types
pfagg_byvarlist <- c('pfcampaign', 'dimtype', 'dim', 'exe_type')
pfagg_sumvarlist <- c('imps', 'cost', 'scaled_imps', 'scaled_cost')
PF_Agg <- aggregate(PF_Agg[, pfagg_sumvarlist],
                                by = PF_Agg[, pfagg_byvarlist],
                                'sum', na.rm = TRUE)
PF_Agg$dim_old <- PF_Agg$dim
dim_exe_type <- paste(PF_Agg$dim, PF_Agg$exe_type, sep = '')
PF_Agg$dim <- dim_exe_type

# uppercase column headers
names(PF_Agg) = toupper(names(PF_Agg))

# ---------------- FUNCTIONS ---------------

# ----- FUNCTION 1: GARBAGE CLEAN UP FUNCTION -----
# CREATES MORE SPACE IN MEMORY
gc_iter <- function(n = 2) {
    for (i in 1:n)
        gc()
}

# ----- FUNCTION 2: FUNCTION make_modeldata -----
# SETS UP MODELLING TABLE BY MERGING SURVEY DATA AND DCM LOG DATA
# DEFINES CONTROL GROUP
# DEFINES EXPOSED GROUP
# OUTPUTS DATA FRAME
make_modeldata <- function(brand, camp, mbd_survey_id, survey.brand.name, dimtype) {
    header_msg <- sprintf("\n----- %s %s %s %s -----", mbd_survey_id, brand, camp, dimtype)
    message(header_msg)
    print(header_msg)

    # ----- load data -----

    # pf_agg list of dimensions for validating
    pfagg_dimlist <- PF_Agg[(PF_Agg$PFCAMPAIGN == camp & PF_Agg$DIMTYPE == dimtype), 'DIM']

    # number of dimensions per campaign and dimtype
    nvar_pfaggdims <- length(pfagg_dimlist)
    if (nvar_pfaggdims == 0) {
        print('ERROR: No dimensions found in PF_AGG.')
    }

    # dep vars
    # like att_responses_ambrosia
    response_table <- paste("att_responses", survey.brand.name, sep = '_')
    depvars_in_df <- rhive.load.table2(response_table)

    # Subset depvars_in_df for the rows with survey date within the window we are interested in	
    depvars_df <- within(subset(depvars_in_df,
                                as.character(depvars_in_df$survey_dt) >= as.character(analysis_start_dt)), { })
    n_depvars <- dim(depvars_df)[1]

    # --- long rhs vars ---
    longrhs_df <- rhive.load.table2(paste('mbd_lr_', dimtype, '_freq', sep = ''))
    longrhs_df <- subset(longrhs_df, longrhs_df$pfcampaign == paste(camp))

    # make freq_count numeric	
    longrhs_df$freq_count <- as.numeric(longrhs_df$freq_count)

    #compile list of unique dimtypes
    longrhs_dimlist <- unique(longrhs_df[, dimtype])

    # ----- process long rhs vars to wide rhs vars -----

    # clean dims
    # change dimtype heading name to be 'dims_old'
    # concatenate dimtype and exe_type to make the new dims variable - used for modelling
    longrhs_df$dims <- paste(longrhs_df[, dimtype], longrhs_df$exe_type, sep = '')
    names(longrhs_df) <- sub(dimtype, 'dims_old', names(longrhs_df))

    # remove commas, periods, spaces and dashes
    # also remove pluses and slashes
    pretrans <- within(subset(longrhs_df, pfcampaign == camp), {
        dims <- gsub("[$/,. +-]", '', dims)
        dims <- gsub("^([0-9])", "s\\1", dims)
    })

    # get unique dimensions
    pretrans_dimlist <- unique(pretrans[, 'dims'])

    # validated dims for modeling
    # pretrans may have dims not appearing in pf_agg
    # converse is also possible but rarer--arises from small samples
    model_dimlist <- intersect(pretrans_dimlist, pfagg_dimlist)

    # --- print status of dims ---
    # print unique dimtypes
    print("All dims appearing in DFA impressions:")
    print(pretrans_dimlist)

    # print dimtypes we will be modelling with
    print("Valid dims that DO appear in both impressions and pf_agg:")
    print(model_dimlist)

    # cast long table to wide table 
    # dcast to return dataframes
    # number of rows/cookies
    posttrans <- dcast(pretrans, dfa_user_id ~ dims, fun.aggregate = sum, value.var = 'freq_count')
    n_post <- dim(posttrans)[1]

    # downselect to only pf_agg validated vars
    posttrans <- posttrans[, c('dfa_user_id', model_dimlist)]

    # merge dep vars with wide rhs vars 
    # merging survey data with the impression data
    mergedata_df <- merge(depvars_df, posttrans, by = 'dfa_user_id', all = T)
    n_merge <- dim(mergedata_df)[1]

    # create list of dimensions 
    # setdiff function grabs everything but 'dfa_user_id'
    dims <- setdiff(colnames(posttrans), 'dfa_user_id')

    # print warning when only one dim
    if (length(dims) == 1) {
        onedim_msg <- paste('WARNING! NO OPTIMIZATION OPPORTUNITIES--ONLY ONE ',
                                    toupper(dimtype), ':\n  ', dims, sep = '')
        message(onedim_msg)
    }

    # CONTROL GROUP
    # valid controls have exposed_control == "control" and non-null (dl) respondent_id
    valid_control <- subset(mergedata_df, exposed_control == 'control' & !is.na(respondent_id))

    # set dimension values to zero for controls (they are now missing)
    # number of rows/cookies in control
    valid_control[, dims] <- 0
    n_ctrl <- dim(valid_control)[1]

    # EXPOSED GROUP 
    # valid exposed have exposed_control == 'test' and non-null dfa_user_id
    # number of rows/cookies in exposed
    valid_exposed <- subset(mergedata_df, exposed_control == 'test' & !is.na(dfa_user_id))
    n_expo_gross <- dim(valid_exposed)[1]

    # calculate sum of NAs per obs
    # apply without as.matrix below fails when dims has
    # only one column because in that case the slice
    # becomes a list, doesn't stay as a dataframe or matrix
    expo_nasum <- apply(is.na(as.matrix(valid_exposed[, dims])), 1, sum)

    # get number of observed with NAs for all exposures
    expo_allna <- subset(valid_exposed, (expo_nasum == length(dims)))
    n_expo_allna <- dim(expo_allna)[1]

    # remove exposed obs with all NA exposures -- means no DFA user ID match
    model_exposed <- subset(valid_exposed, (expo_nasum < length(dims)))

    # stack model exposed and valid control to create model data
    valid_modeldata <- rbind(model_exposed, valid_control)
    n_modeldata <- dim(valid_modeldata)[1]

    # cleanup
    rm(depvars_df, longrhs_df,
           mergedata_df, pretrans, posttrans,
           valid_control, valid_exposed)
    gc_iter()

    # Create bde table name and ensure it is available
    modeldata_str <- paste('mbd', brand, camp, dimtype, cutoff, sep = '_')
    hql9 = paste("drop table if exists", modeldata_str, sep = ' ')
    query9 = rhive.execute(hql9)

    #output to Hive
    rhive.write.table(valid_modeldata, modeldata_str)

    # print counts
    pct_allna <- 100 * (n_expo_allna / n_expo_gross)
    n_expo_notna <- n_expo_gross - n_expo_allna
    n_total <- n_ctrl + n_expo_notna
    message(sprintf("%d dep var rows
                        %d wide rhs var rows
                        %d merged data rows
                        %d modeldata rows, of which
                        %d are valid controls
                        %d are gross exposeds
                        %d (%4.1f%%) of these have all NA values, leaving
                        %d net exposeds
                        %d total rows available for modeling",
                        n_depvars, n_post, n_merge, n_modeldata,
                        n_ctrl, n_expo_gross, n_expo_allna, pct_allna,
                        n_expo_notna, n_total))
}


# ----- FUNCTION 5: FUNCTION test.power.transform -----
# CALLED WITHIN GET.MBD.EFF FUNCTION
# RECORD LOGISTIC REGRESSION OUTPUTS IN A DATAFRAME
test.power.transform <- function(df, dep_var, ind_var, min_pow, max_pow, iter) {
    #create dataframe to store params
    results_log <- data.frame(matrix(vector(), 0, 4, dimnames = list(c(), c("var_name", "coef", "p_val", "power"))), stringsAsFactors = F)
    #define search space

    loop_vals <- rev(seq(min_pow, max_pow, length = iter))
    for (power in loop_vals) {
        p_trans <- df[, ind_var] ** power
        temp_table <- data.frame(cbind(dep_var, p_trans))
        model <- glm(dep_var ~ p_trans, data = temp_table, family = "binomial")
        summary <- summary(model)$coefficients
        if ('p_trans' %in% rownames(summary)) {
            coef <- as.numeric(as.character(summary['p_trans', 'Estimate']))
            p_val <- as.numeric(as.character(summary['p_trans', 'Pr(>|z|)']))
        } else {
            coef <- as.numeric(as.character(summary['p_trans1', 'Estimate']))
            p_val <- as.numeric(as.character(summary['p_trans1', 'Pr(>|z|)']))
        }
        var_name <- ind_var
        results <- cbind(var_name, coef, p_val, power)
        results_log <- rbind(results, results_log)
        # loop will stop once coefficient significance < 10% 
        if (p_val < .1)
            break
    }
    return(results_log)
}

# ----- FUNCTION 6: FUNCTION build.model.data -----
# CALLED WITHIN GET.MBD.EFF FUNCTION
# BUILD NEW DATASET BASED ON OPTIMAL PVALS
build.model.data <- function(table, dep_var, final_model_vars) {
    # initialize DF
    model_data <- data.frame(dep_var)
    # loop over vars & power transform params
    num_vars <- nrow(final_model_vars)
    for (i in 1:num_vars) {
        power <- final_model_vars$power
        temp_var <- table[, final_model_vars$var_name[i]] ** power[i]
        model_data <- cbind(model_data, temp_var)
        names(model_data)[i + 1] <- final_model_vars$var_name[i]
    }
    return(model_data)
}

# ----- FUNCTION 7: FUNCTION create.formula -----
# CALLED WITHIN GET.BASE.INCR FUNCTION
# CALLED WITHIN GET.PROBS FUNCTION
# CREATE MODEL SPECIFICATION FOR GLM FUNCTION
# DEP_VAR IN FIRST COLUMN, REST OF VARIABLES FOLLOW AND ARE USED
# THIS MIGHT BREAK FOR ADSIZES THAT START WITH A NUMBER
create.formula <- function(model_data) {
    vars <- names(model_data)
    fla <- paste("dep_var ~", paste(vars[2:ncol(model_data)], collapse = "+"))
    model_spec <- as.formula(fla)
    return(model_spec)
}

# ----- FUNCTION 8: FUNCTION get.base.incr -----
# CALLED WITHIN GET.MBD.EFF FUNCTION
# GETS PROBABILITY ESTIMATES BASED ON TRANSFORMED DATASET
# USES AGENT BASED DECOMPOSITION
get.base.incr <- function(model_data, final_model_vars, model_vars_subset, brand, campaign, dimtype) {

    exposure_data <- as.data.frame(model_data[, 2:ncol(model_data)])

    # build input for GLM using stat sig vars
    model_spec <- create.formula(model_data)

    # run final model on stat sig vars
    final_model <- glm(model_spec, data = model_data, family = "binomial")

    # get coefficients for decomp
    coef <- data.frame(summary(final_model)$coefficients[, 'Estimate'])
    names(coef) <- 'coef'
    coef <- as.matrix(coef)

    # need to keep track of vars that make the final model, but cause singularities
    # with so few obs, they have the same (probably low) impressions
    nonsing_varlist <- rownames(summary(final_model)$coefficients)[-1]

    # remove dep var from data and create matrix for matrix multiplication
    model_data$dep_var <- NULL
    model_data_decomp <- as.matrix(cbind(1, model_data))

    # adjust for singularities
    model_data_decomp <- model_data_decomp[, c(1, nonsing_varlist)]

    # probability vectors, length in number of users
    # debug error: non-conformable arrays
    # this is due to a singularity
    # coef has fewer rows than model_data_decomp has cols
    user_total_prob <- inv.logit(model_data_decomp %*% coef) # %*% is matrix multiplication  
    base_prob <- inv.logit(coef['(Intercept)', 'coef'])
    user_incr_prob <- user_total_prob - base_prob

    # prepare coefs and data columns for loop
    intercept <- coef['(Intercept)', 'coef']
    var_coefs <- coef[2:nrow(coef),]

    model_data_decomp <- data.frame(model_data_decomp)
    model_data_decomp$X1 <- NULL

    # calculate incremental agent/dim contribution using only paramter i
    agent_contr_withonly <- model_data_decomp
    for (i in 1:length(var_coefs)) {
        agent_contr_withonly[, c(i)] <- inv.logit(intercept + model_data_decomp[, c(i)] * var_coefs[i]) - base_prob
    }

    # calculate incremental agent/dim contribution using all parameters except paramater i
    agent_contr_without <- model_data_decomp
    for (i in 1:length(var_coefs)) {

        drop_var_idx <- i + 1
        temp_data <- as.matrix(cbind(1, model_data_decomp))
        temp_data <- temp_data[, - drop_var_idx]
        temp_coef <- as.matrix(coef[ - drop_var_idx,])
        colnames(user_total_prob) <- names(var_coefs)[i]
        agent_contr_without[, c(i)] <- user_total_prob - inv.logit(temp_data %*% temp_coef)
    }

    # average above two agent contribution methods
    avg_agent_contr <- .5 * (agent_contr_withonly + agent_contr_without)

    # scale values
    row_sum <- rowSums(avg_agent_contr)
    scaled_agent_contr <- user_incr_prob * (avg_agent_contr / row_sum)

    # create exposed control vector
    # first reupload the raw data table
    # get model data
    modeldata_str <- paste('mbd', brand, campaign, dimtype, cutoff, sep = '_')
    table <- rhive.load.table2(modeldata_str)
    exposed <- as.numeric(table$exposed_control == 'test')

    # aggregate over base & incr, distribute incr across dimensions only for exposed group
    user_total_prob <- user_total_prob * exposed
    user_incr_prob <- user_incr_prob * exposed
    campaign_prob <- sum(user_total_prob)
    campaign_base_prob <- base_prob * sum(exposed)
    campaign_incr_prob <- sum(user_incr_prob)

    # Normalize the Attitudinal metric within Exposed Group
    # Suppose Control Group Awareness = 0.45 (45%)
    # Suppose Exposed Group Awareness = 0.57 (57%)
    # After normalizing suppose base_prop = 0.78 (78%) and 
    #                           incr_prop = 0.21 (21%)
    # That means 78% of Exposed Group Awareness is without ad exposure, i.e. 78% * 57% = 45% = Control Group Attitudinal Metric
    base_prop <- campaign_base_prob / campaign_prob # This is % of Attitudinal in Exposed Group without ad exposure (Normalized)
    incr_prop <- campaign_incr_prob / campaign_prob # This is % of Attitudinal in Exposed Group after ad exposure   (Normalized)
    campaign_dim_incr_prob <- colSums(scaled_agent_contr, na.rm = TRUE)
    dim_props <- campaign_dim_incr_prob / campaign_prob

    # format for output
    dim_incr <- data.frame(dim_props)
    dim_incr <- rbind(base_prop, dim_incr)
    row.names(dim_incr)[1] <- "base_prop"

    return(dim_incr)
}

# ----- FUNCTION 9: FUNCTION get.probs -----
# CALLED WITHIN GET.MBD.EFF FUNCTION
# GETS PROBABILITY ESTIMATES BASED ON TRANSFORMED DATASET
get.probs <- function(model_data, final_model_vars) {

    # build input for GLM
    model_spec <- create.formula(model_data)

    # run final model
    final_model <- glm(model_spec, data = model_data, family = "binomial")

    # get coefficients for decomp
    coef <- data.frame(summary(final_model)$coefficients[, 'Estimate'])
    names(coef) <- 'coef'

    # decompose coefficients to probabilities
    intercept <- coef['(Intercept)', 'coef']
    contribution <- within(coef, prob <- inv.logit(intercept + coef)) #inverse logit on sum of coef and intercept
    contribution <- contribution[2:nrow(contribution),] #drop intercept

    # bring power transform value from standalone individual logistic regressions that determine the power to use in joint logistic regression model
    contribution$var_name <- row.names(contribution)
    contribution <- merge(contribution, final_model_vars, by = "var_name", all.x = TRUE)
    contribution$coef.y <- NULL
    names(contribution)[2] <- "coef"
    return(contribution)
}

# ----- FUNCTION 10: FUNCTION compile.contr -----
# CALLED WITHIN GET.MBD.EFF FUNCTION
# MERGES LOGISTIC REGRESSION PROBABILITIES WITH PF_AGG DELIVERY, COST, AND FRACTIONAL ATTRIBUTION
# UPDATED TO INCLUDE INCREMENTAL CONTRIBUTIONS FROM AGENT BASED DECOMP
# CALCULATES EFFICIENCY
# ISH ADDED CODE HERE TO CALCULATE ATT. DIFFERENTLY - WITHOUT BASE, ONLY INCREMENTAL
compile.contr <- function(probs, dim_incr, pf_agg) {

    base_prop <- as.numeric(dim_incr["base_prop",][1])
    dim_incr$var_name <- row.names(dim_incr)
    contribution <- merge(probs, pf_agg, by.x = "var_name", by.y = "DIM", all.x = TRUE, all.y = FALSE)
    contribution <- within(contribution, contr <- prob * IMPS)
    contr_total <- sum(contribution$contr, na.rm = TRUE)
    contribution <- within(contribution, scaled_contr <- contr / contr_total)
    contribution <- within(contribution, base_contr <- base_prop * scaled_contr)
    contribution <- merge(contribution, dim_incr, by = "var_name", all.x = TRUE)

    # assigns zero incr contribution to insignif variables
    contribution$dim_props[!complete.cases(contribution$dim_props)] <- 0

    #SIDE CALCULATION: fix scaled imps/cost
    #pull in all the variable names
    dim_data <- data.frame(contribution$var_name)

    #split the string for only what's LEFT to the first '_'
    dim_temp <- apply(dim_data, 1, function(x) unlist(strsplit(x, '_'))[1])

    #make the split strings a temporary column in contribution table
    contribution$DIM_TEMP <- dim_temp

    #get unique strings - we will want to find cummulative sums
    dim_temp <- unique(dim_temp)
    dim_data <- data.frame(dim_temp)
    dim_data$COST_SUM <- 0
    dim_data$IMPS_SUM <- 0
    names(dim_data) <- c("Dimension", "COST_SUM", "IMPS_SUM")

    #Going row by row sum up the cost and impressions and store it in our temporary data frame "dim_data"
    #Dim data will be used as a lookup table with the values to divide by
    for (i in 1:nrow(contribution)) {
        dim <- unlist(strsplit(contribution$var_name[i], '_'))[1]
        cost_temp <- contribution$COST[i]
        imps_temp <- contribution$IMPS[i]
        dim_data[dim_data$Dimension == dim, 'COST_SUM'] <- dim_data[dim_data$Dimension == dim, 'COST_SUM'] + cost_temp
        dim_data[dim_data$Dimension == dim, 'IMPS_SUM'] <- dim_data[dim_data$Dimension == dim, 'IMPS_SUM'] + imps_temp
    }

    #Merge our sums into contribution table
    contribution <- merge(contribution, dim_data, by.x = 'DIM_TEMP', by.y = 'Dimension', all.x = TRUE)

    #Correct scaled values
    contribution <- within(contribution, {
        SCALED_COST <- COST / COST_SUM
        SCALED_IMPS <- IMPS / IMPS_SUM
    })
    #Delete temporary columns
    contribution$DIM_TEMP <- NULL
    contribution$COST_SUM <- NULL
    contribution$IMPS_SUM <- NULL
    # --------------- RESUME CALCULATIONS ---------------------------

    if (kpitype == 'att_response') {
        contribution <- within(contribution, {
            base_contr <- 0
            final_contr <- dim_props
        })
    } else {
        contribution <- within(contribution, final_contr <- base_contr + dim_props)
    }

    contribution <- within(contribution, eff_mbd <- final_contr / SCALED_COST)
    contribution$eff_mbd <- ifelse(is.na(contribution$eff_mbd), 0, contribution$eff_mbd)
    return(contribution)
}


# ----- FUNCTION 11: FUNCTION compile.contr.nosig -----
# CALLED WITHIN GET.MBD.EFF FUNCTION
# ASSIGNS ZERO IMPACT AND ZERO EFFICIENCY WHEN ALL VARIABLES ARE INSIGNIFICANT AFTER PVALUE < 0.3 FILTER

compile.contr.nosig <- function(final_model_vars, PF_Agg_Campaign) {
    insig_vars <- as.data.frame(final_model_vars$var_name)
    names(insig_vars) <- 'var_name'

    contribution <- merge(insig_vars, PF_Agg_Campaign, by.x = "var_name", by.y = "dim", all.x = TRUE, all.y = TRUE)
    contribution <- subset(contribution, is.na(imps) == F)
    contribution <- within(contribution, {
        COEF <- 0
        PROB <- 0
        POWER <- 1
        P_VAL <- 1
        CONTR <- 0
        SCALED_CONTR <- 0
        BASE_CONTR <- 0
        DIM_PROPS <- 0
        FINAL_CONTR <- 0
        EFF_MBD <- 0
    })

    # uppercase col names in contribution
    names(contribution) <- toupper(names(contribution))

    # put contribution cols in same order as final_eff
    finaleff_vars <- c('BRAND', 'PFCAMPAIGN', 'VAR_NAME', 'DIMTYPE', 'EXE_TYPE',
                           'COEF', 'PROB', 'POWER', 'P_VAL', 'IMPS',
                           'COST', 'PFSCORE', 'SCALED_IMPS', 'SCALED_COST', 'SCALED_PFSCORE',
                           'IMPACT', 'EFFICIENCY', 'CONTR', 'SCALED_CONTR', 'BASE_CONTR', 'DIM_PROPS', 'FINAL_CONTR', 'EFF_MBD')
    contribution <- contribution[, finaleff_vars]

    return(contribution)
}

# ----- FUNCTION 12: FUNCTION get_mbd_eff -----
# CALLED WITHIN RUN_ALL_CAMPAIGNS FUNCTION
# CALCULATES IMPACT AND EFFICIENCY FOR A SELECT CAMPAIGN DIMENSION E.G. ADSIZE
# THIS READS AND CLEANS DATA, FITS MODEL, AND CALCULATES EFF.
get_mbd_eff <- function(brand, campaign, dimtype, campaign.analysis.month, depvar, pf_agg) {

    # get model data
    modeldata_str <- paste('mbd', brand, campaign, dimtype, cutoff, sep = '_')
    table <- rhive.load.table2(modeldata_str)

    # read and clean variable names
    # prepends 's' to var names beginning with a number
    # need to perform same prepend on pf_agg
    var_names <- names(table)
    var_names <- gsub("^([0-9])", "s\\1", var_names)

    # remove commas, spaces and dashes
    # lowercase column names
    var_names <- gsub("[, -]", '', var_names)
    names(table) <- tolower(var_names)

    # define independent vars
    # this doesn't work with multiple kpis
    # can't just pass a simple list of non-dim vars

    # locate the position of att_response
    # no more looping, but still an ugly hack
    tablecolnames <- colnames(table)
    ar_pos <- sum(1:length(tablecolnames) * (tablecolnames == 'att_response'))

    # ind vars is everything to the right of att_response
    ind_vars <- setdiff(tablecolnames, tablecolnames[1:ar_pos])

    # define dependent variable 
    dep_var <- table[, depvar]

    # loop over data and record params, coef, & pval
    # first create a 0x4 matrix to retain column names
    results_log <- data.frame(matrix(vector(), 0, 4, dimnames = list(c(), c("var_name", "coef", "p_val", "power"))), stringsAsFactors = F)


    for (i in 1:length(ind_vars)) {
        indvarsum <- sum(as.numeric(table[, ind_vars[i]]))

        # check if there is at least one respondent that was exposed to the selected dimension, and then 
        if (!is.na(indvarsum) & indvarsum > 0) {
            # test all power transforms
            temp_results <- test.power.transform(table, dep_var, ind_vars[i], .1, 1, 5)
            # store results
            results_log <- rbind(results_log, temp_results)
        }
    }

    # handle datatype errors. Not sure what is converting p_vals and coefs to factors
    results_log$p_val <- as.numeric(as.character(results_log$p_val))
    results_log$coef <- as.numeric(as.character(results_log$coef))
    results_log$var_name <- as.character(results_log$var_name)
    results_log$power <- as.numeric(as.character(results_log$power))

    # find params based on minimum p-vals
    # devin's code does not break min p-val ties -- break with max power
    final_model_vars <- merge(aggregate(p_val ~ var_name, results_log, min),
                                  results_log,
                                  by.x = c("var_name", "p_val"))
    final_model_vars <- merge(aggregate(power ~ var_name, final_model_vars, max),
                                  final_model_vars,
                                  by.x = c("var_name", "power"))

    # subset based on p-val < .3
    # this may render zero rows, i.e. no significant covariate to explain lift
    model_vars_subset <- subset(final_model_vars, p_val < .3)

    # create final dataset based on optimal p_vals
    model_data <- build.model.data(table, dep_var, final_model_vars)
    probs <- get.probs(model_data, final_model_vars)

    # read pf_agg
    #pfagg_csvfile <- paste('pf_agg_final_no_exe_type_', sub('-','_',woc),'_woc.csv', sep='')
    #pfagg_fpath 	<- paste(data_dir,pfagg_csvfile,sep='\\')
    #pf_agg_all 	<- read.csv(pfagg_fpath,stringsAsFactors = F)

    # subset pf_agg table for the campaign to be analyzed (subsetted PF_Agg table has all 7 analysis dimensions)
    # clean pf_agg table from odd characters: space, comma, dot, dash
    # & statements below only work for pfcampaign==campaign logical operator
    pf_agg_all <- PF_Agg

    pf_agg2 <- subset(pf_agg_all, pf_agg_all$PFCAMPAIGN == campaign &
                        pf_agg_all$DIMTYPE == dimtype)

    pf_agg2 <- within(pf_agg2, {
        DIM <- gsub("[ ,.-]", '', DIM)
        DIM <- gsub("^([0-9])", "s\\1", DIM)
    })

    # code from kg to deal with no statistically significant vars
    if (nrow(model_vars_subset) > 0) {
        model_data_sig <- build.model.data(table, dep_var, model_vars_subset)

        # get incremental contribution for statsig vars via agent-based analyet.base.incr(model_data_sig,final_model_vars,model_vars_sis
        dim_incr <- get.base.incr(model_data_sig, final_model_vars, model_vars_subset, brand, campaign, dimtype)
        contribution <- compile.contr(probs, dim_incr, pf_agg2)
        # uppercase all columns
        names(contribution) <- toupper(names(contribution))
    } else {
        contribution <- compile.contr.nosig(final_model_vars, pf_agg2)
    }

    return(contribution)
}

# ----- FUNCTION 13: FUNCTION write_contribs_kpi -----
# CALLED WITHIN RUN_ALL_CAMPAIGNS FUNCTION
# CALCULATES THE CONTRIBUTION FOR SELECTED DIMENSION OF ANALYSIS
write_contribs_kpi <- function(brand, campaign, dimtype) {

    # evaluate dataset
    contribution <- get_mbd_eff(brand, campaign, dimtype, campaign.analysis.month, "att_response", PF_Agg)

    # add new columns for summary and improve output interpretation
    # first reupload the raw data
    modeldata_str <- paste('mbd', brand, campaign, dimtype, cutoff, sep = '_')
    table <- rhive.load.table2(modeldata_str)

    # drop rows with NA values in Att response  
    table <- subset(table, att_response >= 0)
    control_count <- nrow(subset(table, exposed_control == 'control'))
    exposed_count <- nrow(subset(table, exposed_control == 'test'))
    control_att <- sum(subset(table, exposed_control == 'control')$att_response) / control_count
    exposed_att <- sum(subset(table, exposed_control == 'test')$att_response) / exposed_count
    lift <- exposed_att - control_att

    # ensure total incremental contribution adds up to actual lift in exposed
    normalize_factor <- lift / (sum(contribution$DIM_PROPS) * exposed_att)

    # add new metrics and create new metris for final output
    contribution <- within(contribution, {
        BRAND <- brand
        CONTROL_COUNT <- control_count
        EXPOSED_COUNT <- exposed_count
        CONTROL_ATT <- control_att
        EXPOSED_ATT <- exposed_att
        LIFT <- lift
        INCR_CONTR <- exposed_att * DIM_PROPS * normalize_factor
        FINAL_CONTR <- BASE_CONTR + INCR_CONTR
        MBD_IMPACT <- FINAL_CONTR / SCALED_IMPS
        MBD_EFFICIENCY <- FINAL_CONTR / SCALED_COST
    })

    # set columns to zero if there is no incremental
    contribution <- within(contribution, {
        INCR_CONTR <- ifelse(is.na(INCR_CONTR), 0, INCR_CONTR)
        FINAL_CONTR <- ifelse(is.na(FINAL_CONTR), 0, FINAL_CONTR)
        MBD_IMPACT <- ifelse(is.na(MBD_IMPACT), 0, MBD_IMPACT)
        MBD_EFFICIENCY <- ifelse(is.na(MBD_EFFICIENCY), 0, MBD_EFFICIENCY)
    })

    # store results in big table for all campaigns
    if (exists('final_eff')) {
        final_eff <<- rbind(final_eff, contribution)
    } else {
        final_eff <<- contribution
    }

    return(final_eff)
    # write individual campaign results to csv
    # write big table to subdirectory
    #big_tablename = paste("MBD_CAMPAIGNS",'month',as.character(campaign.analysis.month),brand, campaign, dimtype, sep="_")
    #outfile.final = paste(mbd_out, big_tablename, sep = '/')
    #outfile.final = paste(outfile.final, '.csv', sep = '')
    #write.csv(final_eff, file = outfile.final)

    # write big table to main directory for download later
    #outfile.final2 = paste('/home/sabryee_bde/WOC', big_tablename, sep = '')
    #outfile.final2 = paste(outfile.final2, '.csv', sep = '')
    #write.csv(final_eff, file = outfile.final2)       
}



# ----- FUNCTION 14: FUNCTION print_modeling_dates -----
# CALLED IN STEP 4
# 
print_modeling_dates <- function(brand, camp, msi, dimtype) {

    # write valid modeling data to appropriately named dataframe 
    modeldata_str <- paste('mbd', brand, camp, dimtype, cutoff, sep = '_')
    model_df <- rhive.load.table2(modeldata_str)

    # dates
    min_dt <- min(model_df$survey_dt)
    max_dt <- max(model_df$survey_dt)
    model_dt_str <- paste('From', min_dt, 'to', max_dt)

    # exposed vs control
    expoctrl_df <- as.data.frame(table(model_df$exposed_control))
    expoctrl_df$expoctrl <- ifelse(expoctrl_df$Var1 == 1, 'Exposed', 'Control')
    expoctrl_df <- expoctrl_df[, -1]
    names(expoctrl_df) <- c('Number', 'Exposure')
    n_expo <- expoctrl_df[1, 'Number']
    n_ctrl <- expoctrl_df[2, 'Number']
    expoctrl_str <- paste('exposed = ', n_expo, ', ', 'control = ', n_ctrl, sep = '')

    # header info
    brlabel <- mbdlist_df[msi, 'brand']
    camplabel <- mbdlist_df[msi, 'camplabel']
    header_str <- paste('\n-----', brlabel, camplabel, '-----', sep = ' ')

    message(header_str)
    message(model_dt_str)
    message(expoctrl_str)

    tabular_str <- paste(brlabel, camplabel, min_dt, max_dt, n_expo, n_ctrl, sep = '\t')
    tabular_outstr <<- paste(tabular_outstr, tabular_str, sep = '\n')

}


# ---------------- PROCESSING ---------------


for (i in 1:nrow(mbdlist_df)) {
    msi <- as.character(mbdlist_df[i, 'surveyid'])
    br <- tolower(mbdlist_df[i, 'brand'])
    camp <- tolower(mbdlist_df[i, 'campaign'])
    survey.brand.name <- tolower(mbdlist_df[i, 'survey_brand_name'])
    for (dimtype in dimtypes) {
        make_modeldata(br, camp, msi, survey.brand.name, dimtype)
        print(paste("complete with:", camp, msi, dimtype, sep = ' '))
    }
}

#Vet out campaigns with not enough exposed or control cookies

#example: lets say springsteen only had 1 sitetactic, you would want to
#take it out of the mbdlist_df because we are not running the model on it
#mbdlist_df <- mbdlist_df[mbdlist_df$campaign!='springsteen',]


final_all <- data.frame()

for (i in 1:nrow(mbdlist_df)) {
    msi <- as.character(mbdlist_df[i, 'surveyid'])
    br <- tolower(mbdlist_df[i, 'brand'])
    camp <- tolower(mbdlist_df[i, 'campaign'])
    survey.brand.name <- tolower(mbdlist_df[i, 'survey_brand_name'])
    final_eff <- data.frame()
    final_eff_out <- data.frame()
    for (dimtype in dimtypes) {
        final_eff <- write_contribs_kpi(br, camp, dimtype)
    }

    final_eff <- within(final_eff, {
        VAR_NAME <- gsub("^s([0-9])", '\\1', VAR_NAME)
    })

    # Organize Column order & names for easy interpretation
    final_eff$DIM <- final_eff$VAR_NAME
    column_order <- c('BRAND', 'PFCAMPAIGN', 'DIMTYPE', 'DIM', 'EXE_TYPE',
                          'IMPS', 'COST', 'POWER', 'PROB', 'P_VAL',
                        'SCALED_IMPS', 'SCALED_COST', 'SCALED_CONTR',
                        'CONTROL_COUNT', 'EXPOSED_COUNT', 'CONTROL_ATT', 'EXPOSED_ATT', 'LIFT',
                        'BASE_CONTR', 'INCR_CONTR', 'DIM_PROPS', 'FINAL_CONTR', 'MBD_IMPACT', 'MBD_EFFICIENCY')
    final_eff <- final_eff[, column_order]
    names(final_eff)[18:(length(names(final_eff)) - 2)] <- paste('MBD_', names(final_eff)[18:(length(names(final_eff)) - 2)], sep = '')
    final_eff_out <- rbind(final_eff_out, final_eff)

    # write big table to subdirectory
    big_tablename = paste("MBD_CAMPAIGNS", 'month', woc, br, camp, sep = "_")
    outfile.final = paste(mbd_out, big_tablename, sep = '/')
    outfile.final = paste(outfile.final, '.csv', sep = '')
    write.csv(final_eff_out, file = outfile.final)

    # write big table to main directory for download later
    outfile.final2 = paste('/home/sabryee_bde/WOC', big_tablename, sep = '')
    outfile.final2 = paste(outfile.final2, '.csv', sep = '')
    write.csv(final_eff_out, file = outfile.final2)

    big_tablename = paste("MBD_CAMPAIGNS", 'month', woc, br, camp, sep = "_")
    outfile.final2 = paste('/home/sabryee_bde/WOC', big_tablename, sep = '')
    outfile.final2 = paste(outfile.final2, '.csv', sep = '')
    copy.output.data = outfile.final2

    # identify destination file
    file.for.download = paste(mbd_root, big_tablename, sep = '/')
    file.for.download = paste(file.for.download, 'csv', sep = '.')

    # copy the file
    rhive.hdfs.put(copy.output.data, file.for.download)
}



# write big table to subdirectory
big_tablename <- paste('mbd_allcampaigns', freqfilesuffix, sep = '_')
outfile.final = paste(mbd_out, big_tablename, sep = '/')
outfile.final = paste(outfile.final, '.csv', sep = '')
write.csv(final_all, file = outfile.final)

# write big table to main directory for download later
outfile.final2 = paste('/home/rchin_bde/WOC', big_tablename, sep = '')
outfile.final2 = paste(outfile.final2, '.csv', sep = '')
write.csv(final_all, file = outfile.final2)


# identify destination file
file.for.download = paste(mbd_root, big_tablename, sep = '/')
file.for.download = paste(file.for.download, 'csv', sep = '.')

# copy the file
rhive.hdfs.put(copy.output.data, file.for.download)
